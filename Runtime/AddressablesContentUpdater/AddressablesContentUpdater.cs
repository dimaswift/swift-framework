﻿using SwiftFramework.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SwiftFramework.AddressablesContentUpdater
{
    [Configurable(typeof(AddressablesContentUpdaterConfig))]
    [DependsOnModules(typeof(IWindowsManager))]
    public class AddressablesContentUpdater : Module, IContentUpdater
    {
        private long lastCheckTimestamp;
        private long lastUpdateSize;
        private IList<object> lastAssetsToUpdate;
        private AddressablesContentUpdaterConfig config;
        private bool updateSkipped;

        public bool AllowSkip => config.allowSkipUpdate;

        public AddressablesContentUpdater(ModuleConfigLink configLink)
        { 
            SetConfig(configLink);
        }

        protected override IPromise GetInitPromise()
        {
            config = GetModuleConfig<AddressablesContentUpdaterConfig>();

            if (config.automaticallyCheckForUpdates)
            {
                App.Coroutine.Begin(UpdateCheckRoutine());

                if (config.checkOnApplicationFocus)
                {
                    App.Boot.OnFocused += Boot_OnFocused;
                }
            }

            return base.GetInitPromise();
        }

        private void Boot_OnFocused(bool focused)
        {
            if (focused)
            {
                AutoCheck();
            }
        }

        private IPromise Check()
        {
            return AddrCache.CheckForUpdates().Then(result =>
            {
                if (result.size > 0)
                {
                    lastUpdateSize = result.size;
                    lastAssetsToUpdate = result.assets;
                    App.GetModule<IWindowsManager>().Show<ContentUpdateWindow, IContentUpdater>(this);
                }
            });
        }

        private void AutoCheck()
        {
            if (updateSkipped || App.Clock.Now.Value - lastCheckTimestamp < config.minTimeBetweenChecks)
            {
                return;
            }

            lastCheckTimestamp = App.Clock.Now.Value;

            Check();
        }

        private IEnumerator UpdateCheckRoutine()
        {
            while (true)
            {
                if (updateSkipped)
                {
                    yield break;
                }
                AutoCheck();
                yield return new WaitForSecondsRealtime(config.regularCheckFrequencySeconds);
            }
        }

        public IPromise CheckForUpdatesManually()
        {
            return Check();
        }

        public long GetUpdateSize()
        {
            return lastUpdateSize;
        }

        public IPromise Download(FileDownloadHandler callback)
        {
            if (lastUpdateSize == 0)
            {
                return Promise.Rejected(new InvalidOperationException("Check for updates first"));
            }
            return AddrCache.DownloadUpdates(lastAssetsToUpdate, lastUpdateSize, callback);
        }

        public void RestartApp()
        {
            App.Boot.Restart();
            
        }

        public void SkipUpdate()
        {
            updateSkipped = true;
        }
    }
}
