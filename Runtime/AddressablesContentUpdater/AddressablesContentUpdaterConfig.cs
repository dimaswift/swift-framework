﻿using SwiftFramework.Core;
namespace SwiftFramework.AddressablesContentUpdater
{
    public class AddressablesContentUpdaterConfig : ModuleConfig
    {
        public long regularCheckFrequencySeconds = 120;
        public bool checkOnApplicationFocus = true;
        public long minTimeBetweenChecks = 60;
        public bool allowSkipUpdate = true;
        public bool automaticallyCheckForUpdates = true;
    }
}
