﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using UnityEngine;

namespace SwiftFramework.AddressablesContentUpdater
{
    [AddrSingleton]
    public class ContentUpdateWindow : WindowWithArgs<IContentUpdater>
    {
        [SerializeField] private GenericText updateSizeText = null;
        [SerializeField] private GenericButton downloadButton = null;
        [SerializeField] private GenericButton skipButton = null;
        [SerializeField] private ProgressBar downloadBar = null;
        [SerializeField] private GenericText downloadProgessText = null;
        [SerializeField] private GameObject downloadingState = null;
        [SerializeField] private GameObject updateAvailableState = null;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            downloadButton.Value.AddListener(OnDownloadClick);
            skipButton.Value.AddListener(OnSkipClick);
        }

        private void OnDownloadClick()
        {
            updateAvailableState.SetActive(false);
            downloadingState.SetActive(true);
            downloadProgessText.Value.Text = $"0/{Arguments.GetUpdateSize().ToFileSize()}";
            downloadBar.Value.SetUp(0);
            Arguments.Download((current, total) =>
            {
                downloadBar.Value.SetUp((float)current / total);
                downloadProgessText.Value.Text = $"{current.ToFileSize()}/{total.ToFileSize()}";
            })
            .Done(() =>
            {
                Arguments.RestartApp();
            });
        }

        private void OnSkipClick()
        {
            Arguments.SkipUpdate();
            Hide();
        }

        public override void OnStartShowing(IContentUpdater updater)
        {
            base.OnStartShowing(updater);
            updateAvailableState.SetActive(true);
            skipButton.SetActive(updater.AllowSkip);
            downloadingState.SetActive(false);
            updateSizeText.Value.Text = updater.GetUpdateSize().ToFileSize();
        }
    }
}
