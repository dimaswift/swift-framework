﻿using com.adjust.sdk;
using SwiftFramework.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.AdjustManager
{
    public class AdjustManager : BehaviourModule, IAnalyticsManager
    {
        private long delay;
        [SerializeField] private List<EventMap> eventMaps;
        public void LogEvent(string eventName, params (string key, object value)[] args)
        {
            string eventString = eventName + "_" + (int)args[0].value;
            AdjustEvent adjustEvent;
            if (TryGetEvent(eventString, out adjustEvent))
            {
                Debug.Log("ADJUST: TRACKED FACTORY UNLOCK EVENT: " + eventString);
                Adjust.trackEvent(adjustEvent);
            }
        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {
            AdjustEvent adjustEvent;
            if (TryGetEvent("made_IAP", out adjustEvent))
            {
                adjustEvent.setRevenue((double)price, currencyCode);
                adjustEvent.setTransactionId(transactionId);
                Adjust.trackEvent(adjustEvent);
            }
        }

        public void LogRewardedVideoStarted(string placementId)
        {
            AdjustEvent adjustEvent;
            if (TryGetEvent("rewarded_started", out adjustEvent))
            {
                if (delay > 0)
                {
                    delay = App.ServerTime.Now.Value - delay;
                }

                adjustEvent.addCallbackParameter("delay", delay.ToString());
                Adjust.trackEvent(adjustEvent);
            }

        }

        public void LogRewardedVideoSuccess(string placementId)
        {
            AdjustEvent adjustEvent;
            if (TryGetEvent("rewarded_finished", out adjustEvent))
            {
                Debug.Log("ADJUST: TRACKED THE REWARDED AD EVENT");
                Adjust.trackEvent(adjustEvent);
            }

        }

        public void LogRewardedVideoAttemptToShow(string placementId)
        {
            delay = App.ServerTime.Now.Value;
        }

        public void LogLevelUp(int level)
        {

        }

        public void LogRewardedVideoError(string placementId, string errorType, string error)
        {
            AdjustEvent adjustEvent;
            if (TryGetEvent("rewarded_error", out adjustEvent))
            {
                adjustEvent.addCallbackParameter("errorType", errorType);
                adjustEvent.addCallbackParameter("error", error);
                Adjust.trackEvent(adjustEvent);
            }

        }

        public void LogFirstPurchase(string productId)
        {
            AdjustEvent adjustEvent;
            if (TryGetEvent("first_IAP", out adjustEvent))
            {
                adjustEvent.addCallbackParameter("product", productId);
                Adjust.trackEvent(adjustEvent);
            }

        }

        public void LogFirstAdWatched(string placementId)
        {

        }

        private bool TryGetEvent(string eventName, out AdjustEvent adjustEvent)
        {
            foreach (EventMap map in eventMaps)
            {
                if (map.eventName == eventName)
                {
                    adjustEvent = new AdjustEvent(map.eventToken);
                    return true;
                }
            }
            adjustEvent = null;

            return false;
        }


        [Serializable]
        private struct EventMap
        {
            public string eventName;
            public string eventToken;
        }

    }
}
