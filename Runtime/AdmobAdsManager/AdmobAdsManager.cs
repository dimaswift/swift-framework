﻿﻿#if ENABLE_ADMOB
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using System;
using GoogleMobileAds.Api;
using SwiftFramework.Utils.UI;

namespace SwiftFramework.AdmobAdsManager
{
    [Configurable(typeof(AdmobConfig))]
    [DependsOnModules(typeof(ITimer))]
    public class AdmobAdsManager : Module, IAdsManager
    {
        public IStatefulEvent<bool> BannerShown => bannerShown;

        public AdmobAdsManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        public AdmobConfig Config => GetModuleConfig<AdmobConfig>();

        public bool IsBannerEnabled => Config.showBanner;

        private readonly AdmobConfig.AdsConfig androidTestConfig = new AdmobConfig.AdsConfig()
        {
            rewardedAdUnitId = "ca-app-pub-3940256099942544/5224354917",
            bannerUnitId = "ca-app-pub-3940256099942544/6300978111",
            interstitialUnitId = "ca-app-pub-3940256099942544/1033173712"
        };

        private readonly AdmobConfig.AdsConfig iOSTestConfig = new AdmobConfig.AdsConfig()
        {
            rewardedAdUnitId = "ca-app-pub-3940256099942544/1712485313",
            bannerUnitId = "ca-app-pub-3940256099942544/2934735716",
            interstitialUnitId = "ca-app-pub-3940256099942544/4411468910"
        };

        private float adCloseTime;
        public event Action<string> OnRewardedShowStarted = p => { };
        public event Action<string> OnRewardedSuccessfully = p => { };
        public event Action OnRewardedAdLoaded = () => { };
        public event Action<string> OnRewardedClosed = p => { };
        public event Action OnBannerStateChanged = () => { };
        public event Action<string> OnRewardedAttemptToShow = p => { };
        public event Action<RewardedAdErrorArgs> OnRewardedError = args => { };

        private readonly List<RewardedAd> readyRewardedAds = new List<RewardedAd>();

        private RewardedAd loadingRewardedAd;
        private InterstitialAd loadingInterstitialAd;

        private readonly List<InterstitialAd> loadedInterstitialAds = new List<InterstitialAd>();

        private Promise<RewardedShowResult> rewardedShowPromise;
        private string currentPlacementId;
        private bool rewardEarned;
        private int interstitialShowAttemptCounter;
        private IPromise focusResolvePromise;
        private readonly StatefulEvent<bool> bannerShown = new StatefulEvent<bool>();
        private BannerView banner;

        public bool IsShowingRewardedAd()
        {
            return rewardedShowPromise != null;
        }

        protected override IPromise GetInitPromise()
        {
            var loadPromise = Promise.Create();

            if (Application.isEditor)
            {
                loadPromise.Resolve();
                return loadPromise;
            }

            MobileAds.Initialize(initStatus =>
            {
                Log("Admob adapters fully initialized");
            });

            TryAddRewardedToCache();

            TryAddInterstitialToCache();

            loadPromise.Resolve();

            return loadPromise;
        }

        private void TryInitBanner()
        {
            if (Config.showBanner)
            {
                if(banner != null)
                {
                    banner.Destroy();
                }
                banner = new BannerView(GetConfig().bannerUnitId, AdSize.SmartBanner, GetBannerPosition());
                AdRequest request = new AdRequest.Builder().Build();
                banner.LoadAd(request);
                banner.OnAdFailedToLoad += Banner_OnAdFailedToLoad;
                banner.OnAdOpening += Banner_OnAdOpening;
                OnBannerStateChanged();
            }

        }

        private void Banner_OnAdOpening(object sender, EventArgs e)
        {
            OnBannerStateChanged();
            if (bannerShown.Value)
            {
                SetBannerShown(true);
            }
        }

        private void Banner_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            App.Timer.WaitForUnscaled(3).Done(() => TryInitBanner());
        }

        private void TryResolveRewardedPromise(RewardedShowResult result)
        {
            App.Timer.WaitForNextFrame().Done(() =>
            {
                if (rewardedShowPromise != null)
                {
                    adCloseTime = Time.realtimeSinceStartup;
                    rewardedShowPromise.Resolve(rewardEarned ? RewardedShowResult.Success : result);
                    if (rewardEarned)
                    {
                        OnRewardedSuccessfully(currentPlacementId);
                    }
                    OnRewardedClosed(currentPlacementId);
                    rewardedShowPromise = null;
                    TryAddRewardedToCache();
                    App.Timer.Cancel(focusResolvePromise);
                }
            });
        }

        private void TryAddRewardedToCache()
        {
            if (readyRewardedAds.Count >= Config.rewardedCacheSize)
            {
                return;
            }

            loadingRewardedAd = new RewardedAd(GetConfig().rewardedAdUnitId);

            loadingRewardedAd.OnAdLoaded += HandleRewardedAdLoaded;

            loadingRewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;

            loadingRewardedAd.OnAdOpening += HandleRewardedAdOpening;

            loadingRewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;

            loadingRewardedAd.OnUserEarnedReward += HandleUserEarnedReward;

            loadingRewardedAd.OnAdClosed += HandleRewardedAdClosed;

            loadingRewardedAd.OnAdFailedToLoad += LoadingRewardedAd_OnAdFailedToLoad;

            loadingRewardedAd.OnAdFailedToShow += LoadingRewardedAd_OnAdFailedToShow;

            AdRequest request = new AdRequest.Builder().Build();

            loadingRewardedAd.LoadAd(request);

            Log($"Adding rewarded ad to the cache... Current ready ads amount: {readyRewardedAds.Count}");
        }

        private void LoadingRewardedAd_OnAdFailedToShow(object sender, AdErrorEventArgs e)
        {
            RewardedAdErrorArgs args = new RewardedAdErrorArgs
            {
                placementId = currentPlacementId,
                errorType = "failed_showing",
                errorMessage = e?.Message
            };
            OnRewardedError(args);
        }

        private void LoadingRewardedAd_OnAdFailedToLoad(object sender, AdErrorEventArgs e)
        {
            RewardedAdErrorArgs args = new RewardedAdErrorArgs
            {
                placementId = currentPlacementId,
                errorType = "failed_to_load",
                errorMessage = e?.Message
            };
        }

        private void LoadingRewardedAd_OnUserEarnedReward(object sender, Reward e)
        {
            rewardEarned = true;
        }

        private void TryAddInterstitialToCache()
        {
            if (loadedInterstitialAds.Count >= Config.interstitialCacheSize)
            {
                return;
            }

            loadingInterstitialAd = new InterstitialAd(GetConfig().interstitialUnitId);

            loadingInterstitialAd.OnAdLoaded += HandleInterstitialAdLoaded;

            loadingInterstitialAd.OnAdClosed += HandleInterstitialAdClosed;

            loadingInterstitialAd.OnAdFailedToLoad += HandleInterstitialAdFailedToLoad;

            AdRequest request = new AdRequest.Builder().Build();

            loadingInterstitialAd.LoadAd(request);

            Log($"Admob Ads Manager: Adding interstitial ad to the cache... Current cache size: {loadedInterstitialAds.Count}");
        }

        private void HandleInterstitialAdClosed(object sender, EventArgs e)
        {
            App.Timer.WaitForNextFrame().Done(() =>
            {
                adCloseTime = Time.realtimeSinceStartup;
                TryAddInterstitialToCache();
            });
        }

        private void HandleRewardedAdClosed(object sender, EventArgs e)
        {
            Log($"Reward ad closed. Reward earned: {rewardEarned}");
            App.Timer.WaitForNextFrame().Done(() =>
            {
                if(rewardedShowPromise == null)
                {
                    Log($"Reward ad closed, but promise is null!");
                }
                TryResolveRewardedPromise(rewardEarned ? RewardedShowResult.Success : RewardedShowResult.Canceled);
            });
        }

        private void HandleUserEarnedReward(object sender, Reward e)
        {
            Log($"Reward earned...");
            rewardEarned = true;
        }

        private void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs e)
        {
            Log($"Rewarded ad failed to show! {e?.Message}");
            TryResolveRewardedPromise(RewardedShowResult.Error);
           
        }

        private void HandleRewardedAdOpening(object sender, EventArgs e)
        {
            Log($"Rewarded ad is opening...");
        }

        private void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs e)
        {
            App.Timer.WaitForUnscaled(Config.loadDelayAfterLoadingFailure).Done(TryAddRewardedToCache);
            Log($"Admob Ads Manager: Rewarded ad failed to load: {e?.Message}");
            loadingRewardedAd = null;
        }

        private void HandleRewardedAdLoaded(object sender, EventArgs e)
        {
            if (loadingRewardedAd != null)
            {
                readyRewardedAds.Add(loadingRewardedAd);
                loadingRewardedAd = null;
            }

            TryAddRewardedToCache();

            App.Timer.WaitForMainThread().Done(() =>
            {
                OnRewardedAdLoaded();
                
            });
        }

        private AdmobConfig.AdsConfig GetConfig()
        {
#if UNITY_ANDROID
            return Config.useTestAds ? androidTestConfig : Config.androidConfig;
#elif UNITY_IPHONE
            return  Config.useTestAds ? iOSTestConfig : Config.iOSConfig;
#else
            return new AdmobConfig.AdsConfig()
            {
                rewardedAdUnitId = "unexpected_platform",
                bannerUnitId = "unexpected_platform",
                interstitialUnitId = "unexpected_platform"
            };
#endif
        }

        private void HandleInterstitialAdFailedToLoad(object sender, AdFailedToLoadEventArgs e)
        {
            Log($"Admob Ads Manager: Failed to load interstitial ad: {e?.Message}");
            App.Timer.WaitForUnscaled(Config.loadDelayAfterLoadingFailure).Done(TryAddInterstitialToCache);
        }

        private void HandleInterstitialAdLoaded(object sender, EventArgs e)
        {
            if(loadingInterstitialAd != null)
            {
                loadedInterstitialAds.Add(loadingInterstitialAd);
                loadingInterstitialAd = null;
            }
          
            Log($"Admob Ads Manager: Loaded intersitial. Cache size: {loadedInterstitialAds.Count}");
        }

        //public IPromise<bool> ShowRewardedWithLoadingWindow(string placementId)
        //{
        //    IWindowsManager windowsManager = App.GetModule<IWindowsManager>();

        //    if (windowsManager == null)
        //    {
        //        Promise<bool> promise = Promise<bool>.Create();
        //        Debug.Log($"Cannot show rewarded with loading window. IWindowsManager or AdLoadingWindow not found");
        //        ShowRewarded(placementId).Done(res =>
        //        {
        //            promise.Resolve(res == RewardedShowResult.Success);
        //        });
        //        return promise;
        //    }

        //    return windowsManager.Show<AdLoadingWindow, AdLoadingWindow.Args, bool>(new AdLoadingWindow.Args()
        //    {
        //        placementId = placementId,
        //        onTimeout = () => { },
        //        timeout = Config.rewardedLoadingTimeout,
        //        adsManager = this
        //    });
        //}

        public IPromise<RewardedShowResult> ShowRewarded(string placementId)
        {
            App.Timer.Cancel(focusResolvePromise);

            if (Application.isEditor)
            {
                OnRewardedSuccessfully(placementId);
                return Promise<RewardedShowResult>.Resolved(RewardedShowResult.Success);
            }

            if (IsRewardedReady() == false)
            {
                TryAddRewardedToCache();
                return Promise<RewardedShowResult>.Resolved(RewardedShowResult.NotReady);
            }

            currentPlacementId = placementId;

            rewardedShowPromise = Promise<RewardedShowResult>.Create();

            rewardedShowPromise.Done(rewarded => Log($"Rewarded ad resolved. Reward earned: {rewarded}"));

            rewardEarned = false;

            OnRewardedShowStarted(currentPlacementId);

            RewardedAd ad = readyRewardedAds[0];

            readyRewardedAds.RemoveAt(0);

            try
            {
                if (ad.IsLoaded() == false)
                {
                    TryResolveRewardedPromise(RewardedShowResult.NotReady);
                    return rewardedShowPromise;
                }
                App.Boot.IgnoreNextPauseEvent();
                ad.Show();
                Log($"Admob Ads Manager: Showing rewarded from cache. Remaining ready ads amount: {readyRewardedAds.Count}");
            }
            catch (Exception e)
            {
                Log($"Error while trying to show rewarded ad. {e?.Message}");
                TryResolveRewardedPromise(RewardedShowResult.Error);
            }

            TryAddRewardedToCache();

            return rewardedShowPromise;
        }

        public bool IsRewardedReady()
        {
            if (Application.isEditor)
            {
                return true;
            }

            return readyRewardedAds.Count > 0;
        }

        public float GetTimeSinceAdClosed()
        {
            return Time.realtimeSinceStartup - adCloseTime;
        }

        public bool ShowInterstitial(string placementId)
        {
            if (IsInterstitialReady())
            {
                try
                {
                    App.Boot.IgnoreNextPauseEvent();
                    var ad = loadedInterstitialAds[0];
                    loadedInterstitialAds.RemoveAt(0);
                    if (ad.IsLoaded())
                    {
                        ad.Show();
                        TryAddInterstitialToCache();
                        return true;
                    }
                    else
                    {
                        Debug.Log($"interstitial not yet loaded!"); 
                    }
                    TryAddInterstitialToCache();
                    return false;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    TryAddInterstitialToCache();
                }
                
                return true;
            }
            return false;
        }

        public bool IsInterstitialReady()
        {
            return loadedInterstitialAds.Count > 0;
        }

        public bool TryShowInterstitial()
        {
            interstitialShowAttemptCounter++;
            if (interstitialShowAttemptCounter >= Config.interstitialShowFrequency)
            {
                if (ShowInterstitial(""))
                {
                    interstitialShowAttemptCounter = 0;
                    return true;
                }
            }
            return false;
        }

        public void ResetInterstitialCounter()
        {
            interstitialShowAttemptCounter = 0;
        }

        public void CancelRewardedShow()
        {
            TryResolveRewardedPromise(RewardedShowResult.Canceled);
        }

        public void SetBannerShown(bool shown)
        {
            bannerShown.SetValue(shown);

            if (shown && banner == null)
            {
                TryInitBanner();
                return;
            }
            
            if(banner == null)
            {
                return;
            }

            if (shown)
            {
                banner.Show();
            }
            else
            {
                banner.Hide();
            }

        }

        private AdPosition GetBannerPosition()
        {
            switch (GetConfig().bannerPosition)
            {
                case AdBannerPosition.Top:
                    return AdPosition.Top;
                case AdBannerPosition.Bottom:
                    return AdPosition.Bottom;
                default:
                    return AdPosition.Bottom;
            }
        }
    }
}
#endif