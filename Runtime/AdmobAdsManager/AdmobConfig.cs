﻿using SwiftFramework.Core;
using System;
using UnityEngine;

namespace SwiftFramework.AdmobAdsManager
{
    public class AdmobConfig : ModuleConfig
    {
        public int rewardedCacheSize = 3;
        public int interstitialCacheSize = 3;
        public int interstitialShowFrequency = 2;
        public float loadDelayAfterLoadingFailure = 5;
        public bool useTestAds = true;
        public bool showBanner;
        public float rewardedLoadingTimeout = 5;

        [Serializable]
        public class AdsConfig
        {
            public string appId;
            public string rewardedAdUnitId;
            public string interstitialUnitId;
            public string bannerUnitId;
            public AdBannerPosition bannerPosition;
        }

        public AdsConfig androidConfig = new AdsConfig();
        public AdsConfig iOSConfig = new AdsConfig();
    }
}