﻿using SwiftFramework.Core;
using System;
using UnityEditor;
using UnityEngine;

namespace SwiftFramework.AdmobAdsManager
{
    [InitializeOnLoad]
    public class AdmobEditorConfig
    {
        static AdmobEditorConfig()
        {
            EditorUtils.Util.OnModuleConfigApplied += Util_OnModuleConfigApplied;
        }

        private static void Util_OnModuleConfigApplied(Type moduleType, ModuleConfig moduleConfig)
        {
#if ENABLE_ADMOB

            if (moduleType == typeof(AdmobAdsManager))
            {
                AdmobConfig config = moduleConfig as AdmobConfig;
                if (config == null)
                {
                    return;
                }
                var settings = EditorUtils.Util.FindScriptableObject<GoogleMobileAds.Editor.GoogleMobileAdsSettings>();

                if(settings == null)
                {
                    return;
                }

                settings.AdMobAndroidAppId = config.androidConfig.appId;
                settings.AdMobIOSAppId = config.iOSConfig.appId;
                EditorUtility.SetDirty(settings);
            }
#endif

        }
    }
}