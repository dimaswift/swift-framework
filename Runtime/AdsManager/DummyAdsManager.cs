﻿using SwiftFramework.Core;
using SwiftFramework.Utils.UI;
using System;
using System.Collections.Generic;

namespace SwiftFramework.AdsManager
{
    [DependsOnModules(typeof(ITimer))]
    public class DummyAdsManager : Module, IAdsManager
    {
        public event Action<string> OnRewardedShowStarted = placement => { };
        public event Action<string> OnRewardedSuccessfully = placement => { };
        public event Action OnRewardedAdLoaded = () => { };
        public event Action<string> OnRewardedClosed = placement => { };
        public event Action OnBannerStateChanged = () => { };
        public event Action<string> OnRewardedAttemptToShow = placement => { };
        public event Action<RewardedAdErrorArgs> OnRewardedError;

        private string currentPlacement;

        private bool ready = true;

        public bool IsBannerEnabled
        {
            get
            {
                return false;
            }
        }

        public IStatefulEvent<bool> BannerShown => bannerShown;

        private readonly StatefulEvent<bool> bannerShown = new StatefulEvent<bool>();

        public IPromise<RewardedShowResult> ShowRewarded(string placementId)
        {
            currentPlacement = placementId;

            OnRewardedAttemptToShow(currentPlacement);

            OnRewardedShowStarted(placementId);

            Promise<RewardedShowResult> result = Promise<RewardedShowResult>.Create();

            App.Timer.WaitFor(1).Then(() => 
            {
                result.Resolve(RewardedShowResult.Success);
                OnRewardedSuccessfully(currentPlacement);
                OnRewardedClosed(currentPlacement);
            })
            .Catch(e => OnRewardedError(new RewardedAdErrorArgs()));

            return result; 
        }

        protected override void OnInit()
        {
            base.OnInit();
            OnRewardedAdLoaded();
        }

        public bool IsRewardedReady()
        {
            return ready;
        }

        public bool IsShowingRewardedAd()
        {
            return false;
        }

        public float GetTimeSinceAdClosed()
        {
            return float.MaxValue;
        }

        public bool ShowInterstitial(string placementId)
        {
            return false;
        }

        public bool IsInterstitialReady()
        {
            return false;
        }

        public bool TryShowInterstitial()
        {
            return false;
        }

        public void ResetInterstitialCounter()
        {
     
        }

        public void CancelRewardedShow()
        {
            
        }

        public void SetBannerShown(bool shown)
        {
            
        }

        public float GetBannerHeight()
        {
            return 0;
        }

        public IPromise<bool> ShowRewardedWithLoadingWindow(string placementId)
        {
            IWindowsManager windowsManager = App.GetModule<IWindowsManager>();

            if (windowsManager == null)
            {
                Promise<bool> promise = Promise<bool>.Create();
                UnityEngine.Debug.Log($"Cannot show rewarded with loading window. IWindowsManager not found");
                ShowRewarded(placementId).Done(res =>
                {
                    promise.Resolve(res == RewardedShowResult.Success);
                });
                return promise;
            }

            return windowsManager.Show<AdLoadingWindow, AdLoadingWindow.Args, bool>(new AdLoadingWindow.Args()
            {
                placementId = placementId,
                onTimeout = () => { },
                timeout = 3,
                adsManager = this
            });
        }
    }

}
