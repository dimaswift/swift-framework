﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.AdsMediator
{
    [Configurable(typeof(AdsMediatorConfig))]
    public class AdsMediator : Module, IAdsManager
    {
        public event Action<string> OnRewardedShowStarted = i => { };
        public event Action OnRewardedAdLoaded = () => { };
        public event Action<string> OnRewardedSuccessfully = i => { };
        public event Action<string> OnRewardedClosed = i => { };
        public event Action<string> OnRewardedAttemptToShow = i => { };
        public event Action<RewardedAdErrorArgs> OnRewardedError = i => { };

        public IStatefulEvent<bool> BannerShown => bannerShown;

        public bool IsBannerEnabled
        {
            get
            {
                foreach (var m in managers)
                {
                    if (m.IsBannerEnabled)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        protected AdsMediatorConfig config;

        private readonly StatefulEvent<bool> bannerShown = new StatefulEvent<bool>();

        private readonly List<IAdsManager> managers = new List<IAdsManager>();

        public AdsMediator(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        protected override IPromise GetInitPromise()
        {
            Promise promise = Promise.Create();
            
            config = GetModuleConfig<AdsMediatorConfig>();

            CreateNextManager(0, promise);

            return promise;
        }

        private void CreateNextManager(int index, Promise promise)
        {
            if (index >= config.adManagers.Count)
            {
                promise.Resolve();
                return;
            }
            ModuleLink moduleToInit = config.adManagers[index];
            App.CreateModule(moduleToInit).Done(module =>
            {
                IAdsManager adsManager = module as IAdsManager;
                adsManager.BannerShown.OnValueChanged += v => bannerShown.SetValue(v);
                adsManager.OnRewardedAdLoaded += () => OnRewardedAdLoaded();
                adsManager.OnRewardedAttemptToShow += i => OnRewardedAttemptToShow(i);
                adsManager.OnRewardedClosed += i => OnRewardedClosed(i);
                adsManager.OnRewardedError += a => OnRewardedError(a);
                adsManager.OnRewardedShowStarted += i => OnRewardedShowStarted(i);
                adsManager.OnRewardedSuccessfully += i => OnRewardedSuccessfully(i);
                managers.Add(adsManager);
                CreateNextManager(++index, promise);
            });
        }

        public float GetTimeSinceAdClosed()
        {
            float min = float.MaxValue;
            float result = Time.realtimeSinceStartup;
            foreach (var m in managers)
            {
                float t = m.GetTimeSinceAdClosed();
                if (t < min)
                {
                    result = t;
                    min = t;
                }
            }
            return result;
        }

        public bool IsShowingRewardedAd()
        {
            foreach (var m in managers)
            {
                if (m.IsShowingRewardedAd())
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsRewardedReady()
        {
            foreach (var m in managers)
            {
                if (m.IsRewardedReady())
                {
                    return true;
                }
            }
            return false;
        }

        public void SetBannerShown(bool shown)
        {
            foreach (var m in managers)
            {
                if (m.IsBannerEnabled)
                {
                    m.SetBannerShown(shown);
                    break;
                }
            }
        }

        public IPromise<bool> ShowRewardedWithLoadingWindow(string placementId)
        {
            foreach (var m in managers)
            {
                if (m.IsRewardedReady())
                {
                    return m.ShowRewardedWithLoadingWindow(placementId);
                }
            }
            return Promise<bool>.Resolved(false);
        }

        public IPromise<RewardedShowResult> ShowRewarded(string placementId)
        {
            foreach (var m in managers)
            {
                if (m.IsRewardedReady())
                {
                    return m.ShowRewarded(placementId);
                }
            }
            foreach (var m in managers)
            {
                return m.ShowRewarded(placementId);
            }
            return Promise<RewardedShowResult>.Resolved(RewardedShowResult.NotReady);
        }

        public bool ShowInterstitial(string placementId)
        {
            foreach (var m in managers)
            {
                if (m.IsInterstitialReady())
                {
                    return m.ShowInterstitial(placementId);
                }
            }
            return false;
        }

        public bool IsInterstitialReady()
        {
            foreach (var m in managers)
            {
                if (m.IsInterstitialReady())
                {
                    return true;
                }
            }
            return false;
        }

        public void CancelRewardedShow()
        {
            foreach (var m in managers)
            {
                m.CancelRewardedShow();
            }
        }

        public bool TryShowInterstitial()
        {
            foreach (var m in managers)
            {
                if (m.TryShowInterstitial())
                {
                    return true;
                }
            }
            return false;
        }

        public void ResetInterstitialCounter()
        {
            foreach (var m in managers)
            {
                m.ResetInterstitialCounter();
            }
        }
    }
}
