﻿using SwiftFramework.Core;
using System.Collections.Generic;

namespace SwiftFramework.AdsMediator
{
    public class AdsMediatorConfig : ModuleConfig
    {
        [LinkFilter(typeof(IAdsManager))]
        public List<ModuleLink> adManagers;
    }
}
