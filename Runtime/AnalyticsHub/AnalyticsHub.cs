﻿using System.Collections.Generic;
using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.AnalyticsHub
{
    public class AnalyticsHub : CompositeModule, IAnalyticsManager
    {
        private const string WATCHED_AD = "WatchedAd";
        private const string MADE_PURCHASE = "MadePurchase";
        protected override void OnInit()
        {
            base.OnInit();
            IAdsManager adsManager = App.GetModule<IAdsManager>();
            if (adsManager != null)
            {
                adsManager.OnRewardedShowStarted += AdsManager_OnRewardedShowStarted;
                adsManager.OnRewardedSuccessfully += AdsManager_OnRewardedSuccessfully;
                adsManager.OnRewardedAttemptToShow += AdsManager_OnRewardedAttemptToShow;
                adsManager.OnRewardedError += AdsManager_OnRewardedError;
            }
        }

        private void AdsManager_OnRewardedError(RewardedAdErrorArgs args)
        {
            LogRewardedVideoError(args.placementId, args.errorType, args.errorMessage);
        }

        private void AdsManager_OnRewardedAttemptToShow(string placementId)
        {
            LogRewardedVideoAttemptToShow(placementId);
        }

        private void AdsManager_OnRewardedSuccessfully(string placementId)
        {
            LogRewardedVideoSuccess(placementId);
        }

        private void AdsManager_OnRewardedShowStarted(string placementId)
        {
            LogRewardedVideoStarted(placementId);
        }

        public void LogEvent(string eventName, params (string key, object value)[] args)
        {
            Debug.Log($"ANALYTICS EVENT: {eventName}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogEvent(eventName, args);
            }
        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {
            Debug.Log($"ANALYTICS PURCHASE EVENT: {productId}");
            if (PlayerPrefs.HasKey(MADE_PURCHASE) == false)
            {
                LogFirstPurchase(productId);
                PlayerPrefs.SetInt(MADE_PURCHASE, 1);
                PlayerPrefs.Save();
            }

            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogPurchase(productId, currencyCode, price, transactionId);
            }
        }

        public void LogFirstPurchase(string productId)
        {
            Debug.Log($"ANALYTICS FIRST PURCHASE EVENT: {productId}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogFirstPurchase(productId);
            }
        }

        public void LogRewardedVideoStarted(string placementId)
        {
            Debug.Log($"ANALYTICS REWARDED STARTED: {placementId}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogRewardedVideoStarted(placementId);
            }
        }

        public void LogRewardedVideoSuccess(string placementId)
        {
            Debug.Log($"ANALYTICS REWARDED SUCCESS: {placementId}");
            if (PlayerPrefs.HasKey(WATCHED_AD) == false)
            {
                LogFirstAdWatched(placementId);
                PlayerPrefs.SetInt(WATCHED_AD, 1);
                PlayerPrefs.Save();
            }
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                Debug.Log($"{manager}");
                manager.LogRewardedVideoSuccess(placementId);
            }
        }

        public void LogRewardedVideoAttemptToShow(string placementId)
        {
            Debug.Log($"ANALYTICS REWARDED ATTEMPT: {placementId}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogRewardedVideoAttemptToShow(placementId);
            }
        }

        public void LogLevelUp(int level)
        {
            Debug.Log($"ANALYTICS LEVEL UP: {level}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogLevelUp(level);
            }
        }

        public void LogRewardedVideoError(string placementId, string errorType, string error)
        {
            Debug.Log($"ANALYTICS REWARDED ERROR: {placementId}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogRewardedVideoError(placementId, errorType, error);
            }
        }

        public void LogFirstAdWatched(string placementId)
        {
            Debug.Log($"ANALYTICS FIRST REWARDED SUCCESS: {placementId}");
            foreach (IAnalyticsManager manager in GetSubmodules<IAnalyticsManager>())
            {
                manager.LogFirstAdWatched(placementId);
            }
        }
    }
}
