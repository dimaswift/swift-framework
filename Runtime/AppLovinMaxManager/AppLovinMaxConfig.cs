﻿using SwiftFramework.Core;

namespace SwiftFramework.AppLovin
{
    public class AppLovinMaxConfig : ModuleConfig
    {
        public string key;
        public bool useBanner;
        public string rewardedId;
        public string interstitialId;
        public string bannerId;
        public bool showMediationDebugger = true;
        public float minTimeBetweenLoadingAttempts = 3;
    }

}
