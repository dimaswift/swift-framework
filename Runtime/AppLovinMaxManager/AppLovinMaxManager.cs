﻿#if ENABLE_APPLOVIN_MAX
using SwiftFramework.Core;
using System;
using UnityEngine;

namespace SwiftFramework.AppLovin
{
    [Configurable(typeof(AppLovinMaxConfig))]
    public class AppLovinMaxManager : Module, IAdsManager
    {
        public IStatefulEvent<bool> BannerShown => bannerShown;

        public bool IsBannerEnabled => config.useBanner;

        public event Action<string> OnRewardedShowStarted = id => { };
        public event Action OnRewardedAdLoaded = () => { };
        public event Action<string> OnRewardedSuccessfully = id => { };
        public event Action<string> OnRewardedClosed = id => { };
        public event Action<string> OnRewardedAttemptToShow = id => { };
        public event Action<RewardedAdErrorArgs> OnRewardedError = args => { };

        private readonly StatefulEvent<bool> bannerShown = new StatefulEvent<bool>();

        private AppLovinMaxConfig config;
        private Promise<RewardedShowResult> rewardedPromise;
        private float adCloseTime;

        private bool rewarded;
        private float lastRewardedLoadAttempt;
        private float lastInterstitialLoadAttempt;

        public AppLovinMaxManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        protected override IPromise GetInitPromise()
        {
            Promise initPromise = Promise.Create();
            config = GetModuleConfig<AppLovinMaxConfig>();
            MaxSdk.SetSdkKey(config.key);

            MaxSdk.InitializeSdk();

            App.Boot.OnFocused += Boot_OnFocused;

            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                if (config.showMediationDebugger)
                {
                    MaxSdk.ShowMediationDebugger();
                }

                MaxSdk.SetHasUserConsent(true);

                InitializeRewardedAds();

                InitializeInterstitialAds();

                if (config.useBanner)
                {
                    MaxSdk.CreateBanner(config.bannerId, MaxSdkBase.BannerPosition.BottomCenter);
                }

                Log("Applovin Max module initialized");

            };

            initPromise.Resolve();

            return initPromise;
        }

        private void Boot_OnFocused(bool focused)
        {
            if (focused)
            {
                TryLoadRewardedAd();
            }
        }

        public void CancelRewardedShow()
        {
            Resolve(RewardedShowResult.Canceled);
        }

        public float GetTimeSinceAdClosed()
        {
            return Time.realtimeSinceStartup - adCloseTime;
        }

        public bool IsInterstitialReady()
        {
            return MaxSdk.IsInterstitialReady(config.interstitialId);
        }

        public bool IsRewardedReady()
        {
            return MaxSdk.IsRewardedAdReady(config.rewardedId);
        }

        public bool IsShowingRewardedAd()
        {
            return rewardedPromise != null && rewardedPromise.CurrentState == PromiseState.Pending;
        }

        public void ResetInterstitialCounter()
        {

        }

        public void SetBannerShown(bool shown)
        {
            bannerShown.SetValue(shown);
            if (shown)
            {
                MaxSdk.ShowBanner(config.bannerId);
            }
            else
            {
                MaxSdk.HideBanner(config.bannerId);
            }
        }

        public bool ShowInterstitial(string placementId)
        {
            if (MaxSdk.IsInterstitialReady(config.interstitialId))
            {
                MaxSdk.ShowInterstitial(config.interstitialId);
                return true;
            }
            LoadInterstitialAd();
            return false;
        }

        public IPromise<RewardedShowResult> ShowRewarded(string placementId)
        {
            rewardedPromise = Promise<RewardedShowResult>.Create();
            if (IsRewardedReady() == false)
            {
                TryLoadRewardedAd();
                rewardedPromise.Resolve(RewardedShowResult.NotReady);
                return rewardedPromise;
            }
            rewarded = false;
            OnRewardedShowStarted(placementId);
            App.Boot.IgnoreNextPauseEvent();
            MaxSdk.ShowRewardedAd(config.rewardedId);
            return rewardedPromise;
        }

        public bool TryShowInterstitial()
        {
            return ShowInterstitial(config.interstitialId);
        }

        public void InitializeRewardedAds()
        {
            MaxSdkCallbacks.OnRewardedAdLoadedEvent += OnRewardedAdLoadedEvent;
            MaxSdkCallbacks.OnRewardedAdLoadFailedEvent += OnRewardedAdFailedEvent;
            MaxSdkCallbacks.OnRewardedAdFailedToDisplayEvent += OnRewardedAdFailedToDisplayEvent;
            MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRewardedAdDisplayedEvent;
            MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRewardedAdClickedEvent;
            MaxSdkCallbacks.OnRewardedAdHiddenEvent += OnRewardedAdDismissedEvent;
            MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRewardedAdReceivedRewardEvent;

            TryLoadRewardedAd();
        }

        public void InitializeInterstitialAds()
        {
            MaxSdkCallbacks.OnInterstitialAdFailedToDisplayEvent += MaxSdkCallbacks_OnInterstitialAdFailedToDisplayEvent;
            MaxSdkCallbacks.OnInterstitialDisplayedEvent += MaxSdkCallbacks_OnInterstitialDisplayedEvent;
            MaxSdkCallbacks.OnInterstitialLoadedEvent += MaxSdkCallbacks_OnInterstitialLoadedEvent;
            MaxSdkCallbacks.OnInterstitialLoadFailedEvent += MaxSdkCallbacks_OnInterstitialLoadFailedEvent;
            MaxSdkCallbacks.OnInterstitialHiddenEvent += MaxSdkCallbacks_OnInterstitialHiddenEvent;

            LoadInterstitialAd();
        }

        private void LoadInterstitialAd()
        {
            if (Time.realtimeSinceStartup - lastInterstitialLoadAttempt < config.minTimeBetweenLoadingAttempts)
            {
                float delay = config.minTimeBetweenLoadingAttempts - (Time.realtimeSinceStartup - lastInterstitialLoadAttempt);
                Debug.Log($"MAX SDK: Trying to load interstitial too soon. Will try again in {delay}");
                App.Timer.WaitFor(delay).Done(LoadInterstitialAd);

                return;
            }
            lastInterstitialLoadAttempt = Time.realtimeSinceStartup;
            Debug.Log($"MAX SDK: Trying to load interstitial");
            MaxSdk.LoadInterstitial(config.interstitialId);
        }

        private void MaxSdkCallbacks_OnInterstitialHiddenEvent(string obj)
        {
            LoadInterstitialAd();
        }

        private void MaxSdkCallbacks_OnInterstitialLoadFailedEvent(string arg1, int arg2)
        {
            App.Timer.WaitFor(config.minTimeBetweenLoadingAttempts).Done(LoadInterstitialAd);
        }

        private void MaxSdkCallbacks_OnInterstitialLoadedEvent(string obj)
        {
            Debug.Log($"MAX SDK: Interstitial ad loaded");
        }

        private void MaxSdkCallbacks_OnInterstitialDisplayedEvent(string obj)
        {   
            Debug.Log($"MAX SDK: Interstitial displayed");
            LoadInterstitialAd();
        }

        private void MaxSdkCallbacks_OnInterstitialAdFailedToDisplayEvent(string arg1, int arg2)
        {
            Debug.Log($"MAX SDK: Interstitial failed to display");
            LoadInterstitialAd();
        }

        private void TryLoadRewardedAd()
        {
            Debug.Log($"MAX SDK: trying to load rewarded ad.");
            if (IsRewardedReady())
            {
                Debug.Log($"MAX SDK: ad already loaded. Ignoring...");
                return;
            }

            if (Time.realtimeSinceStartup - lastRewardedLoadAttempt < config.minTimeBetweenLoadingAttempts)
            {
                var delay = config.minTimeBetweenLoadingAttempts - (Time.realtimeSinceStartup - lastRewardedLoadAttempt);
                Debug.Log($"MAX SDK: Asked to load rewarded too soon. Will try again in {delay} seconds.");
                App.Timer.WaitFor(delay).Done(TryLoadRewardedAd);
                return;
            }
            Debug.Log($"MAX SDK: Started loading rewarded ad.");
            lastRewardedLoadAttempt = Time.realtimeSinceStartup;
            MaxSdk.LoadRewardedAd(config.rewardedId);
        }

        private void OnRewardedAdLoadedEvent(string adUnitId)
        {
            Debug.Log($"MAX SDK: Rewarded ad loaded");
            OnRewardedAdLoaded();
        }

        private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
        {
            Debug.Log($"MAX SDK: Rewarded ad loading failed: {errorCode}");
            OnRewardedError(new RewardedAdErrorArgs() { errorMessage = errorCode.ToString() });
            App.Timer.WaitForUnscaled(config.minTimeBetweenLoadingAttempts).Done(TryLoadRewardedAd);
        }

        private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
        {
            Debug.Log($"MAX SDK: Rewarded ad failed to display: {errorCode}");
            OnRewardedError(new RewardedAdErrorArgs() { errorMessage = errorCode.ToString() });
            Resolve(RewardedShowResult.Error);
            App.Timer.WaitFor(config.minTimeBetweenLoadingAttempts).Done(TryLoadRewardedAd);
        }

        private void OnRewardedAdDisplayedEvent(string adUnitId)
        {
            Debug.Log($"MAX SDK: Rewarded ad displayed");
            OnRewardedClosed(adUnitId);
            TryLoadRewardedAd();
        }

        private void OnRewardedAdClickedEvent(string adUnitId)
        {
            Debug.Log($"MAX SDK: Rewarded ad clicked");
        }

        private void OnRewardedAdDismissedEvent(string adUnitId)
        {
            Debug.Log($"MAX SDK: Rewarded ad dismissed. Reward earned: {rewarded}");
            adCloseTime = Time.realtimeSinceStartup;
            if (rewarded)
            {
                OnRewardedSuccessfully(adUnitId);
                Resolve(RewardedShowResult.Success);
            }
            else
            {
                Resolve(RewardedShowResult.Canceled);
            }
        }

        private void Resolve(RewardedShowResult result)
        {
            if (rewardedPromise == null)
            {
                Debug.Log($"MAX SDK: Trying to resolved already resolved reward!");
                return;
            }
            rewardedPromise.Resolve(result);
            Debug.Log($"MAX SDK: Reward added successfully!");
            rewardedPromise = null;
            TryLoadRewardedAd();
        }

        private void OnRewardedAdReceivedRewardEvent(string adUnitId, MaxSdk.Reward reward)
        {
            Debug.Log($"MAX SDK: Rewarded received");
            rewarded = true;
        }
    }

}
#endif