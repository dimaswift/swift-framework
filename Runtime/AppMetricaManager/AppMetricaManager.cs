﻿using SwiftFramework.Core;
using System.Runtime.Remoting.Messaging;
using UnityEngine;

namespace SwiftFramework.AppMetricaManager
{
#if ENABLE_APPMETRICA
    [RequireComponent(typeof(AppMetrica))]
#endif
    public class AppMetricaManager : BehaviourModule, IAnalyticsManager
    {
#if ENABLE_APPMETRICA
        [Header("AppMetrica is enabled")]
#else
        [Header("AppMetrica is disable. Use ENABLE_APPMETRICA inside SwiftFramework/Difine Symbols")]
#endif
        [SerializeField] private GameObject appMetrica = null;

        private void OnValidate()
        {
#if ENABLE_APPMETRICA
            appMetrica = GetComponent<AppMetrica>()?.gameObject;
#endif
         }

        public void LogEvent(string eventName, params (string key, object value)[] args)
        {
            
        }

        public void LogFirstAdWatched(string placementId)
        {
           
        }

        public void LogFirstPurchase(string productId)
        {
            
        }

        public void LogLevelUp(int level)
        {
            
        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {

        }

        public void LogRewardedVideoAttemptToShow(string placementId)
        {
            
        }

        public void LogRewardedVideoError(string placementId, string errorType, string errorCode)
        {
           
        }

        public void LogRewardedVideoStarted(string placementId)
        {
            
        }

        public void LogRewardedVideoSuccess(string placementId)
        {
#if ENABLE_APPMETRICA
            AppMetrica.Instance.ReportEvent("rewarded_ad_success");
#endif
        }
    }

}
