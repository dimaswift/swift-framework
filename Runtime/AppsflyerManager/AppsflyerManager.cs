﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Appsflyer
{
    public class AppsflyerManager : BehaviourModule, IAnalyticsManager
    {
        [SerializeField] private string devKey = null;
        [SerializeField] private bool trackCustomEvents = true;

        protected override IPromise GetInitPromise()
        {
            Promise result = Promise.Create();

            AppsFlyer.setAppsFlyerKey(devKey);
#if UNITY_IOS
          
            AppsFlyer.setAppID(devKey);
            AppsFlyer.trackAppLaunch();
            

#elif UNITY_ANDROID

            AppsFlyer.setAppID(Application.identifier);
            gameObject.AddComponent<AppsFlyerTrackerCallbacks>();
            AppsFlyer.init(devKey, name);

#endif
            result.Resolve();
            return result;
        }

        public void LogEvent(string eventName, params (string key, object value)[] args)
        {
            if (trackCustomEvents == false)
            {
                return;
            }
            var eventData = new Dictionary<string, string>();
            foreach (var item in args)
            {
                if (eventData.ContainsKey(item.key) == false)
                {
                    eventData.Add(item.key, item.value?.ToString());
                }
            }
            AppsFlyer.trackRichEvent(eventName, eventData);
        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add(AFInAppEvents.CONTENT_ID, productId);
            eventData.Add(AFInAppEvents.PRICE, price.ToString());
            eventData.Add(AFInAppEvents.CURRENCY, currencyCode);
            eventData.Add(AFInAppEvents.REVENUE, price.ToString());
            AppsFlyer.trackRichEvent(AFInAppEvents.PURCHASE, eventData);
        }

        public void LogRewardedVideoStarted(string placementId)
        {

        }

        public void LogRewardedVideoSuccess(string placementId)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add("af_adrev_ad_type", placementId);
            eventData.Add(AFInAppEvents.CURRENCY, "USD");
            eventData.Add(AFInAppEvents.REVENUE, "0.01");
            AppsFlyer.trackRichEvent("af_ad_view", eventData);
        }

        public void LogRewardedVideoAttemptToShow(string placementId)
        {

        }

        public void LogLevelUp(int level)
        {
            var eventData = new Dictionary<string, string>();
            eventData.Add(AFInAppEvents.LEVEL, level.ToString());
            AppsFlyer.trackRichEvent(AFInAppEvents.LEVEL_ACHIEVED, eventData);
        }

        public void LogRewardedVideoError(string placementId, string errorType, string error)
        {

        }

        public void LogFirstPurchase(string productId)
        {

        }

        public void LogFirstAdWatched(string placementId)
        {

        }
    }
}
