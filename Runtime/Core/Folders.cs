﻿namespace SwiftFramework.Core
{
    public static class Folders
    {
        public const string Views = "Views";
        public const string Configs = "Configs";
        public const string Modules = "Modules";
        public const string Scenes = "Scenes";
        public const string Events = "Events";
        public const string Promises = "Promises";
        public const string Addressables = "Addressables";
        
    }
}
