﻿using SwiftFramework.Core.SharedData.Upgrades;
using UnityEngine;

namespace SwiftFramework.Core
{
    public class BaseStatInfo : ScriptableObject
    {
        public string descriptionKey;
        public SpriteLink icon;
        public IntStat power;
    }
}
