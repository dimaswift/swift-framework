﻿using System;

namespace SwiftFramework.Core.Boosters
{
    [Serializable]
    public class BoosterContainer
    {
        public BoosterConfigLink configLink;
        public int amount;
    }
}
