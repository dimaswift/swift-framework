﻿namespace SwiftFramework.Core.Boosters
{
    public enum BoosterType
    {
        Temporary = 0,
        Permanent = 1
    }

    public enum BoosterOperation
    {
        Addition = 0,
        Multiplication = 1
    }
}
