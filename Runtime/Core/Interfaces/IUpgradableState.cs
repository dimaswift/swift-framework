﻿namespace SwiftFramework.Core
{
    public interface IUpgradableState
    {
        long Level { get; }
    }
}
