﻿namespace SwiftFramework.Core
{
    public static class AddrLabels
    {
        public const string Prewarm = "Prewarm";
        public const string Remote = "Remote";
        public const string Booster = "Booster";
        public const string Window = "Window";
    }
}
