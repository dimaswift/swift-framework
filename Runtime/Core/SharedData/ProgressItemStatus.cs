﻿namespace SwiftFramework.Core.SharedData
{
    public enum ProgressItemStatus
    {
        Active,
        Completed,
        Rewarded,
        OnCooldown,
        Obsolete,
    }

}
