﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.DailyRewardManager
{
    [PrewarmAsset]
    [CreateAssetMenu(menuName = "SwiftFramework/DailyRewards/DailyReward")]
    public class DailyRewardConfig : ScriptableObject
    {
        public List<RewardLink> rewardItems;
    }
}