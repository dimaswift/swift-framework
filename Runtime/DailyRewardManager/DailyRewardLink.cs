﻿using SwiftFramework.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.DailyRewardManager
{
    [Serializable]
    [FlatHierarchy]
    [LinkFolder(Folders.Configs + "Rewards/DailyRewards")]
    public class DailyRewardLink : LinkToScriptable<DailyRewardConfig>
    {

    }
}