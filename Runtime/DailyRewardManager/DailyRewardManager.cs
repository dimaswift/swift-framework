﻿using SwiftFramework.Core;
using System;
using UnityEngine;

namespace SwiftFramework.DailyRewardManager
{
    [Configurable(typeof(DailyRewardsManagerConfig))]
    public class DailyRewardManager : Module, IDailyRewardManager
    {
        private DailyRewardsManagerConfig config;
        private DailyRewardManagerState state;
        public DailyRewardManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        public void ClaimReward()
        {
            config.dailyRewards[state.currentRewardId].Load(loadedReward =>
            {
                foreach (var reward in loadedReward.rewardItems)
                {
                    reward.Load(item =>
                    {
                        item.AddReward();
                    });
                }

            });

            state.currentRewardId++;

            if (state.currentRewardId >= config.dailyRewards.Count)
            {
                state.currentRewardId = 0;
            }

            state.lastRewardTimeStamp = App.Clock.Now.Value;

        }

        public IPromise<bool> TryShowRewardWindow()
        {
            Promise<bool> promise = Promise<bool>.Create();

            App.Timer.WaitForUnscaled(config.displayDelay).Done(() =>
            {
                if (CanGetDailyGift())
                {
                    DailyRewardWindowArgs args = new DailyRewardWindowArgs { config = config, currentRewardId = state.currentRewardId };

                    App.GetModule<IWindowsManager>().Show<DailyRewardWindow, DailyRewardWindowArgs>(args).Done(window =>
                    {
                        window.HidePromise.Done(() =>
                        {
                            promise.Resolve(true);
                        });

                    });
                }
                else
                {
                    promise.Resolve(false);
                }
            });

            return promise;
        }

        protected override IPromise GetInitPromise()
        {
            config = GetModuleConfig<DailyRewardsManagerConfig>();

            App.Storage.OnAfterLoad += LoadState;
            App.Storage.RegisterState(() => state);
            LoadState();

            return base.GetInitPromise();
        }


        private void LoadState()
        {
            state = App.Storage.LoadOrCreateNew<DailyRewardManagerState>();
        }

        private bool CanGetDailyGift()
        {
            if (state.lastRewardTimeStamp == 0)
                return true;

            int now = DateTimeOffset.FromUnixTimeSeconds(App.Clock.Now.Value).DayOfYear;

            if (DateTimeOffset.FromUnixTimeSeconds(state.lastRewardTimeStamp).DayOfYear != now)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        [Serializable]
        private class DailyRewardManagerState
        {
            public long lastRewardTimeStamp;
            public int currentRewardId;
        }

    }


}