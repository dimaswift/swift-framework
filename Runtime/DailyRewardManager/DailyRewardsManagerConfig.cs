﻿using SwiftFramework.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.DailyRewardManager
{
    public class DailyRewardsManagerConfig : ModuleConfig
    {
        public List<DailyRewardLink> dailyRewards;
        public float displayDelay;
    }
}