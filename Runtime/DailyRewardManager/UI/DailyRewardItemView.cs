﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Utils;
using SwiftFramework.Utils.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.DailyRewardManager
{
    public class DailyRewardItemView : ElementFor<DailyRewardLink>
    {
        [SerializeField] private TMP_Text rewardAmountText = null;
        [SerializeField] private Image icon = null;
        [SerializeField] private GameObject currentRewardBg = null;
        [SerializeField] private GameObject takenRewardBg = null;
        [SerializeField] private GameObject availableRewardBg = null;
        protected override void OnClicked()
        {

        }

        protected override void OnSetUp(DailyRewardLink item)
        {
            item.Load(loadedItem =>
            {
                loadedItem.rewardItems[0].Load(loadedReward =>
                {
                    icon.SetSpriteAsync(loadedReward.Icon);
                    rewardAmountText.text = loadedReward.GetAmount().Value.ToPrettyString();
                });
            });

        }

        public void SetBackgroundAvailable(bool value)
        {
            SetBackgroundCurrent(false);
            availableRewardBg.SetActive(value);
            takenRewardBg.SetActive(!value);
        }

        public void SetBackgroundCurrent(bool value)
        {
            currentRewardBg.SetActive(value);
        }
    }
}