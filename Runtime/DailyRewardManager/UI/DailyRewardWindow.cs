﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Utils.UI;
using SwiftFramework.Core.Windows;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.DailyRewardManager
{
    [AddrSingleton]
    public class DailyRewardWindow : WindowWithArgs<DailyRewardWindowArgs>
    {
        [SerializeField] private Button claimButton = null;
        [SerializeField] private ElementSet dailyRewardSet = null;

        private TMP_Text claimButtonText;

        private IDailyRewardManager dailyRewardManager;

        private int currentRewardId;
        public override void WarmUp()
        {
            dailyRewardManager = App.Core.GetModule<IDailyRewardManager>();

            claimButtonText = claimButton.GetComponentInChildren<TMP_Text>();

            claimButton.onClick.AddListener(OnClaimClicked);
        }

        public override void OnStartShowing(DailyRewardWindowArgs args)
        {
            base.OnStartShowing(args);

            dailyRewardSet.SetUp<DailyRewardItemView, DailyRewardLink>(args.config.dailyRewards);
            currentRewardId = args.currentRewardId;

            int i = 0;
            foreach (DailyRewardItemView item in dailyRewardSet.GetActiveElements<DailyRewardItemView>())
            {
                if (currentRewardId < i)
                {
                    item.SetBackgroundAvailable(true);
                }
                else if (currentRewardId > i)
                {
                    item.SetBackgroundAvailable(false);
                }
                else if (currentRewardId == i)
                {
                    item.SetBackgroundCurrent(true);
                }

                i++;
            }

        }

        private void OnClaimClicked()
        {
            dailyRewardManager.ClaimReward();

            claimButtonText.text = App.Core.Local.GetText("#claimed");
            claimButton.interactable = false;

            int i = 0;
            foreach (DailyRewardItemView item in dailyRewardSet.GetActiveElements<DailyRewardItemView>())
            {
                if (currentRewardId == i)
                {
                    item.SetBackgroundAvailable(false);
                }

                i++;
            }
        }

    }

    public struct DailyRewardWindowArgs
    {
        public DailyRewardsManagerConfig config;
        public int currentRewardId;
    }

}