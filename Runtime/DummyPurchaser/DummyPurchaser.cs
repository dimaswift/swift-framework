﻿using SwiftFramework.Core;
using System;

public class DummyPurchaser : Module, IInAppPurchaseManager
{
    public IStatefulEvent<bool> PurchasingInitialized => initialized;

    public event Action<string> OnItemPurchased = i => { };

    private readonly StatefulEvent<bool> initialized = new StatefulEvent<bool>();

    public IPromise<bool> Buy(string productId)
    {
        return Promise<bool>.Resolved(true);
    }

    public string GetCurrencyCode(string productId)
    {
        return "USD";
    }

    public decimal GetPrice(string productId)
    {
        return 0.01m;
    }

    public string GetPriceString(string productId)
    {
        return "$0.01";
    }

    public SubscriptionData GetSubscriptionData(string productId)
    {
        return default;
    }

    public bool IsAlreadyPurchased(string productId)
    {
        return false;
    }

    public bool IsSubscribed(string productId)
    {
        return false;
    }

    public void ObfuscateKeys(string googlePlayPublicKey, byte[] appleCertData)
    {
        
    }

    public IPromise<bool> RestorePurchases()
    {
        return Promise<bool>.Resolved(true);
    }
}
