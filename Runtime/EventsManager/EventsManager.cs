﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SwiftFramework.EventsManager
{
    [DefaultModule]
    [DisallowCustomModuleBehaviours]
    public class EventsManager : BehaviourModule, IEventManager
    {
        private readonly Collider2D[] colliderBuffer = new Collider2D[8];

        private readonly List<IPhysics2DClick> physics2DClickHandlers = new List<IPhysics2DClick>();
        private readonly List<IPhysics2DClick> clickedHandlers = new List<IPhysics2DClick>();

        private Camera cam;
        private Vector2 clickScreenPoint;
        private const float DRAG_THRESHOLD = 0.1f;

        public bool IsPointerHandledByUI
        {
            get
            {
                if (Input.touchSupported)
                {
                    return Input.touchCount > 0 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
                }
                else
                {
                    return EventSystem.current.IsPointerOverGameObject();
                }
            }
        }

        public bool IsPhysics2DClicked()
        {
            return clickedHandlers.Count > 0;
        }

        private Vector3 GetWorldPoint()
        {
            return cam.ScreenToWorldPoint(Input.mousePosition);
        }

        private Vector2 GetScreenPoint()
        {
            var y = (Input.mousePosition.y / Screen.height) * (cam.orthographicSize * 2);
            var x = (Input.mousePosition.x / Screen.width) * (cam.orthographicSize * 2 * cam.aspect);
            return new Vector2(x, y);
        }

        public void Register(IPhysics2DClick physics2DClick)
        {
            physics2DClickHandlers.Add(physics2DClick);
            physics2DClickHandlers.Sort((m1, m2) => -m1.gameObject.layer.CompareTo(m2.gameObject.layer));
        }

        private void Raycast(Action<IPhysics2DClick, Vector3> handler)
        {
            if (IsPointerHandledByUI)
            {
                return;
            }
            Vector3 worldPos = GetWorldPoint();
            int hits = Physics2D.OverlapPointNonAlloc(worldPos, colliderBuffer);
            for (int j = 0; j < physics2DClickHandlers.Count; j++)
            {
                for (int i = 0; i < hits; i++)
                {
                    IPhysics2DClick physics2DClick = physics2DClickHandlers[j];
                    if (physics2DClick.gameObject == colliderBuffer[i].gameObject)
                    {
                        handler(physics2DClick, worldPos);
                        return;
                    }
                }
            }
        }

        private void Update()
        {
            if(cam == null)
            {
                cam = Camera.main;
            }

            if(cam == null)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0))
            {
                clickScreenPoint = GetScreenPoint();
                Raycast((handler, worldPos) => 
                {
                    handler.IsPointerDown = true;
                    handler.OnPointerDown(worldPos);
                    clickedHandlers.Add(handler);
                });
            }

            if (Input.GetMouseButtonUp(0))
            {
                Vector3 worldPos = GetWorldPoint();

                if (Vector2.SqrMagnitude(GetScreenPoint() - clickScreenPoint) >= DRAG_THRESHOLD)
                {
                    return;
                }

                foreach (IPhysics2DClick h in clickedHandlers)
                {
                    h.OnPointerUp(worldPos);
                }
                clickedHandlers.Clear();
                Raycast((handler, pos) =>
                {
                    if (handler.IsPointerDown)
                    {
                        handler.IsPointerDown = false;
                        handler.OnClick(worldPos);
                    }
                });
            }
        }
    }
}
