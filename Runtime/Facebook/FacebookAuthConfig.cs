﻿using SwiftFramework.Core;

namespace SwiftFramework.FacebookAuth
{
    public class FacebookAuthConfig : ModuleConfig
    {
        public string[] permisions = { "public_profile", "email", "user_friends" };
        public int avatarSize = 256;
    }

}
