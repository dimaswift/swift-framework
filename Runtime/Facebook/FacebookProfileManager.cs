﻿using SwiftFramework.Core;
using UnityEngine;
using System;
using Facebook.Unity;

namespace SwiftFramework.FacebookAuth
{
    [Configurable(typeof(FacebookAuthConfig))]
    public class FacebookProfileManager : Module, IFacebookProfile
    {
        public event Action<FacebookProfile> OnLogin = p => { };

        public FacebookProfile Profile { get; private set; }

        private Promise<FacebookAuthResult> loginPromise;

        private FacebookAuthConfig config;

        private FacebookAuthResult authResult;

        public FacebookProfileManager(ModuleConfigLink config)
        {
            SetConfig(config);
        }

        protected override IPromise GetInitPromise()
        {
            config = GetModuleConfig<FacebookAuthConfig>();
            Promise result = Promise.Create();
            OnFacebookInit(() =>
            {
                if (FB.IsLoggedIn)
                {
                    TryRefreshToken(result.Resolve);
                }
                else
                {
                    result.Resolve();
                }
            });
            return result;
        }

        private void OnFacebookInit(Action action)
        {
            if (FB.IsInitialized)
            {
                action();
            }
            else
            {
                FB.Init(() => action());
            }
        }
 
        private void TryRefreshToken(Action callback)
        {
            FB.Mobile.RefreshCurrentAccessToken(result =>
            {
                if (IsValidResult(result))
                {
                    ProcessLogin(result.AccessToken);
                    callback();
                }
                else
                {
                    RejectLogin(result?.Error);
                    callback();
                }
            });
        }

        private IPromise<Texture2D> GetAvatar(string id)
        {
            Promise<Texture2D> resultPromise = Promise<Texture2D>.Create();
            string querry = $"/{id}/picture?type=square&height={config.avatarSize}&width={config.avatarSize}";
            FB.API(querry, HttpMethod.GET, result => 
            {
                if (result.Cancelled == false && result.Texture != null)
                {
                    resultPromise.Resolve(result.Texture);
                }
                else
                {
                    resultPromise.Reject(new Exception(string.IsNullOrEmpty(result.Error) ? "unknown error" : result.Error));
                }
            });
            return resultPromise;
        }

        private IPromise<string> GetEmail(string id)
        {
            Promise<string> resultPromise = Promise<string>.Create();
            string querry = $"/{id}";
            FB.API(querry, HttpMethod.GET, result =>
            {
                if (ExtractResult(result, "email", out object email, out string error))
                {
                    resultPromise.Resolve(email.ToString());
                }
                else
                {
                    resultPromise.Reject(new Exception(error));
                }
            });
            return resultPromise;
        }

        private IPromise<string> GetName(string id)
        {
            Promise<string> resultPromise = Promise<string>.Create();
            string querry = $"/{id}";
            FB.API(querry, HttpMethod.GET, result =>
            {
                if (ExtractResult(result, "name", out object name, out string error))
                {
                    resultPromise.Resolve(name.ToString());
                }
                else
                {
                    resultPromise.Reject(new Exception(error));
                }
            });
            return resultPromise;
        }

        private bool ExtractResult(IGraphResult result, string key, out object value, out string error)
        {
            if (result.Cancelled == false && result.ResultDictionary.TryGetValue(key, out value))
            {
                error = null;
                return true;
            }
            else
            {
                error = string.IsNullOrEmpty(result.Error) ? "unknown error" : result.Error;
                value = null;
                return false;
            }
        }

        public IPromise<FacebookAuthResult> Login()
        {
            if(loginPromise != null)
            {
                return loginPromise;
            }

            loginPromise = Promise<FacebookAuthResult>.Create();

            if(authResult != null && authResult.Success)
            {
                Promise<FacebookAuthResult> resolvedPromise = loginPromise;
                loginPromise.Resolve(authResult);
                loginPromise = null;
                return resolvedPromise;
            }

            OnFacebookInit(() => 
            {
                TryLoginToFacebook();
            });

            return loginPromise;
        }

        private void TryLoginToFacebook()
        {
            if (FB.IsLoggedIn)
            {
                TryRefreshToken(() => { });
            }
            else
            {
                FB.LogInWithReadPermissions(config.permisions, (result =>
                {
                    App.Timer.WaitForMainThread().Done(() =>
                    {
                        if (IsValidResult(result))
                        {
                            ProcessLogin(result.AccessToken);
                        }
                        else
                        {
                            RejectLogin(result?.Error);
                        }
                    });
                }));
            }
        }

        private void RejectLogin(string error)
        {
            Debug.LogError($"Facebook login failed: {error}");
            if(loginPromise == null)
            {
                return;
            }
            loginPromise.Resolve(new FacebookAuthResult() { Success = false, Error = error });
            loginPromise = null;
        }

        private bool IsValidResult(IResult result)
        {
            if (result.Cancelled || string.IsNullOrEmpty(result.Error) == false)
            {
                return false;
            }
            return string.IsNullOrEmpty(result.RawResult) == false;
        }

        private void ProcessLogin(AccessToken accessToken)
        {
            if(accessToken == null)
            {
                RejectLogin("null token");
                return;
            }

            Profile = new FacebookProfile()
            {
                Id = accessToken.UserId,
                Avatar = GetAvatar(accessToken.UserId),
                Email = GetEmail(accessToken.UserId),
                Name = GetName(accessToken.UserId),
                AccessToken = accessToken.TokenString,
            };

            authResult = new FacebookAuthResult()
            {
                Success = true,
                Profile = Profile
            };

            OnLogin(Profile);

            if (loginPromise == null)
            {
                return;
            }

            loginPromise.Resolve(authResult);
            loginPromise = null;
        }

        public void Logout()
        {
            OnFacebookInit(() =>
            {
                if (FB.IsLoggedIn)
                {
                    FB.LogOut();
                }
            });
           
            authResult = null;
            loginPromise = null;
            Profile = null;
        }
    }
}