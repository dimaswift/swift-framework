﻿using System.Collections.Generic;
using SwiftFramework.Core;
using Facebook.Unity;

namespace SwiftFramework.Facebook
{
    [DependsOnModules(typeof(ITimer))]
    public class FacebookAnalytics : BehaviourModule, IAnalyticsManager
    {
        private readonly Dictionary<string, object> paramsBuffer = new Dictionary<string, object>(10);

        protected override IPromise GetInitPromise()
        {
            if (FB.IsInitialized)
            {
                return Promise.Resolved();
            }

            Promise initPromise = Promise.Create();

            FB.Init(() => 
            {
                initPromise.Resolve();
            });

            return Promise.Race(App.Timer.WaitForUnscaled(1f), initPromise);
        }

        public void LogEvent(string eventName, params (string key, object value)[] args)
        { 
            if (FB.IsInitialized == false)
            {
                return;
            }
            paramsBuffer.Clear();
            foreach (var pair in args)
            {
                if(paramsBuffer.ContainsKey(pair.key) == false)
                {
                    paramsBuffer.Add(pair.key, pair.value);
                }
            }
            FB.LogAppEvent(eventName, 1, paramsBuffer);
        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {
            if(FB.IsInitialized == false)
            {
                return;
            }
            var parameters = new Dictionary<string, object>();
            parameters.Add("transactionID", transactionId);
            parameters.Add("productId", productId);
            FB.LogPurchase((float)price, currencyCode, parameters);
        }

        public void LogRewardedVideoStarted(string placementId)
        {
            if (FB.IsInitialized == false)
            {
                return;
            }
            var parameters = new Dictionary<string, object>()
            {
                { AppEventParameterName.ContentID, placementId },
            };
            FB.LogAppEvent("rewarded_ad_started", 1, parameters);
        }

        public void LogRewardedVideoSuccess(string placementId)
        {
            if (FB.IsInitialized == false)
            {
                return;
            }
           
            var parameters = new Dictionary<string, object>()
            {
                { AppEventParameterName.ContentID, placementId }
            };
            FB.LogAppEvent(AppEventName.ViewedContent, 1, parameters); 
        }

        public void LogRewardedVideoAttemptToShow(string placementId)
        {
            if (FB.IsInitialized == false)
            {
                return;
            }

            var parameters = new Dictionary<string, object>()
            {
                { AppEventParameterName.ContentID, placementId }
            };
            FB.LogAppEvent("rewarded_ad_attempt_to_show", 1, parameters);
        }

        public void LogLevelUp(int level)
        {
            if (FB.IsInitialized == false)
            {
                return;
            }

            var parameters = new Dictionary<string, object>()
            {
                { AppEventParameterName.Level, level }
            };
            FB.LogAppEvent("leveled_up", 1, parameters);
        }

        public void LogRewardedVideoError(string placementId, string errorType, string error)
        {
           
        }

        public void LogFirstPurchase(string productId)
        {
            if (FB.IsInitialized == false)
            {
                return;
            }

            var parameters = new Dictionary<string, object>()
            {
                { "product", productId }
            };
            FB.LogAppEvent("first_IAP", 1, parameters);

        }

        public void LogFirstAdWatched(string placementId)
        {
            if (FB.IsInitialized == false)
            {
                return;
            }

            var parameters = new Dictionary<string, object>()
            {
                { AppEventParameterName.ContentID, placementId }
            };
            FB.LogAppEvent("First_Rewarded_Ad", 1, parameters);
        }
    }
}
