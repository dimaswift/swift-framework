﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Analytics;

namespace SwiftFramework.FirebaseWrapper
{
    public class FirebaseAnalytics : BehaviourModule, IAnalyticsManager
    {
        [SerializeField] private int[] sendEventOnLevels;

        public void LogEvent(string eventName, params (string key, object value)[] args)
        {

        }

        public void LogFirstAdWatched(string placementId)
        {
            
        }

        public void LogFirstPurchase(string productId)
        {
            
        }

        public void LogLevelUp(int level)
        {
            foreach (var l in sendEventOnLevels)
            {
                if(l == level)
                {
                    Firebase.Analytics.FirebaseAnalytics.LogEvent($"level_{level + 1}_achieved");
                    break;
                }
            }
        }

        public void LogPurchase(string productId, string currencyCode, decimal price, string transactionId)
        {
            
        }

        public void LogRewardedVideoAttemptToShow(string placementId)
        {
            
        }

        public void LogRewardedVideoError(string placementId, string errorType, string errorCode)
        {
            
        }

        public void LogRewardedVideoStarted(string placementId)
        {
            
        }

        public void LogRewardedVideoSuccess(string placementId)
        {
            
        }
    }
}
