﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.FirebaseWrapper
{
    [DependsOnModules(typeof(ITimer))]
    public class FirebaseWrapper : Module, IFirebase
    {
        protected override IPromise GetInitPromise()
        {
            if (Application.isEditor)
            {
                return Promise.Resolved();
            }

            Promise loadPromise = Promise.Create();
            try
            {

                Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
                {
                    App.Timer.WaitForNextFrame().Done(() =>
                    {
                        var dependencyStatus = task.Result;
                        if (dependencyStatus == Firebase.DependencyStatus.Available)
                        {
                            Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
                            Debug.Log($"Firebase app initialized: {app.Name}");
                            loadPromise.Resolve();
                        }
                        else
                        {
                            loadPromise.Resolve();
                            Debug.LogError(string.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                        }
                    });
                });
            }
            catch (System.Exception e)
            {
                Debug.LogError(string.Format("Could not init Firebase: {0}", e.Message));
                loadPromise.Resolve();
            }

            return Promise.Race(App.Timer.WaitFor(5), loadPromise);
        }
    }
}
