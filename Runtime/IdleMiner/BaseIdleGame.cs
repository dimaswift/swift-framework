﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using SwiftFramework.Core.Prestige;
using SwiftFramework.Core.SharedData.Funds;
using SwiftFramework.Core.SharedData.SeasonPass;
using SwiftFramework.IdleMiner;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Controllers;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.UI;
using SwiftFramework.IdleMiner.Views;
using System;
using System.Numerics;
using UnityEngine;

namespace GoblinMiner.Core
{
    [DependsOnModules(typeof(IEventManager), typeof(ILocalizationManager), typeof(IDailyRewardManager), typeof(ISeasonPass))]
    public abstract class BaseIdleGame<T> : BehaviourModule, IIdleGameModule where T : GameController, new()
    {
        private GameController game;
        private GameConfig config;
        private GameState state;

        public event Action OnZoneChanged = () => { };
        public event Action<ILink> OnZoneUnlocked = z => { };
        public event Action OnGameCreated = () => { };
        public event Action OnSeasonEventFinished = () => { };

        public event Action<ILink> OnOfflineRevenueClaimed = a => { };

        public ISharedSupervisorsManager SharedSupervisors { get; private set; }

        public IFundsSource SoftFunds => game.World.Funds;

        public IFundsSource HardFunds => config.hardFunds.Value;

        public IStageManager StageManager { get; private set; }

        public FracturedBigNumber ProductionSpeed => game.ProductionSpeed;

        public long FirstSessionTimestamp => state.firstSessionTimestamp;

        private IGameView gameView;

        private IIldeWorldMap map;

        private ISeasonPass seasonPass;

        public virtual bool IsSeasonEventZoneActive() => config.currentEvent.HasValue && config.currentEvent.Value.world.GetPath() == GetActiveWorld().GetPath();

        protected abstract IPromise<GameConfig> LoadConfig();

        protected override IPromise GetInitPromise()
        {
            StageManager = new StageManager();

            StageManager.OnStageLoaded += StageManager_OnStageLoaded;

            Promise promise = Promise.Create();

            seasonPass = App.GetModule<ISeasonPass>();

            LoadConfig().Done(config =>
            {
                this.config = config;

                state = App.Storage.LoadOrCreateNew(GetDefaultState);

                App.Storage.RegisterState(() => state);

                App.Boot.OnFocused += OnFocused;

                SetActiveZone(state.activeZone).Done(() =>
                {
                    promise.Resolve();
                });
            });

            return promise;
        }

        private void StageManager_OnStageLoaded(ViewLink stage)
        {
            if (stage == config.gameView)
            {
                OnGameStageOpened();
            }
        }

        private void OnFocused(bool focused)
        {
            if (focused == false)
            {
                state.lastSessionTimestamp = App.Clock.Now.Value;
                return;
            }

            if (App.Clock.Now.Value - state.lastSessionTimestamp >= state.activeZone.Value.offlineRevenueTimeMin * 60)
            {
                TryShowCurrentZoneOfflineRevenue();
            }
        }

        public bool IsZoneUnlocked(ZoneConfigLink zone)
        {
            return state.GetZone(zone) != null;
        }

        public bool IsZoneUnlocked(ILink zone)
        {
            return IsZoneUnlocked(zone as ZoneConfigLink);
        }

        public IPromise<bool> UnlockZone(ILink link)
        {
            Promise<bool> promise = Promise<bool>.Create();

            ZoneConfigLink zoneLink = link as ZoneConfigLink;

            zoneLink.Load(config =>
            {
                config.unlockPrice.Pay().Done(paid =>
                {
                    if (paid)
                    {
                        ZoneController.CreateZone(zoneLink).Done(zone =>
                        {
                            zone.lastRevenueEarnTimestamp = game.GlobalClock.Now.Value;
                            state.AddZone(zone);
                            OnZoneUnlocked(link);
                            promise.Resolve(true);
                        });
                    }
                    else
                    {
                        promise.Resolve(false);
                    }
                });
            });

            return promise;
        }

        private IPromise CreateGame(GameConfig gameConfig, WorldConfigLink world)
        {
            Promise promise = Promise.Create();

            GameController.Create<T>(gameConfig, state, App.Clock, seasonPass, world).Then(game =>
            {
                this.game = game;

                SharedSupervisors = game.SharedSupervisors;
                StageManager.Load<GameView>(config.gameView).Done(gameView =>
                {
                    this.gameView = gameView;
                    gameView.Render(game.World.ActiveZone).Then(zoneView =>
                    {
                        game.Run().Done(() =>
                        {
                            promise.Resolve();
                            OnGameCreated();
                        });
                    });
                });

            })
            .Catch(e => promise.Reject(e));

            return promise;
        }

        private GameState GetDefaultState()
        {
            return new GameState()
            {
                boosters = new SwiftFramework.Core.Boosters.BoostersState(),
                lastSessionTimestamp = App.Clock.Now.Value,
                tutorial = new Tutorial(),
                activeWorld = config.startWorld,
                activeZone = config.startWorld.Value.startZone,
                firstSessionTimestamp = App.Clock.Now.Value
            };
        }

        private void FixedUpdate()
        {
            if (game == null || SwiftFramework.Core.App.Initialized == false)
            {
                return;
            }

            if (StageManager.ActiveStageLink.GetPath() != config.gameView.GetPath())
            {
                return;
            }

            if (game.SeasonEvent != null && (game.SeasonEvent.TimeTillStart == 0 && game.SeasonEvent.TimeTillEnd == 0) && state.activeWorld == config.currentEvent.Value.world)
            {
                OnSeasonEventFinished();
                SetActiveZone(config.startWorld.Value.startZone);
            }

            game.Update(Time.fixedDeltaTime);
        }

        public IBoosterManager GetCurrentBoosterManager()
        {
            return game.Boosters;
        }

        public IPromise OpenMap()
        {
            return StageManager.Load<IIldeWorldMap>(config.mapView).Then();
        }

        protected virtual void OnGameStageOpened() { }

        public IPromise OpenGameStage()
        {
            return StageManager.Load<GameView>(config.gameView).Then();
        }

        public (BigNumber amount, FundsSourceLink funds, long duration) GetOfflineRevenue(ILink link, bool ignoreActiveZone)
        {
            ZoneConfigLink zone = link as ZoneConfigLink;

            Zone zoneState = state.GetZone(zone);

            if ((ignoreActiveZone && state.activeZone == zone) || zoneState == null)
            {
                return (0, null, 0);
            }

            WorldConfig world = GetWorld(zone).Value;

            long duration = App.Clock.Now.Value - zoneState.lastRevenueEarnTimestamp;

            return (GetOfflineRevenueFromZone(zoneState), world.softCurrency, duration);
        }

        private WorldConfigLink GetWorld(ZoneConfigLink zone)
        {
            foreach (var world in config.worlds)
            {
                foreach (var otherZone in world.Value.zones)
                {
                    if (otherZone == zone)
                    {
                        return world;
                    }
                }
            }
            return null;
        }


        public (BigNumber amount, FundsSourceLink funds) GetWorldOfflineRevenue(ILink worldLink)
        {
            WorldConfigLink world = worldLink as WorldConfigLink;

            (BigNumber amount, FundsSourceLink funds) result = (0, world.Value.softCurrency);

            foreach (ZoneConfigLink zoneLink in world.Value.zones)
            {
                if (zoneLink == state.activeZone)
                {
                    continue;
                }
                Zone zoneState = state.GetZone(zoneLink);
                if (zoneState == null)
                {
                    continue;
                }
                result.amount += GetOfflineRevenueFromZone(zoneState);
            }
            return result;
        }

        private BigNumber GetOfflineRevenueFromZone(Zone zone)
        {
            BigNumber result = 0;
            if (zone.idleProductionSpeed.Value == 0)
            {
                result = new BigNumber(Mathf.FloorToInt(zone.idleProductionSpeedFloat * (App.Clock.Now.Value - zone.lastRevenueEarnTimestamp)));
            }
            else
            {
                result = zone.idleProductionSpeed * (App.Clock.Now.Value - zone.lastRevenueEarnTimestamp);
            }
            return BigInteger.Min(zone.workplace.resources.Value, result.Value);
        }

        public ILink GetActiveWorld()
        {
            return GetWorld(state.activeZone);
        }

        public ILink GetActiveZone()
        {
            return state.activeZone;
        }

        public T1 GetGameController<T1>() where T1 : class
        {
            return game as T1;
        }

        public void ClaimOfflineRevenue(ILink zoneLink, long multiplier)
        {
            var revenue = GetOfflineRevenue(zoneLink, false);
            if (revenue.amount > 0)
            {
                revenue.funds.Value.Add(revenue.amount * multiplier);
                Zone zone = state.GetZone(zoneLink as ZoneConfigLink);
                zone.lastRevenueEarnTimestamp = App.Clock.Now.Value;
                OnOfflineRevenueClaimed(zoneLink);
            }
        }

        public void ClaimWorldOfflineRevenue(ILink worldLink, long multiplier)
        {
            WorldConfigLink link = worldLink as WorldConfigLink;
            foreach (ZoneConfigLink zone in link.Value.zones)
            {
                ClaimOfflineRevenue(zone, multiplier);
            }
        }

        public long GetZoneRevenueMultiplier(ILink zoneLink)
        {
            long multiplier = 0;
            foreach (var booster in state.boosters.boosters)
            {
                if (booster.context == null)
                {
                    continue;
                }
                if (booster.context.GetPath() == zoneLink.GetPath())
                {
                    multiplier += booster.link.Value.multiplier;
                }
            }
            return multiplier;
        }

        public IPromise<long> TryShowCurrentZoneOfflineRevenue()
        {
            Promise<long> promise = Promise<long>.Create();

            ILink zone = GetActiveZone();

            var offlineRevenue = GetOfflineRevenue(zone, false);

            if (offlineRevenue.amount == 0)
            {
                promise.Resolve(0);
                return promise;
            }

            ZoneConfigLink zoneConfig = zone as ZoneConfigLink;

            if (offlineRevenue.duration < zoneConfig.Value.offlineRevenueTimeMin * 60)
            {
                promise.Resolve(0);
                return promise;
            }
            else
            {
                App.GetModule<IWindowsManager>().Show<OfflineRevenueWindow, (BigNumber amount, FundsSourceLink funds, long), bool>(offlineRevenue).Done(doubled =>
                {
                    if (doubled)
                    {
                        ClaimOfflineRevenue(zone, 2);
                        promise.Resolve(2);
                    }
                    else
                    {
                        ClaimOfflineRevenue(zone, 1);
                        promise.Resolve(1);
                    }
                });

            }

            return promise;
        }

        public ILink GetCurrentEvent()
        {
            return config.currentEvent;
        }

        public IPromise SetActiveZone(ILink zoneLink)
        {
            Promise promise = Promise.Create();

            if (game != null)
            {

                if (game.World.ActiveZone.Link.GetPath() != zoneLink.GetPath())
                {
                    Zone loadedZoneState = state.GetZone(game.World.ActiveZone.Link as ZoneConfigLink);

                    loadedZoneState.lastRevenueEarnTimestamp = App.Clock.Now.Value;
                }
                else
                {
                    return Promise.Resolved();
                }
            }

            ZoneConfigLink zone = zoneLink as ZoneConfigLink;

            state.activeWorld = config.worlds.Find(w => w.Value.zones.FindIndex(z => zone == z) != -1);
            if (state.activeWorld == null)
            {
                Debug.LogError($"World for zone {zoneLink.GetPath()} not found. Add the world with this zone to GameConfig!");
                state.activeWorld = config.startWorld;
            }
            state.SetActiveZone(zone);

            game = null;

            App.Views.ReturnEverythingToPool();

            CreateGame(config, state.activeWorld).Done(() =>
            {
                OnZoneChanged();

                promise.Resolve();
            });

            return promise;
        }

        public bool TrySetSeasonEventActive()
        {
            if (config.currentEvent == null || config.currentEvent.HasValue == false)
            {
                return false;
            }
            SetActiveZone(config.currentEvent.Value.world.Value.startZone);
            return true;
        }

        public bool IsPrestigeAvailableForCurrentZone()
        {
            return state.activeZone != null && state.activeZone.HasValue && state.activeZone.Value.prestige.levels.Length > 0;
        }

        public IPromise OpenSeasonPass()
        {
            seasonPass.TryGetCurrentSeason(out SeasonLink season);
            return StageManager.Load<ISeasonPassView>(season.Value.view).Then();
        }

        public S GetState<S>() where S : class
        {
            return state as S;
        }

        public IPromise ReloadActiveZone()
        {
            game = null;
            return SetActiveZone(state.activeZone);
        }
    }
}
