﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [PrewarmAsset]
    public class AbilitiesHubConfig : ScriptableObject
    {
        public AbilityLink totalSpeedAbility;
        public AbilityLink moveSpeedAbility;
        public AbilityLink upgradeDiscountAbility;

        public PerkLink upgradeDiscountPerk;
        public PerkLink moveSpeedPerk;
    }

    [System.Serializable]
    public class AbilitiesHubLink : LinkToScriptable<AbilitiesHubConfig>
    {

    }

}
