﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [PrewarmAsset]
    public class BoostersHubConfig : ScriptableObject
    {
        public BoosterTargetLink moveSpeedBooster;
    }

    [System.Serializable]
    public class BoostersHubLink : LinkToScriptable<BoostersHubConfig>
    {

    }
}
