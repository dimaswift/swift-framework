﻿using SwiftFramework.Core.Supervisors;
using SwiftFramework.IdleMiner.Configs;
using UnityEngine;
namespace SwiftFramework.Core
{
    public class ControllerConfig : ScriptableObject
    {
        public SupervisorTemplateLink supervisor;
        public AbilitiesHubLink abilitiesHub;
        public BoostersHubLink boostersHub;
    }

}
