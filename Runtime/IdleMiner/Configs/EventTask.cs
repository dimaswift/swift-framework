﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;

namespace SwiftFramework.IdleMiner.Configs
{
    [Serializable]
    public class EventTask
    {
        public EventTaskType type;
        public long target;
        public long index;
        public UnixTimestamp dueDate;
        public int seasonPassPoints;
        public List<RewardLink> rewards = new List<RewardLink>();
        public bool limitTime;

        public string GetLongDescription()
        {
            switch (type)
            {
                case EventTaskType.PuchaseFactory:
                    return App.Core.Local.GetText("#purchase_factory_long_message", index + 1);
                case EventTaskType.UpgradeFactory:
                    return App.Core.Local.GetText("#upgrade_factory_task_long_message", index + 1, target + 1);
                case EventTaskType.UpgradeZoneToMax:
                    return App.Core.Local.GetText("#upgrade_zone_to_max_task_long_message");
                case EventTaskType.FinishEventUntilDate:
                    return App.Core.Local.GetText("#finish_event_zone_until_date_long_message", dueDate.timestampSeconds.ToDurationString(App.Core.Local));
                default:
                    return null;
            }
        }

        public string GetShortDescription()
        {
            switch (type)
            {
                case EventTaskType.PuchaseFactory:
                    return App.Core.Local.GetText("#purchase_factory_short_message", index + 1);
                case EventTaskType.UpgradeFactory:
                    return App.Core.Local.GetText("#upgrade_factory_task_short_message", index + 1, target + 1);
                case EventTaskType.UpgradeZoneToMax:
                    return App.Core.Local.GetText("#upgrade_zone_to_max_task_short_message");
                case EventTaskType.FinishEventUntilDate:
                    return App.Core.Local.GetText("#finish_event_zone_until_date_short_message", dueDate.timestampSeconds.ToDurationString(App.Core.Local));
                default:
                    return null;
            }
        }
    }

    public enum EventTaskType
    {
        PuchaseFactory, UpgradeFactory, UpgradeZoneToMax, FinishEventUntilDate
    }
}
