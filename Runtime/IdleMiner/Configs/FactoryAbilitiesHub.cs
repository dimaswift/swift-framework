﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Factory/Abilities Hub")]
    public class FactoryAbilitiesHub : AbilitiesHubConfig
    {
        public PerkLink batchSizePerk;
        public PerkLink factoryUnlockPricePerk;

        public AbilityLink productionSpeedAbility;
        public AbilityLink[] workForceAmountAbilities;
        public AbilityLink[] batchSizeAbilities;
    }
}
    