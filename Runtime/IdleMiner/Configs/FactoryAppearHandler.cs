﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop.OfferSystem;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Offer Triggers/Purchase Factory")]
    public class FactoryAppearHandler : OfferAppearHandler
    {
        [SerializeField] private ZoneConfigLink zone = null;
        [SerializeField] private int factoryIndex = 0;
        [SerializeField] private Action action = Action.Purchase;
        private IIdleGameModule idleModule;

        public enum Action
        {
            Unlock, Purchase
        }

        protected override void OnInit(IShopManager shop)
        {
            idleModule = App.Core.GetModule<IIdleGameModule>();
            idleModule.OnZoneChanged += IdleModule_OnZoneChanged;
            IdleModule_OnZoneChanged();
        }

        private void IdleModule_OnZoneChanged()
        {
            IZone activeZone = idleModule.GetGameController<IGame>().World.ActiveZone;


            switch (action)
            {
                case Action.Unlock:

                    activeZone.OnFactoryUnlocked -= ActiveZone_OnFactoryUnlocked;
                    activeZone.OnFactoryUnlocked += ActiveZone_OnFactoryUnlocked;

                    break;
                case Action.Purchase:

                    activeZone.OnFactoryPurchased -= ActiveZone_OnFactoryPurchased;
                    activeZone.OnFactoryPurchased += ActiveZone_OnFactoryPurchased;

                    break;
            }

        }

        private bool IsZoneValid()
        {
            if (zone.HasValue == false)
            {
                return true;
            }
            return idleModule.GetActiveZone().GetPath() == zone.GetPath();
        }

        private void ActiveZone_OnFactoryUnlocked(IFactory factory)
        {
            if (IsZoneValid() && factory.OrderIndex == factoryIndex)
            {
                shouldBeOffered.SetValue(true);
            }
        }

        private void ActiveZone_OnFactoryPurchased(IFactory factory)
        {
            if (IsZoneValid() && factory.OrderIndex == factoryIndex)
            {
                shouldBeOffered.SetValue(true);
            }
        }
    }
}
