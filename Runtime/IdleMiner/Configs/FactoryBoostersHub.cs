﻿using SwiftFramework.Core.Boosters;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Factory/Boosters Hub")]
    public class FactoryBoostersHub : BoostersHubConfig
    {
        public BoosterTargetLink batchSizeMultiplier;
        public BoosterTargetLink forceMultiplier;
        public BoosterTargetLink workerProductionSpeedMultiplier;
    }
}
    