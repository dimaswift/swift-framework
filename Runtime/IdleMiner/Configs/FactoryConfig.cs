﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Views;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Factory")]
    public class FactoryConfig : ControllerConfig
    {
        public string factoryName = "#factory";

        public FactoryUpgradeSettings upgradeConfig;
        public ConfigPerLevel[] levels;

        public Generator generator;

        [Serializable]
        public class Generator
        {
            public Factory defaultState;
            public UnlockerLink defaultUnlocker;
            public PriceLink softPriceLink;
            public PriceLink hardPriceLink;
            [LinkFilter(typeof(IFactoryView))]
            public ViewLink defaultSharedView;
            [LinkFilter(typeof(ICustomFactoryView))]
            public List<ViewLink> customViews;
            public int amountToGenerate;
            public BigNumber baseCostSoft;
            public BigNumber baseCostSoftMultiplier;
            public BigNumber baseCostHard;
            public BigNumber baseCostHardMultiplier;
            public BigNumber baseIncome;
            public BigNumber baseIncomeMultiplier;
            public BigNumber baseUpgradeCostMultiplier;

        }

        [Serializable]
        public class ConfigPerLevel
        {
            public Factory defaultState;
            public BigNumber overrideBaseUpgradeCost;
            public bool canBeBoughtForHardCurrency = true;
            public PriceLink priceSoftCurrency;
            public PriceLink priceHardCurrency;
            public UnlockerLink unlockerConfig;
            [LinkFilter(typeof(IFactoryView))]
            public ViewLink sharedView;
            [LinkFilter(typeof(ICustomFactoryView))]
            public ViewLink customView;
            public float productionTimeFactor = 1;
        }
    }

    [Serializable]
    [LinkFolder(Folders.Configs + "/Factories")]
    public class FactoryConfigLink : LinkTo<FactoryConfig>
    {

    }
}
    