﻿using SwiftFramework.Core.SharedData.Upgrades;
using System;

namespace SwiftFramework.IdleMiner.Configs
{
    [Serializable]
    public class FactoryUpgradeSettings : UpgradeSettings
    {
        public float productionTimeMultiplier = 1;
        public float workerTransportationTimeMultiplier = 1;
        public float workerForceMultiplier = 1;
        public int batchSizeIncrement = 1;
    }
}
