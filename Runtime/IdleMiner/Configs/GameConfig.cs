﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.IdleMiner.Views;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Game")]
    public class GameConfig : ScriptableObject
    {
        public BoosterManagerConfigLink boostersConfig;
        public SeasonEventLink currentEvent;
        public BoosterTargetLink revenueBooster;
        public WorldConfigLink startWorld;
        public ViewLink mapView;
        public BoosterTargetLink globalRevenueBooster;
        public FundsSourceLink hardFunds;
        public SoundsConfigLink soundConfig;
        public List<WorldConfigLink> worlds = new List<WorldConfigLink>();
        [LinkFilter(typeof(GameView))] public ViewLink gameView;
    }

    [LinkFolder(Folders.Configs)]
    [System.Serializable]
    public class GameConfigLink : LinkTo<GameConfig>
    {

    }
}
