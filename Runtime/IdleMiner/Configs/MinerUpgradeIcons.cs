﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Upgrades;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Upgrade Icons")]
    public class MinerUpgradeIcons : UpgradeIcons
    {
        public SpriteLink speedIcon;
        public SpriteLink capacityIcon;
        public SpriteLink loadSpeedIcon;
        public SpriteLink oneBundleIcon;
        public SpriteLink totalProductionIcon;
        public SpriteLink totalTransferIcon;
        public SpriteLink incomeIcon;
    }
}
