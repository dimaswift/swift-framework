﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [PrewarmAsset]
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Season Event")]
    public class SeasonEvent : ScriptableObject, ITimeLimit
    {
        public UnixTimestamp startTime;
        public UnixTimestamp endTime;
        public string nameKey;
        public string descKey;
        public WorldConfigLink world;
        public List<EventTask> tasks = new List<EventTask>();
        public SpriteLink icon;

        public long TimeTillStart => Math.Max(0, startTime.timestampSeconds - App.Core.Clock.Now.Value);

        public long TimeTillEnd => Math.Max(0, endTime.timestampSeconds - App.Core.Clock.Now.Value);
    }

    [Serializable]
    [LinkFolder(Folders.Configs + "/Events")]
    public class SeasonEventLink : LinkTo<SeasonEvent>
    {

    }
}
