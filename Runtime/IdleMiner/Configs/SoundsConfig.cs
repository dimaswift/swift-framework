﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [PrewarmAsset]
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Sounds")]
    public class SoundsConfig : ScriptableObject
    {
        public AudioClipLink backgroundMusic;
        public float musicVolume;

        [Header("SFX")]
        public AudioClipLink popUpSfx;
        public AudioClipLink openWindowSfx;
        public AudioClipLink boosterActivatedSfx;
        public AudioClipLink boosterAddedToInventorySfx;
        public AudioClipLink producedWorkforceSfx;
        public AudioClipLink activateManagerAbilitySfx;
        public AudioClipLink buyRandomBoosterSfx;
        public AudioClipLink startedWorkSfx;
        public AudioClipLink gotSoftCurrency;
        public AudioClipLink factoryPurchased;
        public AudioClipLink onTransporterActivated;
    }

    [LinkFolder(Folders.Configs)]
    [System.Serializable]
    public class SoundsConfigLink : LinkTo<SoundsConfig>
    {

    }
}