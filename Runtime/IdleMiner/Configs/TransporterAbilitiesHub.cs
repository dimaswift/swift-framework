﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Transporter/Abilities Hub")]
    public class TransporterAbilitiesHub : AbilitiesHubConfig
    {
        public AbilityLink loadSpeedAbility;
        public AbilityLink capacityAbility;
    }
}
