﻿using SwiftFramework.Core.Boosters;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Transporter/Boosters Hub")]
    public class TransporterBoostersHub : BoostersHubConfig
    {
        public BoosterTargetLink loadingSpeedBooster;
    }
}
