﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Views;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Transporter")]
    public class TransporterConfig : ControllerConfig
    {
        public float travelTimeBetweenFactories = 2;
        public float travelTimeToFirstFactory = 0.5f;
        public float loadTime = .25f;
        public float minLoadTime = 1f;
        public float maxLoadTime = 3f;
        public float travelTime = 3;
        public bool useCommonTravelTime = true;
        public float travelToWorkplaceTime = 0;
        public float unloadingTime;
        public TransporterUpgradeSettings upgradeSettings;
        [LinkFilter(typeof(ITransporterView))]
        public ViewLink view;
        public Transporter defaultState;
    }

    [Serializable]
    [LinkFolder(Folders.Configs + "/Transporters")]
    public class TransporterConfigLink : LinkTo<TransporterConfig>
    {

    }
}
