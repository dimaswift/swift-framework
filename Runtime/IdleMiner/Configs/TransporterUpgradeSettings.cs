﻿using SwiftFramework.Core.SharedData.Upgrades;
using System;

namespace SwiftFramework.IdleMiner.Configs
{
    [Serializable]
    public class TransporterUpgradeSettings : UpgradeSettings
    {
        public float capacityMultiplier;
        public float moveSpeedMultiplier;
        public float loadingRateMultiplier;
    }
}
