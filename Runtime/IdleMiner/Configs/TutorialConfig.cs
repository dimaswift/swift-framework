﻿using SwiftFramework.Core;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Tutorial/TutorialConfig")]
    public class TutorialConfig : ScriptableObject
    {
        [Header("Steps")]
        public TutorialStepLink buyFactory;
        public TutorialStepLink produceWorkforce;
        public TutorialStepLink useTransporter;
        public TutorialStepLink work;
        public TutorialStepLink upgradeFactory;
        public TutorialStepLink upgradeFactoryUI;
        public TutorialStepLink buySupervisor;
        public TutorialStepLink buySupervisorUI;
        public TutorialStepLink wait;
        public TutorialStepLink waitForShop;
        public TutorialStepLink freeShopItemHud;
        public TutorialStepLink freeShopItemOpenShop;
        public TutorialStepLink freeShopItemTake;
        public TutorialStepLink useBooster;
        public TutorialStepLink complete;

    }

    [LinkFolder(Folders.Configs)]
    [System.Serializable]
    public class TutorialConfigLink : LinkTo<TutorialConfig>
    {

    }

}

