﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Views;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [PrewarmAsset]
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Tutorial/TutorialStep")]
    public class TutorialStep : ScriptableObject
    {
        public int order;
        public ViewLink view;
    }

    [System.Serializable]
    [FlatHierarchy]
    public class TutorialStepLink : LinkTo<TutorialStep>
    {

    }

}