﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop.OfferSystem;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Offer Triggers/Offer Triggers/Unlock Zone")]
    public class UnlockZoneAppearHandler : OfferAppearHandler
    {
        [SerializeField] private ZoneConfigLink zone = null;

        protected override void OnInit(IShopManager shop)
        {
            IIdleGameModule idleModule = App.Core.GetModule<IIdleGameModule>();
            idleModule.OnZoneUnlocked -= OnZoneUnlocked;
            idleModule.OnZoneUnlocked += OnZoneUnlocked;
            shouldBeOffered.SetValue(idleModule.IsZoneUnlocked(zone));
        }

        private void OnZoneUnlocked(ILink zoneLink)
        {
            if (zoneLink.GetPath() == zone.GetPath())
            {
                shouldBeOffered.SetValue(true);
            }
        }
    }
}
