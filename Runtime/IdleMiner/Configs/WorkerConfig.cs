﻿using SwiftFramework.Core;
using System;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    public abstract class WorkerConfig : ScriptableObject
    {
        
    }

    [Serializable]
    [LinkFolder(Folders.Configs + "/Workers")]
    public class WorkerLink : LinkTo<WorkerConfig>
    {

    }
}
 