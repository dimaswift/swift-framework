﻿using SwiftFramework.Core.Boosters;

namespace SwiftFramework.IdleMiner.Configs
{
    public class WorkplaceBoostersHub : BoostersHubConfig
    {
        public BoosterTargetLink revenueMultiplier;
        public BoosterTargetLink autoReload;
    }
}
