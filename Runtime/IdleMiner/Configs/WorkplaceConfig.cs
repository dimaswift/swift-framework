﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Views;
using System;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Workplace")]
    public class WorkplaceConfig : ControllerConfig
    {
        [LinkFilter(typeof(IWorkplaceView))] public ViewLink view;
        public WorkplaceUpgradeSettings upgradeSettings;
        public Workplace defaultState;
        public WorkplaceSkinLink[] skins;
        public float batchProcessTime = 3;
        [Header("Multiplier used to multiply resources with each next level.")]
        public float resourcesProgressMultiplier;
        public float minWorkTime = 1.5f;
        public float maxWorkTime = 1.5f;
    }

    [Serializable]
    [LinkFolder(Folders.Configs + "/Workplaces")]
    public class WorkplaceConfigLink : LinkTo<WorkplaceConfig>
    {

    }
}
