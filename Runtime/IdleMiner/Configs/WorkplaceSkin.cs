﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Workplace Skin")]
    public class WorkplaceSkin : ScriptableObject
    {

    }

    [System.Serializable]
    [LinkFolder(Folders.Configs)]
    public class WorkplaceSkinLink : LinkTo<WorkplaceSkin>
    {

    }
}
