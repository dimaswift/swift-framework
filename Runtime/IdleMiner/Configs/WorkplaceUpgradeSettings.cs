﻿using SwiftFramework.Core.SharedData.Upgrades;
using System;

namespace SwiftFramework.IdleMiner.Configs
{
    [Serializable]
    public class WorkplaceUpgradeSettings : UpgradeSettings
    {
        public float bandwidthMultiplier;
        public int workersAmountIncreaseStep;
        public float moveSpeedMultiplier;
        public float productionSpeedMultiplier;
    }
}
