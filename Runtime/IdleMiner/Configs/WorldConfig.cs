﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Configs
{
    [PrewarmAsset]
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/World")]
    public class WorldConfig : ScriptableObject
    {
        public FundsSourceLink softCurrency;
        public ZoneConfigLink startZone;
        public TutorialConfigLink tutorialConfigLink;
        public List<ZoneConfigLink> zones = new List<ZoneConfigLink>();
    }


    [Serializable]
    [LinkFolder(Folders.Configs + "/Worlds")]
    public class WorldConfigLink : LinkTo<WorldConfig>
    {

    }
}
