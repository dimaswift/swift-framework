﻿using SwiftFramework.Core;
using SwiftFramework.Core.Prestige;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Views;
using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace SwiftFramework.IdleMiner.Configs
{
    [PrewarmAsset]
    [CreateAssetMenu(menuName = "SwiftFramework/IdleMiner/Configs/Zone")]
    public class ZoneConfig : ScriptableObject
    {
        public SpriteLink icon;
        public ZonePrestigeState defaultPrestige;
        public PriceLink unlockPrice;
        public PrestigeSettings prestige;
        public TransporterConfigLink transporter;
        public WorkplaceConfigLink workplace;
        public FactoryConfigLink factory;
        public ZoneConfigLink parentZone;

        [LinkFilter(typeof(IZoneView))]
        public ViewLink view;
        [LinkFilter(typeof(IWorkerTransportView))]
        public ViewLink factoryWorkerView;
        [LinkFilter(typeof(IWorkerView))]
        [FormerlySerializedAs("workerView")]
        public ViewLink workplaceWorkerView;

        [Header("Offline Revenue Settings (In Minutes)")]
        public float offlineRevenueTimeMax = 60 * 60 * 24;
        public float offlineRevenueTimeMin = 60;

    }

    [Serializable]
    [LinkFolder(Folders.Configs + "/Zones")]
    public class ZoneConfigLink : LinkTo<ZoneConfig>
    {

    }


}
