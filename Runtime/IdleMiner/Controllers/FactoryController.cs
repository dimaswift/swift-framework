﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Controllers
{
    public class FactoryController : SupervisedController<Factory, FactoryConfig, FactoryBoostersHub, FactoryAbilitiesHub>, IFactory
    {
        public event Action<BigNumber> OnWorkForceTaken = a => { };
        public event Action OnRanOutOfWorkForce = () => { };
        public event Action<IWorkerTransport> OnWorkerProduced = w => { };
        public event Action OnFactoryPurchased = () => { };

        public IStatefulEvent<BigNumber> WorkForce => accumulatedWorkForce;
        public IStatefulEvent<FactoryStatus> CurrentState => currentState;
        public IStatefulEvent<PurchaseStatus> CurrentPurchaseStatus => purchaseStatus;

        public override string SupervisorsTitle => App.Core.Local.GetText("factory_supervisor", OrderIndex);
        public override ViewLink View => Config.levels[OrderIndex].sharedView;
        public ViewLink CustomView => Config.levels[OrderIndex].customView;
        public ViewLink WorkerTransportView => Zone.Config.factoryWorkerView;
        public int ActiveWorkersAmount => producedWorkers.Count;

        public override string Title => App.Core.Local.GetText("factory_with_index", OrderIndex + 1);

        public override IUpgradeController UpgradeController => factoryUpgradeController;
        public string FactoryName => Config.factoryName;
        public override bool ReadyToWork => Supervisors.IsAssigned.Value;
        public BigNumber OneLevelUpgradePrice => factoryUpgradeController.GetUpgradePrice(1);
        public long Level => State.level;
        public int WorkersAmount => producedWorkers.Count;
        public long OrderIndex { get; private set; }
        public float WorkerTransportTime => State.workerTransportTime;
        public float CurrentProductionTimeNormalized => producingTimer / ProductionTime;
        public IUnlocker Unlocker { get; private set; }

        private float ProductionTime => State.productionTime * Config.levels[OrderIndex].productionTimeFactor;

        public PriceLink PriceSoftCurrency => Config.levels[OrderIndex].priceSoftCurrency;
        public PriceLink PriceHardCurrency => Config.levels[OrderIndex].priceHardCurrency;
        public bool CanBeBoughtForHardCurrnecy => Config.levels[OrderIndex].canBeBoughtForHardCurrency;

        private readonly FactoryUpgradeController factoryUpgradeController;

        private readonly StatefulEvent<BigNumber> accumulatedWorkForce = new StatefulEvent<BigNumber>(0);
        private readonly StatefulEvent<FactoryStatus> currentState = new StatefulEvent<FactoryStatus>(FactoryStatus.Idle);
        private readonly SimplePool<WorkerTransport> workersPool;
        private readonly List<WorkerTransport> producedWorkers = new List<WorkerTransport>();
        private readonly StatefulEvent<PurchaseStatus> purchaseStatus = new StatefulEvent<PurchaseStatus>(PurchaseStatus.Purchased);

        private float producingTimer;
        private long batchSizeMultiplier = 1;
        private long workForceMultiplier = 1;
        private float moveSpeedMultiplier = 1;
        private long workerProductionSpeedMultiplier = 1;
        private Promise<IUnlocker> unlockerPromise;


        public FactoryController(SharedSupervisorsManager sharedSupervisors, IFactory previousFactory, Factory state, FactoryConfig config, IZone zone, int orderIndex)
            : base(sharedSupervisors, state.subordinate, zone.State.supervisorsPool, state, config, zone)
        {
            OrderIndex = orderIndex;
            workersPool = new SimplePool<WorkerTransport>(CreateWorkerForPool);
            accumulatedWorkForce.SetValue(state.accumulatedWorkForce.Value);
            accumulatedWorkForce.OnValueChanged += v => state.accumulatedWorkForce = v;

            factoryUpgradeController = new FactoryUpgradeController(state, config, config.upgradeConfig, GetUpgradeDiscount);
            Zone.Boosters.OnMultipliersUpdated += RecalculateMultipliers;
            Supervisors.OnAbilitiesChanged += RecalculateMultipliers;
            Supervisors.IsAssigned.OnValueChanged += IsAssigned_OnValueChanged;

            if (previousFactory != null)
            {
                previousFactory.CurrentPurchaseStatus.OnValueChanged += OnPreviousFactoryPurchaseStatusChanged;
            }

            DeterminePurchaseStatus(previousFactory);

            RecalculateMultipliers();
        }

        private void IsAssigned_OnValueChanged(bool value)
        {
            UpdateProductionSpeed();
        }

        private void OnPreviousFactoryPurchaseStatusChanged(PurchaseStatus status)
        {
            if (status == PurchaseStatus.Purchased)
            {
                purchaseStatus.SetValue(PurchaseStatus.Available);
            }
        }

        private void DeterminePurchaseStatus(IFactory previousFactory)
        {
            if (previousFactory == null && !State.purchased)
            {
                purchaseStatus.SetValue(PurchaseStatus.Available);
            }
            else if (previousFactory == null || State.purchased)
            {
                purchaseStatus.SetValue(PurchaseStatus.Purchased);
            }
            else if (previousFactory != null)
            {
                if (previousFactory.CurrentPurchaseStatus.Value == PurchaseStatus.Purchased)
                {
                    purchaseStatus.SetValue(PurchaseStatus.Available);
                }
                else
                {
                    purchaseStatus.SetValue(PurchaseStatus.Locked);
                }
            }
        }


        public override IPromise Init()
        {
            return Promise.All(GetUnlocker().Then(), base.Init());
        }

        private void RecalculateMultipliers()
        {
            workForceMultiplier = Zone.Boosters.GetTotalMultiplier(boosters.forceMultiplier);
            moveSpeedMultiplier = Zone.Boosters.GetTotalMultiplier(boosters.moveSpeedBooster);
            batchSizeMultiplier = Zone.Boosters.GetTotalMultiplier(boosters.batchSizeMultiplier);
            workerProductionSpeedMultiplier = Zone.Boosters.GetTotalMultiplier(boosters.workerProductionSpeedMultiplier);

            if (Supervisors.IsAbilityActive(abilities.moveSpeedAbility, out AbilityState moveSpeedAbility))
            {
                moveSpeedMultiplier *= moveSpeedAbility.power;
            }

            if (Supervisors.IsAbilityActive(abilities.productionSpeedAbility, out AbilityState productionSpeedAbility))
            {
                workerProductionSpeedMultiplier *= productionSpeedAbility.power;
            }

            foreach (AbilityLink ability in abilities.workForceAmountAbilities)
            {
                if (Supervisors.IsAbilityActive(ability, out AbilityState workForceAmountAbility))
                {
                    workForceMultiplier *= workForceAmountAbility.power;
                }
            }

            if (Supervisors.IsAbilityActive(abilities.totalSpeedAbility, out AbilityState totalSpeedAbility))
            {
                workerProductionSpeedMultiplier *= totalSpeedAbility.power;
                moveSpeedMultiplier *= totalSpeedAbility.power;
            }


            if (Supervisors.IsPerkActive(abilities.moveSpeedPerk, Zone.Link as Link))
            {
                moveSpeedMultiplier *= abilities.moveSpeedPerk.Value.multiplier;
            }

            foreach (AbilityLink a in abilities.batchSizeAbilities)
            {
                if (Supervisors.IsAbilityActive(a, out AbilityState batchSizeAbility))
                {
                    batchSizeMultiplier += batchSizeAbility.power;
                }
            }

            if (Supervisors.IsPerkActive(abilities.batchSizePerk, Zone.Link as Link))
            {
                batchSizeMultiplier = Mathf.RoundToInt(batchSizeMultiplier + abilities.batchSizePerk.Value.multiplier);
            }

            UpdateProductionSpeed();
        }

        public override void SyncState()
        {
            accumulatedWorkForce.SetValue(State.accumulatedWorkForce.Value);
        }


        protected override void OnActivate()
        {
            if (currentState.Value == FactoryStatus.Idle)
            {
                StartProducing();
            }
        }

        public void DeliverWorker(WorkerTransport worker)
        {
            BigNumber power = worker.Power;
            power *= workForceMultiplier;
            SetAccumulatedWorkForce(accumulatedWorkForce.Value + power);
        }

        public void StartProducing()
        {
            producingTimer = 0;
            currentState.SetValue(FactoryStatus.Producing);
        }

        public BigNumber TakeWorkForce(BigNumber amountToTake)
        {
            if (amountToTake <= 0)
            {
                OnRanOutOfWorkForce();
                return 0;
            }

            SetAccumulatedWorkForce(accumulatedWorkForce.Value - amountToTake);
            OnWorkForceTaken(amountToTake);

            if (accumulatedWorkForce.Value <= 0)
            {
                SetAccumulatedWorkForce(0);
                OnRanOutOfWorkForce();
            }
            return amountToTake;
        }

        private void SetAccumulatedWorkForce(BigNumber value)
        {
            base.State.accumulatedWorkForce.Value = value.Value;
            accumulatedWorkForce.SetValue(value);
        }

        private WorkerTransport CreateWorkerForPool()
        {
            WorkerTransport worker = new WorkerTransport(this);
            return worker;
        }

        public override void Update(float delta)
        {
            for (int i = producedWorkers.Count - 1; i >= 0; i--)
            {
                producedWorkers[i].Process(delta * moveSpeedMultiplier, out bool finished);
                if (finished)
                {
                    producedWorkers.RemoveAt(i);
                }
            }
            if (currentState.Value == FactoryStatus.Idle)
            {
                if (ReadyToWork)
                {
                    StartProducing();
                }
            }
            else if (currentState.Value == FactoryStatus.Producing)
            {
                producingTimer += delta * workerProductionSpeedMultiplier;
                if (producingTimer >= ProductionTime)
                {
                    var batchSize = State.batchSize * batchSizeMultiplier;
                    float transportDelayPerWorker = (State.workerTransportTime / 2) / batchSize;
                    for (long i = 0; i < batchSize; i++)
                    {
                        WorkerTransport worker = workersPool.Take();
                        worker.BeginWork(i * (transportDelayPerWorker), State.incomePerWorker.Value, State.workerTransportTime, producedWorkers.Count);
                        producedWorkers.Add(worker);
                        OnWorkerProduced(worker);
                    }

                    if (ReadyToWork)
                    {
                        producingTimer = 0;
                        StartProducing();
                    }
                    else
                    {
                        currentState.SetValue(FactoryStatus.Idle);
                    }
                }
            }
        }


        protected override void OnBoost()
        {

        }

        public IPromise<IUnlocker> GetUnlocker()
        {
            if (unlockerPromise != null)
            {
                return unlockerPromise;
            }

            unlockerPromise = Promise<IUnlocker>.Create();

            Config.levels[OrderIndex].unlockerConfig.Load(unlockerCfg =>
            {
                Unlocker = new Unlocker(Link.Create<FactoryConfigLink>("factory_" + OrderIndex), unlockerCfg, GetFactoryUnlockDiscount);
                unlockerPromise.Resolve(Unlocker);
            },
            e => unlockerPromise.Reject(e));

            return unlockerPromise;
        }

        private float GetFactoryUnlockDiscount()
        {
            foreach (IFactory factory in Zone.GetFactories())
            {
                if (factory.Supervisors.IsPerkActive(abilities.factoryUnlockPricePerk, Zone.Link as Link))
                {
                    return abilities.factoryUnlockPricePerk.Value.multiplier;
                }
            }
            return 0;
        }

        public float GetPurchaseDiscount() => GetFactoryUnlockDiscount();

        public IPromise<bool> Purchase(bool payWithSoft)
        {
            Promise<bool> promise = Promise<bool>.Create();

            if(payWithSoft)
            {
                PriceSoftCurrency.Pay().Done(success =>
                {
                    OnPurchase(success);
                    promise.Resolve(success);
                });
            }
            else
            {
                PriceHardCurrency.Pay().Done(success =>
                {
                    OnPurchase(success);
                    promise.Resolve(success);
                });

            }
         
            return promise;
        }

        private void OnPurchase(bool success)
        {
            if (success)
            {
                State.purchased = true;
                purchaseStatus.SetValue(PurchaseStatus.Purchased);
                OnFactoryPurchased();
            }
        
        }
        public void Dispose()
        {
            workersPool.Dispose();
        }

        public override void UpdateProductionSpeed()
        {
            if (Supervisors.IsAssigned.Value == false)
            {
                ProductionSpeed.Set(0, 0);
                return;
            }

            BigNumber amountToProduce = State.batchSize * batchSizeMultiplier * State.incomePerWorker.Value * workForceMultiplier;
            float deliveryTime = (ProductionTime / workerProductionSpeedMultiplier) + (State.workerTransportTime / moveSpeedMultiplier);
            BigNumber newProductionSpeed = amountToProduce.Value.DivideByFloat(deliveryTime);

            if (amountToProduce.FitsInFloat && newProductionSpeed.FitsInFloat)
            {
                ProductionSpeed.Set(newProductionSpeed, (float)amountToProduce.Value / deliveryTime);
            }
            else
            {
                ProductionSpeed.Set(newProductionSpeed, 0);
            }
        }
    }
}