﻿using System.Collections.Generic;
using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Configs;
using System;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Controllers
{
    public class FactoryUpgradeController : UpgradeController<Factory, FactoryUpgradeSettings>
    {
        protected readonly FactoryConfig config;
        private readonly Func<float> discountHandler;
        public FactoryUpgradeController(Factory factory, FactoryConfig config, FactoryUpgradeSettings upgradeSettings, Func<float> discountHandler) : base(upgradeSettings, factory, discountHandler)
        {
            this.config = config;
            this.discountHandler = discountHandler;
        }

        protected override void OnLevelUp(Factory factory, long count, bool isCopy = false)
        {
            for (long i = 0; i < count; i++)
            {
                factory.level++;
                foreach (var l in Settings.boostLevels)
                {
                    if(factory.level == l.level - 1)
                    {
                        factory.batchSize = factory.batchSize + Settings.batchSizeIncrement;
                        factory.productionTime = factory.productionTime * Settings.productionTimeMultiplier;
                        factory.workerTransportTime = factory.workerTransportTime * Settings.workerTransportationTimeMultiplier;
                        if(isCopy == false)
                        {
                            l.reward.AddReward();
                        }
                      
                        break;
                    }
                }
              
                factory.incomePerWorker = factory.incomePerWorker.Value.MultiplyByFloat(Settings.workerForceMultiplier);
            }
        }

        public override BigNumber GetUpgradePrice(long count = 1)
        {
            BigNumber cost = 0;

            count = AdjustCountToLimit(count);

            for (long i = 0; i < count; i++)
            {
                cost += GetCertainLevelCost(state, i);
            }

            return cost.Value - cost.Value.MultiplyByFloat(discountHandler());
        }

        protected override BigNumber GetCertainLevelCost(IUpgradableState upgradable, long count)
        {
            BigNumber multiplier = UpgradeSettings.upgradeCostMultiplier.BigNumberPow(upgradable.Level + count + 1);

            if (multiplier > 1000)
            {
                return GetBaseUpgradeCost().Value * multiplier.Value;
            }
            else
            {
                return GetBaseUpgradeCost().Value.MultiplyByFloat(Mathf.Pow(UpgradeSettings.upgradeCostMultiplier, upgradable.Level + count + 1));
            }
        }

        protected override BigNumber GetBaseUpgradeCost()
        {
            return config.levels[state.id].overrideBaseUpgradeCost;
        }

        protected override IEnumerable<ValueDelta> GetUpgradedValues(Factory upgradable, Factory upgradedCopy, long count)
        {
            MinerUpgradeIcons upgradeIcons = Settings.upgradeIcons.Value as MinerUpgradeIcons;
            bool isMaxLevel = IsMaxLevel();
            yield return new ValueDelta()
            {
                baseValue = new BigNumber(upgradable.batchSize),
                upgradeValue = new BigNumber(upgradedCopy.batchSize),
                reachedMaxLevel = isMaxLevel,
                name = "#totalbundles",
                icon = upgradeIcons.totalTransferIcon
            };

            yield return new ValueDelta()
            {
                baseValue = upgradable.incomePerWorker.Value,
                upgradeValue = upgradedCopy.incomePerWorker.Value,
                reachedMaxLevel = isMaxLevel,
                name = "#onebundle",
                icon = upgradeIcons.oneBundleIcon
            };

            yield return new ValueDelta()
            {
                baseValue = upgradable.incomePerWorker * upgradable.batchSize,
                upgradeValue = upgradedCopy.incomePerWorker * upgradedCopy.batchSize,
                reachedMaxLevel = isMaxLevel,
                name = "#incomelabel",
                icon = upgradeIcons.incomeIcon
            };

            yield return new ValueDelta()
            {
                baseValue = (upgradable.incomePerWorker.Value * upgradable.batchSize).DivideByFloat(upgradable.productionTime),
                upgradeValue = (upgradedCopy.incomePerWorker.Value * upgradedCopy.batchSize).DivideByFloat(upgradedCopy.productionTime),
                reachedMaxLevel = isMaxLevel,
                name = "#makingspeed",
                postFix = "/s",
                icon = upgradeIcons.loadSpeedIcon
            };

            yield return new ValueDelta()
            {
                baseFloatValue = upgradable.productionTime,
                upgradeByFloatValue = upgradedCopy.productionTime,
                reachedMaxLevel = isMaxLevel,
                name = "#production_time",
                postFix = "sec",
                icon = upgradeIcons.totalProductionIcon
            };
        }
    }
}

