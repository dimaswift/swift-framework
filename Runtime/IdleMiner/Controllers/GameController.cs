﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Controllers;
using SwiftFramework.IdleMiner.Model;
using System;
using System.Collections.Generic;

namespace SwiftFramework.IdleMiner
{
    public abstract class GameController : IGame
    {
        public ISharedSupervisorsManager SharedSupervisors => sharedSupervisors;

        public IBoosterManager Boosters { get; private set; }

        public IClock GlobalClock { get; private set; }

        public IWorld World { get; private set; }

        public ITutorialController Tutorial { get; private set; }

        public ISeasonEvent SeasonEvent { get; private set; }

        public ISoundController Sound { get; private set; }

        public SeasonEventLink CurrentEventLink => Config.currentEvent;

        public event Action OnGameCreated = () => { };

        public event Action<BigNumber> OnRevenueGenerated = r => { };

        public FracturedBigNumber ProductionSpeed { get; } = new FracturedBigNumber(FracturedBigNumber.PRODUCTION_SPEED);

        public GameConfig Config { get; private set; }

        public long LastSession => state.lastSessionTimestamp;

        protected GameState state;

        private float boosterUpdateTimer;

        private readonly Dictionary<ILink, IPrestigeController> prestigeControllers = new Dictionary<ILink, IPrestigeController>();

        protected SharedSupervisorsManager sharedSupervisors;

        private bool isRunning;

        private ISeasonPass seasonPass;

        public void Init(GameConfig config, IClock clock, ISeasonPass seasonPass)
        {
            this.seasonPass = seasonPass;
            GlobalClock = clock;
            Config = config;
        }

        protected GameController() { }

        public static IPromise<T> Create<T>(GameConfig config, GameState state, IClock clock, ISeasonPass seasonPass, WorldConfigLink world) where T : GameController, new()
        {
            Promise<T> initPromise = Promise<T>.Create();

            T game = new T();

            game.Init(config, clock, seasonPass);

            game.InitWithState(world, state).Then(() =>
            {
                initPromise.Resolve(game as T);
            })
            .Catch(e =>
            {
                initPromise.Reject(e);
            });

            return initPromise;
        }

        protected abstract IPrestigeController CreatePrestigeController(ILink zone);

        public IPrestigeController GetZonePrestige(ILink zoneLink)
        {
            if (prestigeControllers.TryGetValue(zoneLink, out IPrestigeController controller))
            {
                return controller;
            }

            controller = CreatePrestigeController(zoneLink);

            controller.OnPrestigeChanged += () => OnPrestigeChanged(zoneLink);

            prestigeControllers.Add(zoneLink, controller);

            return controller;
        }

        private void OnPrestigeChanged(ILink zoneLink)
        {
            state.activeZone = zoneLink as ZoneConfigLink;

            foreach (var supervisor in sharedSupervisors.GetAllSupervisors())
            {
                if (supervisor.assginedZone == null)
                {
                    continue;
                }
                if (supervisor.assginedZone.GetPath() == zoneLink.GetPath())
                {
                    sharedSupervisors.Unassign(supervisor.supervisor);
                }
            }
        }

        public int GetActivetedFactoriesAmount()
        {
            return World.ActiveZone.FactoryCount;
        }

        private IPromise InitWithState(WorldConfigLink worldLink, GameState state)
        {
            Promise promise = Promise.Create();

            this.state = state;

            sharedSupervisors = new SharedSupervisorsManager(state.sharedSupervisors, GlobalClock);

            Config.boostersConfig.Load(config =>
            {
                Boosters = new BoosterManager(state.boosters, config, GlobalClock);

                CreateSeasonEvent(Config.currentEvent).Done(e =>
                {
                    SeasonEvent = e;

                    WorldController.Create(Boosters, this, state, CreateWorkplace, CreateFactory, CreateTransporter, worldLink).Done(world =>
                    {
                        World = world;

                        CreateTutorial(world.Config.tutorialConfigLink, state.tutorial).Done(tutorial =>
                        {
                            Tutorial = tutorial;
                            CreateSoundController(Config.soundConfig).Done(sound =>
                            {
                                Sound = sound;

                                World.ActiveZone.CreateProductionLoop().Done(() =>
                                {
                                    UpdateProductionSpeed();
                                    World.ActiveZone.ProductionSpeed.OnValueChanged += UpdateProductionSpeed;
                                    World.ActiveZone.OnCreditsProduced += OnCreditsProduced;
                                    promise.Resolve();

                                    if (SeasonEvent != null && Config.currentEvent != null && Config.currentEvent.HasValue && Config.currentEvent.Value.world == worldLink)
                                    {
                                        SeasonEvent.ListenToWorldEvents(world);
                                    }

                                    OnGameCreated();
                                });
                            });
                        });
                    });
                });
            });

            return promise;
        }

        protected abstract IPromise<IWorkplace> CreateWorkplace(WorkplaceConfigLink config, Workplace state, IZone zone);

        protected abstract IPromise<ITutorialController> CreateTutorial(TutorialConfigLink configLink, Tutorial state);

        protected abstract IPromise<ISoundController> CreateSoundController(SoundsConfigLink configLink);

        protected virtual IPromise<ISeasonEvent> CreateSeasonEvent(SeasonEventLink eventLink)
        {
            Promise<ISeasonEvent> promise = Promise<ISeasonEvent>.Create();

            if (eventLink == null || eventLink.HasValue == false)
            {
                promise.Resolve(null);
                return promise;
            }

            promise.Resolve(new SeasonEventController(eventLink, seasonPass));

            return promise;
        }

        protected virtual IPromise<IFactory> CreateFactory(IFactory previousFactory, Factory state, FactoryConfigLink config, IZone zone, int orderIndex)
        {
            Promise<IFactory> promise = Promise<IFactory>.Create();
            config.Load(cfg =>
            {
                FactoryController factory = new FactoryController(sharedSupervisors, previousFactory, state, cfg, zone, orderIndex);
                factory.Init().Done(() => promise.Resolve(factory));
            });
            return promise;
        }

        protected abstract IPromise<ITransporter> CreateTransporter(Transporter transporter, TransporterConfigLink config, IZone zone);

        private void UpdateProductionSpeed()
        {
            if (World == null)
            {
                return;
            }
            ProductionSpeed.Set(World.ActiveZone.ProductionSpeed.Value.Value * Boosters.GetTotalMultiplier(Config.globalRevenueBooster), World.ActiveZone.ProductionSpeed.FloatValue.Value * Boosters.GetTotalMultiplier(Config.globalRevenueBooster));
        }

        private void OnCreditsProduced(BigNumber amount)
        {
            long multiplier = Boosters.GetTotalMultiplier(Config.globalRevenueBooster);

            if (multiplier > 1)
            {
                amount *= multiplier;
            }

            if (World.ActiveZone.Workplace.RevenueMultiplier > 1)
            {
                amount *= World.ActiveZone.Workplace.RevenueMultiplier;
            }

            OnRevenueGenerated(amount);
            World.Funds.Add(amount);
        }

        public void SyncWithCloud(ICloudSave save)
        {
            InitWithState(state.activeWorld, save as GameState);
        }

        public void Update(float delta)
        {
            if (isRunning == false || World == null)
            {
                return;
            }

            World.ActiveZone.Update(delta);

            boosterUpdateTimer += delta;

            if (boosterUpdateTimer >= 1)
            {
                Boosters.CheckExpiredBoosters();
                boosterUpdateTimer = 0;
            }
        }

        public IPromise Run()
        {
            isRunning = true;
            return Promise.Resolved();
        }

        public void Dispose()
        {
            World.Dispose();
        }

        public bool IsWorldUnlocked(WorldConfigLink link)
        {
            int zonesUnlocked = 0;
            foreach (var zoneLink in link.Value.zones)
            {
                Zone zone = state.GetZone(zoneLink);
                if (zone != null)
                {
                    zonesUnlocked++;
                }
            }
            return zonesUnlocked == link.Value.zones.Count;
        }

        public ZoneStatus GetZoneStatus(ZoneConfigLink link)
        {
            Zone zone = state.GetZone(link);

            if (zone != null)
            {
                return ZoneStatus.Unlocked;
            }

            if (link.Value.parentZone.HasValue == false)
            {
                return ZoneStatus.AvailableToUnlock;
            }

            ZoneStatus parentZoneStatus = GetZoneStatus(link.Value.parentZone);

            if (parentZoneStatus == ZoneStatus.Unlocked)
            {
                return ZoneStatus.AvailableToUnlock;
            }

            return ZoneStatus.Locked;
        }
    }
}
