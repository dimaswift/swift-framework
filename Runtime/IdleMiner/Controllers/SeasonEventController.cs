﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner
{
    public class SeasonEventController : ISeasonEvent
    {
        public event Action<EventTask> OnTaskCompleted = t => { };
        public IStatefulEvent<bool> IsCleared => isCleared;

        public long TimeTillStart => Math.Max(0, eventLink.Value.startTime.timestampSeconds - App.Core.Clock.Now.Value);

        public long TimeTillEnd => Math.Max(0, eventLink.Value.endTime.timestampSeconds - App.Core.Clock.Now.Value);

        [Serializable]
        protected class SeasonEventState
        {
            public List<int> completedTasks = new List<int>();
            public bool rewardsClaimed;
        }

        private readonly SeasonEventState state;
        private readonly SeasonEventLink eventLink;
        private readonly ISeasonPass seasonPass;
        private readonly StatefulEvent<bool> isCleared = new StatefulEvent<bool>();

        private IWorld world;

        public SeasonEventController(SeasonEventLink eventLink, ISeasonPass seasonPass)
        {
            this.eventLink = eventLink;
            this.seasonPass = seasonPass;
            state = App.Core.Storage.LoadOrCreateNew(eventLink, () => GetDefaultState(eventLink));
            App.Core.Storage.RegisterState(() => state, eventLink);

            isCleared.SetValue(state.completedTasks.Count >= eventLink.Value.tasks.Count);

        }

        public void ListenToWorldEvents(IWorld world)
        {
            this.world = world;
            world.ActiveZone.OnFactoryPurchased += OnFactoryPurchased;
            foreach (IFactory factory in world.ActiveZone.GetFactories())
            {
                factory.UpgradeController.OnLeveledUp += () => OnFactoryUpgraded(factory);
            }
            world.ActiveZone.Transporter.UpgradeController.OnLeveledUp += OnTransporterLevelUp;
            world.ActiveZone.Workplace.UpgradeController.OnLeveledUp += OnWorkplaceLevelUp;
        }

        public IEnumerable<(bool isCompleted, EventTask config)> GetTasks()
        {
            SeasonEvent seasonEvent = eventLink.Value;
            for (int i = 0; i < seasonEvent.tasks.Count; i++)
            {
                EventTask task = seasonEvent.tasks[i];
                yield return (state.completedTasks.Contains(i), task);
            }
        }

        private void OnWorkplaceLevelUp()
        {
            if (world.ActiveZone.Transporter.UpgradeController.IsMaxLevel())
            {
                TryCompleteMaxUpdateTask();
            }
        }

        private void OnTransporterLevelUp()
        {
            if (world.ActiveZone.Transporter.UpgradeController.IsMaxLevel())
            {
                TryCompleteMaxUpdateTask();
            }
        }

        private void TryCompleteMaxUpdateTask()
        {
            if (TryCompleteTask(EventTaskType.UpgradeZoneToMax))
            {
                TryCompleteTask(EventTaskType.FinishEventUntilDate);
            }
        }

        private void OnFactoryUpgraded(IFactory factory)
        {
            TryCompleteTask(EventTaskType.UpgradeFactory, factory.OrderIndex, factory.UpgradeState.Level);
            if (factory.UpgradeController.IsMaxLevel())
            {
                TryCompleteMaxUpdateTask();
            }
        }

        protected virtual bool TryCompleteTask(EventTaskType type, long index = -1, long target = -1)
        {
            SeasonEvent seasonEvent = eventLink.Value;
            long now = App.Core.Clock.Now.Value;
            for (int i = 0; i < seasonEvent.tasks.Count; i++)
            {
                EventTask task = seasonEvent.tasks[i];
                if (task.limitTime && now > task.dueDate.timestampSeconds)
                {
                    continue;
                }
                if (task.type != type)
                {
                    continue;
                }
                if (index != -1 && task.index != index)
                {
                    continue;
                }
                if (target != -1 && target < task.target)
                {
                    continue;
                }
                if (state.completedTasks.Contains(i))
                {
                    continue;
                }
                state.completedTasks.Add(i);
                seasonPass.AddPoints(task.seasonPassPoints);
                OnTaskCompleted(task);
                if (state.completedTasks.Count >= seasonEvent.tasks.Count)
                {
                    isCleared.SetValue(true);
                }
                return true;
            }
            return false;
        }

        private void OnFactoryPurchased(IFactory factory)
        {
            TryCompleteTask(EventTaskType.PuchaseFactory, factory.OrderIndex, 1);
        }

        private SeasonEventState GetDefaultState(SeasonEventLink eventLink)
        {
            return new SeasonEventState()
            {
                completedTasks = new List<int>()
            };
        }

        public bool IsTaskCompleted(int index)
        {
            return state.completedTasks.Contains(index);
        }

        public IPromise<IEnumerable<RewardLink>> ClaimRewards()
        {
            Promise<IEnumerable<RewardLink>> promise = Promise<IEnumerable<RewardLink>>.Create();

            if (state.rewardsClaimed)
            {
                promise.Reject(new InvalidOperationException("Rewards already claimed"));
                return promise;
            }

            if (TimeTillEnd > 0)
            {
                promise.Reject(new InvalidOperationException("Event is not ended yet"));
                return promise;
            }

            if (TimeTillStart > 0)
            {
                promise.Reject(new InvalidOperationException("Event is not started yet"));
                return promise;
            }

            SeasonEvent seasonEvent = eventLink.Value;

            List<IPromise> rewardPromises = new List<IPromise>();
            List<RewardLink> rewardLinks = new List<RewardLink>();

            foreach (int completedTaskIndex in state.completedTasks)
            {
                if (completedTaskIndex >= seasonEvent.tasks.Count)
                {
                    continue;
                }
                EventTask task = seasonEvent.tasks[completedTaskIndex];
                foreach (RewardLink rewardLink in task.rewards)
                {
                    rewardLink.Load(reward =>
                    {
                        rewardLinks.Add(rewardLink);
                        rewardPromises.Add(reward.AddReward());
                    });
                }
            }

            Promise.All(rewardPromises).Always(() =>
            {
                state.rewardsClaimed = true;

                if (rewardLinks.Count > 0)
                {
                    promise.Resolve(rewardLinks);
                }
                else
                {
                    promise.Reject(new InvalidOperationException("No tasks completed"));
                }
            });

            return promise;
        }

        public bool IsRewardAvailable()
        {
            if (App.Core.Clock.Now.Value >= eventLink.Value.endTime.timestampSeconds && state.rewardsClaimed == false)
            {
                return state.completedTasks.Count > 0;
            }
            return false;
        }

        public string GetTitle()
        {
            return App.Core.Local.GetText(eventLink.Value.nameKey);
        }
    }
}
