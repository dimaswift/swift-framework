﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.IdleMiner.Configs;
using System;

namespace SwiftFramework.IdleMiner.Controllers
{
    public abstract class SupervisedController<TState, TConfig, TBoosters, TAbilities> 
        : IController, ISubordinate, IUpgradable where TState : IUpgradableState where TConfig : ControllerConfig where TBoosters : BoostersHubConfig where TAbilities : AbilitiesHubConfig
    {
        public abstract ViewLink View { get; }
        public event Action OnActivated = () => { };
        public event Action OnBoosted = () => { };
        protected TState State { get; }
        public TConfig Config { get; }
        public IZone Zone { get; }
        public ISupervisorsManager Supervisors { get; }
        public FracturedBigNumber ProductionSpeed { get; } = new FracturedBigNumber(FracturedBigNumber.PRODUCTION_SPEED);

        public abstract string Title { get; }

        private float upgradeDiscount;
        protected TAbilities abilities;
        protected TBoosters boosters;

        protected SupervisedController(SharedSupervisorsManager sharedSupervisors, SubordinateState subordinateState, SupervisorsPool supervisorsPool, TState state, TConfig config, IZone zone)
        {
            Config = config;
            Supervisors = new SupervisorsManager(zone.Link as Link, sharedSupervisors, supervisorsPool, subordinateState, this, zone.Clock);
            abilities = Config.abilitiesHub.Value as TAbilities;
            boosters = Config.boostersHub.Value as TBoosters;
            Supervisors.OnAbilitiesChanged += Supervisors_OnAbilitiesChanged;
            State = state;
            Zone = zone;
            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                Supervisors.Init();
                Supervisors_OnAbilitiesChanged();
            });
        }

        protected T GetAbilitiesHub<T>() where T : AbilitiesHubConfig
        {
            return Config.abilitiesHub.Value as T;
        }

        protected T GetBoostersHub<T>() where T : BoostersHubConfig
        {
            return Config.abilitiesHub.Value as T;
        }

        private void Supervisors_OnAbilitiesChanged()
        {
            upgradeDiscount = 0;
            if (Supervisors.IsAbilityActive(abilities.upgradeDiscountAbility, out AbilityState state))
            {
                upgradeDiscount = (float)state.power / 100;
            }
            if (Supervisors.IsPerkActive(abilities.upgradeDiscountPerk, Zone.Link as Link))
            {
                upgradeDiscount += abilities.upgradeDiscountPerk.Value.multiplier;
            }
            if(upgradeDiscount > 1)
            {
                upgradeDiscount = 1;
            }
        }

        public SupervisorTemplateLink GetSupervisorTemplate() => Config.supervisor;

        public virtual bool ReadyToWork => Supervisors.IsAssigned.Value;

        public abstract IUpgradeController UpgradeController { get; }

        IUpgradableState IUpgradable.UpgradeState => State;

        public abstract string SupervisorsTitle { get; }

        public void Activate()
        {
            OnActivate();
            OnActivated();
        }

        public virtual IPromise Init()
        {
            UpdateProductionSpeed();
            UpgradeController.OnLeveledUp += UpdateProductionSpeed;
            return Promise.Resolved();
        }

        public abstract void SyncState();

        public void Boost()
        {
            OnBoost();
            OnBoosted();
        }

        protected virtual float GetUpgradeDiscount()
        {
            return upgradeDiscount;
        }

        protected virtual void OnActivate() { }

        protected virtual void OnBoost() { }

        public abstract void Update(float delta);

        public abstract void UpdateProductionSpeed();
    }
}
