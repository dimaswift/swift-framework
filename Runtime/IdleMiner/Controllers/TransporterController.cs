﻿using System.Collections.Generic;
using SwiftFramework.Core;
using System;
using System.Numerics;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.Core.SharedData;
using UnityEngine;
using SwiftFramework.Core.Supervisors;

namespace SwiftFramework.IdleMiner.Controllers
{
    public abstract class TransporterController<S> : SupervisedController<Transporter, TransporterConfig, TransporterBoostersHub, TransporterAbilitiesHub>, ITransporter where S : struct, Enum
    {
        public event Action<BigNumber> OnUnloaded = v => { };
        public event Action<BigNumber> OnLoaded = v => { };
        public event Action<(BigNumber amount, float duration)> OnUnloadStarted = d => { };
        public event Action OnMoveToStart = () => { };
        public event Action<int> OnStatusChanged = i => { };

        public IStatefulEvent<BigNumber> LoadedAmount => loadedAmount;

        public override ViewLink View => Config.view;

        private int CurrentFactoryIndex { get; set; }
        public int TargetFactoryIndex { get; private set; }

        public float TimeNormalized => phaseLoop.TimeNormalized;

        public int CurrentIteration => phaseLoop.CurrentIteration;

        public BigNumber LastLoadedAmount { get; private set; }
        public BigNumber Capacity => State.capacity.Value.MultiplyByFloat(capacityMultiplier);

        public override IUpgradeController UpgradeController => upgradeController;

        public override string SupervisorsTitle => App.Core.Local.GetText("transporter_supervisor");
        public override string Title => App.Core.Local.GetText("transporter");

        public T GetStatus<T>() where T : struct, Enum
        {
            return (T)(object)phaseLoop.State.Value;
        }

        protected readonly TransporterUpgradeController upgradeController;
        protected readonly IZone zone;
        protected readonly PhaseLoop<S> phaseLoop = new PhaseLoop<S>();
        protected readonly StatefulEvent<BigNumber> loadedAmount = new StatefulEvent<BigNumber>(0);

        protected float moveSpeedMultiplier = 1;
        protected float loadSpeedMultiplier = 1;
        protected float capacityMultiplier = 1;
        protected bool clearedFactory;

        public TransporterController(SharedSupervisorsManager sharedSupervisors, Transporter transporter, TransporterConfig config, IZone zone) : base(sharedSupervisors, transporter.subordinate, transporter.supervisorsPool, transporter, config, zone)
        {
            upgradeController = new TransporterUpgradeController(transporter, config, config.upgradeSettings, zone, GetUpgradeDiscount);
            this.zone = zone;
            upgradeController.OnLeveledUp += UpdateMultipliers;

            loadedAmount.SetValue(0);
            phaseLoop.State.OnValueChangedDelta += State_OnValueChangedDelta;
            phaseLoop.SetLoop(GetPhases(), () => ReadyToWork);
            Supervisors.IsAssigned.OnValueChanged += IsAssigned_OnValueChanged;
            Supervisors.OnAbilitiesChanged += UpdateMultipliers;
            zone.Boosters.OnMultipliersUpdated += UpdateMultipliers;
            UpdateMultipliers();
        }

        private void State_OnValueChangedDelta(S oldValue, S newValue)
        {
            OnStatusChanged((int)(object)newValue);
            OnStateChanged(oldValue, newValue);
        }

        protected abstract void OnStateChanged(S oldValue, S newValue);

        private void IsAssigned_OnValueChanged(bool assigned)
        {
            if (assigned)
            {
                Activate();
            }
            UpdateProductionSpeed();
        }

        private void UpdateMultipliers()
        {
            capacityMultiplier = 1;
            moveSpeedMultiplier = zone.Boosters.GetTotalMultiplier(boosters.moveSpeedBooster);
            loadSpeedMultiplier = zone.Boosters.GetTotalMultiplier(boosters.loadingSpeedBooster);

            if (Supervisors.IsAbilityActive(abilities.moveSpeedAbility, out AbilityState moveSpeedState))
            {
                moveSpeedMultiplier += moveSpeedState.power;
            }

            if (Supervisors.IsAbilityActive(abilities.loadSpeedAbility, out AbilityState loadSpeedState))
            {
                loadSpeedMultiplier += loadSpeedState.power;
            }

            if (Supervisors.IsAbilityActive(abilities.totalSpeedAbility, out AbilityState totalSpeedState))
            {
                loadSpeedMultiplier += totalSpeedState.power;
                moveSpeedMultiplier += totalSpeedState.power;
            }

            if (Supervisors.IsPerkActive(abilities.moveSpeedPerk, Zone.Link as Link))
            {
                moveSpeedMultiplier *= abilities.moveSpeedPerk.Value.multiplier;
            }

            if (Supervisors.IsAbilityActive(abilities.capacityAbility, out AbilityState capacityState))
            {
                capacityMultiplier *= capacityState.power;
            }

            phaseLoop.UpdatePhases(GetPhases());
            UpdateProductionSpeed();
        }

        private int GetNextFactoryIndex(int current)
        {
            CurrentFactoryIndex = current;
            while (current < zone.FactoryCount && zone.GetFactory(current).WorkForce.Value == 0)
            {
                current++;
            }

            return current;
        }

        protected abstract void MoveToFactory();

        protected void ReturnToStartPoint()
        {
            LastLoadedAmount = loadedAmount.Value;
            OnMoveToStart();
        }

        protected void Load()
        {
            IFactory factory = Zone.GetFactory(TargetFactoryIndex);

            BigInteger amountToTake = factory.WorkForce.Value.Value;

            amountToTake = BigInteger.Min(amountToTake, Capacity.Value - LoadedAmount.Value.Value);

            BigNumber amount = factory.TakeWorkForce(amountToTake);

            loadedAmount.SetValue(loadedAmount.Value + amount);

            LastLoadedAmount += amount;

            clearedFactory = factory.WorkForce.Value <= 0;

            OnLoaded(amount);
        }


        protected void Unload()
        {
            BigNumber amountToUnload = loadedAmount.Value.Value;
            loadedAmount.SetValue(0);
            Zone.Workplace.AddWorkForce(amountToUnload);
            OnUnloaded(amountToUnload);
        }

        protected bool TryToFinishLoading()
        {
            if (LoadedAmount.Value >= Capacity.Value)
            {
                return true;
            }

            int index = TargetFactoryIndex;

            if (clearedFactory)
            {
                clearedFactory = false;
                index++;
            }

            int nextFactoryIndex = GetNextFactoryIndex(index);

            if (nextFactoryIndex >= Zone.FactoryCount)
            {
                return true;
            }

            TargetFactoryIndex = nextFactoryIndex;

            LastLoadedAmount = 0;

            MoveToFactory();

            return false;
        }


        protected abstract IEnumerable<Phase<S>> GetPhases();

        protected virtual float GetTravelTimeToFactory()
        {
            float time = Config.useCommonTravelTime ? Config.travelTime : Config.travelTimeBetweenFactories;
            if (TargetFactoryIndex == 0)
            {
                return Config.useCommonTravelTime ? Config.travelTime : Config.travelTimeToFirstFactory;
            }
            int factoriesDistance = TargetFactoryIndex - CurrentFactoryIndex;
            if (factoriesDistance != 0)
            {
                return time * (factoriesDistance + 1);
            }
            return time;
        }

        protected float GetTravelTimeToStartPoint()
        {
            return Config.travelTime * (TargetFactoryIndex + 1);
        }

        protected virtual bool CanMoveToNextFactory()
        {
            int nextFactoryIndex = GetNextFactoryIndex(0);
            if (nextFactoryIndex >= Zone.FactoryCount)
            {
                return false;
            }
            TargetFactoryIndex = nextFactoryIndex;
            return true;
        }

        protected abstract bool IsIdle();

        public override void Update(float delta)
        {
            if (IsIdle() && Supervisors.IsAssigned.Value && CanMoveToNextFactory())
            {
                phaseLoop.Activate();
            }
            phaseLoop.Update(delta);
        }

        protected override void OnActivate()
        {
            if (CanMoveToNextFactory())
            {
                phaseLoop.Activate();
            }
        }

        protected override void OnBoost()
        {

        }

        public override void SyncState()
        {

        }

        public float GetTargetFactoryLoadingTime()
        {
            BigInteger amountToTake = GetTargetFactoryLoadAmount().Value;

            float time = Math.Max(Config.minLoadTime, Mathf.Min((amountToTake.DivideFloat(State.loadingSpeed.Value) + Config.loadTime) * moveSpeedMultiplier, Config.maxLoadTime));

            time /= loadSpeedMultiplier;

            return time;
        }

        public BigNumber GetTargetFactoryLoadAmount()
        {
            IFactory factory = Zone.GetFactory(TargetFactoryIndex);

            BigInteger amountToTake = BigInteger.Min(factory.WorkForce.Value.Value, Capacity.Value - LoadedAmount.Value.Value);

            return amountToTake;
        }

        protected float GetUnloadingTime()
        {
            return (LoadedAmount.Value.Value.DivideFloat(State.loadingSpeed.Value) + Config.unloadingTime) / loadSpeedMultiplier;
        }

        protected void StartUnloading()
        {
            OnUnloadStarted((loadedAmount.Value, GetUnloadingTime()));
        }

        public override void UpdateProductionSpeed()
        {
            if (Supervisors.IsAssigned.Value == false)
            {
                ProductionSpeed.Set(0, 0);
                return;
            }
            float deliveryTime = Config.travelTime / (State.moveSpeed * moveSpeedMultiplier);
            float loadingTime = (Config.loadTime + Config.unloadingTime);
            BigNumber newProductionSpeed = State.loadingSpeed.Value.DivideByFloat(deliveryTime + loadingTime);

            if (newProductionSpeed.FitsInFloat)
            {
                ProductionSpeed.Set(newProductionSpeed, (float)State.loadingSpeed.Value / (deliveryTime + loadingTime + Config.unloadingTime));
            }
            else
            {
                ProductionSpeed.Set(newProductionSpeed, 0);
            }
        }

        public abstract bool IsLoading();
    }
}
