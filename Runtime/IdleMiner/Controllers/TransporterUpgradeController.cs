﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using System;
using System.Collections.Generic;

namespace SwiftFramework.IdleMiner.Controllers
{
    public class TransporterUpgradeController : UpgradeController<Transporter, TransporterUpgradeSettings>
    {
        private readonly TransporterConfig config;
        private readonly IZone zone;
        private readonly TransporterUpgradeSettings upgradeSettings;

        public TransporterUpgradeController(Transporter transporter, TransporterConfig config, TransporterUpgradeSettings upgradeSettings, IZone zone, Func<float> discountHandler)
            : base(upgradeSettings, transporter, discountHandler)
        {
            this.zone = zone;
            this.config = config;
            this.upgradeSettings = upgradeSettings;
        }

        protected override IEnumerable<ValueDelta> GetUpgradedValues(Transporter upgradable, Transporter upgradedCopy, long count)
        {
            bool maxLevel = IsMaxLevel();

            float loadingTimeUpgradable = (float)(upgradable.capacity.Value / upgradable.loadingSpeed.Value * 2) + (zone.ActiveFactoriesAmount * config.loadTime) + config.unloadingTime;
            float movingTimeUpgradable = ((config.travelTimeBetweenFactories * zone.ActiveFactoriesAmount) + config.travelTime) / upgradable.moveSpeed;

            float loadingTimeUpgradedCopy = (float)(upgradedCopy.capacity.Value / upgradedCopy.loadingSpeed.Value * 2) + (zone.ActiveFactoriesAmount * config.loadTime) + config.unloadingTime;
            float movingTimeUpgradedCopy = ((config.travelTimeBetweenFactories * zone.ActiveFactoriesAmount) + config.travelTime) / upgradedCopy.moveSpeed;

            MinerUpgradeIcons upgradeIcons = Settings.upgradeIcons.Value as MinerUpgradeIcons;

            yield return new ValueDelta()
            {
                baseValue = upgradable.capacity.Value.DivideByFloat(loadingTimeUpgradable + movingTimeUpgradable),
                upgradeValue = upgradedCopy.capacity.Value.DivideByFloat(loadingTimeUpgradedCopy + movingTimeUpgradedCopy),
                postFix = "/s",
                name = "#totaltransfer",
                reachedMaxLevel = maxLevel,
                icon = upgradeIcons.totalTransferIcon
            };
            yield return new ValueDelta()
            {
                baseValue = upgradable.capacity.Value,
                upgradeValue = upgradedCopy.capacity.Value,
                name = "#loadcapacity",
                reachedMaxLevel = maxLevel,
                icon = upgradeIcons.capacityIcon
            };
            yield return new ValueDelta()
            {
                baseFloatValue = upgradable.moveSpeed,
                upgradeByFloatValue = upgradedCopy.moveSpeed,
                name = "#movingspeed",
                reachedMaxLevel = maxLevel,
                icon = upgradeIcons.speedIcon
            };
            yield return new ValueDelta()
            {
                baseValue = upgradable.loadingSpeed,
                upgradeValue = upgradedCopy.loadingSpeed,
                name = "#loadspeed",
                postFix = "/s",
                reachedMaxLevel = maxLevel,
                icon = upgradeIcons.loadSpeedIcon
            };
        }

        protected override void OnLevelUp(Transporter transporter, long count, bool isCopy = false)
        {
            for (long i = 0; i < count; i++)
            {
                transporter.level++;
                foreach (var l in Settings.boostLevels)
                {
                    if (transporter.level == l.level - 1)
                    {
                        transporter.moveSpeed = transporter.moveSpeed * Settings.moveSpeedMultiplier;

                        transporter.capacity = transporter.capacity.Value * 2;

                        transporter.loadingSpeed = transporter.loadingSpeed.Value * 2;
                        if (isCopy == false)
                        {
                            l.reward.AddReward();
                        }

                        break;
                    }
                }

                transporter.capacity = transporter.capacity.Value.MultiplyByFloat(Settings.capacityMultiplier);

                transporter.loadingSpeed = transporter.loadingSpeed.Value.MultiplyByFloat(Settings.loadingRateMultiplier);
            }

        }
    }
}
