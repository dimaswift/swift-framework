﻿using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using System;
using System.Numerics;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Controllers
{
    public class WorkerTransport : IPooled, IWorkerTransport
    {
        public BigNumber Power { get; private set; }
        public event Action OnStarted = () => { };
        public event Action<IWorkerTransport> OnFinished = t => { };

        public IFactory Factory => factoryController;

        public float TimeNormalized => transportationTime > 0 ? timer / transportationTime : 0;

        public int InitialPoolCapacity => 1;

        public int Order { get; private set; }

        public bool Active { get; set; }

        private IPool pool;

        private float timer;
        private float delay;
        private float transportationTime;
        private FactoryController factoryController;

        public WorkerTransport(FactoryController factory)
        {
            factoryController = factory;
        }

        public void Init(IPool pool)
        {
            this.pool = pool;
        }

        public void BeginWork(float delay, BigInteger power, float transportationTime, int order)
        {
            this.delay = delay;
            this.Power = power;
            this.transportationTime = transportationTime;
            Order = order;

            if (delay == 0)
            {
                OnStarted();
            }
        }

        public void MultiplyPower(float multiplier)
        {
            Power = Power.Value.MultiplyByFloat(multiplier);
        }

        public void Process(float delta, out bool finishedWork)
        {
            finishedWork = false;
            if (delay > 0)
            {
                delay -= delta;

                if (delay <= 0)
                {
                    OnStarted();
                }

                return;
            }

            timer += delta;

            if (timer >= transportationTime)
            {
                factoryController.DeliverWorker(this);
                ReturnToPool();
                finishedWork = true;
                return;
            }
        }

        public void TakeFromPool()
        {
            Active = true;
            timer = 0;
            Power = 0;
        }

        public void ReturnToPool()
        {
            OnFinished(this);


            OnFinished = t => { };
            OnStarted = () => { };

            Active = false;
            pool.Return(this);
        }

        public void Dispose()
        {
            OnFinished = t => { };
            OnStarted = () => { };
        }
    }
}