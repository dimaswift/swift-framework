﻿using SwiftFramework.Core;
using System;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using System.Numerics;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.Core.Boosters;

namespace SwiftFramework.IdleMiner.Controllers
{
    public abstract class WorkplaceController<TConfig, TAbilities, TBoosters> : SupervisedController<Workplace, WorkplaceConfig, TBoosters, TAbilities>, IWorkplace where TBoosters : WorkplaceBoostersHub where TAbilities : AbilitiesHubConfig where TConfig : WorkplaceConfig
    {
        public WorkplaceController(SharedSupervisorsManager sharedSupervisors, Workplace state, WorkplaceConfig config, IZone zone) : base(sharedSupervisors, state.subordinate, state.supervisorsPool, state, config, zone)
        {
            customConfig = Config as TConfig;
            if (state.resources > state.currentCapacity)
            {
                state.resources = state.currentCapacity;
            }

            UpgradeController = CreateUpgradeController();
            zone.Boosters.OnMultipliersUpdated += Boosters_OnMultipliersUpdated;

            workForce.SetValue(state.workForce);
            workForce.OnValueChanged += w => state.workForce = w;

            resources.SetValue(state.resources);
            resources.OnValueChanged += v => state.resources = v;

            Boosters_OnMultipliersUpdated();
        }

        public override string Title => App.Core.Local.GetText("workpalce");

        public override string SupervisorsTitle => App.Core.Local.GetText("workplace_supervisors");

        public BigNumber CurrentCapacity => State.currentCapacity;

        public event Action OnRanOutOfResources = () => { };

        public event Action OnRanOutOfWorkForce = () => { };

        public override ViewLink View => Config.view;

        public long RevenueMultiplier { get; private set; }

        public bool IsEmpty => WorkForce.Value == 0;

        public bool IsReloading { get; protected set; }

        public IStatefulEvent<BigNumber> WorkForce => workForce;

        public IStatefulEvent<BigNumber> Resources => resources;

        public override IUpgradeController UpgradeController { get; }

        public event Action<BigNumber> OnCreditsProduced = c => { };
        public event Action OnReloaded = () => { };

        private readonly StatefulEvent<BigNumber> workForce = new StatefulEvent<BigNumber>();
        private readonly StatefulEvent<BigNumber> resources = new StatefulEvent<BigNumber>();

        protected TConfig customConfig;

        private void Boosters_OnMultipliersUpdated()
        {
            RevenueMultiplier = Zone.Boosters.GetTotalMultiplier(boosters.revenueMultiplier);
        }

        public virtual IPromise<T> GetCurrentSkin<T>() where T : WorkplaceSkin
        {
            Promise<T> promise = Promise<T>.Create();
           
            if (State.skinIndex >= Config.skins.Length)
            {
                State.skinIndex = 0;
            }
            Config.skins[State.skinIndex].Load(cfg => { promise.Resolve(cfg as T); });
            return promise;
        }

        public virtual void AddWorkForce(BigNumber amount)
        {
            workForce.SetValue(workForce.Value + amount);
        }

        protected abstract IUpgradeController CreateUpgradeController();

        public virtual BigNumber TakeResources(BigNumber amount)
        {
            BigInteger result = BigInteger.Min(amount.Value, resources.Value.Value);

            resources.SetValue(resources.Value - result);

            if (result > 0 && resources.Value <= 0)
            {
                OnRanOutOfResources();
            }

            if (result <= 0)
            {
                return 0;
            }

            OnCreditsProduced(result);

            return result;
        }


        public virtual BigNumber TakeWorkforce(BigNumber amount)
        {
            BigInteger result = BigInteger.Min(amount.Value, workForce.Value.Value);

            workForce.SetValue(workForce.Value - result);

            if (result > 0 && workForce.Value <= 0)
            {
                OnRanOutOfWorkForce();
            }

            return BigInteger.Max(result, 0);
        }

        public IPromise Reload()
        {
            IsReloading = true;

            State.skinIndex++;

            State.progressCounter++;

            if (State.skinIndex >= Config.skins.Length)
            {
                State.skinIndex = 0;
            }

            BigInteger newAmount = Config.defaultState.resources.Value;

            float multiplier = Config.resourcesProgressMultiplier;

            for (int i = 0; i < State.progressCounter; i++)
            {
                newAmount = newAmount.MultiplyByFloat(multiplier);
            }

            State.currentCapacity = newAmount;

            resources.SetValue(0);

            resources.SetValue(newAmount);

            return GetReloadPromise().Then(() => OnReloaded());
        }

        protected virtual IPromise GetReloadPromise()
        {
            IsReloading = false;
            return Promise.Resolved();
        }

        public override void SyncState()
        {
            
        }

        public override void Update(float delta)
        {
            
        }

    }
}