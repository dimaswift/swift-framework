﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using System;
using System.Collections.Generic;

namespace SwiftFramework.IdleMiner.Controllers
{
    public class WorldController : IWorld
    {
        public IGame Game { get; private set; }

        public event Action<IZone> OnZoneActivated = z => { };

        public WorldConfig Config { get; private set; }

        public IFundsSource Funds { get; private set; }

        public IZone ActiveZone { get; private set; }

        public WorldConfigLink Link { get; private set; }

        private GameState state;

        private WorldConfigLink config;

        private WorldController(GameState state, IGame idleMiner, WorldConfigLink configLink)
        {
            this.state = state;
            Link = configLink; 
            config = configLink;
            Game = idleMiner;
            Funds = config.Value.softCurrency.Value;
            Config = configLink.Value;
        }

        public static IPromise<WorldController> Create(
            IBoosterManager boosters,
            IGame game,
            GameState state,
            WorkplaceHandler workplaceResolver,
            FactoryHandler factoryResolver,
            TransporterHandler transporterResolver,
            WorldConfigLink worldLink)
        {
            Promise<WorldController> promise = Promise<WorldController>.Create();

            WorldController world = new WorldController(state, game, worldLink);

            world.GetActiveZone().Done(activeZone =>
            {
                ZoneController.Create(game.GlobalClock, boosters, world, activeZone, workplaceResolver, factoryResolver, transporterResolver).Then(zone =>
                {
                    world.SetActiveZone(zone);
                    promise.Resolve(world);
                })
                .Catch(e => promise.Reject(e));
            });

            return promise;
        }

        private IPromise<Zone> GetActiveZone()
        {
            Promise<Zone> promise = Promise<Zone>.Create();

            Zone activeZone = state.FindActiveZone();

            if (activeZone != null)
            {
                promise.Resolve(activeZone);
                return promise;
            }

            ZoneController.CreateZone(state.activeZone).Done(z =>
            {
                z.lastRevenueEarnTimestamp = Game.GlobalClock.Now.Value;
                state.AddZone(z);
                promise.Resolve(z);
            });

            return promise;

        }

        private void SetActiveZone(IZone zone)
        {
            ActiveZone = zone;
            OnZoneActivated(ActiveZone);
        }

        public void Dispose()
        {
            ActiveZone.Dispose();
        }

        public IEnumerable<Zone> GetZones()
        {
            foreach (var zone in state.zones)
            {
                foreach (ZoneConfigLink zoneLink in Config.zones)
                {
                    if (zone.link == zoneLink)
                    {
                        yield return zone;
                    }
                }
            }
        }

        public bool IsCompletelyUnlocked()
        {
            return GetZones().CountFast() == Config.zones.Count;
        }
    }
}