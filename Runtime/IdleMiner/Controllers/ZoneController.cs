﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using System;
using System.Collections.Generic;
using System.Numerics;

namespace SwiftFramework.IdleMiner.Controllers
{
    public class ZoneController : IZone
    {
        public IClock Clock { get; }
        public IWorld World { get; }
        public IBoosterManager Boosters { get; }
        public ViewLink View => Config.view;
        public Zone State { get; private set; }

        public ITransporter Transporter { get; private set; }
        public IWorkplace Workplace { get; private set; }

        public event Action<BigNumber> OnCreditsProduced = c => { };
        public event Action OnZoneCreated = () => { };
        public event Action OnStateSync = () => { };
        public event Action<IController> OnControllerCreated = c => { };
        public event Action<IFactory> OnFactoryUnlocked = f => { };
        public event Action<IFactory> OnFactoryPurchased = f => { };
        public FracturedBigNumber ProductionSpeed { get; } = new FracturedBigNumber(FracturedBigNumber.PRODUCTION_SPEED);
        public FracturedBigNumber TotalFactoryProductionSpeed { get; } = new FracturedBigNumber(FracturedBigNumber.PRODUCTION_SPEED);

        public IEnumerable<IFactory> GetFactories() => factories;

        public ZoneConfig Config { get; private set; }
        private float prestigeMultiplier;

        public int ActiveFactoriesAmount
        {
            get
            {
                int result = 0;
                foreach (FactoryController factory in factories)
                {
                    if (factory.CurrentPurchaseStatus.Value == PurchaseStatus.Purchased)
                    {
                        result++;
                    }
                }
                return result;
            }
        }

        public int FactoryCount => factories.Count;

        public ILink Link => State.link;

        private readonly WorkplaceHandler workplaceResolver;
        private readonly TransporterHandler transporterResolver;
        private readonly FactoryHandler factoryResolver;

        private readonly List<IFactory> factories = new List<IFactory>();
        private Promise<OfflineRevenueResult> offlineRevenuePromise;

        public IFactory GetFactory(int index)
        {
            if (index >= factories.Count)
            {
                return null;
            }
            return factories[index];
        }

        public static IPromise<ZoneController> Create(
            IClock clock,
            IBoosterManager boosters,
            IWorld world,
            Zone state,
            WorkplaceHandler workplaceResolver,
            FactoryHandler factoryResolver,
            TransporterHandler transporterResolver)
        {
            Promise<ZoneController> promise = Promise<ZoneController>.Create();

            state.link.Load(config =>
            {
                ZoneController zone = new ZoneController(clock, boosters, world, state, config, workplaceResolver, factoryResolver, transporterResolver);
                promise.Resolve(zone);
            },
            e => promise.Reject(e));

            return promise;
        }

        private ZoneController(IClock clock,
            IBoosterManager boosters,
            IWorld world,
            Zone state,
            ZoneConfig config,
            WorkplaceHandler workplaceResolver,
            FactoryHandler factoryResolver,
            TransporterHandler transporterResolver)
        {
            this.workplaceResolver = workplaceResolver;
            this.factoryResolver = factoryResolver;
            this.transporterResolver = transporterResolver;
            State = state;
            Clock = clock;
            World = world;
            Config = config;
            Boosters = boosters;

            prestigeMultiplier = world.Game.GetZonePrestige(state.link).GetCurrentPrestige().multiplier;

        }


        private void UpdateProductionSpeed()
        {
            BigNumber totalFactoryProductionSpeed = 0;
            float totalFactoryProductionSpeedSmall = 0;

            foreach (IFactory factory in GetFactories())
            {
                if (factory.CurrentPurchaseStatus.Value == PurchaseStatus.Purchased)
                {
                    totalFactoryProductionSpeed += factory.ProductionSpeed.Value.Value;
                    totalFactoryProductionSpeedSmall += factory.ProductionSpeed.FloatValue.Value;
                }
            }

            TotalFactoryProductionSpeed.Set(totalFactoryProductionSpeed, totalFactoryProductionSpeedSmall);

            BigNumber transporterProductionSpeed = BigInteger.Min(Transporter.ProductionSpeed.Value.Value.Value, totalFactoryProductionSpeed.Value);
            float transporterProductionSpeedSmall = Math.Min(Transporter.ProductionSpeed.FloatValue.Value, totalFactoryProductionSpeedSmall);

            BigNumber workplaceProductionSpeed = BigInteger.Min(Workplace.ProductionSpeed.Value.Value.Value, transporterProductionSpeed.Value);
            float workplaceProductionSpeedSmall = Math.Min(Workplace.ProductionSpeed.FloatValue.Value, transporterProductionSpeedSmall);

            ProductionSpeed.Set(workplaceProductionSpeed, workplaceProductionSpeedSmall);

            State.idleProductionSpeed = ProductionSpeed.Value.Value;
            State.idleProductionSpeedFloat = workplaceProductionSpeedSmall;

        }

        public static IPromise<Zone> CreateZone(ZoneConfigLink zoneLink)
        {
            Promise<Zone> promise = Promise<Zone>.Create();

            zoneLink.Load(zoneConfig =>
            {
                Zone zone = new Zone();
                zone.link = zoneLink;
                zoneConfig.workplace.Load(workplaceConfig =>
                {
                    zone.workplace = workplaceConfig.defaultState.DeepCopy();
                    zoneConfig.transporter.Load(transporterConfig =>
                    {
                        zone.transporter = transporterConfig.defaultState.DeepCopy();
                        zone.prestige = zoneConfig.defaultPrestige.DeepCopy();
                        zoneConfig.factory.Load(factoryConfig =>
                        {
                            CreateNextFactory(factoryConfig, zoneConfig, zone, 0, promise);
                        });
                    });
                });
            });

            return promise;
        }

        private static void CreateNextFactory(FactoryConfig factoryConfig, ZoneConfig zoneConfig, Zone zone, int index, Promise<Zone> finalPromise)
        {
            Factory factory = factoryConfig.levels[index].defaultState.DeepCopy();

            factory.incomePerWorker = factoryConfig.levels[index].defaultState.incomePerWorker;

            factory.id = index;

            zone.factories.Add(factory);

            if (index == factoryConfig.levels.Length - 1)
            {
                finalPromise.Resolve(zone);
            }
            else
            {
                CreateNextFactory(factoryConfig, zoneConfig, zone, ++index, finalPromise);
            }

        }

        public IPromise CreateProductionLoop()
        {
            Promise promise = Promise.Create();

            transporterResolver(State.transporter, Config.transporter, this).Done(transporter =>
            {
                Transporter = transporter;
                OnControllerCreated(Transporter);

                workplaceResolver.Invoke(Config.workplace, State.workplace, this).Done(workplace =>
                {
                    Workplace = workplace;
                    OnControllerCreated(Workplace);

                    Workplace.OnCreditsProduced += Workplace_OnCreditsProduced;
                    factories.Clear();
                    promise.Done(() =>
                    {
                        Transporter.ProductionSpeed.OnValueChanged += UpdateProductionSpeed;
                        Workplace.ProductionSpeed.OnValueChanged += UpdateProductionSpeed;
                        foreach (IFactory factory in GetFactories())
                        {
                            factory.ProductionSpeed.OnValueChanged += UpdateProductionSpeed;
                        }
                        UpdateProductionSpeed();
                    });
                    CreateNextFactory(0, promise);
                });
            });

            return promise;
        }

        private void CreateNextFactory(int index, Promise finishPromise)
        {
            FactoryConfigLink factoryConfigLink = Config.factory;

            factoryConfigLink.Load(factoryCfg =>
            {
                IFactory prevFactory = index == 0 ? null : factories[index - 1];
                Factory factoryState;
                if (index < State.factories.Count)
                {
                    factoryState = State.factories[index];
                }
                else
                {
                    factoryState = factoryCfg.levels[index].defaultState.DeepCopy();
                    State.factories.Add(factoryState);
                }
                factoryState.id = index;
                factoryResolver.Invoke(prevFactory, factoryState, factoryConfigLink, this, index).Done(factory =>
                {
                    OnControllerCreated(factory);
                    factory.OnFactoryPurchased += () => OnFactoryPurchased(factory);
                    factory.Unlocker.UnlockStatus.OnValueChanged += status =>
                    {
                        if (status == UnlockStatus.Unlocked)
                        {
                            OnFactoryUnlocked(factory);
                        }
                    };
                    factories.Add(factory);
                    index++;
                    if (index < factoryCfg.levels.Length)
                    {
                        CreateNextFactory(index, finishPromise);
                    }
                    else
                    {
                        finishPromise.Resolve();
                    }
                });
            });
        }

        private void Workplace_OnCreditsProduced(BigNumber amount)
        {
            OnCreditsProduced(amount.Value.MultiplyByFloat(prestigeMultiplier));
        }

        public void SyncState(object state)
        {
            this.State = state as Zone;

            foreach (FactoryController factory in factories)
            {
                factory.SyncState();
            }

            Workplace.SyncState();

            Transporter.SyncState();

            OnStateSync();
        }

        private void ProcessFactories(float delta)
        {
            for (int i = 0; i < factories.Count; i++)
            {
                IFactory factory = factories[i];
                factory.Update(delta);
            }
        }

        public void Update(float delta)
        {
            ProcessFactories(delta);
            Transporter.Update(delta);
            Workplace.Update(delta);
        }

        public void Dispose()
        {
            foreach (IFactory factory in factories)
            {
                factory.Dispose();
            }
        }

        public bool HasAtLeastOneReadyInactiveSupervisorAbility()
        {
            if (Transporter.Supervisors.ReadyToUseAbility())
            {
                return true;
            }

            if (Workplace.Supervisors.ReadyToUseAbility())
            {
                return true;
            }

            foreach (IFactory factory in GetFactories())
            {
                if (factory.Supervisors.ReadyToUseAbility())
                {
                    return true;
                }
            }

            return false;
        }

        public void ActivateAllSupervisors()
        {
            Transporter.Supervisors.ActivateCurrentAbility();
            Workplace.Supervisors.ActivateCurrentAbility();
            foreach (IFactory factory in GetFactories())
            {
                factory.Supervisors.ActivateCurrentAbility();
            }
        }
    }
}
