﻿using SwiftFramework.Core;
using SwiftFramework.Core.Prestige;
using SwiftFramework.IdleMiner.Model;

namespace SwiftFramework.IdleMiner.Controllers
{
    public class ZonePrestige : PrestigeController<ZonePrestigeState, Zone>
    {
        public ZonePrestige(Zone zone, PrestigeSettings config) : base(zone.prestige, zone, config)
        {
        }


        public BigNumber GetCurrentTotalZonesCleared()
        {
            return new BigNumber(prestigeState.zonesCleared);
        }

        protected override IPromise ProcessUpgrade()
        {
            Promise promise = Promise.Create();

            Zone zone = state;

            zone.factories.Clear();
            zone.idleProductionSpeed = 0;
            zone.idleProductionSpeedFloat = 0;

            zone.lastRevenueEarnTimestamp = 0;
            zone.supervisorsPool.supervisors.Clear();
            zone.supervisorsPool.supervisorsPurchased = 0;
            zone.supervisorsPool.abilityState = new Core.Abilities.AbilityManagerState();

            zone.link.Value.transporter.Load(transporter =>
            {
                zone.transporter = transporter.defaultState.DeepCopy();
                zone.link.Value.workplace.Load(workplace =>
                {
                    zone.workplace = workplace.defaultState.DeepCopy();
                    promise.Resolve();
                });
            });

            return promise;
        }
    }
}