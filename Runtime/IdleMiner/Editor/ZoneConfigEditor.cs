﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SwiftFramework.IdleMiner.Configs;
using System;
using SwiftFramework.Core;
using System.Numerics;
using SwiftFramework.IdleMiner.Model;

namespace SwiftFramework.IdleMiner.Editor
{
    [CustomEditor(typeof(FactoryConfig))]
    [CanEditMultipleObjects]
    public class ZoneConfigEditor : UnityEditor.Editor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Calculate"))
            {
                var config = target as FactoryConfig;
                config.levels = new FactoryConfig.ConfigPerLevel[config.generator.amountToGenerate];

                for (int i = 0; i < config.levels.Length; i++)
                {
                    var priceSoft = Link.Create<PriceLink>(config.generator.softPriceLink.GetPath());
                    var priceHard = Link.Create<PriceLink>(config.generator.hardPriceLink.GetPath());

                    priceSoft.amount = config.generator.baseCostSoft * BigInteger.Pow(config.generator.baseCostSoftMultiplier.Value, i);
                    priceHard.amount = config.generator.baseCostHard * BigInteger.Pow(config.generator.baseCostHardMultiplier.Value, i);

                    Factory defState = config.generator.defaultState.DeepCopy();
                    defState.id = i;
                    defState.incomePerWorker = config.generator.baseIncome * BigInteger.Pow(config.generator.baseIncomeMultiplier.Value, i);

                    config.levels[i] = new FactoryConfig.ConfigPerLevel()
                    {
                        defaultState = defState,
                        priceSoftCurrency = priceSoft,
                        priceHardCurrency = priceHard,
                        sharedView = Link.Create<ViewLink>(config.generator.defaultSharedView.GetPath()),
                        customView = Link.Create<ViewLink>(config.generator.customViews[i % config.generator.customViews.Count].GetPath()),
                        unlockerConfig = Link.Create<UnlockerLink>(config.generator.defaultUnlocker.GetPath()),
                    };
                }

                EditorUtility.SetDirty(target);
            }
        }
    }

}
