﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using System;

namespace SwiftFramework.IdleMiner
{
    public enum FactoryStatus { Idle, Producing }

    public enum PurchaseStatus { Purchased, Available, Locked }

    public interface IFactory : IController, ISubordinate, IUnlockable, IUpgradable
    {
        float CurrentProductionTimeNormalized { get; }
        ViewLink WorkerTransportView { get; }
        ViewLink CustomView { get; }
        float WorkerTransportTime { get; }
        IZone Zone { get; }
        IStatefulEvent<BigNumber> WorkForce { get; }
        event Action<IWorkerTransport> OnWorkerProduced;
        event Action OnFactoryPurchased;
        int ActiveWorkersAmount { get; }
        BigNumber TakeWorkForce(BigNumber amount);
        IStatefulEvent<FactoryStatus> CurrentState { get; }
        long OrderIndex { get; }
        string FactoryName { get; }
        IStatefulEvent<PurchaseStatus> CurrentPurchaseStatus { get; }
        IPromise<bool> Purchase(bool payWithSoft);
        PriceLink PriceSoftCurrency { get; }
        PriceLink PriceHardCurrency { get; }
        float GetPurchaseDiscount();
        void Dispose();
        bool CanBeBoughtForHardCurrnecy { get; }
    }

    public delegate IPromise<IFactory> FactoryHandler(IFactory previousFactory, Factory state, FactoryConfigLink config, IZone zone, int orderIndex);

}

