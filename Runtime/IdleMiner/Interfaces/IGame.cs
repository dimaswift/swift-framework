﻿using System;
using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;

namespace SwiftFramework.IdleMiner
{
    public interface IGame
    {
        event Action<BigNumber> OnRevenueGenerated;
        IClock GlobalClock { get; }
        bool IsWorldUnlocked(WorldConfigLink link);
        ZoneStatus GetZoneStatus(ZoneConfigLink link);
        IPrestigeController GetZonePrestige(ILink zoneLink);
        long LastSession { get; }
        void Update(float delta);
        event Action OnGameCreated;
        IPromise Run();
        ITutorialController Tutorial { get; }
        void Dispose();
        ISharedSupervisorsManager SharedSupervisors { get; }
        IWorld World { get; }
        ISeasonEvent SeasonEvent { get; }
        SeasonEventLink CurrentEventLink { get; }

    }
}
