﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using System;
using System.Collections.Generic;

namespace SwiftFramework.IdleMiner
{
    public interface ISeasonEvent : ITimeLimit
    {
        IStatefulEvent<bool> IsCleared { get; }
        bool IsTaskCompleted(int index);
        event Action<EventTask> OnTaskCompleted;
        IEnumerable<(bool isCompleted, EventTask config)> GetTasks();
        IPromise<IEnumerable<RewardLink>> ClaimRewards();
        bool IsRewardAvailable();
        string GetTitle();
        void ListenToWorldEvents(IWorld world);
    }
}