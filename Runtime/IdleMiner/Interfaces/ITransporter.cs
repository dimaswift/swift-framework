﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Controllers;
using SwiftFramework.IdleMiner.Model;
using System;

namespace SwiftFramework.IdleMiner
{
    public interface ITransporter : IController, ISubordinate, IUpgradable
    {
        BigNumber LastLoadedAmount { get; }
        int TargetFactoryIndex { get; }
        IZone Zone { get; }
        BigNumber Capacity { get; }
        event Action<BigNumber> OnUnloaded;
        event Action<BigNumber> OnLoaded;
        event Action<(BigNumber amount, float duration)> OnUnloadStarted;
        event Action OnMoveToStart;
        IStatefulEvent <BigNumber> LoadedAmount { get; }
        T GetStatus<T>() where T : struct, Enum;
        event Action<int> OnStatusChanged;
        float TimeNormalized { get; }
        float GetTargetFactoryLoadingTime();
        BigNumber GetTargetFactoryLoadAmount();
        bool IsLoading();
    }

    public delegate IPromise<ITransporter> TransporterHandler(Transporter transporter, TransporterConfigLink config, IZone zone);
}

