﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;

namespace SwiftFramework.IdleMiner
{
    public interface ITutorialController
    {
        void GoToStep(TutorialStepLink step);
        bool IsStepActive(TutorialStepLink step);
        bool IsStepCompleted(TutorialStepLink step);
        bool IsTutorialCompleted();
        IStatefulEvent<TutorialStepLink> CurrentStep { get; }
    }


}