﻿using System;
using SwiftFramework.Core;
namespace SwiftFramework.IdleMiner
{
    public interface IWorkerTransport 
    {
        int Order { get; }
        IFactory Factory { get; }
        BigNumber Power { get; }
        float TimeNormalized { get; }
        event Action OnStarted;
        event Action<IWorkerTransport> OnFinished;
    }
}