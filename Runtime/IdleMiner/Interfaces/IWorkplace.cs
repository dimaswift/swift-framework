﻿using System;
using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;

namespace SwiftFramework.IdleMiner
{
    public interface IWorkplace : IController, ISubordinate, IUpgradable
    {
        event Action OnReloaded;
        IZone Zone { get; }
        WorkplaceConfig Config { get; }
        long RevenueMultiplier { get; }
        bool IsEmpty { get; }
        bool IsReloading { get; }
        event Action<BigNumber> OnCreditsProduced;
        event Action OnRanOutOfResources;
        event Action OnRanOutOfWorkForce;
        IStatefulEvent<BigNumber> WorkForce { get; }
        IStatefulEvent<BigNumber> Resources { get; }
        BigNumber CurrentCapacity { get; }
        void AddWorkForce(BigNumber amount);
        IPromise<T> GetCurrentSkin<T>() where T : WorkplaceSkin;
        IPromise Reload();
    }

    public delegate IPromise<IWorkplace> WorkplaceHandler(WorkplaceConfigLink config, Workplace state, IZone zone);

}

