﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Views;
using System;

namespace SwiftFramework.IdleMiner
{
    public interface IWorld
    {
        IGame Game { get; }
        event Action<IZone> OnZoneActivated;
        IFundsSource Funds { get; }
        IZone ActiveZone { get; }
        void Dispose();
        WorldConfigLink Link { get; }
    }
}

