﻿using System;
using System.Collections.Generic;
using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;

namespace SwiftFramework.IdleMiner
{
    public interface IZone
    {
        ZoneConfig Config { get; }
        ViewLink View { get; }
        Zone State { get; }
        IClock Clock { get; }
        IWorld World { get; }
        event Action OnZoneCreated;
        event Action OnStateSync;
        event Action<BigNumber> OnCreditsProduced;
        event Action<IFactory> OnFactoryUnlocked;
        event Action<IFactory> OnFactoryPurchased;

        FracturedBigNumber ProductionSpeed { get; }

        int FactoryCount { get; }
        int ActiveFactoriesAmount { get; }

        IWorkplace Workplace { get; }
        ITransporter Transporter { get; }
        IFactory GetFactory(int index);
        IBoosterManager Boosters { get; }

        void SyncState(object state);
        void Update(float delta);
        IPromise CreateProductionLoop();
        IEnumerable<IFactory> GetFactories();
        void ActivateAllSupervisors();

        ILink Link { get; }
        FracturedBigNumber TotalFactoryProductionSpeed { get; }

        void Dispose();
    }
}

