using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using System;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Model
{
    [Serializable]
    public class Factory : IUpgradableState, IDeepCopy<Factory>
    {
        public int id;
        public long level;
        public BigNumber incomePerWorker;
        public long batchSize;
        public BigNumber accumulatedWorkForce;
        [HideInInspector] public long unlockTimestamp;

        public float productionTime;
        public bool purchased;
        public float workerTransportTime;
        public long Level => level;

        public int Id => id;

        public SubordinateState subordinate;

        public Factory DeepCopy()
        {
            return new Factory()
            {
                level = level,
                id = id,
                incomePerWorker = incomePerWorker,
                batchSize = batchSize,
                productionTime = productionTime,
                purchased = purchased,
                accumulatedWorkForce = accumulatedWorkForce,
                workerTransportTime = workerTransportTime,
                subordinate = subordinate.DeepCopy(),
                unlockTimestamp = unlockTimestamp,
            };
        }
    }
}
