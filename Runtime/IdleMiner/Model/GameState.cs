﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.IdleMiner.Configs;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Model
{
    [Serializable]
    public class GameState : IDeepCopy<GameState>
    {
        public BoostersState boosters;
        public Tutorial tutorial;
        public long lastSessionTimestamp;
        public long firstSessionTimestamp;
        public List<Zone> zones = new List<Zone>();
        public ZoneConfigLink activeZone;
        public WorldConfigLink activeWorld;
        public SharedSupervisorsManagerState sharedSupervisors = new SharedSupervisorsManagerState();

        public GameState DeepCopy()
        {
            return new GameState()
            {
                zones = zones.DeepCopy(),
                boosters = boosters.DeepCopy(),
                tutorial = tutorial.DeepCopy(),
                lastSessionTimestamp = lastSessionTimestamp,
                activeZone = activeZone,
                sharedSupervisors = sharedSupervisors.DeepCopy(),
                activeWorld = activeWorld,
                firstSessionTimestamp = firstSessionTimestamp,
            };
        }

        public Zone GetZone(ZoneConfigLink zoneLink)
        {
            foreach (var z in zones)
            {
                if (z.link == zoneLink)
                {
                    return z;
                }
            }
            
            return null;
        }

        public void AddZone(Zone zone)
        {
            if (GetZone(zone.link) == null)
            {
                zones.Add(zone);
            }
        }

        public void SetActiveZone(ZoneConfigLink link)
        {
            activeZone = link;
        }

        public Zone FindActiveZone()
        {
            foreach (var z in zones)
            {
                if (z.link == activeZone)
                {
                    return z;
                }
            }
                
            return null;
        }
    }
}
