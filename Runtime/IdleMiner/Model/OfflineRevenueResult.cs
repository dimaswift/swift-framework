﻿namespace SwiftFramework.Core
{
    public struct OfflineRevenueResult
    {
        public long secondsSpentOffline;
        public BigNumber revenueEarned;
    }
}
