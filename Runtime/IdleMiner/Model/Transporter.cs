﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using System;
using UnityEngine.Serialization;

namespace SwiftFramework.IdleMiner.Model
{
    [Serializable]
    public class Transporter : IDeepCopy<Transporter>, IUpgradableState
    {
        public long level;
        public BigNumber capacity;
        [FormerlySerializedAs("loadingRate")]
        public BigNumber loadingSpeed;
        public float moveSpeed;

        public long Level => level;

        public SubordinateState subordinate;
        public SupervisorsPool supervisorsPool;

        public Transporter DeepCopy()
        {
            return new Transporter()
            {
                level = level,
                capacity = capacity, 
                loadingSpeed = loadingSpeed,
                moveSpeed = moveSpeed,
                subordinate = subordinate.DeepCopy(),
                supervisorsPool = supervisorsPool.DeepCopy()
            };
        }
    }
}
