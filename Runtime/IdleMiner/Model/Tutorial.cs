﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using System;

namespace SwiftFramework.IdleMiner.Model
{
    [Serializable]
    public class Tutorial : IDeepCopy<Tutorial>
    {
        public bool isCompleted;
        public TutorialStepLink currentStep;


        public Tutorial DeepCopy()
        {
            return new Tutorial()
            {
                isCompleted = isCompleted,
                currentStep = currentStep
            };
        }
    }
}