﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Model
{
    [System.Serializable]
    public class Workplace : IDeepCopy<Workplace>, IUpgradableState
    {
        [HideInInspector] public int id;
        [HideInInspector] public int skinIndex;
        [HideInInspector] public int progressCounter;
        [Header("Start amount of resources")]
        public BigNumber resources;
        public BigNumber currentCapacity;
        public BigNumber workForceBandwidth;
        public BigNumber workForce;
        public int workersAmount;
        public long level;
        public BigNumber productionSpeed;

        public float workerMoveSpeed;
        [HideInInspector] public SubordinateState subordinate;
        [HideInInspector] public SupervisorsPool supervisorsPool;
        public long Level => level;

        public int Id => id;

        public Workplace DeepCopy()
        {
            return new Workplace()
            {
                level = level,
                id = id,
                productionSpeed = productionSpeed,
                workerMoveSpeed = workerMoveSpeed,
                resources= resources,
                skinIndex = skinIndex,
                progressCounter = progressCounter,
                subordinate = subordinate.DeepCopy(),
                workForce = workForce,
                workForceBandwidth = workForceBandwidth,
                currentCapacity = currentCapacity,
                workersAmount = workersAmount,
                supervisorsPool = supervisorsPool.DeepCopy()
            };
        }
    }
}
