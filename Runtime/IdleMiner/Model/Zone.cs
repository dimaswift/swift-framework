﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.IdleMiner.Configs;
using System;
using System.Collections.Generic;

namespace SwiftFramework.IdleMiner.Model
{
    [Serializable]
    public class Zone : IDeepCopy<Zone>
    {
        public ZoneConfigLink link;
        public ZonePrestigeState prestige;
        public Workplace workplace = new Workplace();
        public Transporter transporter = new Transporter();
        public List<Factory> factories = new List<Factory>();
        public SupervisorsPool supervisorsPool = new SupervisorsPool();
        public long lastRevenueEarnTimestamp;
        public BigNumber idleProductionSpeed;
        public float idleProductionSpeedFloat;

        public Zone DeepCopy()
        {
            return new Zone()
            {
                link = link,
                workplace = workplace.DeepCopy(),
                transporter = transporter.DeepCopy(),
                factories = factories.DeepCopy(),
                prestige = prestige.DeepCopy(),
                supervisorsPool = supervisorsPool.DeepCopy(),
                lastRevenueEarnTimestamp = lastRevenueEarnTimestamp,
                idleProductionSpeed = idleProductionSpeed,
                idleProductionSpeedFloat = idleProductionSpeedFloat
            };
        }
    }

    public enum ZoneStatus
    {
        Locked = 0,
        AvailableToUnlock = 1,
        Unlocked = 2,
    }
}
