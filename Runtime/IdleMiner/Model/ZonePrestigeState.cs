﻿using SwiftFramework.Core;
using SwiftFramework.Core.Prestige;

namespace SwiftFramework.IdleMiner.Model
{
    [System.Serializable]
    public class ZonePrestigeState : PrestigeState, IDeepCopy<ZonePrestigeState>
    {
        public long zonesCleared;

        public new ZonePrestigeState DeepCopy()
        {
            return new ZonePrestigeState()
            {
                notifiedAboutAvailablePresitge = notifiedAboutAvailablePresitge,
                zonesCleared = zonesCleared
            };
        }
    }
}
