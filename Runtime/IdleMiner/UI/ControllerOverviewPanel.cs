﻿using SwiftFramework.Core;
using SwiftFramework.Utils.UI;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{
    public class ControllerOverviewPanel : ElementFor<(IController controller, ISubordinate subordinate)>
    {
        [SerializeField] private GenericText titleText = null;
        [SerializeField] private GenericText productionSpeedText = null;
        [SerializeField] private SupervisorInfoPanel supervisorInfo = null;

        protected override void OnClicked()
        {
            
        }

        public override void Init()
        {
            base.Init();
            if (supervisorInfo)
            {
                supervisorInfo.Init();
            }
        }

        private void OnDisable()
        {
            if (Value.controller == null)
            {
                return;
            }
            Value.controller.ProductionSpeed.OnValueChanged -= ProductionSpeed_OnValueChanged;
        }

        protected override void OnSetUp((IController controller, ISubordinate subordinate) value)
        {
            if (supervisorInfo)
            {
                supervisorInfo.SetUp((value.subordinate.Supervisors, value.subordinate.Supervisors.Current));
            }
            if (titleText.HasValue)
            {
                titleText.Value.Text = value.controller.Title;
            }

            value.controller.ProductionSpeed.OnValueChanged -= ProductionSpeed_OnValueChanged;
            value.controller.ProductionSpeed.OnValueChanged += ProductionSpeed_OnValueChanged;


            productionSpeedText.Value.Text = value.controller.ProductionSpeed.ToString();
        }

        private void ProductionSpeed_OnValueChanged()
        {
            productionSpeedText.Value.Text = Value.controller.ProductionSpeed.ToString();
        }
    }
}

