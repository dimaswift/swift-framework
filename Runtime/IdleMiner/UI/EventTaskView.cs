﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.Utils.UI;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{
    public class EventTaskView : ElementFor<(bool isCompleted, EventTask config)>
    {
        [SerializeField] private GenericText title = null;
        [SerializeField] private GenericText seasonPointsAmount = null;
        [SerializeField] private ElementSet rewards = null;
        [SerializeField] private GameObject[] completedState = null;
        [SerializeField] private GameObject[] notCompletedState = null;


        protected override void OnClicked()
        {
            if (Value.isCompleted)
            {
                App.Core.GetModule<IWindowsManager>().Show<PopUpMessage, string>(App.Core.Local.GetText("#wait_for_event_end_to_claim_rewards"));
            }
            else
            {
                App.Core.GetModule<IWindowsManager>().Show<PopUpMessage, string>(Value.config.GetLongDescription());
            }
        }

        protected override void OnSetUp((bool isCompleted, EventTask config) value)
        {
            title.Value.Text = Value.config.GetShortDescription();
            seasonPointsAmount.Value.Text = App.Core.Local.GetText("#season_pass_points", value.config.seasonPassPoints);

            foreach (GameObject obj in completedState)
            {
                obj.SetActive(value.isCompleted);
            }
            foreach (GameObject obj in notCompletedState)
            {
                obj.SetActive(value.isCompleted == false);
            }

            rewards.SetUp<RewardText, RewardLink>(value.config.rewards);
        }
    }
}
