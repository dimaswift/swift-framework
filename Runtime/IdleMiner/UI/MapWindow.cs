﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Views;
using SwiftFramework.Utils.UI;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.IdleMiner.UI
{
    [AddrSingleton]
    public class MapWindow : WindowWithArgs<IIldeWorldMap>
    {
        [SerializeField] private float sideMenuShowSpeed = 10;

        [SerializeField] private Button showZones = null;
        [SerializeField] private Button closeMapButton = null;
        [SerializeField] private Button hideZones = null;
        [SerializeField] private ElementSet zonesSet = null;
        [SerializeField] private RectTransform zonesContainer = null;

        [SerializeField] private FundsAmountIcon worldOfflineRevenue = null;
        [SerializeField] private Button collectWorldRevenue = null;

        private bool zonesVisible;
        private IIdleGameModule game;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            collectWorldRevenue.onClick.AddListener(OnCollectRevenueClick);
            hideZones.onClick.AddListener(OnHideZonesClick);
            showZones.onClick.AddListener(OnShowZonesClick);
            closeMapButton.onClick.AddListener(HandleCloseButtonClick);
          
            hideZones.image.raycastTarget = false;
        }

        public override void HandleCloseButtonClick()
        {
            zonesVisible = false;
            game.OpenGameStage();
        }

        public override void WarmUp()
        {
            base.WarmUp();
            game = App.Core.GetModule<IIdleGameModule>();
            game.OnZoneUnlocked += Game_OnZoneUnlocked;
        }

        private void Game_OnZoneUnlocked(ILink zoneLink)
        {
            if (IsShown)
            {
                UpdateElements();
            }
        }

        private void OnCollectRevenueClick()
        {
            App.Core.GetModule<IAdsManager>().ShowRewardedWithLoadingWindow("collect_world_offline_revenue").Done(success => 
            {
                if (success)
                {
                    game.ClaimWorldOfflineRevenue(game.GetActiveWorld(), 2);
                    UpdateElements();
                }
            });
        }

        private void OnHideZonesClick()
        {
            hideZones.image.raycastTarget = false;
            zonesVisible = false;
        }

        private void OnShowZonesClick()
        {
            hideZones.image.raycastTarget = true;
            zonesVisible = true;
        }

        public override void OnStartShowing(IIldeWorldMap map)
        {
            base.OnStartShowing(map);
            SetSidePanelPosition(GetZoneTargetX());
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
            UpdateElements();
        }

        public override void OnHidden()
        {
            base.OnHidden();
            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
        }

        private void Now_OnValueChanged(long value)
        {
            UpdateWorldOfflineRevenue();
        }

        private void UpdateElements()
        {
            UpdateWorldOfflineRevenue();
            zonesSet.SetUp<ZoneInfo, Zone>(Arguments.GetActiveZones(), (zone, info) =>
            {
                info.SetUp(zone);
                info.OnPrestigeClick -= OnPrestigeClick;
                info.OnPrestigeClick += OnPrestigeClick;
            });
        }

        private void OnPrestigeClick(Zone zone)
        {
            IPrestigeController prestige = game.GetGameController<GameController>().GetZonePrestige(zone.link);
            App.Core.GetModule<IWindowsManager>().Show<PrestigeWindow, IPrestigeController, bool>(prestige).Done(getPresigeClicked => 
            {
                if (getPresigeClicked)
                {
                    prestige.UpgradePrestige().Done(success =>
                    {
                        if (success)
                        {
                            game.OpenGameStage();
                        }
                    });
                }
            });
        }

        private void Update()
        {
            float targetX = GetZoneTargetX();

            if (Mathf.Approximately(targetX, zonesContainer.anchoredPosition.x) == false)
            {
                Vector3 pos = zonesContainer.anchoredPosition;
                pos.x = Mathf.Lerp(pos.x, targetX, Time.unscaledDeltaTime * sideMenuShowSpeed);
                zonesContainer.anchoredPosition = pos;
            }
        }

        private void SetSidePanelPosition(float x)
        {
            Vector3 pos = zonesContainer.anchoredPosition;
            pos.x = x;
            zonesContainer.anchoredPosition = pos;
        }

        private float GetZoneTargetX()
        {
           return zonesVisible ? -zonesContainer.rect.width : 0;
        }

        private void UpdateWorldOfflineRevenue()
        {
            var revenue = game.GetWorldOfflineRevenue(game.GetActiveWorld());
            worldOfflineRevenue.SetUp((revenue.amount, revenue.funds));
        }

    }
}
