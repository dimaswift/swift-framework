﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Core.Windows;
using SwiftFramework.Utils.UI;
using System;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{
    [AddrSingleton]
    public class OfflineRevenueWindow : WindowWithArgsAndResult<(BigNumber amount, FundsSourceLink source, long duration), bool>
    {
        [SerializeField] private FundsAmountIcon amountText = null;
        [SerializeField] private GenericText durationText = null;
        [SerializeField] private GenericButton doubleRewardButton = null;


        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            doubleRewardButton.Value.AddListener(OnDoubleRewardClick);
        }

        private void OnDoubleRewardClick()
        {
            App.Core.GetModule<IAdsManager>().ShowRewardedWithLoadingWindow("double_offline_revenue").Done(success =>
            {
                Resolve(success);
                Hide();
            });
        }

        public override void HandleCloseButtonClick()
        {
            Resolve(false);
            base.HandleCloseButtonClick();
        }

        public override void OnStartShowing((BigNumber amount, FundsSourceLink source, long duration) arguments)
        {
            base.OnStartShowing(arguments);
            amountText.SetUp((arguments.amount, arguments.source));
            durationText.Value.Text = arguments.duration.ToDurationString(App.Core.Local);
        }
    }
}

