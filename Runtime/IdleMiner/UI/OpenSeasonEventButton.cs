﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.Utils.UI;
using System;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{

    public class OpenSeasonEventButton : MonoBehaviour
    {
        [SerializeField] private GameObject comingSoonState = null;
        [SerializeField] private GameObject validState = null;

        [SerializeField] private GenericButton goToEventButton = null;
        [SerializeField] private GenericText statusText = null;
        [SerializeField] private GenericText nameText = null;
        [SerializeField] private GenericImage icon = null;

        private SeasonEvent seasonEvent;

        private void Awake()
        {
            goToEventButton.Value.AddListener(OnGoToEventClick);

            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                SeasonEventLink currentEventLink = App.Core.GetModule<IIdleGameModule>().GetGameController<IGame>().CurrentEventLink;
                if (currentEventLink != null && currentEventLink.HasValue)
                {
                    seasonEvent = currentEventLink.Value;
                    nameText.Value.Text = App.Core.Local.GetText(currentEventLink.Value.nameKey);
                    SetComingSoon(false);
                    icon.Value.SetSprite(currentEventLink.Value.icon);

                    Now_OnValueChanged(0);
                }
                else
                {
                    SetComingSoon(true);
                }
            });
        }

        private void OnDisable()
        {
            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
        }

        private void OnEnable()
        {
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
            Now_OnValueChanged(0);
        }

        private void SetComingSoon(bool comingSoon)
        {
            validState.SetActive(comingSoon == false);
            comingSoonState.SetActive(comingSoon);
        }

        private void Now_OnValueChanged(long value)
        {
            if (seasonEvent == null)
            {
                return;
            }
            statusText.Value.Text = seasonEvent.GetDescription();
        }

        private void OnGoToEventClick()
        {
            if (seasonEvent != null)
            {
                ISeasonEvent season = App.Core.GetModule<IIdleGameModule>().GetGameController<IGame>().SeasonEvent;
                if(season.TimeTillEnd > 0 || season.IsRewardAvailable())
                {
                    App.Core.GetModule<IWindowsManager>().Show<SeasonEventInfoWindow, ISeasonEvent>(season);
                }
                else
                {
                    App.Core.GetModule<IWindowsManager>().Show<PopUpMessage, string>(App.Core.Local.GetText("event_ended"));
                }
            }
        }
    }
}