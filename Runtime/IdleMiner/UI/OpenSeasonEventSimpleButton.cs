﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.Utils.UI;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{
    public class OpenSeasonEventSimpleButton : MonoBehaviour
    {
        [SerializeField] private GenericButton goToEventButton = null;

        private SeasonEvent seasonEvent;

        private void Awake()
        {
            goToEventButton.Value.AddListener(OnGoToEventClick);

            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                SeasonEventLink currentEventLink = App.Core.GetModule<IIdleGameModule>().GetGameController<IGame>().CurrentEventLink;
                if (currentEventLink != null && currentEventLink.HasValue)
                {
                    seasonEvent = currentEventLink.Value;
                }
            });
        }
        private void OnGoToEventClick()
        {
            if (seasonEvent != null)
            {
                ISeasonEvent season = App.Core.GetModule<IIdleGameModule>().GetGameController<IGame>().SeasonEvent;
                if (season.TimeTillEnd > 0 || season.IsRewardAvailable())
                {
                    App.Core.GetModule<IWindowsManager>().Show<SeasonEventInfoWindow, ISeasonEvent>(season);
                }
                else
                {
                    App.Core.GetModule<IWindowsManager>().Show<PopUpMessage, string>(App.Core.Local.GetText("event_ended"));
                }
            }
            else
            {
                App.Core.GetModule<IWindowsManager>().Show<PopUpMessage, string>(App.Core.Local.GetText("event_ended"));
            }
        }
    }
}