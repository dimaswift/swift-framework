﻿using SwiftFramework.Core;
using SwiftFramework.Core.Views;
using SwiftFramework.Core.Windows;
using SwiftFramework.Utils.UI;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{

    [AddrSingleton]
    public class PrestigeWindow : WindowWithArgsAndResult<IPrestigeController, bool>
    {
        [SerializeField] private PriceView price = null;
        [SerializeField] private ProgressBar prestigeProgress = null;
        [SerializeField] private GenericText currentMultiplier = null;
        [SerializeField] private GenericText nextMultiplier = null;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
        }

        public override void OnStartShowing(IPrestigeController prestige)
        {
            base.OnStartShowing(prestige);

            var next = prestige.GetNextPrestige();
            var current = prestige.GetCurrentPrestige();

            currentMultiplier.Value.Text = $"x {current.multiplier.ToString("0.0")}";
            nextMultiplier.Value.Text = $"x {next.multiplier.ToString("0.0")}";

            price.Init(next.price, OnGetPrestigeClick);

            prestigeProgress.Value.SetUp(current.level, prestige.GetMaxLevel());
        }

        public override void HandleCloseButtonClick()
        {
            Resolve(false);
            base.HandleCloseButtonClick();
        }

        private void OnGetPrestigeClick()
        {
            var prestige = Arguments.GetNextPrestige();
            if (prestige.price.Value.CanAfford(prestige.price.amount) == false)
            {
                PopUpMessage.ShowNoGold();
                return;
            }
            Resolve(true);
            Hide();
        }
    }
}
