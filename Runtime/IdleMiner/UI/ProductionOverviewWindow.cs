﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using SwiftFramework.Utils.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{

    [AddrSingleton]
    public class ProductionOverviewWindow : WindowWithArgs<IZone>
    {
        [SerializeField] private ControllerOverviewPanel workplacePanel = null;
        [SerializeField] private ControllerOverviewPanel transporterPanel = null;

        [SerializeField] private InfiniteElementSet factoriesPanelSet = null;
        [SerializeField] private GenericText totalFactoryProductionText = null;
        [SerializeField] private GenericButton activateAllManagersButton = null;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            workplacePanel.Init();
            transporterPanel.Init();
            activateAllManagersButton.Value.AddListener(OnActivateAllClick);

        }

        private void OnActivateAllClick()
        {
            Arguments.ActivateAllSupervisors();
        }

        public override void OnStartShowing(IZone zone)
        {
            base.OnStartShowing(zone);

            workplacePanel.SetUp((zone.Workplace, zone.Workplace)); 
            transporterPanel.SetUp((zone.Transporter, zone.Transporter));

            totalFactoryProductionText.Value.Text = zone.TotalFactoryProductionSpeed.ToString();

            zone.TotalFactoryProductionSpeed.OnValueChanged -= TotalFactoryProductionSpeed_OnValueChanged;
            zone.TotalFactoryProductionSpeed.OnValueChanged += TotalFactoryProductionSpeed_OnValueChanged;

            factoriesPanelSet.SetUp<ControllerOverviewPanel, (IController, ISubordinate)>(GetPurchasedFactories(), InitFactory);
        }

        public override void OnStartHiding()
        {
            base.OnStartHiding();
            Arguments.TotalFactoryProductionSpeed.OnValueChanged -= TotalFactoryProductionSpeed_OnValueChanged;
        }

        private void TotalFactoryProductionSpeed_OnValueChanged()
        {
            totalFactoryProductionText.Value.Text = Arguments.TotalFactoryProductionSpeed.ToString();
        }

        private void InitFactory((IController, ISubordinate) data, ControllerOverviewPanel panel)
        {
            panel.SetUp(data);
        }

        private void Update()
        {
            factoriesPanelSet.UpdateElements<ControllerOverviewPanel, (IController, ISubordinate)>();
        }

        private IEnumerable<(IController, ISubordinate)> GetPurchasedFactories()
        {
            foreach (var f in Arguments.GetFactories())
            {
                if (f.CurrentPurchaseStatus.Value == PurchaseStatus.Purchased)
                {
                    yield return (f, f);
                }
            }
        }
    }
}

