﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.Utils.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{

    [AddrSingleton]
    public class SeasonEventInfoWindow : WindowWithArgs<ISeasonEvent>
    {
        [SerializeField] private GenericText eventTitle = null;
        [SerializeField] private GenericText eventDescription = null;
        [SerializeField] private GenericText timeTillEnd = null;
        [SerializeField] private GenericButton claimRewardsButton = null;
        [SerializeField] private GenericButton enterEventButton = null;
        [SerializeField] private ElementSet tasks = null;



        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            claimRewardsButton.Value.AddListener(OnClaimRewardsClick);
            enterEventButton.Value.AddListener(OnEnterEventClick);
        }

        private void OnEnterEventClick()
        {
            IIdleGameModule idleGame = App.Core.GetModule<IIdleGameModule>();
            if (!idleGame.TrySetSeasonEventActive())
            {
                Hide();
            }
        }

        private void OnClaimRewardsClick()
        {
            Arguments.ClaimRewards().Then(rewards =>
            {
                windowsManager.Show<RewardAnounceWindow, IEnumerable<RewardLink>>(rewards);
                Hide();
            })
            .Catch(e =>
             {
                 PopUpMessage.ShowLocalized("no_tasks_completed");
                 Hide();
             });
        }

        public override void OnStartShowing(ISeasonEvent seasonEvent)
        {
            base.OnStartShowing(seasonEvent);
            eventTitle.Value.Text = seasonEvent.GetTitle();
            timeTillEnd.Value.Text = seasonEvent.TimeTillEnd.ToDurationString(App.Core.Local);
            eventDescription.Value.Text = seasonEvent.GetDescription();
            claimRewardsButton.Value.Interactable = seasonEvent.TimeTillEnd <= 0;
            tasks.SetUp<EventTaskView, (bool isCompleted, EventTask task)>(seasonEvent.GetTasks());
            enterEventButton.GameObject.SetActive(seasonEvent.TimeTillEnd > 0);
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
            UpdateTimers();
        }

        public override void OnStartHiding()
        {
            base.OnStartHiding();
            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
        }

        private void UpdateTimers()
        {
            if (Arguments.TimeTillEnd > 0)
            {
                timeTillEnd.Value.Text = Arguments.TimeTillEnd.ToDurationString(App.Core.Local);
            }
            else
            {
                timeTillEnd.Value.Text = App.Core.Local.GetText("event_ended");
            }
        }

        private void Now_OnValueChanged(long value)
        {
            UpdateTimers();
        }
    }
}
