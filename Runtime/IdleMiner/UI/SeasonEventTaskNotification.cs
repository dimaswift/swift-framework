﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{
    public class SeasonEventTaskNotification : AppListener
    {
        [SerializeField] private EventTaskView taskView = null;
        [SerializeField] private float appearTime = .5f;
        [SerializeField] private AppearAnimationHandler appearAnimation = null;
        [SerializeField] private Effect effect = null;

        private IIdleGameModule game;

        protected override void OnAppInitialized()
        {
            game = App.Core.GetModule<IIdleGameModule>();
            game.OnGameCreated += Game_OnGameCreated;
            taskView.Init();
            taskView.gameObject.SetActive(false);
        }

        private void Game_OnGameCreated()
        {
            ISeasonEvent seasonEvent = game.GetGameController<IGame>().SeasonEvent; 
            if (seasonEvent != null)
            {
                seasonEvent.OnTaskCompleted += SeasonEvent_OnTaskCompleted;
            }
        }

        private void SeasonEvent_OnTaskCompleted(Configs.EventTask task)
        {
            taskView.SetUp((true, task));
            appearAnimation.Value.ShowUntilTap(appearTime, () => taskView.gameObject.SetActive(false));
            taskView.gameObject.SetActive(true);
            if (effect.HasValue)
            {
                effect.Value.Play(transform.position);
            }
        }

    }
}
