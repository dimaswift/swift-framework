﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData;
using System.Collections;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{

    public class SeasonEventTimerButton : MonoBehaviour
    {
        [SerializeField] private GenericText timeTillEnd = null;
        [SerializeField] private GenericButton openTasksButton = null;

        private ISeasonEvent seasonEvent;
        private IIdleGameModule game;


        private void Awake()
        {
            openTasksButton.Value.AddListener(OnOpenTasksClick);

            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                game = App.Core.GetModule<IIdleGameModule>();
                game.OnZoneChanged += OnZoneChanged;
                game.OnSeasonEventFinished += Game_OnSeasonEventFinished;
                OnZoneChanged();
            });
        }

        private void Game_OnSeasonEventFinished()
        {
            App.Core.GetModule<IIdleGameModule>().OpenMap().Done(() => 
            {
                App.Core.GetModule<IWindowsManager>().Show<SeasonEventInfoWindow, ISeasonEvent>(App.Core.GetModule<IIdleGameModule>().GetGameController<IGame>().SeasonEvent);
            });
            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
        }

        private void OnZoneChanged()
        {
            if (App.Core.GetModule<IIdleGameModule>().IsSeasonEventZoneActive())
            {
                seasonEvent = App.Core.GetModule<IIdleGameModule>().GetGameController<IGame>().SeasonEvent;
                gameObject.SetActive(true);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        private void OnEnable()
        {
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;

            UpdateTimer();
        }

        private void Now_OnValueChanged(long value)
        {
            UpdateTimer();
        }

        private void OnDisable()
        {
            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
        }

        private void OnOpenTasksClick()
        {
            App.Core.GetModule<IWindowsManager>().Show<SeasonEventInfoWindow, ISeasonEvent>(App.Core.GetModule<IIdleGameModule>().GetGameController<IGame>().SeasonEvent);
        }

        private void UpdateTimer()
        {
            if (seasonEvent == null)
            {
                return;
            }

            timeTillEnd.Value.Text = seasonEvent.TimeTillEnd.ToDurationString(App.Core.Local);
        }
    }
}
