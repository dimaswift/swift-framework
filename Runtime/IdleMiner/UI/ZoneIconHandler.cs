﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{
    public class ZoneIconHandler : MonoBehaviour, ILinkIconHandler
    {
        public SpriteLink GetIcon(ILink link)
        {
            if (link == null)
            {
                return null;
            }
            ZoneConfigLink zoneLink = link as ZoneConfigLink;
            if (zoneLink == null)
            {
                Debug.LogError($"{link} of type {link.GetType()} is not a ZoneConfigLink");
                return null;
            }
            return zoneLink.Value.icon;
        }
    }
}
