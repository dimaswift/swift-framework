﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.Utils.UI;
using System;
using UnityEngine;

namespace SwiftFramework.IdleMiner.UI
{
    public class ZoneInfo : ElementFor<Zone>
    {
        public event Action<Zone> OnPrestigeClick = z => { };

        [SerializeField] private GenericImage icon = null;
        [SerializeField] private GenericButton prestigeButton = null;
        [SerializeField] private GenericText offlineRevenue = null;
        [SerializeField] private GenericText zoneName = null;
        [SerializeField] private GenericText productionSpeed = null;
        [SerializeField] private GenericText multipliers = null;
        [SerializeField] private GenericText prestige = null;

        private IIdleGameModule game;

        public override void Init()
        {
            base.Init();
            game = App.Core.GetModule<IIdleGameModule>();
            prestigeButton.Value.AddListener(HandlePrestigeClick);
           
        }

        private void Now_OnValueChanged(long value)
        {
            if (Value != null)
            {
                offlineRevenue.Value.Text = game.GetOfflineRevenue(Value.link, true).amount.ToString();
            }
        }

        private void OnEnable()
        {
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
        }

        private void OnDisable()
        {
            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
        }

        private void HandlePrestigeClick()
        {
            OnPrestigeClick(Value);
        }

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp(Zone zone)
        {
            zone.link.Load(config => 
            {
                icon.Value.SetSprite(config.icon);
                zoneName.Value.Text = App.Core.Local.GetText(zone.link.GetName() + "_title");
                offlineRevenue.Value.Text = game.GetOfflineRevenue(zone.link, true).amount.ToString();
                prestige.Value.Text = App.Core.Local.GetText("#prestige_level", zone.prestige.level);
                string idleProductionSpeed = Value.idleProductionSpeed < 1000 
                ? App.Core.Local.GetText(FracturedBigNumber.PRODUCTION_SPEED, Value.idleProductionSpeedFloat.ToString("0.00")) :
                App.Core.Local.GetText(FracturedBigNumber.PRODUCTION_SPEED, Value.idleProductionSpeed);
                multipliers.Value.Text = $"x {game.GetZoneRevenueMultiplier(Value.link)}";
                productionSpeed.Value.Text = idleProductionSpeed;
            });
        }
    }
}
