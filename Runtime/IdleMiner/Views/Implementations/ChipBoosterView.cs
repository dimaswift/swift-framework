﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Utils.UI;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Views
{
    public class ChipBoosterView : ElementFor<BoosterConfigLink>
    {
        [SerializeField] private GenericImage spriteRenderer = null;

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp(BoosterConfigLink boosterConfigLink)
        {
            spriteRenderer.Value.SetSprite(boosterConfigLink.Value.icon);
        }
    }
}
