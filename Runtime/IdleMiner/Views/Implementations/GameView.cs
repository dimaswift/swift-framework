﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Core.Views;
using SwiftFramework.Utils.UI;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Views
{
    public class GameView : View, IGameView
    {
        [SerializeField] protected ElementSet chipBoosters = null;
        [SerializeField] protected BoosterTargetLink chipBoosterTarget = null;

        public IZoneView ZoneView { get; private set; }

        public ITransporterView TransporterView { get; private set; }

        public IWorkplaceView WorkplaceView { get; private set; }

        private Dictionary<IFactory, IFactoryView> factories = new Dictionary<IFactory, IFactoryView>();

        private readonly List<IFactory> factoriesBuffer = new List<IFactory>();

        protected IZone zone;
        private readonly List<long> activeMultipliers = new List<long>();
        private readonly List<BoosterConfigLink> boostersBuffer = new List<BoosterConfigLink>();

        public IPromise<IZoneView> Render(IZone zone)
        {
            this.zone = zone;

            zone.Boosters.OnMultipliersUpdated -= Boosters_OnMultipliersUpdated;
            zone.Boosters.OnMultipliersUpdated += Boosters_OnMultipliersUpdated;

            Boosters_OnMultipliersUpdated();

            Promise<IZoneView> promise = Promise<IZoneView>.Create();

            RenderZone(zone).Done(zoneView =>
            {
                RenderWorkplace(zone.Workplace).Done(workplaceView =>
                {
                    RenderTransporter(zone.Transporter).Done(transporterView =>
                    {
                        RenderFactories(zone).Done(() =>
                        {
                            promise.Resolve(zoneView);
                        });
                    });
                });
            });

            return promise;
        }

        private void Boosters_OnMultipliersUpdated()
        {
            chipBoosters.SetUp<ChipBoosterView, BoosterConfigLink>(GetActiveChipBoosters());
        }

        protected IEnumerable<BoosterConfigLink> GetActiveChipBoosters()
        {
            activeMultipliers.Clear();
            boostersBuffer.Clear();
            foreach (Booster booster in zone.Boosters.GetActiveBoosters(BoosterType.Temporary, chipBoosterTarget))
            {
                if (activeMultipliers.Contains(booster.link.Value.multiplier))
                {
                    continue;
                }
                activeMultipliers.Add(booster.link.Value.multiplier);
                boostersBuffer.Add(booster.link);
            }

            boostersBuffer.Sort((b1, b2) => b1.Value.multiplier.CompareTo(b2.Value.multiplier));

            return boostersBuffer;
        }

        private IPromise<IWorkplaceView> RenderWorkplace(IWorkplace workplace)
        {
            WorkplaceView?.ReturnToPool();

            Promise<IWorkplaceView> promise = Promise<IWorkplaceView>.Create();

            App.Core.Views.CreateAsync<IWorkplaceView>(workplace.View).Done(workplaceView =>
            {
                WorkplaceView = workplaceView;
                WorkplaceView.Init(workplace, this).Done(() =>
                {
                    promise.Resolve(WorkplaceView);
                });

            });
            return promise;
        }

        private IPromise RenderFactories(IZone zone)
        {
            factories.Clear();

            Promise promise = Promise.Create();
            factoriesBuffer.Clear();
            factoriesBuffer.AddRange(zone.GetFactories());

            CreateNextFactory(promise, 0);

            return promise;
        }

        private void CreateNextFactory(Promise promise, int index)
        {
            if(index >= factoriesBuffer.Count)
            {
                promise.Resolve();
                return;
            }

            IFactory factory = factoriesBuffer[index];
            App.Core.Views.CreateAsync<IFactoryView>(factory.View).Done(factoryView =>
            {
                factoryView.Init(factory, this).Done(() =>
                {
                    factories.Add(factory, factoryView);
                    CreateNextFactory(promise, ++index); 
                });
            });
        }

        private IPromise<ITransporterView> RenderTransporter(ITransporter transporter)
        {
            TransporterView?.ReturnToPool();

            Promise<ITransporterView> promise = Promise<ITransporterView>.Create();

            App.Core.Views.CreateAsync<ITransporterView>(transporter.View).Done(transporterView =>
            {
                TransporterView = transporterView;
                transporterView.Init(transporter, this).Done(() =>
                {
                    promise.Resolve(TransporterView);
                });

            });
            return promise;
        }

        private IPromise<IZoneView> RenderZone(IZone zone)
        {
            ZoneView?.ReturnToPool();

            Promise<IZoneView> promise = Promise<IZoneView>.Create();

            App.Core.Views.CreateAsync<IZoneView>(zone.View).Done(zoneView =>
            {
                ZoneView = zoneView;
                ZoneView.Init(zone, this);
                promise.Resolve(ZoneView);

            });
            return promise;
        }

        public IFactoryView GetFactory(IFactory factory)
        {
            if (factories.TryGetValue(factory, out IFactoryView factoryView))
            {
                return factoryView;
            }
            return null;
        }

        public IPromise CloseStage(IStageManager stageManager)
        {
            foreach (IView view in ChildViews())
            {
                view.Active = false;
            }

            return Promise.Resolved();
        }

        private IEnumerable<IView> ChildViews()
        {
            yield return ZoneView;
            yield return TransporterView;
            yield return WorkplaceView;
            foreach (var factory in factories)
            {
                yield return factory.Value;
            }
        }

        public IPromise OpenStage(IStageManager stageManager)
        {
            if (ZoneView == null)
            {
                return Promise.Resolved();
            }

            foreach (IView view in ChildViews())
            {
                view.Active = true;
            }

            return Promise.Resolved();
            
        }

        public IPromise UnloadStage(IStageManager stageManager)
        {
            foreach (IView view in ChildViews())
            {
                view.ReturnToPool();
            }
            ZoneView = null;
            TransporterView = null;
            WorkplaceView = null;
            factories.Clear();

            return Promise.Resolved();
        }
    }
}
