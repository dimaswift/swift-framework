﻿using SwiftFramework.Core;

namespace SwiftFramework.IdleMiner.Views
{
    public interface IControllerView<T> : IView
    {
        IPromise Init(T controller, IGameView gameView);
    }
}
