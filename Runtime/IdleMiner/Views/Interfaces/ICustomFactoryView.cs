﻿using SwiftFramework.Core;
using SwiftFramework.Core.Views;
using UnityEngine;

public interface ICustomFactoryView : IView
{
    GameObject PurchasedState { get; }
    CustomUnlockerView AvailableToPuchaseState { get; }
    GameObject LockedState { get; }

}
