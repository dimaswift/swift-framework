﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Views
{
    public interface IFactoryView : IControllerView<IFactory>
    {
        Vector3 GetProductionStartPosition();
        Vector3 GetProductionTargetPosition();
        Vector3 GetTransporterLoadPosition();
    }
}
