﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Views
{
    public interface IGameView : IView, IStage
    { 
        IZoneView ZoneView { get; }
        ITransporterView TransporterView { get; }
        IWorkplaceView WorkplaceView { get; }
        IFactoryView GetFactory(IFactory factory);
        IPromise<IZoneView> Render(IZone zone);
    }
}
