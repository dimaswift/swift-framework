﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using System.Collections.Generic;

namespace SwiftFramework.IdleMiner.Views
{
    public interface IIldeWorldMap : IView, IStage
    {
        IStatefulEvent<Zone> SelectedZone { get; }
        void Select(Zone zone);
        void Enter(Zone zone);
        Zone GetZone(ZoneConfigLink link);
        IIdleGameModule Game { get; }
        IEnumerable<Zone> GetActiveZones();
    }
}
