﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Views
{
    public interface ITransporterView : IControllerView<ITransporter>
    {
        void Board(IWorkerTransportView workerTransportView);
        bool HasBoardedWorker { get; }
        void UpdateWorkersAmountText(BigNumber amount);
        Vector3 GetBoardingPoint();
    }
}
