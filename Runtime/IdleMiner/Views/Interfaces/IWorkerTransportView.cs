﻿using SwiftFramework.Core;

namespace SwiftFramework.IdleMiner.Views
{
    public interface IWorkerTransportView : IView
    {
        void Init(IWorkerTransport workerTransport, IGameView gameView);
        IWorkerTransport Controller { get; }
    }
}
