﻿using SwiftFramework.Core;

namespace SwiftFramework.IdleMiner.Views
{
    public interface IWorkplaceView : IControllerView<IWorkplace>
    {

    }
}
