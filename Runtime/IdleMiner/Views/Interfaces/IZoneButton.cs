﻿using SwiftFramework.Core;
using SwiftFramework.IdleMiner.Configs;

namespace SwiftFramework.IdleMiner.Views
{
    public interface IZoneButton : IView
    {
        void Init(ZoneConfigLink zone);
    }
}
