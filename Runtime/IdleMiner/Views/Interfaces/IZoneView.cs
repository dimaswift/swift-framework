﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Views
{
    public interface IZoneView : IView
    {
        void Init(IZone zone, IGameView gameView);
        Vector3 GetFactoryPosition(long index);
        Vector3 GetWorkplacePosition();
        Vector3 GetTransporterPosition();
        Vector3 GetWorkForceTextPosition();
        float GetFactoriesBottomPoint();
        void SetBattleCloudShown(bool shown, bool animate);
    }
}
