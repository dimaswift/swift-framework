﻿using SwiftFramework.Core;
using SwiftFramework.Core.Views;
using SwiftFramework.IdleMiner;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.Views;
using SwiftFramework.Utils;
using SwiftFramework.Utils.UI;
using System;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Views
{
    public class ZoneContainer : MonoBehaviour
    {
        [SerializeField] private ZoneConfigLink zoneLink = Link.CreateNull<ZoneConfigLink>();
        [SerializeField] private GameObject selectionFrame = null;
        [SerializeField] private GameObject lockState = null;
        [SerializeField] private GameObject unlockedState = null;
        [SerializeField] private GameObject availableState = null;

        [SerializeField] private Transform infoPanel = null;
        [SerializeField] private GenericText nameText = null;
        [SerializeField] private Physics2DButton selectButton = null;
        [SerializeField] private Physics2DButton enterButton = null;
        [SerializeField] private PriceView unlockPrice = null;
        [SerializeField] private FundsAmountIcon offlineRevenueText = null;

        private ZoneConfig zoneConfig;

        private Zone zone;

        private IIldeWorldMap map;

        private bool selected;

        private GameController game;

        private void Awake()
        {
            selectButton.AddListener(OnClick);
            enterButton.AddListener(OnEnterClick);

        }

        private void OnEnterClick()
        {
            if (zone != null && selected)
            {
                map.Enter(zone);
            }
        }

        private void OnClick()
        {
            if (zone != null)
            {
                map.Select(zone);
            }
        }

        public IPromise Init(IIldeWorldMap map)
        {
            Promise promise = Promise.Create();

            this.map = map;
            game = map.Game.GetGameController<GameController>();

            infoPanel.localScale = Vector3.zero;
            map.SelectedZone.OnValueChanged -= SelectedZone_OnValueChanged;
            map.SelectedZone.OnValueChanged += SelectedZone_OnValueChanged;

            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;

            map.Game.OnOfflineRevenueClaimed -= Game_OnOfflineRevenueClaimed;
            map.Game.OnOfflineRevenueClaimed += Game_OnOfflineRevenueClaimed;

            map.Game.OnZoneUnlocked -= Render;
            map.Game.OnZoneUnlocked += Render;

            zoneLink.Load(zoneConfig =>
            {
                this.zoneConfig = zoneConfig;
                zone = map.GetZone(zoneLink);
                Render(zoneLink);
                promise.Resolve();
            },
            e => gameObject.SetActive(false));
            return promise;
        }

        private void Game_OnOfflineRevenueClaimed(ILink zoneLink)
        {
            Render(zoneLink);
        }

        private void Now_OnValueChanged(long value)
        {
            UpdateOfflineRevenueText();
        }

        private bool UpdateOfflineRevenueText()
        {
            if (zone == null)
            {
                return false;
            }
            var revenue = map.Game.GetOfflineRevenue(zone.link, true);

            if (revenue.amount == 0)
            {
                return false;
            }

            if (offlineRevenueText.gameObject.activeSelf == false)
            {
                offlineRevenueText.gameObject.SetActive(true);
            }

            offlineRevenueText.SetUp((revenue.amount, revenue.funds));
            return true;
        }

        private void SelectedZone_OnValueChanged(Zone value)
        {
            SetSelected(value.link == zoneLink);
        }

        private void Render(ILink zoneLink)
        {
            nameText.Value.Text = App.Core.Local.GetText(zoneConfig.name + "_title");

            ZoneStatus status = game.GetZoneStatus(this.zoneLink);


            unlockedState.SetActive(status == ZoneStatus.Unlocked);
            lockState.SetActive(status == ZoneStatus.Locked);
            availableState.SetActive(status == ZoneStatus.AvailableToUnlock);

            if (zone != null)
            {

                bool showOfflineRevenue = UpdateOfflineRevenueText();
                offlineRevenueText.gameObject.SetActive(showOfflineRevenue);
                SetSelected(zone.link == map.SelectedZone.Value.link);
            }
            else
            {
                unlockPrice.Init(zoneConfig.unlockPrice, OnUnlockClick);
                SetSelected(false);
            }
        }

        private void OnUnlockClick()
        {
            map.Game.UnlockZone(zoneLink).Done(success =>
            {
                zone = map.GetZone(zoneLink);
                Render(zoneLink);
            });
        }

        public void SetSelected(bool selected)
        {
            this.selected = selected;
            selectionFrame.SetActive(selected);
        }

        private void Update()
        {
            Vector3 targetScale = selected ? Vector3.one : Vector3.zero;
            infoPanel.localScale = Vector3.Lerp(infoPanel.localScale, targetScale, Time.deltaTime * 10);
        }
    }
}