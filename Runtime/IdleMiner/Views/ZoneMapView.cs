﻿using GoblinMiner.Core;
using SwiftFramework.Core;
using SwiftFramework.Core.Pooling;
using SwiftFramework.Core.Views;
using SwiftFramework.IdleMiner;
using SwiftFramework.IdleMiner.Configs;
using SwiftFramework.IdleMiner.Model;
using SwiftFramework.IdleMiner.UI;
using SwiftFramework.IdleMiner.Views;
using SwiftFramework.Utils.UI;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.IdleMiner.Views
{
    public class ZoneMapView : View, IIldeWorldMap, IStage
    {
        [SerializeField] private OpenSeasonPassButton seasonPassButton = null;

        private List<ZoneContainer> zones = new List<ZoneContainer>();

        public IStatefulEvent<Zone> SelectedZone => selected;

        public IIdleGameModule Game { get; private set; }

        public IPooled Root => this;

        private GameState state;

        private readonly StatefulEvent<Zone> selected = new StatefulEvent<Zone>();

        private readonly List<IPromise> promiseBuffer = new List<IPromise>();

        private IStageManager stageManager;

        protected override void OnInit()
        {
            base.OnInit();
            zones.Clear();
            GetComponentsInChildren(zones);
            seasonPassButton.OnGoToEventClick += SeasonPassButton_OnGoToEventClick;
            seasonPassButton.OnOpenSeasonViewClick += SeasonPassButton_OnOpenSeasonViewClick;
        }

        private void SeasonPassButton_OnOpenSeasonViewClick()
        {
            Game.OpenSeasonPass();
        }

        private void SeasonPassButton_OnGoToEventClick()
        {
            App.Core.GetModule<IWindowsManager>().Show<SeasonEventInfoWindow, ISeasonEvent>(App.Core.GetModule<IIdleGameModule>().GetGameController<IGame>().SeasonEvent);
        }

        public void SetState(GameState state)
        {
            this.state = state;
        }

        public override void ReturnToPool()
        {
            base.ReturnToPool();
        }

        public void Select(Zone zone)
        {
            selected.SetValue(zone);
        }

        public void Enter(Zone zone)
        {
            Game.SetActiveZone(zone.link);
        }

        public Zone GetZone(ZoneConfigLink link)
        {
            return state.GetZone(link);
        }

        public IEnumerable<Zone> GetActiveZones()
        {
            return state.zones;
        }

        public IPromise OpenStage(IStageManager stageManager)
        {
            Game = App.Core.GetModule<IIdleGameModule>();
            this.stageManager = stageManager;
            Active = true;
            state = Game.GetState<GameState>();
            App.Core.Windows.Show<MapWindow, IIldeWorldMap>(this);
            selected.SetValue(state.FindActiveZone());
            Promise promise = Promise.Create();
            promiseBuffer.Clear();

            foreach (var z in zones)
            {
                promiseBuffer.Add(z.Init(this));
            }

            promise.Resolve();

            return Promise.All(promiseBuffer);
        }

        public IPromise CloseStage(IStageManager stageManager)
        {
            Active = false;
            return Promise.Resolved();
        }


        public IPromise UnloadStage(IStageManager stageManager)
        {
            ReturnToPool();
            return Promise.Resolved();
        }
    }
}