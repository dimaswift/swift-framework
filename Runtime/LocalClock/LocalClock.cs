﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using System;

namespace SwiftFramework.ServerTime
{
    [DefaultModule]
    [DisallowCustomModuleBehaviours]
    public class LocalClock : BehaviourModule, IClock
    {
        public IStatefulEvent<long> Now
        {
            get
            {
                Update();
                return now;
            }
        }

        private long prevNow;
        private long startTime;

        private readonly StatefulEvent<long> now = new StatefulEvent<long>(0);
   
        private float startTimeOffset;

        protected override IPromise GetInitPromise()
        {
            Promise result = Promise.Create();
            GetUnixNow().Done(now => 
            {
                startTime = now;
                startTimeOffset = Time.time;
                result.Resolve();
            });
            return result;
        }

        private void Update()
        {
            if(Initialized == false)
            {
                return;
            }
            var newNow = DateTimeOffset.Now.ToUnixTimeSeconds();
            if (prevNow != newNow)
            {
                prevNow = newNow;
                now.SetValue(newNow);
            }
        }

        public IPromise<DateTime> GetNow()
        {
            return Promise<DateTime>.Resolved(DateTime.Now);
        }

        public long GetSecondsLeft(long timeStamp)
        {
            if(Now.Value > timeStamp)
            {
                return 0;
            }

            return timeStamp - Now.Value;
        }

        public IPromise<long> GetUnixNow()
        {
            return Promise<long>.Resolved(DateTimeOffset.Now.ToUnixTimeSeconds());
        }
    }
}