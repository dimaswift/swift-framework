﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEditor;
using System;

namespace SwiftFramework.Localization
{
    [CustomEditor(typeof(LocalizationConfig))]
    public class LocalizationConfigEditor : UnityEditor.Editor
    {
        private bool downloading;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GUI.enabled = downloading == false;
            if (GUILayout.Button("Download"))
            {
                downloading = true;
                var config = target as LocalizationConfig;
                Download(config.publishedGoogleSheetUrl, () => downloading = false);
            }
        }

        [ContextMenu("SwiftFramework/Localization/Download To Resources")]
        public static void Download()
        {
            var config = EditorUtils.Util.GetAsset<LocalizationConfig>();

            Download(config.publishedGoogleSheetUrl, () => { });
        }

        private static void Download(string url, Action action)
        {
            UnityWebRequest request = UnityWebRequest.Get(url);

            var operation = request.SendWebRequest();

            operation.completed += response =>
            {
                if (string.IsNullOrEmpty(request.downloadHandler.text) == false)
                {
                    string path = Application.dataPath + "/Resources/localization.csv";
                    File.WriteAllText(path, request.downloadHandler.text);
                    AssetDatabase.Refresh();
                    Debug.Log($"<color=green>Localization downloaded to {path}</color>");
                    action();
                }
            };
        }
    }

}
