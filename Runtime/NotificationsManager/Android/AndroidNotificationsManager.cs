﻿using System;
using Unity.Notifications.Android;
using UnityEngine;
using SwiftFramework.Core;
using System.Collections.Generic;

namespace SwiftFramework.Notifications
{
    [Configurable(typeof(NotificationsConfig))]
    [DependsOnModules(typeof(ILocalizationManager))]
    public class AndroidNotificationsManager : Module, INotificationsManager
    {
        public AndroidNotificationsManager(ModuleConfigLink config)
        {
            SetConfig(config);
        }

        private NotificationsCache notificationsCache;

        private NotificationsCache NotificationsCache
        {
            get
            {
                if(notificationsCache == null)
                {
                    notificationsCache = CreateNotificationsCache();
                }
                return notificationsCache;
            }
        }

        private NotificationsConfig config;

        public bool Enabled
        {
            get
            {
                return NotificationsCache.enabled;
            }
            set
            {
                if(NotificationsCache.enabled != value)
                {
                    NotificationsCache.enabled = value;
                    App.Storage.Save(NotificationsCache);
                }
            }
        }

        protected override IPromise GetInitPromise()
        {
            AndroidNotificationCenter.Initialize();

            config = GetModuleConfig<NotificationsConfig>();

            RegisterChannel(config.MainChannel);
            
            AndroidNotificationCenter.CancelAllDisplayedNotifications();

            AndroidNotificationCenter.CancelAllScheduledNotifications();

            AndroidNotificationCenter.CancelAllNotifications();

            AndroidNotificationCenter.OnNotificationReceived += OnNotificationReceived;

            CheckIfAppOpenedFromNotification();

            SubscribeNotificationReceivedCallback();

            App.Boot.OnPaused += Boot_OnPaused;

            foreach (NotificationLink notification in config.GetComeBackNotifications())
            {
                TryClearNotification(notification.Id);
            }

            return base.GetInitPromise();
        }

        private NotificationsCache CreateNotificationsCache()
        {
            notificationsCache = App.Storage.Exists<NotificationsCache>()
                   ? App.Storage.Load<NotificationsCache>()
                   : new NotificationsCache();

            return notificationsCache;
        }

        private void Boot_OnPaused()
        {
            List<NotificationLink> notifications = new List<NotificationLink>(config.GetComeBackNotifications());
            notifications.Shuffle();
            int i = 0;
            foreach (int hours in config.GetComebackMessageIntervals())
            {
                if(i >= notifications.Count)
                {
                    i = 0;
                }
                Schedule(notifications[i++], hours * 60 * 60);
            }
        }

        private void RegisterChannel(NotificationChannel channelConfig)
        {
            AndroidNotificationChannel channel = new AndroidNotificationChannel()
            {
                Id = channelConfig.id,
                Name = App.Local.GetText(channelConfig.nameKey),
                Importance = GetImportance(channelConfig.importance),
                Description = App.Local.GetText(channelConfig.description),
            };

            AndroidNotificationCenter.RegisterNotificationChannel(channel);
        }

        private Importance GetImportance(NotificationImportance importance)
        {
            switch (importance)
            {
                case NotificationImportance.None:
                    return Importance.None;
                case NotificationImportance.Low:
                    return Importance.Low;
                case NotificationImportance.High:
                    return Importance.High;
                default:
                    return Importance.Default;
            }
        }

        public override void Unload()
        {
            AndroidNotificationCenter.OnNotificationReceived -= OnNotificationReceived;
            base.Unload();
        }

        private void OnNotificationReceived(AndroidNotificationIntentData data)
        {
            AndroidNotificationCenter.CancelNotification(data.Id);
        }

        public void Schedule(ILink notificationLink, long secondsDelay)
        {
            if(CanSchedule(secondsDelay, out DateTime localTime) == false)
            {
                return;
            }
            NotificationLink notification = notificationLink as NotificationLink;
            SetNewNotification(GenerateNotification(notification, localTime), notification.Id);
        }

        private void TryClearNotification(string id)
        {
            if (NotificationsCache.TryGetId(id, out int notificationId) == true)
            {
                AndroidNotificationCenter.CancelNotification(notificationId);
            }
        }

        private void SetNewNotification(AndroidNotification notification, string id)
        {
            TryClearNotification(id);

            int notificationId = AndroidNotificationCenter.SendNotification(notification, config.MainChannel.id);

            NotificationsCache.SetId(id, notificationId);

            Debug.Log($"Notification scheduled: {id}, time: {notification.FireTime}");

            App.Storage.Save(NotificationsCache);
        }

        private void CheckIfAppOpenedFromNotification()
        {
            AndroidNotificationIntentData notificationIntentData = AndroidNotificationCenter.GetLastNotificationIntent();

            if (notificationIntentData != null)
            {
                int id = notificationIntentData.Id;
                string channel = notificationIntentData.Channel;
                AndroidNotification notification = notificationIntentData.Notification;
                Debug.Log($"app opened from notification: id {id} from channel {channel}, intent data is: {notification.IntentData}");
            }
        }

        private AndroidNotification GenerateNotification(NotificationLink link, DateTime time)
        {
            return new AndroidNotification()
            {
                Title = App.Local.GetText(link.Value.titleKey),
                Text = App.Local.GetText(link.Value.textKey),
                FireTime = time,
                SmallIcon = link.Value.smallIconId,
                LargeIcon = link.Value.largeIconId,
                IntentData = $"type:{link} time:{time}"
            };
        }

        private void SubscribeNotificationReceivedCallback()
        {
            AndroidNotificationCenter.NotificationReceivedCallback receivedNotificationHandler =
                delegate (AndroidNotificationIntentData data)
                {
                    string msg =
                        $"Notification received : {data.Id}\n" +
                        $"\n" +
                        $" Notification received: \n" +
                        $" .Title: {data.Notification.Title}\n" +
                        $" .Body: {data.Notification.Text}\n" +
                        $" .Channel: {data.Channel}";
                    Debug.Log(msg);
                };

            AndroidNotificationCenter.OnNotificationReceived += receivedNotificationHandler;
        }

        public void Cancel(ILink notificationLink)
        {
            TryClearNotification((notificationLink as NotificationLink).Id);
        }

        public void ScheduleDefault(string titleKey, string messageKey, long secondsDelay)
        {
            if(CanSchedule(secondsDelay, out DateTime localTime) == false)
            {
                return;
            }

            string id = titleKey + messageKey;
           
            SetNewNotification(GenerateDefaultNotification(localTime, titleKey, messageKey), id);
        }

        private bool CanSchedule(long secondsDelay, out DateTime localTime)
        {
            if (Enabled == false)
            {
                localTime = new DateTime();
                return false;
            }

            if (secondsDelay < config.MinSecondsUntilFirstNotification)
            {
                secondsDelay = config.MinSecondsUntilFirstNotification;
            }

            long time = DateTimeOffset.Now.ToUnixTimeSeconds() + secondsDelay;

            localTime = DateTimeOffset.FromUnixTimeSeconds(time).DateTime.ToLocalTime();

            return true;
        }

        private AndroidNotification GenerateDefaultNotification(DateTime time, string titleKey, string messageKey)
        {
            return new AndroidNotification()
            {
                Title = App.Local.GetText(titleKey),
                Text = App.Local.GetText(messageKey),
                FireTime = time,
                SmallIcon = config.DefaultSmallIcon,
                LargeIcon = config.DefaultLargeIcon,
                IntentData = $"type:{titleKey} time:{time}"
            };
        }
    }
}
