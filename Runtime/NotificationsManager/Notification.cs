﻿using UnityEngine;

namespace SwiftFramework.Notifications
{
    public class Notification : ScriptableObject
    {
        public string titleKey;
        public string textKey;
        public string smallIconId;
        public string largeIconId;
    }
}
