﻿using UnityEngine;

namespace SwiftFramework.Notifications
{
    public class NotificationChannel : ScriptableObject
    {
        public string id;
        public string nameKey;
        public string description;
        public NotificationImportance importance = NotificationImportance.High;
    }

    public enum NotificationImportance
    {
        None, Low, High
    }
}
