﻿using SwiftFramework.Core;
using System;

namespace SwiftFramework.Notifications
{
    [Serializable]
    [LinkFolder(Folders.Configs)]
    public class NotificationLink : LinkTo<Notification>
    {
        public string Id => GetPath();
    }

    [Serializable]
    [LinkFolder(Folders.Configs)]
    public class NotificationChannelLink : LinkTo<NotificationChannel>
    {

    }
}
