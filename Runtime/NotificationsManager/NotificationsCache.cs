﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Notifications
{
    [Serializable]
    public class NotificationsCache
    {
        public bool enabled = true;


        [Serializable]
        private struct NotificationCache
        {
            public string notificationId;
            public int id;
        }

        [SerializeField] private List<NotificationCache> notifications = new List<NotificationCache>();

        public bool TryGetId(string notificationId, out int id)
        {
            foreach (NotificationCache n in notifications)
            {
                if (n.notificationId == notificationId)
                {
                    id = n.id;
                    return true;
                }
            }

            id = -1;
            return false;
        }

        public void SetId(string notificationId, int id)
        {
            notifications.RemoveAll(x => x.notificationId == notificationId);
            notifications.Add(new NotificationCache() { notificationId = notificationId, id = id });
        }
    }
}
