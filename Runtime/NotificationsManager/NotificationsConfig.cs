﻿using SwiftFramework.Core;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Notifications
{
    public class NotificationsConfig : ModuleConfig
    {
        public string DefaultSmallIcon => defaultSmallIcon;

        public string DefaultLargeIcon => defaultLargeIcon;

        public int MinSecondsUntilFirstNotification => minSecondsUntilFirstNotification;

        public NotificationChannel MainChannel => defaultChannel.Value;

        public IEnumerable<NotificationLink> GetComeBackNotifications() => comeBackNotifications;

        public IEnumerable<int> GetComebackMessageIntervals() => comeBackHoursIntervals;

        [SerializeField] private NotificationLink[] comeBackNotifications = { };

        [SerializeField] private int[] comeBackHoursIntervals = { };

        [SerializeField] private NotificationChannelLink defaultChannel = null;

        [SerializeField] private int minSecondsUntilFirstNotification = 60 * 5;

        [SerializeField] private string defaultSmallIcon = "icon_small";

        [SerializeField] private string defaultLargeIcon = "icon_large";
    }
}
