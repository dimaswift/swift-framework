﻿using System;
using SwiftFramework.Core;
using System.Collections.Generic;
using Unity.Notifications.iOS;

namespace SwiftFramework.Notifications
{
    [Configurable(typeof(NotificationsConfig))]
    [DependsOnModules(typeof(ILocalizationManager), typeof(ISerializator))]
    public class IOSNotificationsManager : Module, INotificationsManager
    {
        public IOSNotificationsManager(ModuleConfigLink config)
        {
            SetConfig(config);
        }

        private NotificationsCache notificationsCache;

        private NotificationsCache NotificationsCache
        {
            get
            {
                if(notificationsCache == null)
                {
                    notificationsCache = CreateNotificationsCache();
                }
                return notificationsCache;
            }
        }

        private NotificationsConfig config;

        public bool Enabled
        {
            get
            {
                return NotificationsCache.enabled;
            }
            set
            {
                if(NotificationsCache.enabled != value)
                {
                    NotificationsCache.enabled = value;
                    App.Serializator.Serialize(NotificationsCache);
                }
            }
        }


        protected override IPromise GetInitPromise()
        {
            using (var req = new AuthorizationRequest(AuthorizationOption.Alert | AuthorizationOption.Badge, false))
            {
                App.Timer.WaitUntil(() => req.IsFinished).Done(() => 
                {
                    NotificationsCache.enabled = req.Granted;
                    App.Serializator.Serialize(notificationsCache);
                });
            }

            config = GetModuleConfig<NotificationsConfig>();

            App.Boot.OnPaused += Boot_OnPaused;


            foreach (NotificationLink notification in config.GetComeBackNotifications())
            {
                TryClearNotification(notification.Id);
            }

            return base.GetInitPromise();
        }

        private NotificationsCache CreateNotificationsCache()
        {
            notificationsCache = App.Serializator.Exists<NotificationsCache>()
                   ? App.Serializator.Deserialize<NotificationsCache>()
                   : new NotificationsCache();

            return notificationsCache;
        }

        private void Boot_OnPaused()
        {
            List<NotificationLink> notifications = new List<NotificationLink>(config.GetComeBackNotifications());
            notifications.Shuffle();
            int i = 0;
            foreach (int hours in config.GetComebackMessageIntervals())
            {
                if(i >= notifications.Count)
                {
                    i = 0;
                }
                Schedule(notifications[i++], hours * 60 * 60);
            }
        }

        public void Schedule(ILink notificationLink, long secondsDelay)
        {
            if(CanSchedule(secondsDelay, out DateTime localTime) == false)
            {
                return;
            }
            NotificationLink notification = notificationLink as NotificationLink;
            iOSNotificationCenter.ScheduleNotification(GenerateNotification(notification, secondsDelay));
        }

        private void TryClearNotification(string id)
        {
            iOSNotificationCenter.RemoveScheduledNotification(id);
        }

        private iOSNotification GenerateNotification(NotificationLink link, long seconds)
        {
            return new iOSNotification()
            {
                Title = App.Local.GetText(link.Value.titleKey),
                Body = App.Local.GetText(link.Value.textKey),
                Identifier = link.Id,
                Trigger = new iOSNotificationTimeIntervalTrigger()
                {
                    TimeInterval = TimeSpan.FromSeconds(seconds),
                    Repeats = false,
                },
                Badge = 0,
                ShowInForeground = false
            };
        }

        public void Cancel(ILink notificationLink)
        {
            TryClearNotification((notificationLink as NotificationLink).Id);
        }

        public void ScheduleDefault(string titleKey, string messageKey, long secondsDelay)
        {
            if(CanSchedule(secondsDelay, out DateTime localTime) == false)
            {
                return;
            }

            string id = titleKey + messageKey;

            iOSNotification notification = new iOSNotification()
            {
                Title = App.Local.GetText(titleKey),
                Body = App.Local.GetText(messageKey),
                Identifier = id,
                Trigger = new iOSNotificationTimeIntervalTrigger()
                {
                    TimeInterval = TimeSpan.FromSeconds(secondsDelay),
                    Repeats = false,
                },
                Badge = 0,
                ShowInForeground = false
            };

            iOSNotificationCenter.ScheduleNotification(notification);
        }

        private bool CanSchedule(long secondsDelay, out DateTime localTime)
        {
            if (Enabled == false)
            {
                localTime = new DateTime();
                return false;
            }

            if (secondsDelay < config.MinSecondsUntilFirstNotification)
            {
                secondsDelay = config.MinSecondsUntilFirstNotification;
            }

            long time = DateTimeOffset.Now.ToUnixTimeSeconds() + secondsDelay;

            localTime = DateTimeOffset.FromUnixTimeSeconds(time).DateTime.ToLocalTime();

            return true;
        }

        private iOSNotification GenerateDefaultNotification(string titleKey, string messageKey)
        {
            return new iOSNotification()
            {
                Title = App.Local.GetText(titleKey),
                Body = App.Local.GetText(messageKey),
                Identifier = titleKey + messageKey
            };
        }
    }
}
