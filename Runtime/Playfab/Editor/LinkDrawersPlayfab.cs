//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwiftFramework.Core.Editor
{
    using UnityEditor;
    
    
    [CustomPropertyDrawer(typeof(SwiftFramework.Playfab.PlayfabLeaderboardConfigLink))]
    public class PlayfabLeaderboardConfigLinkDrawer : LinkPropertyDrawer<SwiftFramework.Playfab.PlayfabLeaderboardConfig>
    {
    }
    
    [CustomPropertyDrawer(typeof(SwiftFramework.Playfab.ScoresHandlerLink))]
    public class ScoresHandlerLinkDrawer : LinkPropertyDrawer<SwiftFramework.Playfab.ScoresHandler>
    {
    }
}
