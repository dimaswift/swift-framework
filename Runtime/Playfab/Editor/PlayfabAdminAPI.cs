﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SwiftFramework.Core;
using SwiftFramework.EditorUtils;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace SwiftFramework.Playfab.Editor
{
    [Configurable(typeof(PlayfabConfig))]
    public class PlayfabAdminAPI : Module, ICloudStorage
    {
        public PlayfabAdminAPI(ModuleConfigLink config)
        {
            SetConfig(config);
        }

        [Serializable]
        private struct UploadRequest
        {
            public string Key;
            public string Value;
        }

        [Serializable]
        private struct SetTitleDataRequest
        {
            public string Key;
            public string Value;
        }

        [Serializable]
        private struct GetTitleDataRequest
        {
            public string[] Keys;
        }

        [Serializable]
        private class UploadResponse
        {
            public int code = 0;
            public string status = null;
            public Data data = default;
            [Serializable]
            public class Data
            {
                public string URL = null;
            }
        }

        public const string SERVER = "Server";
        public const string CLIENT = "Client";
        public const string ADMIN = "Admin";

        public IPromise<string> UploadFile(string path, string destination, string contentType)
        {
            Promise<string> promise = Promise<string>.Create();

            PlayfabConfig config = GetModuleConfig<PlayfabConfig>();

            if (config == null)
            {
                return Promise<string>.Rejected(new FileNotFoundException("Playfab config not found!"));
            }

            UploadRequest reqData = new UploadRequest()
            {
                Value = contentType,
                Key = destination
            };

            PostAdmin(reqData, "GetContentUploadUrl", ADMIN).Then(json =>
            {
                UploadResponse uploadResponse;
                try
                {
                    uploadResponse = JsonUtility.FromJson<UploadResponse>(json);
                }
                catch (System.Exception e)
                {
                    promise.Reject(e);
                    return;
                }

                var getUrlRequest = (HttpWebRequest)WebRequest.Create($"https://{config.titleId}.playfabapi.com/{ADMIN}/");
                var uploadRequest = (HttpWebRequest)WebRequest.Create(uploadResponse.data.URL);
                uploadRequest.Method = "PUT";
                uploadRequest.ContentType = contentType;

                Stream dataStream = uploadRequest.GetRequestStream();
                byte[] data = File.ReadAllBytes(path);
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();

                HttpWebResponse response = (HttpWebResponse)uploadRequest.GetResponse();

                if (response != null && response.StatusCode == HttpStatusCode.OK)
                {
                    promise.Resolve(destination);
                }
                else
                {
                    promise.Reject(new System.Exception(response != null ? response.StatusDescription : "empty response"));
                }
            })
            .Catch(e => promise.Reject(e));

            return promise;
        }

        public IPromise<byte[]> DownloadRaw(string path)
        {
            throw new System.NotImplementedException();
        }

        private IPromise<string> Post(object requet, string method, string type)
        {
            Promise<string> promise = Promise<string>.Create();

            PlayfabConfig config = GetModuleConfig<PlayfabConfig>();

            if (config == null)
            {
                return Promise<string>.Rejected(new FileNotFoundException("Playfab config not found!"));
            }

            var req = new HttpClient();
            var msg = new HttpRequestMessage(HttpMethod.Post, new Uri($"https://{config.titleId}.playfabapi.com/{type}/{method}"));
            msg.Content = new StringContent(JsonConvert.SerializeObject(requet, Formatting.None), Encoding.UTF8, "application/json");
            msg.Headers.Add("X-SecretKey", config.secretKey);
            var resp = req.SendAsync(msg);
            resp.ContinueWith(res =>
            {
                res.Result.Content.ReadAsStringAsync().ContinueWith(s => { promise.Resolve(s.Result); });
            });
            return promise;
        }

        private IPromise<string> PostAdmin<T>(T request, string method, string type)
        {
            Promise<string> promise = Promise<string>.Create();

            PlayfabConfig config = GetModuleConfig<PlayfabConfig>();

            if (config == null)
            {
                return Promise<string>.Rejected(new FileNotFoundException("Playfab config not found!"));
            }

            new System.Threading.Thread(() =>
            {
                var getUrlRequest = (HttpWebRequest)WebRequest.Create($"https://{config.titleId}.playfabapi.com/{type}/{method}");
                getUrlRequest.Method = "POST";
                getUrlRequest.ContentType = ContentType.Json;
                getUrlRequest.Headers.Add("X-SecretKey", config.secretKey);
                Stream postDataStream = getUrlRequest.GetRequestStream();
                byte[] postData = Encoding.UTF8.GetBytes(JsonUtility.ToJson(request, false));
                postDataStream.Write(postData, 0, postData.Length);
                postDataStream.Close();

                HttpWebResponse getUrlResponse = (HttpWebResponse)getUrlRequest.GetResponse();

                if (getUrlResponse != null && getUrlResponse.StatusCode == HttpStatusCode.OK)
                {
                    new System.Threading.Thread(() =>
                    {
                        try
                        {
                            Stream str = getUrlResponse.GetResponseStream();

                            TextReader tr = new StreamReader(str);

                            string responseJson = tr.ReadToEnd();

                            promise.Resolve(responseJson, true);
                        }
                        catch (Exception e)
                        {

                            promise.Reject(e, true);
                        }

                    }).Start();
                }
                else
                {
                    promise.Reject(new Exception(getUrlResponse != null ? getUrlResponse.StatusDescription : "empty response"), true);
                }

            }).Start();

            return promise;
        }

        public IPromise UploadConfig(ScriptableObject configToUpload)
        {
            Promise promise = Promise.Create();

            Type type = configToUpload.GetType();

            PlayfabConfig config = GetModuleConfig<PlayfabConfig>();

            if (config == null)
            {
                return Promise.Rejected(new FileNotFoundException("Playfab config not found!"));
            }

            SetTitleDataRequest reqData = new SetTitleDataRequest()
            {
                Value = JsonUtility.ToJson(configToUpload),
                Key = type.Name
            };

            PostAdmin(reqData, "SetTitleData", SERVER).Then(response =>
            {
                Debug.Log(response);
                promise.Resolve();
            }
            , e => promise.Reject(e));

            return promise;
        }


        public IPromise DownloadConfig(ScriptableObject configToOverride)
        {
            Promise result = Promise.Create();

            PlayfabConfig config = GetModuleConfig<PlayfabConfig>();

            if (config == null)
            {
                return Promise.Rejected(new FileNotFoundException("Playfab config not found!"));
            }

            string key = configToOverride.GetType().Name;

            GetTitleDataRequest reqData = new GetTitleDataRequest()
            {
                Keys = new[] { key }
            };

            PostAdmin(reqData, "GetTitleData", SERVER).Then(response =>
            {
                try
                {
                    JObject obj = JObject.Parse(response);
                    if (obj.Value<int>("code") != 200)
                    {
                        result.Reject(new EntryPointNotFoundException($"Config with key '{key}' not found"));
                    }
                    else
                    {
                        JObject root = obj.Value<JObject>("data");
                        JObject data = root.Value<JObject>("Data");
                        string json = data.Value<string>(key);
                        if (string.IsNullOrEmpty(json) == false)
                        {
                            JsonUtility.FromJsonOverwrite(json, configToOverride);
                            result.Resolve();
                        }
                        else
                        {
                            result.Reject(new EntryPointNotFoundException($"Config with key '{key}' not found. {response}"));
                        }
                    }
                }
                catch (Exception e)
                {
                    Debug.Log($"Cannot parse response: {response}");
                    result.Reject(e);
                }
            }
            , e => result.Reject(e));

            return result;
        }

        public IPromise DeleteAccount()
        {
            Promise result = Promise.Create();

            PlayfabConfig config = GetModuleConfig<PlayfabConfig>();

            if (config == null)
            {
                return Promise.Rejected(new FileNotFoundException("Playfab config not found!"));
            }

            var request = new { CreateAccount = false, CustomId = config.debugEditorDeviceId, TitleId = config.titleId };

            Post(request, "LoginWithCustomID", "Client").Then(r =>
            {
                var json = JObject.Parse(r);
                if (json.Value<int>("code") == 200)
                {
                    var playfabId = json.Value<JObject>("data").Value<string>("PlayFabId");

                    Post(new { PlayFabId = playfabId }, "DeletePlayer", "Server").Then(deleteResp =>
                    {
                        if (JObject.Parse(deleteResp).Value<int>("code") == 200)
                        {
                            result.Resolve();
                        }
                        else
                        {
                            result.Reject(new Exception(JObject.Parse(deleteResp).Value<string>("errorMessage")));
                        }
                    })
                    .Catch(e => { Debug.LogException(e); });
                }
                else
                {
                    Debug.LogError($"{r}");
                }
            })
            .Catch(e => { Debug.LogException(e); });


            return result;
        }

        [MenuItem("CONTEXT/ScriptableObject/Upload To Playfab")]
        private static void UploadScriptableMenuItem(MenuCommand menuCommand)
        {
            PlayfabConfig config = Util.PromptChooseScriptable<PlayfabConfig>();

            if (config == null)
            {
                return;
            }

            new PlayfabAdminAPI(Link.Create<ModuleConfigLink>(Folders.Configs, config)).UploadConfig(menuCommand.context as ScriptableObject)
                .Then(() =>
                {
                    Debug.Log("Config uploaded");
                },
                e => Debug.LogException(e));
        }

        [MenuItem("CONTEXT/ScriptableObject/Download From Playfab")]
        private static void DownloadScriptableMenuItem(MenuCommand menuCommand)
        {
            PlayfabConfig config = Util.PromptChooseScriptable<PlayfabConfig>();
            if (config == null)
            {
                Debug.LogError("PlayfabConfig not found");
                return;
            }

            new PlayfabAdminAPI(Link.Create<ModuleConfigLink>(Folders.Configs, config)).DownloadConfig(menuCommand.context as ScriptableObject)
                .Then(() =>
                {
                    EditorUtility.SetDirty(menuCommand.context);
                    Debug.Log("Config Downloaded");
                },
                e => Debug.LogException(e));
        }

        [MenuItem("Roblominer/Playfab/Delete Editor Player Account")]
        private static void DeleteEditorPlayerAccount(MenuCommand menuCommand)
        {
            PlayfabConfig config = Util.PromptChooseScriptable<PlayfabConfig>();
            if (config == null)
            {
                Debug.LogError("PlayfabConfig not found");
                return;
            }

            new PlayfabAdminAPI(Link.Create<ModuleConfigLink>(Folders.Configs, config)).DeleteAccount()
                .Then(() =>
                {
                    Debug.Log("Playfab account and PlayerPrefs deleted");
                    PlayerPrefs.DeleteAll();
                    PlayerPrefs.Save();

                })
                .Catch(e => Debug.LogException(e));
        }

    }

}