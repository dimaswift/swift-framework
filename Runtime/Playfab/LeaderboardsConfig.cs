﻿using SwiftFramework.Core;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Playfab
{
    [CreateAssetMenu(menuName = "SwiftFramework/Playfab/LeaderboardsConfig")]
    public class LeaderboardsConfig : ModuleConfig
    {
        public List<PlayfabLeaderboardConfigLink> leaderboards;
        public ScoresHandlerLink scoresHandler;
    }
}