﻿using PlayFab;
using UnityEngine;
using SwiftFramework.Core;
using PlayFab.ClientModels;
using System.Collections.Generic;
using System;

namespace SwiftFramework.Playfab
{
    [Configurable(typeof(PlayfabConfig))]
    [DependsOnModules(typeof(INetworkManager), typeof(IPrivacyPolicyProvider))]
    public class PlayfabAuthManager : Module, ICloudAuthentication
    {
        public event Action OnLogin = () => { };
        public event Action OnLogout = () => { };
        public event Action<ICloudProfile> OnProfileChanged = p => { };
        public event Action<string> OnAppRedirectionReceived = id => { };
        public event Action OnLoginAttempt = () => {};
        public event Action OnUserOpenedWebInterface = () => { };

        public PlayfabAuthManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }
            
        private const string SaveDataKey = "save_data";

        public IStatefulEvent<bool> Connected => connected;

        public bool LoggedIn => profile != null;

        public ICloudProfile Profile => profile;

        private PlayfabConfig config;

        private StatefulEvent<bool> connected = new StatefulEvent<bool>();

        private PlayfabProfile profile;

        private string loggedInCustomId;


        private static readonly GetPlayerCombinedInfoRequestParams infoRequestParameters = new GetPlayerCombinedInfoRequestParams()
        {
            GetPlayerProfile = true,
            GetUserAccountInfo = true
        };

        protected override IPromise GetInitPromise()
        {
            Promise initPromise = Promise.Create();

            if (App.Storage.Exists<PlayfabProfile>())
            {
                profile = App.Storage.Load<PlayfabProfile>();
            }

            config = GetModuleConfig<PlayfabConfig>();

            if (IsConfigValid(config) == false)
            {
                initPromise.Reject(new InvalidOperationException("PlayfabAuthManager is not configured"));
                return initPromise;
            }

            PlayFabSettings.TitleId = config.titleId;

            IPrivacyPolicyProvider privacyPolicy = App.GetModule<IPrivacyPolicyProvider>();

            IPromise<bool> privacyPromise = privacyPolicy != null ? privacyPolicy.Comply() : Promise<bool>.Resolved(true);

            if (privacyPolicy == null && config.useAdvertisementdIdForAuth)
            {
                Debug.LogError("Using advertisement id without IPrivacyPolicyProvider. Consider adding it!");
            }

            privacyPromise.Done(complied =>
            {
                if (complied == true)
                {
                    LoginWithDeviceId().Always(res =>
                    {
                        initPromise.Resolve();
                    });
                }
                else
                {
                    Application.Quit();
                }
            });

            return initPromise;
        }

        private bool IsConfigValid(PlayfabConfig config)
        {
            if (string.IsNullOrEmpty(config.titleId))
            {
                LogError("You need to specify Playfab Title Id in the PlayfabConfig");
                return false;
            }
            return true;
        }

        private void ValidateProductionBundleId()
        {
            Core.App.InitPromise.Done(() =>
            {
                GetTitleData("production_bundle_id").Done(id =>
                {
                    if (id != App.Boot.GlobalConfig.bundleId)
                    {
                        Log($"Production bundle ids does not match! Actual bundle id: {id}");
                        OnAppRedirectionReceived(id);
                    }
                });
            });
        }

        private IPromise<string> GetTitleData(string key)
        {
            Promise<string> promise = Promise<string>.Create();

            GetTitleDataRequest request = new GetTitleDataRequest()
            {
                Keys = new List<string>() { key }
            };

            PlayFabClientAPI.GetTitleData(request, result => 
            {
                if(result.Data != null && result.Data.TryGetValue(key, out string data))
                {
                    promise.Resolve(data);
                }
                else
                {
                    promise.Reject(new Exception("Empty response"));
                }
            }, 
            error => 
            {
                promise.Reject(new Exception(error.Error.ToString()));
            });

            return promise;
        }

       
        public void CheckLogin<T>(Action callback, Promise<T> promiseToReject)
        {
            if (LoggedIn && Connected.Value)
            {
                callback();
                return;
            }
            Login().Done(r =>
            {
                if (r == CloudActionResult.Success)
                {
                    callback();
                }
                else
                {
                    promiseToReject.Reject(new Exception(r.ToString()));
                }
            });
        }

        public IPromise<CloudActionResult> LoginWithDeviceId()
        {
            Promise<CloudActionResult> promise = Promise<CloudActionResult>.Create();

            GetAdvertisingId().Done(advertisingId =>
            {
                LoginWithCustomId(advertisingId, true).Then(result =>
                {
                    promise.Resolve(result);
                })
                .Catch(e =>
                {
                    promise.Reject(e);
                });
            });

            return promise;
        }

        private IPromise LinkNewCustomId(string newId)
        {
            Promise promise = Promise.Create();
            LinkCustomIDRequest request = new LinkCustomIDRequest()
            {
                CustomId = newId
            };
            PlayFabClientAPI.LinkCustomID(request, 
                result =>
                {
                    loggedInCustomId = newId;
                    promise.Resolve();
                }, 
                error =>
                {
                    promise.Reject(new Exception(error.ErrorMessage));
                });
            return promise;
        }

        public IPromise<SetDisplayNameResult> SetDisplayName(string newName)
        {
            Promise<SetDisplayNameResult> promise = Promise<SetDisplayNameResult>.Create();

            if(Connected.Value == false)
            {
                promise.Reject(new Exception("Not connected"));
                return promise;
            }

            UpdateUserTitleDisplayNameRequest request = new UpdateUserTitleDisplayNameRequest()
            {
                DisplayName = newName
            };

            PlayFabClientAPI.UpdateUserTitleDisplayName(request,
                result =>
                {
                    Log($"DisplayName changed: {result.ToJson()}");
                    if (profile != null)
                    {
                        profile.SetName(result.DisplayName);
                        OnProfileChanged(profile);
                    }
                    promise.Resolve(SetDisplayNameResult.Success);
                   
                },
                error =>
                {
                    Log($"Error trying to set DisplayName: {error.Error}");
                    promise.Resolve(SetDisplayNameResult.UnknownError);
                });

            return promise;
        }

        private new void Log(string message)
        {
            App.Timer.WaitForMainThread().Done(() => base.Log($"PLAYFAB: {message}"));
        }

        private IPromise<CloudActionResult> LoginWithCustomId(string id, bool createAccount)
        {
            OnLoginAttempt();

            Promise<CloudActionResult> promise = Promise<CloudActionResult>.Create();

           
            Log($"Trying to log into playfab with id '{id}'...Create account: {createAccount}");

            LoginWithCustomIDRequest customRequest = new LoginWithCustomIDRequest()
            {
                CustomId = id,
                TitleId = config.titleId,
                CreateAccount = createAccount,
                InfoRequestParameters = infoRequestParameters
            };

            PlayFabClientAPI.LoginWithCustomID(customRequest, result =>
            {
                Dispatcher.RunOnMainThread(() =>
                {
                    loggedInCustomId = id;
                    ProcessProfile(result.NewlyCreated, result.InfoResultPayload, promise);

                    Log($"Logged in using custom device id: new account: {result.NewlyCreated}, '{id}'. json: {result.ToJson()}");
                });
            },
            error =>
            {
                Dispatcher.RunOnMainThread(() =>
                {
                    Log($"Login using custom device id failed: {error?.Error}");
                    connected.SetValue(false);
                    promise.Resolve(GetLoginResultFromCode(error.Error));
                });
            });
            

            Promise<CloudActionResult> timeout = Promise<CloudActionResult>.Create();
            App.Timer.WaitForUnscaled(5).Done(() =>
            {
                if (promise.CurrentState == PromiseState.Pending)
                {
                    timeout.Resolve(CloudActionResult.Timeout);
                }
            });

            return Promise<CloudActionResult>.Race(timeout, promise);
        }

        private CloudActionResult GetLoginResultFromCode(PlayFabErrorCode code)
        {
            switch (code)
            {
                case PlayFabErrorCode.AccountDeleted:
                case PlayFabErrorCode.AccountNotFound:
                    return CloudActionResult.AccountNotFound;
                default:
                    return CloudActionResult.UnknownError;
            }
        }

        private void ProcessProfile(bool newlyCreated, GetPlayerCombinedInfoResultPayload info, Promise<CloudActionResult> promise)
        {
            if (info == null)
            {
                Log($"ProcessPlayerInfo failed! Empty payload");
                promise.Reject(new EntryPointNotFoundException("PLAYFAB: Empty payload!"));
                return;
            }

            profile = new PlayfabProfile(info, App.Net);
           
            OnLogin();

            App.Storage.Save(profile);

            if (config.validateProductionBundleId)
            {
                ValidateProductionBundleId();
            }

            connected.SetValue(true);

            promise.Resolve(CloudActionResult.Success, true);
        }

        private IPromise<PlayerProfileModel> GetProfile(string playFabId)
        {
            Promise<PlayerProfileModel> promise = Promise<PlayerProfileModel>.Create();

            GetPlayerProfileRequest request = new GetPlayerProfileRequest()
            {
                PlayFabId = playFabId
            };

            PlayFabClientAPI.GetPlayerProfile(request,
                result =>
                {
                    promise.Resolve(result.PlayerProfile);
                },
                error => 
                {
                    promise.Reject(new Exception(error?.ErrorMessage));
                });

            return promise;
        }

        public IPromise UploadData(string key, string data)
        {
            Promise promise = Promise.Create();
            UpdateUserDataRequest request = new UpdateUserDataRequest()
            {
                Data = new Dictionary<string, string>() { { key, data } },
                Permission = UserDataPermission.Public,
            };
            PlayFabClientAPI.UpdateUserData(request, result =>
            {
                Log($"data saved: {result.ToJson()}");
                promise.Resolve();
            },
            error => 
            {
                Log($"data saving error: {error.ErrorMessage}");
                promise.Reject(new Exception(error.ErrorMessage));
            });
            return promise;
        }

        public IPromise<string> GetData(string key)
        {
            Promise<string> promise = Promise<string>.Create();
            GetUserDataRequest request = new GetUserDataRequest()
            {
                Keys = new List<string>() { key }
            };
            PlayFabClientAPI.GetUserData(request, result =>
            {
                if(result != null && result.Data.TryGetValue(key, out UserDataRecord record))
                {
                    promise.Resolve(record.Value);
                }
                else
                {
                    Log($"Cannot get data: {key}");
                    promise.Reject(new Exception($"Cannot get data with key {key}"));
                }
            },
            error =>
            {
                Log($"Cannot get data: {error.ErrorMessage}");
                promise.Reject(new Exception(error.ErrorMessage));
            });
            return promise;
        }

        public IPromise<CloudActionResult> Login()
        {
            if(LoggedIn && Connected.Value && PlayFabClientAPI.IsClientLoggedIn())
            {
                return Promise<CloudActionResult>.Resolved(CloudActionResult.Success);
            }
            return LoginWithDeviceId();
        }

        private T GetLocalState<T>(Func<T> defaultSave, out bool newSaveCreated)
        {
            T state;

            if (App.Storage.Exists<T>() == false)
            {
                newSaveCreated = true;
                state = defaultSave();
                App.Storage.Save(state);
            }
            else
            {
                newSaveCreated = false;
                state = App.Storage.Load<T>();
            }
            return state;
        }

        public IPromise<T> GetSaveData<T>(Func<T> defaultSave) where T : ICloudSave
        {
            Promise<T> promise = Promise<T>.Create();
            T localState = GetLocalState(defaultSave, out bool defaultStateCreated);
            Login().Then(result =>
            {
                if (result == CloudActionResult.Success)
                {
                    if (defaultStateCreated)
                    {
                        DownloadSaveData<T>().Then(cloudState =>
                        {
                            promise.Resolve(cloudState);
                        })
                        .Catch(e =>
                        {
                            UploadSaveData(localState);
                            promise.Resolve(localState);
                        });
                    }
                    else
                    {
                        promise.Resolve(localState);
                    }
                }
                else
                {
                    promise.Resolve(localState);
                }
            })
            .Catch(e =>
            {
                promise.Resolve(localState);
            });
            return promise;
        }

        public IPromise<ICloudSave> DownloadSaveData(Type type)
        {
            Promise<ICloudSave> promise = Promise<ICloudSave>.Create();

            if(type.GetInterface(typeof(ICloudSave).Name) == null)
            {
                promise.Reject(new Exception($"PLAYFAB: Save file of type {type.Name} should implement IPlayfabCloudSave interface!"));
                return promise;
            }

            if(LoggedIn == false)
            {
                promise.Reject(new Exception("PLAYFAB: Not logged in"));
                return promise;
            }

            GetData(SaveDataKey).Then(data =>
            {
                if (string.IsNullOrEmpty(data) == false)
                {
                    try
                    {
                        ICloudSave save = JsonUtility.FromJson(data, type) as ICloudSave;
                        if (save != null)
                        {
                            promise.Resolve(save);
                        }
                        else
                        {
                            promise.Reject(new Exception($"PLAYFAB: Cannot parse Playfab save data: {data}"));
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"PLAYFAB: Cannot parse Playfab save data: {data} \n {e.Message}");
                        promise.Reject(e);
                    }
                }
                else
                {
                    promise.Reject(new Exception($"PLAYFAB: parse Playfab save data"));
                }
            })
            .Catch(e => promise.Reject(e));

            return promise;
            
        }

        public IPromise UploadSaveData<T>(T data)
        {
            if (LoggedIn == false)
            {
                return Promise.Rejected(new Exception("PLAYFAB: Not logged in"));
            }
            return UploadData(SaveDataKey, JsonUtility.ToJson(data, false));
        }

        public IPromise<T> DownloadSaveData<T>() where T : ICloudSave
        {
            Promise<T> result = Promise<T>.Create();
            DownloadSaveData(typeof(T)).Then(p => result.Resolve((T)p)).Catch(e => result.Reject(e));
            return result;
        }

        public void Logout()
        {
            PlayFabClientAPI.ForgetAllCredentials();
            profile = null;
            App.Storage.Delete<PlayfabProfile>();
            OnLogout();
        }

        public void ProcessExternalLogin(object payload, Promise<CloudActionResult> promise)
        {
            ProcessProfile(false, payload as GetPlayerCombinedInfoResultPayload, promise);
        }

        public void NotifyAboutDisconnect()
        {
            connected.SetValue(false);
        }

        private IPromise<string> GetAdvertisingId()
        {
            Promise<string> result = Promise<string>.Create();

            if (config.useAdvertisementdIdForAuth == false)
            {
                result.Resolve(SystemInfo.deviceUniqueIdentifier);
                return result;
            }

            if (Application.isEditor)
            {
                if (string.IsNullOrEmpty(config.debugEditorDeviceId) == false)
                {
                    result.Resolve(config.debugEditorDeviceId);
                }
                else
                {
                    result.Resolve(SystemInfo.deviceUniqueIdentifier);
                }
               
                return result;
            }

            Application.RequestAdvertisingIdentifierAsync((string advertisingId, bool trackingEnabled, string error) =>
            {
                if (string.IsNullOrEmpty(advertisingId) == false)
                {
                    result.Resolve(advertisingId);
                }
                else
                {
                    Log("Cannot get Advertising Identifier: " + error);
                    result.Resolve(SystemInfo.deviceUniqueIdentifier);
                }
            });
            Promise<string> timeout = Promise<string>.Create();
            App.Timer.WaitForUnscaled(5).Done(() => timeout.Resolve(SystemInfo.deviceUniqueIdentifier));
            return Promise<string>.Race(result, timeout);
        }

        public void OpenWebInterface()
        {
            if (string.IsNullOrEmpty(loggedInCustomId) == false)
            {
                Application.OpenURL($"{config.host}/user?id={SecurityUtil.Encrypt(loggedInCustomId)}&titleId={config.titleId}");
            }
            else
            {
                Application.OpenURL($"{config.host}");
            }
            OnUserOpenedWebInterface();
        }
    }
}