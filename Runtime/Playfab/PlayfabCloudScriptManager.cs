﻿using PlayFab;
using UnityEngine;
using SwiftFramework.Core;
using PlayFab.ClientModels;
using System;

namespace SwiftFramework.Playfab
{
    [DependsOnModules(typeof(ICloudAuthentication))]
    public class PlayfabCloudScriptManager : Module, ICloudScriptManager
    {
        private ICloudAuthentication auth;

        protected override void OnInit()
        {
            base.OnInit();
            auth = App.GetModule<ICloudAuthentication>();
        }

        public IPromise<(T result, CloudScriptResponseCode code)> ExecuteCloudScript<T>(string function, object args, bool logStreamEvent = true, bool executeLatestRevision = false)
        {
            Promise<(T result, CloudScriptResponseCode code)> promise = Promise<(T result, CloudScriptResponseCode code)>.Create();
            try
            {
                PlayFabClientAPI.ExecuteCloudScript(new ExecuteCloudScriptRequest()
                {
                    FunctionName = function,
                    FunctionParameter = args,
                    GeneratePlayStreamEvent = logStreamEvent,
                    RevisionSelection = executeLatestRevision ? CloudScriptRevisionOption.Latest : CloudScriptRevisionOption.Live
                },
                res =>
                {
                    Log($"Cloud Script Call Response: {res.ToJson()}");

                    if (res.Error != null || res.FunctionResult == null)
                    {
                        if (res.Error != null)
                        {
                            switch (res.Error.Error)
                            {
                                case "CloudScriptNotFound":
                                    promise.Resolve((default, CloudScriptResponseCode.CloudFunctionNotFound));
                                    break;
                                case "JavascriptException":
                                    promise.Resolve((default, CloudScriptResponseCode.ScriptException));
                                    break;
                                default:
                                    promise.Resolve((default, CloudScriptResponseCode.BadRequest));
                                    break;
                            }
                        }
                        else
                        {
                            promise.Resolve((default, CloudScriptResponseCode.EmptyResponse));
                        }
                    }
                    else
                    {
                        try
                        {
                            T result = JsonUtility.FromJson<T>(res.FunctionResult.ToString());
                            if (result != null)
                            {
                                promise.Resolve((result, CloudScriptResponseCode.OK));
                            }
                            else
                            {
                                promise.Resolve((default, CloudScriptResponseCode.InvalidJson));
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                            promise.Resolve((default, CloudScriptResponseCode.InvalidJson));
                        }
                    }
                },
                err =>
                {
                    Log($"Cloud Script Call Error - {function}: {err.Error}");
                    if(err.Error == PlayFabErrorCode.ServiceUnavailable)
                    {
                        auth.NotifyAboutDisconnect();
                        promise.Resolve((default, CloudScriptResponseCode.NoInternet));
                    }
                    else
                    {
                        promise.Resolve((default, CloudScriptResponseCode.BadRequest));
                    }
                  
                });

            }
            catch (Exception e)
            {
                LogError(e.Message);
                promise.Resolve((default, CloudScriptResponseCode.NotLoggedIn));
            }
            
            return promise;
        }

    }
}