﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.Playfab
{
    [CreateAssetMenu(menuName = "SwiftFramework/Playfab/Config")]
    public class PlayfabConfig : ModuleConfig
    {
        public string secretKey;
        public string titleId;
        public string host = "https://roblominer.com";
        public string debugEditorDeviceId = "editor_test";
        public bool validateProductionBundleId;
        public bool useAdvertisementdIdForAuth = true;
    }
}
