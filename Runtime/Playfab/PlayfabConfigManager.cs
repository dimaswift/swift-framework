﻿using PlayFab;
using UnityEngine;
using SwiftFramework.Core;
using PlayFab.ClientModels;
using System.Collections.Generic;
using System;

namespace SwiftFramework.Playfab
{
    [Configurable(typeof(PlayfabConfigManagerConfig))]
    [DependsOnModules(typeof(ICloudAuthentication))]
    public class PlayfabConfigManager : Module, ICloudConfigManager
    {
        private ICloudAuthentication auth;
        private PlayfabConfigManagerConfig config;

        public PlayfabConfigManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        protected override IPromise GetInitPromise()
        {
            Promise promise = Promise.Create();
            config = GetModuleConfig<PlayfabConfigManagerConfig>();
            auth = App.GetModule<ICloudAuthentication>();
            foreach (ScriptableObject configToSync in config.configsToSync)
            {
                if (App.Storage.Exists(configToSync.GetType()))
                {
                    App.Storage.OverwriteScriptable(configToSync);
                }
            }
            if (auth.LoggedIn)
            {
                SyncConfigs().Always(() =>
                {
                    promise.Resolve();
                });
            }
            else
            {
                auth.OnLogin += () =>
                {
                    SyncConfigs();
                };

                promise.Resolve();
            }

            return promise;
        }

        private IPromise SyncConfigs()
        {
            Promise result = Promise.Create();

            if (config.configsToSync.Length == 0)
            {
                result.Resolve();
            }
            else
            {
                int configsLeft = config.configsToSync.Length;
                foreach (var c in config.configsToSync)
                {
                    LoadConfig(c).Always(() =>
                    {
                        configsLeft--;
                        if (configsLeft <= 0)
                        {
                            result.Resolve();
                        }
                    });
                }
            }

            return result;
        }


        public IPromise<T> LoadConfig<T>() where T : new()
        {
            Promise<T> promise = Promise<T>.Create();
            T config = default;
            if (App.Storage.Exists<T>())
            {
                config = App.Storage.Load<T>();
            }

            void LoadConfig()
            {
                string key = typeof(T).Name;
                GetTitleDataRequest request = new GetTitleDataRequest()
                {
                    Keys = new List<string>() { key }
                };
                PlayFabClientAPI.GetTitleData(request, result =>
                {
                    if (result.Data == null || result.Data.ContainsKey(key) == false)
                    {
                        Log($"Cannot get title data with key {key}");
                        promise.Resolve(config);
                    }
                    else
                    {
                        try
                        {
                            config = JsonUtility.FromJson<T>(result.Data[key]);
                            if (result != null)
                            {
                                App.Storage.Save(config);
                                promise.Resolve(config);
                            }
                            else
                            {
                                promise.Resolve(config);
                            }
                        }
                        catch (Exception e)
                        {
                            Log(e.Message);
                            promise.Resolve(config);
                        }
                    }
                },
                error =>
                {
                    Log(error.ErrorMessage);
                    promise.Resolve(config);
                });
            }

            if (auth.LoggedIn && auth.Connected.Value)
            {
                LoadConfig();
                return promise;
            }

            auth.Login().Done(r =>
            {
                if (r == CloudActionResult.Success)
                {
                    LoadConfig();
                }
                else
                {
                    Log($"Cannot login, using default config of type {typeof(T).Name}");
                    promise.Resolve(config);
                }
            });

            return promise;
        }

        public IPromise LoadConfig<T>(T configToOverride) where T : ScriptableObject
        {
            Promise promise = Promise.Create();

            Type type = configToOverride.GetType();

            if (App.Storage.Exists(type))
            {
                App.Storage.OverwriteScriptable(configToOverride);
            }

            void LoadConfig()
            {
                string key = configToOverride.GetType().Name;
                GetTitleDataRequest request = new GetTitleDataRequest()
                {
                    Keys = new List<string>() { key }
                };
                PlayFabClientAPI.GetTitleData(request, result =>
                {
                    if (result.Data == null || result.Data.ContainsKey(key) == false)
                    {
                        Log($"Cannot get title data with key {key}");
                        promise.Resolve();
                    }
                    else
                    {
                        try
                        {
                            if (result != null)
                            {
                                JsonUtility.FromJsonOverwrite(result.Data[key], configToOverride);
                                App.Storage.Save(configToOverride);
                                Log("Config loaded");
                            }
                        }
                        catch (Exception e)
                        {
                            Debug.LogException(e);
                        }
                        promise.Resolve();
                    }
                },
                error =>
                {
                    Log(error.ErrorMessage);
                    promise.Resolve();
                });
            }

            if (auth.LoggedIn && auth.Connected.Value)
            {
                LoadConfig();
                return promise;
            }

            auth.Login().Done(r =>
            {
                if (r == CloudActionResult.Success)
                {
                    LoadConfig();
                }
                else
                {
                    Log($"Cannot login, using default config of type {typeof(T).Name}");
                    promise.Resolve();
                }
            });

            return promise;
        }

    }
}