﻿using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Playfab
{
    public class PlayfabConfigManagerConfig : ModuleConfig
    {
        public ScriptableObject[] configsToSync;
    }
}