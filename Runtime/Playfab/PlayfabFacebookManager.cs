﻿using PlayFab;
using UnityEngine;
using SwiftFramework.Core;
using PlayFab.ClientModels;
using System;

namespace SwiftFramework.Playfab
{
    [Configurable(typeof(PlayfabConfig))]
    [DependsOnModules(typeof(IFacebookProfile), typeof(ICloudAuthentication), typeof(INetworkManager))]
    public class PlayfabFacebookManager : Module, IFacebookCloudAuthentication
    {
        private IFacebookProfile facebook;
        protected ICloudAuthentication auth;
        private PlayfabConfig config;

        private static readonly GetPlayerCombinedInfoRequestParams infoRequestParameters = new GetPlayerCombinedInfoRequestParams()
        {
            GetPlayerProfile = true,
            GetUserAccountInfo = true
        };

        public event Action OnLogin = () => { };

        public FacebookProfile Profile => auth.Profile.FacebookProfile;

        public PlayfabFacebookManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        protected override IPromise GetInitPromise()
        {
            auth = App.GetModule<ICloudAuthentication>();
            facebook = App.GetModule<IFacebookProfile>();
            config = GetModuleConfig<PlayfabConfig>();
            return base.GetInitPromise();
        }

        public bool IsLinked()
        {
            return auth.LoggedIn && auth.Profile.LinkedToFacebook();
        }

        public IPromise<CloudActionResult> Login()
        {
            return LoginWithFacebook(Promise<CloudActionResult>.Create());
        }

        public IPromise UnlinkAccount()
        {
            if (facebook == null)
            {
                LogError($"Cannot use Facebook. {nameof(IFacebookProfile)} Module not found");
                return Promise.Rejected(new NotImplementedException($"{ nameof(IFacebookProfile) } Module not found"));
            }

            if (auth.Connected.Value == false)
            {
                return Promise.Rejected(new Exception("Cannot unlink facebook account! Not connected!"));
            }

            if (IsLinked() == false)
            {
                return Promise.Rejected(new Exception("Facebook account not linked!"));
            }

            Log("Trying to unlink Facebook account...");

            Promise promise = Promise.Create();

            UnlinkFacebookAccountRequest request = new UnlinkFacebookAccountRequest();


            PlayFabClientAPI.UnlinkFacebookAccount(request, result =>
            {
                auth.Profile.UnlinkFacebookProfile();
                facebook.Logout();
                promise.Resolve();
                Log("Facebook account unlinked!");
            },
            error =>
            {
                promise.Reject(new Exception(error.ErrorMessage));
                Log($"Error trying to unlink Facebook account! {error.ErrorMessage}");
            });

            return promise;
        }

        public IPromise LinkDevice()
        {
            Promise promise = Promise.Create();

            switch (Application.platform)
            {
                case RuntimePlatform.Android:

                    LinkAndroidDeviceIDRequest androidRequest = new LinkAndroidDeviceIDRequest()
                    {
                        AndroidDevice = SystemInfo.deviceModel,
                        AndroidDeviceId = PlayFabSettings.DeviceUniqueIdentifier,
                        OS = SystemInfo.operatingSystem,
                    };

                    PlayFabClientAPI.LinkAndroidDeviceID(androidRequest, result =>
                    {
                        Log($"Android device linked: {result.ToJson()}");
                        promise.Resolve();
                    },
                    error =>
                    {
                        Log($"Android device linking failed: {error?.Error}");
                        promise.Reject(new Exception(error?.Error.ToString()));
                    });


                    break;
                case RuntimePlatform.IPhonePlayer:

                    LinkIOSDeviceIDRequest iosRequest = new LinkIOSDeviceIDRequest()
                    {
                        DeviceId = PlayFabSettings.DeviceUniqueIdentifier,
                        DeviceModel = SystemInfo.deviceModel,
                        OS = SystemInfo.operatingSystem,
                    };

                    PlayFabClientAPI.LinkIOSDeviceID(iosRequest, result =>
                    {
                        Log($"IOS device linked: {result.ToJson()}");
                        promise.Resolve();
                    },
                    error =>
                    {
                        Log($"IOS device linking failed: {error?.Error}");
                        promise.Reject(new Exception(error?.Error.ToString()));
                    });

                    break;
                default:
                    string errorMessage = $"PLAYFAB: {Application.platform} platform not supported for linking device id.";
                    promise.Reject(new NotSupportedException(errorMessage));
                    break;
            }


            return promise;
        }


        private IPromise<FacebookProfile> LinkFacebookAccount(Promise<FacebookProfile> linkFacebookPromise)
        {
            if (facebook == null)
            {
                LogError($"Cannot use Facebook. {nameof(IFacebookProfile)} Module not found");
                return Promise<FacebookProfile>.Rejected(new NotImplementedException($"{ nameof(IFacebookProfile) } Module not found"));
            }
            if (facebook.Profile == null)
            {
                facebook.Login().Then(r =>
                {
                    if (r.Success && r.Profile != null)
                    {
                        LinkFacebookAccount(linkFacebookPromise);
                    }
                    else
                    {
                        linkFacebookPromise.Reject(new Exception());
                    }
                })
                .Catch(e =>
                {
                    linkFacebookPromise.Reject(e);
                });
                return linkFacebookPromise;
            }

            auth.CheckLogin(() =>
            {
                LinkFacebookAccountRequest request = new LinkFacebookAccountRequest()
                {
                    AccessToken = facebook.Profile.AccessToken
                };

                PlayFabClientAPI.LinkFacebookAccount(request, result =>
                {
                    Log($"Facebook account linked: {result.ToJson()}");
                    auth.Profile.LinkFacebookProfile(facebook.Profile);
                    linkFacebookPromise.Resolve(facebook.Profile);
                },
                error =>
                {
                    Log($"Facebook account linking failed: {error?.ErrorMessage}");
                    linkFacebookPromise.Reject(new Exception(error?.ErrorMessage));
                });
            }, linkFacebookPromise);

            return linkFacebookPromise;
        }

        public IPromise<FacebookProfile> LinkAccount()
        {
            Log("Trying to link Facebook account...");

            Promise<FacebookProfile> linkFacebookPromise = Promise<FacebookProfile>.Create();

            return LinkFacebookAccount(linkFacebookPromise);
        }


        private IPromise<CloudActionResult> LoginWithFacebook(Promise<CloudActionResult> promise)
        {
            if (facebook == null)
            {
                LogError($"Cannot use Facebook. {nameof(IFacebookProfile)} Module not found");
                return Promise<CloudActionResult>.Resolved(CloudActionResult.UnknownError);
            }

            if (facebook.Profile == null)
            {
                Log($"Facebook profile not found. Logging in...");
                facebook.Login().Then(res =>
                {
                    if (res.Profile != null)
                    {
                        Log($"Logged in to Facebook. Proceeding...");
                        LoginWithFacebook(promise);
                    }
                    else
                    {
                        Log($"Logging to Facebook failed: {res.Error}");
                        promise.Resolve(CloudActionResult.UnknownError);
                    }
                })
                .Catch(e =>
                {
                    Log($"Logging to Facebook failed: {e.Message}");
                    promise.Resolve(CloudActionResult.UnknownError);
                });
                return promise;
            }
            Log($"Logging to Facebook using access token...");
            LoginWithFacebookRequest request = new LoginWithFacebookRequest()
            {
                AccessToken = facebook.Profile.AccessToken,
                TitleId = config.titleId,
                InfoRequestParameters = infoRequestParameters
            };
            PlayFabClientAPI.LoginWithFacebook(request, result =>
            {
                Log($"Logged to Facebook. Getting player info...");
                auth.ProcessExternalLogin(result.InfoResultPayload, promise);
                OnLogin();
                LinkDevice();
            },
            error =>
            {
                Log($"Logging to Facebook using access token failed! {error.ErrorMessage}");
                promise.Resolve(CloudActionResult.AccountNotFound);
            });
            return promise;
        }

    }
}