﻿using PlayFab;
using SwiftFramework.Core;
using PlayFab.ClientModels;
using System.Collections.Generic;
using System;
using SwiftFramework.Core.Pooling;
using UnityEngine;

namespace SwiftFramework.Playfab
{
    public class PlayfabLeaderboard : ILeaderboard
    {
        public bool EntriesInSync => entriesInSync;

        public event Action OnPlayerEntryUpdated = () => { };

        public PlayfabLeaderboard(PlayfabLeaderboardConfig config, INetworkManager network, Func<BigNumber> scoreHandler, Func<ICloudProfile> profileHandler)
        {
            this.config = config;
            this.scoreHandler = scoreHandler;
            this.network = network;
            this.profileHandler = profileHandler;
            updateRequest = new UpdatePlayerStatisticsRequest()
            {
                Statistics = new List<StatisticUpdate>() { new StatisticUpdate() { StatisticName = config.id } }
            };
        }

        private readonly INetworkManager network;
        private readonly PlayfabLeaderboardConfig config;
        private readonly Func<ICloudProfile> profileHandler;

        private LeaderboardPlayerEntry playerEntry;
        private readonly UpdatePlayerStatisticsRequest updateRequest;
        private Func<BigNumber> scoreHandler;

        private List<LeaderboardPlayerEntry> leaderboardBuffer = new List<LeaderboardPlayerEntry>();
        private SimplePool<LeaderboardPlayerEntry> leaderboardBufferEntryPool = new SimplePool<LeaderboardPlayerEntry>(() => new LeaderboardPlayerEntry());

        private BigNumber lastPostedScore;
        private float lastPostedScoreTime;
        private bool entriesInSync;
        private ICloudProfile cloudProfile;

        private static readonly PlayerProfileViewConstraints leaderboardProfileConstraints = new PlayerProfileViewConstraints()
        {
            ShowAvatarUrl = true,
            ShowDisplayName = true,
            ShowStatistics = true,
            ShowValuesToDate = true
        };

        public IEnumerable<LeaderboardPlayerEntry> GetPlayers()
        {
            return leaderboardBuffer;
        }

        public IPromise<LeaderboardPlayerEntry> FetchPlayerEntry()
        {
            Promise<LeaderboardPlayerEntry> promise = Promise<LeaderboardPlayerEntry>.Create();

            List<StatisticUpdate> list = new List<StatisticUpdate>();

            GetLeaderboardAroundPlayerRequest request = new GetLeaderboardAroundPlayerRequest()
            {
                StatisticName = config.id,
                MaxResultsCount = 1,
                Version = 0
            };

            PlayFabClientAPI.GetLeaderboardAroundPlayer(request, result =>
            {
                if (result.Leaderboard.Count > 0)
                {
                    PlayerLeaderboardEntry playfabEntry = result.Leaderboard[0];
                    playerEntry = leaderboardBufferEntryPool.Take();
                    FillEntry(playerEntry, playfabEntry);
                    UpdatePlayerEntryInfo();
                    promise.Resolve(playerEntry);
                    entriesInSync = false;
                    OnPlayerEntryUpdated();
                }
                else
                {
                    promise.Reject(new EntryPointNotFoundException());
                }
            },
            error =>
            {
                Debug.LogError($"PLAYFAB: Cannot get player leaderboard entry: {error}");
                promise.Reject(new EntryPointNotFoundException(error?.ErrorMessage));
            });

            return promise;
        }

        private void UpdatePlayerEntryInfo()
        {
            ICloudProfile playerProfile = profileHandler();
            if (playerProfile != cloudProfile)
            {
                cloudProfile = playerProfile;

                if (playerEntry != null)
                {
                    if (cloudProfile.FacebookProfile != null && cloudProfile.FacebookProfile.Name != null)
                    {
                        cloudProfile.FacebookProfile.Name.Done(n => 
                        {
                            playerEntry.DisplayName = cloudProfile.DisplayName;
                        });
                    }
                    else
                    {
                        playerEntry.DisplayName = cloudProfile.DisplayName;
                    }
                  
                    playerEntry.Avatar = playerEntry.Avatar;
                }
              
            }
        }

        private LeaderboardPlayerEntry FillEntry(LeaderboardPlayerEntry entry, PlayerLeaderboardEntry playfabEntry)
        {
            if (string.IsNullOrEmpty(playfabEntry.Profile.AvatarUrl) == false)
            {
                playerEntry.Avatar = network.DownloadImage(playfabEntry.Profile.AvatarUrl);
            }

            entry.ValueIcon = config.valueIcon;
            entry.Score = BigNumber.FromCompressedInt(playfabEntry.StatValue);
            entry.DisplayName = playfabEntry.DisplayName;
            entry.Position = playfabEntry.Position + 1;
            return entry;
        }

        public IPromise<IEnumerable<LeaderboardPlayerEntry>> FetchEntries(bool isFriendsOnly)
        {
            Promise<IEnumerable<LeaderboardPlayerEntry>> promise = Promise<IEnumerable<LeaderboardPlayerEntry>>.Create();

            if (isFriendsOnly)
            {
                GetFriendLeaderboardRequest request = new GetFriendLeaderboardRequest()
                {
                    StatisticName = config.id,
                    MaxResultsCount = Math.Min(config.maxResultsCount, 100),
                    IncludeFacebookFriends = true,
                    Version = 0
                };

                PlayFabClientAPI.GetFriendLeaderboard(request, result =>
                {
                    leaderboardBuffer.Clear();

                    foreach (var entry in result.Leaderboard)
                    {
                        LeaderboardPlayerEntry entryFromPool = leaderboardBufferEntryPool.Take();
                        FillEntry(entryFromPool, entry);
                        leaderboardBuffer.Add(entryFromPool);
                    }
                    promise.Resolve(leaderboardBuffer);
                    entriesInSync = true;
                },
                error =>
                {
                    Debug.LogError($"PLAYFAB: Cannot get leaderboard: {error}");
                    promise.Reject(new Exception(error?.ErrorMessage));
                });
            }
            else
            {
                GetLeaderboardRequest request = new GetLeaderboardRequest()
                {
                    StatisticName = config.id,
                    MaxResultsCount = Math.Min(config.maxResultsCount, 100),
                    Version = 0
                };

                PlayFabClientAPI.GetLeaderboard(request, result =>
                {
                    leaderboardBuffer.Clear();

                    foreach (var entry in result.Leaderboard)
                    {
                        LeaderboardPlayerEntry entryFromPool = leaderboardBufferEntryPool.Take();
                        FillEntry(entryFromPool, entry);
                        leaderboardBuffer.Add(entryFromPool);
                    }
                    promise.Resolve(leaderboardBuffer);
                    entriesInSync = true;
                },
                error =>
                {
                    Debug.LogError($"PLAYFAB: Cannot get leaderboard: {error}");
                    promise.Reject(new Exception(error?.ErrorMessage));
                });
            }


            return promise;
        }



        public LeaderboardPlayerEntry GetPlayerEntry()
        {
            UpdatePlayerEntryInfo();
            return playerEntry;
        }

        public string GetTitle()
        {
            return config.titleKey;
        }

        public void TryPostScore()
        {
            if (Time.time - lastPostedScoreTime < config.scorePostFrequency)
            {
                return;
            }
            BigNumber score = scoreHandler();
            if (lastPostedScore == score.Value)
            {
                return;
            }
            updateRequest.Statistics[0].Value = score.Value.ToCompressedInt();
            lastPostedScoreTime = Time.time;
            lastPostedScore = score;
            PlayFabClientAPI.UpdatePlayerStatistics(updateRequest,
                result =>
                {
                    Debug.Log($"PLAYFAB: Score posted: {result.ToJson()}");
                    FetchPlayerEntry();
                },
                error =>
                {
                    Debug.LogError($"PLAYFAB: Score post failed: {error?.ErrorMessage}");
                });
            return;
        }

        public IPromise PostScore()
        {
            Promise promise = Promise.Create();
            BigNumber score = scoreHandler();
            updateRequest.Statistics[0].Value = score.Value.ToCompressedInt();
            lastPostedScore = score;
            PlayFabClientAPI.UpdatePlayerStatistics(updateRequest,
                result =>
                {
                    Debug.Log($"PLAYFAB: Score posted: {result.ToJson()}");
                    FetchPlayerEntry().Then(e =>
                    {
                        promise.Resolve();
                    })
                    .Catch(e => promise.Reject(e));
                },
                error =>
                {
                    Debug.LogError($"PLAYFAB: Score post failed: {error?.ErrorMessage}");
                    promise.Reject(new Exception(error.Error.ToString()));
                });
            return promise;
        }

        public string GetId()
        {
            return config.id;
        }
    }
}