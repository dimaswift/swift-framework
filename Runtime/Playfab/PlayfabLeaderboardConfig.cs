﻿using SwiftFramework.Core;
using System;
using UnityEngine;

namespace SwiftFramework.Playfab
{
    public class PlayfabLeaderboardConfig : ScriptableObject
    {
        public SpriteLink valueIcon;
        public string titleKey;
        public string id;
        public int maxResultsCount = 100;
        public int scorePostFrequency = 60;
    }

    [Serializable]
    public class PlayfabLeaderboardConfigLink : LinkTo<PlayfabLeaderboardConfig>
    {
    }
}