﻿using SwiftFramework.Core;
using System.Collections.Generic;

namespace SwiftFramework.Playfab
{
    [Configurable(typeof(LeaderboardsConfig))]
    [DependsOnModules(typeof(ICloudAuthentication))]

    public class PlayfabLeaderboardManager : BehaviourModule, ICloudLeaderboards
    {
        private ICloudAuthentication auth;
        private LeaderboardsConfig config;

        private readonly List<ILeaderboard> leaderboards = new List<ILeaderboard>();


        protected override void OnInit()
        {
            base.OnInit();
            auth = App.GetModule<ICloudAuthentication>();
            auth.OnLogin += Auth_OnLogin;
            auth.OnLogout += Auth_OnLogout;
            auth.OnProfileChanged += Auth_OnProfileChanged;
            config = GetModuleConfig<LeaderboardsConfig>();

            if (auth.LoggedIn)
            {
                GetLeaderboards();
            }
        }

        private void Auth_OnLogout()
        {
            leaderboards.Clear();
        }

        private void Auth_OnLogin()
        {
            GetLeaderboards();
        }

        private void Auth_OnProfileChanged(ICloudProfile profile)
        {
            leaderboards.Clear();
        }

        private void Update()
        {
            if (Initialized && auth.Connected.Value && config.scoresHandler.HasValue)
            {
                foreach (ILeaderboard leaderboard in leaderboards)
                {
                    if (config.scoresHandler.Value.CanPostScore(leaderboard.GetId()))
                    {
                        leaderboard.TryPostScore();
                    }
                }
            }
        }

        public IEnumerable<ILeaderboard> GetLeaderboards()
        {
            if (auth.LoggedIn == false)
            {
                return leaderboards;
            }

            if (leaderboards.Count != 0)
            {
                return leaderboards;
            }

            foreach (PlayfabLeaderboardConfig config in config.leaderboards.GetValues())
            {
                ILeaderboard leaderboard = new PlayfabLeaderboard(config, App.Net, this.config.scoresHandler.Value.GetScoreForLeaderboard(config.id), () => auth.Profile);
                leaderboards.Add(leaderboard);
            }

            return leaderboards;
        }

        public IPromise ForcePostAllScores()
        {
            List<IPromise> scorePromises = new List<IPromise>();
            foreach (ILeaderboard leaderboard in GetLeaderboards())
            {
                scorePromises.Add(leaderboard.PostScore());
            }
            return Promise.All(scorePromises).Then(() =>
            {
                foreach (ILeaderboard leaderboard in GetLeaderboards())
                {
                    leaderboard.FetchPlayerEntry();
                }
            });
        }


    }
}