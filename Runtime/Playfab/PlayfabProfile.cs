﻿using UnityEngine;
using SwiftFramework.Core;
using PlayFab.ClientModels;
using System;

namespace SwiftFramework.Playfab
{
    [Serializable]
    public class PlayfabProfile : ICloudProfile
    {
        [SerializeField] private GetPlayerCombinedInfoResultPayload info;

        public IPromise<Texture2D> Avatar => avatar;

        private readonly Promise<Texture2D> avatar;

        private FacebookProfile facebookProfile;

        public PlayfabProfile(GetPlayerCombinedInfoResultPayload info, INetworkManager net)
        {
            this.info = info;
            avatar = Promise<Texture2D>.Create();

            if (info.PlayerProfile != null && string.IsNullOrEmpty(info.PlayerProfile.AvatarUrl) == false)
            {
                net.DownloadImage(info.PlayerProfile.AvatarUrl).Done(t => avatar.Resolve(t));
            }
        }

        public bool LinkedToFacebook()
        {
            return info.AccountInfo != null 
                && info.AccountInfo.FacebookInfo != null 
                && string.IsNullOrEmpty(info.AccountInfo.FacebookInfo.FacebookId) == false;
        }

        public string DisplayName => info.PlayerProfile.DisplayName;

        public void LinkFacebookProfile(FacebookProfile facebookProfile)
        {
            this.facebookProfile = facebookProfile;

            info.AccountInfo.FacebookInfo = new UserFacebookInfo() { FacebookId = facebookProfile.Id };

            if (facebookProfile.Name != null)
            {
                facebookProfile.Name.Done(n =>
                {
                    info.AccountInfo.FacebookInfo.FullName = n;
                });
            }
        }

        public void UnlinkFacebookProfile()
        {
            info.AccountInfo.FacebookInfo = null;
            facebookProfile = null;
        }

        public FacebookProfile FacebookProfile
        {
            get
            {
                if (facebookProfile == null && LinkedToFacebook())
                {
                    facebookProfile = new FacebookProfile()
                    {
                        Name = Promise<string>.Resolved(info.AccountInfo.FacebookInfo.FullName),
                    };
                }
                return facebookProfile;
            }
        }

        public void SetName(string displayName)
        {
            if (info == null)
            {
                return;
            }
            info.PlayerProfile.DisplayName = displayName;
        }
    }
}