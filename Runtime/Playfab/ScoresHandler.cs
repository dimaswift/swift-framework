﻿using UnityEngine;
using SwiftFramework.Core;
using System;

namespace SwiftFramework.Playfab
{
    public abstract class ScoresHandler : ScriptableObject
    {
        public abstract Func<BigNumber> GetScoreForLeaderboard(string id);
        public abstract bool CanPostScore(string id);
    }

    [Serializable]
    public class ScoresHandlerLink : LinkTo<ScoresHandler>
    {
    }
}