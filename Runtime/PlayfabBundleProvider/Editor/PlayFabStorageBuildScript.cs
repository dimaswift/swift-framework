﻿using System.IO;
using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Build.DataBuilders;
using UnityEditor.Build;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.Initialization;
using UnityEngine.AddressableAssets.ResourceLocators;

namespace SwiftFramework.PlayfabBundleProvider
{
    [CreateAssetMenu(fileName = "PlayFabStorageBuildScript.asset", menuName = "SwiftFramework/Bundle Providers/PlayFab Build")]
    public class PlayFabStorageBuildScript : BuildScriptPackedMode
    {
        public override string Name => "PlayFab Build";

        protected override TResult DoBuild<TResult>(AddressablesDataBuilderInput builderInput, AddressableAssetsBuildContext aaContext)
        {
            var buildResult = base.DoBuild<TResult>(builderInput, aaContext);
            if (aaContext.settings.BuildRemoteCatalog)
            {
                PatchSettingsFile(builderInput);
            }
            else
            {
                Debug.LogWarning("PlayFab: Addressables Remote Catalog is not enabled, skipping patching of the settings file");
            }
            return buildResult;
        }

        private void PatchSettingsFile(AddressablesDataBuilderInput builderInput)
        {
            var settingsJsonPath = Addressables.BuildPath + "/" + builderInput.RuntimeSettingsFilename;
            var settingsJson = JsonUtility.FromJson<ResourceManagerRuntimeData>(File.ReadAllText(settingsJsonPath));

            var originalRemoteHashCatalogLocation = settingsJson.CatalogLocations.Find(locationData => locationData.Keys[0] == "AddressablesMainContentCatalogRemoteHash");
            var isRemoteLoadPathValid = originalRemoteHashCatalogLocation.InternalId.StartsWith("playfab://");
            if (isRemoteLoadPathValid == false)
            {
                throw new BuildFailedException("RemoteBuildPath must start with playfab://");
            }
            var newRemoteHashCatalogLocation = new ResourceLocationData(originalRemoteHashCatalogLocation.Keys, originalRemoteHashCatalogLocation.InternalId, typeof(PlayFabStorageHashProvider), originalRemoteHashCatalogLocation.ResourceType, originalRemoteHashCatalogLocation.Dependencies);
            settingsJson.CatalogLocations.Remove(originalRemoteHashCatalogLocation);
            settingsJson.CatalogLocations.Add(newRemoteHashCatalogLocation);
            File.WriteAllText(settingsJsonPath, JsonUtility.ToJson(settingsJson));
        }
    }
}