﻿using SwiftFramework.Core;
namespace SwiftFramework.Privacy
{
    public class PrivacyPolicyConfig : ModuleConfig
    {
        public string privacyLink;
        public string termsOfServiceLink;
        public string localizationKey;
        public bool enableToggleByDefault;
    }

}
