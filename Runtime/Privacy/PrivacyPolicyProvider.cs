﻿using SwiftFramework.Core;
using UnityEngine;
using SwiftFramework.Privacy.UI;

namespace SwiftFramework.Privacy
{
    [DependsOnModules(typeof(IWindowsManager), typeof(ILocalizationManager))]
    [Configurable(typeof(PrivacyPolicyConfig))]
    public class PrivacyPolicyProvider : Module, IPrivacyPolicyProvider
    {
        private const string PRIVACY_POLICY_ACCEPTED_KEY = "privacy_policy_accepted";

        public bool IsInCompliance => PlayerPrefs.HasKey(PRIVACY_POLICY_ACCEPTED_KEY);

        public PrivacyPolicyProvider(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        public IPromise<bool> Comply()
        {
            PrivacyPolicyConfig config = GetModuleConfig<PrivacyPolicyConfig>();

            Promise<bool> promise = Promise<bool>.Create();

            if (IsInCompliance)
            {
                promise.Resolve(true);
                return promise;
            }

            PrivacyPolicyWindow.Args args = new PrivacyPolicyWindow.Args()
            {
                acceptedByDefault = config.enableToggleByDefault,
                messageBody = App.Local.GetText(config.localizationKey, config.privacyLink, config.termsOfServiceLink),
            };

            App.GetModule<IWindowsManager>().Show<PrivacyPolicyWindow, PrivacyPolicyWindow.Args, bool>(args).Done(complied =>
            {
                if (complied)
                {
                    PlayerPrefs.SetInt(PRIVACY_POLICY_ACCEPTED_KEY, 1);
                    PlayerPrefs.Save();
                }
                promise.Resolve(true);
            });

            return promise;
           
        }
    }

}
