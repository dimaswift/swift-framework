﻿using SwiftFramework.Utils.UI;
using SwiftFramework.Core.Windows;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Privacy.UI
{
    public class PrivacyPolicyWindow : WindowWithArgsAndResult<PrivacyPolicyWindow.Args, bool>
    {
        public struct Args
        {
            public bool acceptedByDefault;
            public string messageBody;
        }

        [SerializeField] private Button acceptButton = null;
        [SerializeField] private SimpleToggle agreementToggle = null;
        [SerializeField] private HyperLink privacyText = null;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            acceptButton.onClick.AddListener(OnAccepClick);
            agreementToggle.OnValueChanged.AddListener(OnAgreementToggleValueChanged);
        }

        public override void OnStartShowing(Args arguments)
        {
            base.OnStartShowing(arguments);
            privacyText.Text.text = arguments.messageBody;
            agreementToggle.SetValue(arguments.acceptedByDefault);
            acceptButton.interactable = arguments.acceptedByDefault;
        }

        private void OnAgreementToggleValueChanged(bool agreed)
        {
            acceptButton.interactable = agreed;
        }

        private void OnDenyClick()
        {
            Resolve(false);
            Hide(); 
        }

        private void OnAccepClick()
        {
            Resolve(true);
            Hide();
        }
    }
}