﻿using SwiftFramework.Core;
using System.Collections.Generic;
using System;
using SwiftFramework.Core.SharedData;
using UnityEngine;
using System.Security.Cryptography;
using System.Text;

namespace SwiftFramework.ProgressTracker
{
    [Configurable(typeof(ProgressTrackerConfig))]
    [DependsOnModules(typeof(ICloudScriptManager), typeof(ICloudConfigManager), typeof(IWalletManager))]
    public class ProgressTracker : BehaviourModule, IProgressTracker
    {
       
        public event Action<AchievementData> OnAchievementUpdated = a => { };
        public event Action<QuestData> OnQuestUpdated = q => { };

        private ICloudScriptManager cloudScriptManager;
        private ICloudConfigManager cloudConfigManager;
        private IWalletManager walletManager;
        private ProgressTrackerConfig config;
 
 
        private readonly Dictionary<string, AchievementConfig> achievementConfigs = new Dictionary<string, AchievementConfig>();
        private readonly Dictionary<string, QuestConfig> questConfigs = new Dictionary<string, QuestConfig>();

        private ProgressData localData;

        private float lastSyncTime;
   
        protected override IPromise GetInitPromise()
        {
            Promise initPromise = Promise.Create();
            cloudScriptManager = App.GetModule<ICloudScriptManager>();
            cloudConfigManager = App.GetModule<ICloudConfigManager>();
            walletManager = App.GetModule<IWalletManager>();
            config = Instantiate(GetModuleConfig<ProgressTrackerConfig>());

        

            cloudConfigManager.LoadConfig(config).Always(() => 
            {
                foreach (QuestConfig questConfig in config.quests)
                {
                    if (questConfigs.ContainsKey(questConfig.id))
                    {
                        LogError($"Multiple quests with same id: {questConfig.id}");
                        continue;
                    }
                    questConfigs.Add(questConfig.id, questConfig);
                }

                foreach (AchievementConfig achievementConfig in config.achievements)
                {
                    if (achievementConfigs.ContainsKey(achievementConfig.id))
                    {
                        LogError($"Multiple achievement with same id: {achievementConfig.id}");
                        continue;
                    }
                    achievementConfigs.Add(achievementConfig.id, achievementConfig);
                }

                if (App.Storage.Exists<ProgressData>())
                {
                    localData = App.Storage.Load<ProgressData>();
                }
                else
                {
                    localData = new ProgressData();
                    foreach (AchievementConfig achievementConfig in config.achievements)
                    {
                        localData.achievements.Add(achievementConfig.CreateAchievement());
                    }
                    foreach (QuestConfig questConfig in config.quests)
                    {
                        localData.quests.Add(questConfig.CreateQuest());
                    }
                   
                    Core.App.InitPromise.Done(() => RestoreData(localData));
                }

               
                SyncData().Done(() => initPromise.Resolve());
                App.Boot.OnPaused += Boot_OnPaused;
            });
           
            return initPromise;
        }

        private void UpdateQuest(string id, QuestData data)
        {
            for (int i = 0; i < localData.quests.Count; i++)
            {
                if(localData.quests[i].id == id)
                {
                    localData.quests[i] = data;
                    OnQuestUpdated(data);
                    break;
                }
            }
        }

        private void UpdateAchievement(string id, AchievementData data)
        {
            for (int i = 0; i < localData.achievements.Count; i++)
            {
                if (localData.achievements[i].id == id)
                {
                    localData.achievements[i] = data;
                    OnAchievementUpdated(data);
                    break;
                }
            }
        }

        public IPromise IncrementQuest(string id, int amount)
        {
            Promise promise = Promise.Create();

            QuestData data = FindQuestData(id);

            if (data == null)
            {
                promise.Reject(new EntryPointNotFoundException(id));
                return promise;
            }

            IncrementLocalQuest(id, amount);

            SyncData();

            promise.Resolve();

            return promise;
        }

        public IPromise ClaimQuestReward(string id)
        {
            QuestData data = FindQuestData(id);

            if (data == null)
            {
                return Promise.Rejected(new EntryPointNotFoundException(id));
            }

            Promise result = Promise.Create();

            cloudScriptManager.ExecuteCloudScript<ClaimQuestRewardResponse>("ClaimQuestReward", data, true).Done(response =>
            {
                if (response.code != CloudScriptResponseCode.OK)
                {
                    result.Reject(new Exception(response.code.ToString()));
                }
                else
                {
                    if (response.result != null)
                    {
                        if(response.result.code == 200)
                        {
                            walletManager.ForceRefresh(response.result.wallet.rubyCredits);
                            UpdateQuest(data.id, response.result.quest);
                            result.Resolve();
                        }
                        else
                        {
                            result.Reject(new Exception(response.result.code.ToString()));
                        }
                    }
                    else
                    {
                        result.Reject(new Exception("Invalid response"));
                    }
                }
            });

            return result;
        }

        public TimeSpan GetQuestCooldown(QuestData quest)
        {
            if (quest.unlockTimestamp == 0)
            {
                return new TimeSpan();
            }

            TimeSpan result = DateTimeOffset.FromUnixTimeSeconds(quest.unlockTimestamp) - DateTimeOffset.UtcNow;

            if (result.TotalSeconds < 0)
            {
                return new TimeSpan();
            }

            return result;
        }

        public ProgressItemStatus GetAchievementStatus(string id)
        {
            if (TryGetAchievement(id, out AchievementData data, out AchievementConfig config) == false)
            {
                return ProgressItemStatus.Obsolete;
            }

            return data.GetStatus(config);
        }

        public ProgressItemStatus GetQuestStatus(string id)
        {
            if (TryGetQuest(id, out QuestData data, out QuestConfig config) == false)
            {
                return ProgressItemStatus.Obsolete;
            }

            return data.GetStatus(config);
        }

        public IPromise IncrementAchievement(string id, int amount)
        {
            Promise promise = Promise.Create();

            AchievementData data = FindAchievementData(id);

            if (data == null)
            {
                promise.Reject(new EntryPointNotFoundException(id));
                return promise;
            }

            IncrementLocalAchievement(id, amount);

            SyncData();

            promise.Resolve();

            return promise;
        }

        public IPromise ClaimAchievementReward(string id)
        {
            AchievementData data = FindAchievementData(id);

            if (data == null)
            {
                return Promise.Rejected(new EntryPointNotFoundException(id));
            }

            Promise result = Promise.Create();

            cloudScriptManager.ExecuteCloudScript<ClaimAchievementRewardResponse>("ClaimAchievementReward", data, true).Done(response =>
            {
                if (response.code != CloudScriptResponseCode.OK)
                {
                    result.Reject(new Exception(response.code.ToString()));
                }
                else
                {
                    if (response.result != null)
                    {
                        if(response.result.code == 200)
                        {
                            walletManager.ForceRefresh(response.result.wallet.rubyCredits);
                            UpdateAchievement(data.id, response.result.achievement);
                            result.Resolve();
                        }
                        else
                        {
                            result.Reject(new Exception(response.result.code.ToString()));
                        }
                    }
                    else
                    {
                        result.Reject(new Exception("Invalid response"));
                    }
                }
            });

            return result;
        }

        public IPromise<IEnumerable<(QuestConfig config, QuestData data)>> GetQuests()
        {
            var promise = Promise<IEnumerable<(QuestConfig config, QuestData data)>>.Create();

            promise.Resolve(GetActiveQuests());

            return promise;
        }

        private int CalculateTotalProgress(ProgressData progress)
        {
            int total = 0;
            foreach (AchievementData data in progress.achievements)
            {
                foreach (AchievementData.Step step in data.steps)
                {
                    total += step.progress;
                    if (step.rewardClaimed)
                    {
                        total += 1;
                    }
                }
            }
            foreach (QuestData quest in progress.quests)
            {
                QuestConfig config = GetQuestConfig(quest.id);
                if (config == null)
                {
                    continue;
                }
                total += quest.progress;
                total += quest.completedTimesAmount * config.targetAmount;
            }
            return total;
            
        }

        private IPromise SyncData()
        {
            if (Time.realtimeSinceStartup - lastSyncTime < 3)
            {
                return Promise.Resolved();
            }

            lastSyncTime = Time.realtimeSinceStartup;

            foreach (QuestConfig questConfig in questConfigs.Values)
            {
                if (FindQuestData(questConfig.id) == null)
                {
                    if (questConfig.autoStart)
                    {
                        QuestData newQuest = questConfig.CreateQuest();
                        localData.quests.Add(newQuest);
                    }
                }
            }

            foreach (AchievementConfig achievementConfig in achievementConfigs.Values)
            {
                if (FindAchievementData(achievementConfig.id) == null)
                {
                    AchievementData newAchievement = achievementConfig.CreateAchievement();
                    localData.achievements.Add(newAchievement);
                }
            }

            Promise promise = Promise.Create();

            cloudScriptManager.ExecuteCloudScript<GetSummaryResponse>("GetProgressSummary", null).Then(cloudSummary =>
            {
                if (cloudSummary.code != CloudScriptResponseCode.OK)
                {
                    promise.Resolve();
                    return;
                }

                string checksum = localData.CalculateChecksum();
          
                if (checksum == cloudSummary.result.md5)
                {
                    promise.Resolve();
                    return;
                }

                int localProgress = CalculateTotalProgress(localData);

                if (localProgress - cloudSummary.result.totalProgress > 1 || cloudSummary.result.totalProgress == 0)
                {
                    cloudScriptManager.ExecuteCloudScript<UploadProgressResponse>("UploadProgress", new GetProgressDataResponse()
                    {
                        achievements = localData.achievements.ToArray(),
                        quests = localData.quests.ToArray(),
                        md5 = localData.CalculateChecksum(),
                        totalProgress = CalculateTotalProgress(localData)
                    })
                    .Then(response =>
                    {
                        if(response.code == CloudScriptResponseCode.OK)
                        {
                            Log($"Uploaded local progress to cloud");
                        }
                        else
                        {
                            Log($"Uploaded local progress to cloud failed: {response.code}");
                        }
                        promise.Resolve();
                    })
                    .Catch(e =>
                    {
                        LogError($"SyncData error: {e.Message}");
                        promise.Resolve();
                    });
                }
                else
                {
                    Log($"Getting progress from cloud...");
                    cloudScriptManager.ExecuteCloudScript<GetProgressDataResponse>("GetProgress", null)
                    .Then(response =>
                    {
                        if (response.code == CloudScriptResponseCode.OK && response.result != null)
                        {
                            ProgressData cloudData = new ProgressData(response.result.achievements, response.result.quests);
                            int cloudProgress = CalculateTotalProgress(cloudData);
                            if (cloudProgress >= localProgress)
                            {
                                Log($"Downloaded progress from cloud");
                                localData = cloudData;
                            }
                          
                            promise.Resolve();
                        }
                    })
                    .Catch(e =>
                    {
                        LogError($"SyncData error: {e.Message}");
                        promise.Resolve();
                    });
                }
            })
            .Catch(e =>
            {
                LogError($"SyncData error: {e.Message}");
                promise.Resolve();
            });

            return promise;
        }

        private void RestoreData(ProgressData freshData)
        {
            IProgressRestoreHandler restoreHandler = GetComponent<IProgressRestoreHandler>();
            if (restoreHandler != null)
            {
                foreach (AchievementData data in freshData.achievements)
                {
                    restoreHandler.RestoreAchievements(data.id, data.steps[data.currentStep].progress, out int incrementAmount);
                    IncrementLocalAchievement(data.id, incrementAmount);
                }
                foreach (QuestData data in freshData.quests)
                {
                    restoreHandler.RestoreAchievements(data.id, data.progress, out int incrementAmount);
                    IncrementLocalQuest(data.id, incrementAmount);
                }
            }
            lastSyncTime = 0;
            SyncData();
            
        }

        private void Boot_OnPaused()
        {
            INotificationsManager notifications = App.GetModule<INotificationsManager>();
            if(notifications != null)
            {
                foreach (QuestData questData in localData.quests)
                {
                    QuestConfig config = GetQuestConfig(questData.id);
                    if(config == null)
                    {
                        continue;
                    }
                    if (questData.GetStatus(config) == ProgressItemStatus.OnCooldown)
                    {
                        long delay = questData.unlockTimestamp - App.Clock.Now.Value;
                        if(delay > 300)
                        {
                            notifications.ScheduleDefault("daily_quest_notification_msg", "daily_quest_notification_desc", delay);
                        }
                    }
                }
              
            }
            SaveCache();
        }

        private void SaveCache()
        {
            App.Storage.Save(new ProgressChecksum() { md5 = localData.CalculateChecksum() });
            App.Storage.Save(localData);
        }

        public IPromise<IEnumerable<(AchievementConfig config, AchievementData data)>> GetAchievements()
        {
            var promise = Promise<IEnumerable<(AchievementConfig config, AchievementData data)>>.Create();

            promise.Resolve(GetActiveAchievements());
                
            return promise;
        }

        private IEnumerable<(AchievementConfig config, AchievementData data)> GetActiveAchievements()
        {
            foreach (AchievementData achievement in localData.achievements)
            {
                AchievementConfig config = GetAhievementConfig(achievement.id);
                if (config == null)
                {
                    continue;
                }
                yield return (config, achievement);
            }
        }

        private AchievementConfig GetAhievementConfig(string id)
        {
            if (id == null)
            {
                return null;
            }
            if (achievementConfigs.TryGetValue(id, out AchievementConfig config))
            {
                return config;
            }
            return null;
        }

        private QuestConfig GetQuestConfig(string id)
        {
            if (id == null)
            {
                return null;
            }
            if (questConfigs.TryGetValue(id, out QuestConfig config))
            {
                return config;
            }
            return null;
        }


        private AchievementData FindAchievementData(string id)
        {
            foreach (AchievementData c in localData.achievements)
            {
                if (c.id == id)
                {
                    return c;
                }
            }
            return null;
        }

        private QuestData FindQuestData(string id)
        {
            foreach (QuestData c in localData.quests)
            {
                if (c.id == id)
                {
                    return c;
                }
            }

            return null;
        }

        private void LogError(string error)
        {
            Debug.LogError($"QUEST MANAGER: {error}");
        }

        private void Log(string message)
        {
            Debug.Log($"QUEST MANAGER: {message}");
        }

        private void IncrementLocalQuest(string id, int amount)
        {
            if (amount <= 0)
            {
                return;
            }

            QuestConfig config = GetQuestConfig(id);

            if (config == null)
            {
                LogError($"Cannot increment quest with id '{id}'! Doesn't exist");
                return;
            }

            QuestData data = FindQuestData(id);

            if (data.GetStatus(config) != ProgressItemStatus.Active)
            {
                return;
            }

            data.progress += amount;

            if (data.progress >= config.targetAmount)
            {
                data.progress = config.targetAmount;
            }

            OnQuestUpdated(data);
        }

        private bool IncrementLocalAchievement(string id, int amount)
        {
            if (amount <= 0)
            {
                return false;
            }

            if (TryGetAchievement(id, out AchievementData data, out AchievementConfig config) == false)
            {
                return false;
            }

            if (data.currentStep < config.steps.Length)
            {
                AchievementData.Step step = data.GetCurrentStep();

                void Increment(int a)
                {
                    int target = config.steps[data.currentStep].targetAmount;
                    step.progress += a;
                    if(step.progress >= target)
                    {
                        int overshoot = step.progress - target;
                        step.progress = target;
                        if (overshoot > 0 && config.allowOvershoot && data.currentStep < config.steps.Length - 1)
                        {
                            data.currentStep++;
                            if (data.currentStep == data.steps.Length)
                            {
                                Array.Resize(ref data.steps, data.steps.Length + 1);
                                data.steps[data.currentStep] = new AchievementData.Step()
                                {
                                    progress = config.steps[data.currentStep].startAmount,
                                    rewardClaimed = false
                                };
                            }
                            step = data.GetCurrentStep();
                            Increment(overshoot);
                        }
                    }
                }

                Increment(amount);
            }

            OnAchievementUpdated(data);

            return true;

        }

        private bool TryGetAchievement(string id, out AchievementData data, out AchievementConfig config)
        {
            config = GetAhievementConfig(id);
            data = FindAchievementData(id);

            if (config == null)
            {
                LogError($"Cannot find Achievement config with id '{id}'!");
                return false;
            }

            if (data == null)
            {
                LogError($"Cannot find Achievement data with id '{id}'!");
                return false;
            }

            return true;
        }

        private bool TryGetQuest(string id, out QuestData data, out QuestConfig config)
        {
            config = GetQuestConfig(id);
            data = FindQuestData(id);

            if (config == null)
            {
                LogError($"Cannot find Quest config with id '{id}'!");
                return false;
            }

            if (data == null)
            {
                LogError($"Cannot find Quest data with id '{id}'!");
                return false;
            }

            return true;
        }

        private IEnumerable<(QuestConfig config, QuestData data)> GetActiveQuests()
        {
            foreach (QuestData quest in localData.quests)
            {
                QuestConfig config = GetQuestConfig(quest.id);
                if (config == null)
                {

                    continue;
                }
                yield return (config, quest);
            }
        }

        public void RefreshCredits()
        {
            walletManager.Refresh();
        }

        [Serializable]
        private class GetProgressDataResponse
        {
            public AchievementData[] achievements;
            public QuestData[] quests;
            public int totalProgress;
            public string md5;
        }

        [Serializable]
        private class UploadProgressResponse
        {
            public int code = 0;
        }

        [Serializable]
        private class GetSummaryResponse
        {
            public int totalProgress = 0;
            public string md5 = null;
        }

        [Serializable]
        private class ClaimAchievementRewardResponse
        {
            public int code = 0;
            public AchievementData achievement = null;
            public Wallet wallet = new Wallet();
        }

        [Serializable]
        private class ClaimQuestRewardResponse
        {
            public int code = 0;
            public QuestData quest = null;
            public Wallet wallet = new Wallet();
        }

        [Serializable]
        private class IncrementRequest
        {
            public string id = null;
            public int amount = 0;
        }

        [Serializable]
        private class ProgressData
        {
            public List<AchievementData> achievements = new List<AchievementData>();
            public List<QuestData> quests = new List<QuestData>();

            public ProgressData(IEnumerable<AchievementData> achievements, IEnumerable<QuestData> quests)
            {
                this.achievements = new List<AchievementData>(achievements);
                this.quests = new List<QuestData>(quests);
            }

            public ProgressData()
            {

            }

            public string CalculateChecksum()
            {
                string json = JsonUtility.ToJson(this);
                byte[] data = Encoding.UTF8.GetBytes(json);
                byte[] hash = new MD5CryptoServiceProvider().ComputeHash(data);
                return Convert.ToBase64String(hash);
            }

        }

        [Serializable]
        private class ProgressChecksum
        {
            public string md5;
        }

    }
}
