﻿using UnityEngine;
using SwiftFramework.Core.SharedData;
using SwiftFramework.Core;

namespace SwiftFramework.ProgressTracker
{
    [CreateAssetMenu(menuName = "SwiftFramework/Playfab/ProgressTrackerConfig")]
    public class ProgressTrackerConfig : ModuleConfig
    {
        public AchievementConfig[] achievements;
        public QuestConfig[] quests;

    }

}
