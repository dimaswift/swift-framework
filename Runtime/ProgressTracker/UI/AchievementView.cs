﻿using UnityEngine;
using SwiftFramework.Core;
using SwiftFramework.Core.SharedData;
using SwiftFramework.Utils.UI;

namespace SwiftFramework.ProgressTracker.UI
{
    public class AchievementView : BaseProgressView<AchievementConfig, AchievementData>
    {
        [SerializeField] private StarBar starBar = null;

        private IWindowsManager windowsManager;

        protected override void OnClicked()
        {
            
        }

        public override void Init()
        {
            base.Init();
            windowsManager = App.Core.GetModule<IWindowsManager>();
            OnClaimClicked += AchievementView_OnClaimClicked;
        }

        private void AchievementView_OnClaimClicked((AchievementConfig config, AchievementData data) value)
        {
            SetClaimLoading(true);
            App.Core.GetModule<IProgressTracker>().ClaimAchievementReward(value.config.id).Then(() =>
            {
                SetClaimLoading(false);
                OnSetUp(Value);
            })
            .Catch(e => 
            {
                SetClaimLoading(false);
                windowsManager.Show<PopUpWindow, string>("ad_network_error");
            });
        }


        protected override void OnSetUp((AchievementConfig config, AchievementData data) value)
        {
            base.OnSetUp(value);
            ProgressItemStatus status = value.data.GetStatus(value.config);
          
            starBar.gameObject.SetActive(value.config.steps.Length > 1);

            if (status == ProgressItemStatus.Rewarded)
            {
                starBar.SetUp((value.config.steps.Length, value.config.steps.Length));
            }
            else
            {
                starBar.SetUp((value.data.currentStep, value.config.steps.Length));
            }
        }
    }

}
