﻿using UnityEngine;
using SwiftFramework.Core;
using SwiftFramework.Core.SharedData;
using SwiftFramework.Utils.UI;
using TMPro;
using UnityEngine.UI;
using System;

namespace SwiftFramework.ProgressTracker.UI
{
    [Serializable]
    public class StatusElement
    {
        public ProgressItemStatus status;
        public GameObject[] elements;
    }

    public class BaseProgressView<TConfig, TData> : ElementFor<(TConfig config, TData data)> where TData : IProgressData where TConfig : ProgressItemConfig
    {
        [SerializeField] private GameObject claimLoading = null;
        [SerializeField] private TMP_Text title = null;
        [SerializeField] private TMP_Text description = null;
        [SerializeField] private TMP_Text reward = null;
        [SerializeField] private TMP_Text progressText = null;

        [SerializeField] private Image icon = null;
        [SerializeField] private Image progressBar = null;
        [SerializeField] private Button claimButton = null;
        [SerializeField] private StatusElement[] elements = { };

        public Button ClaimButton => claimButton;

        public event Action<(TConfig config, TData data)> OnClaimClicked = v => { };

        public override void Init()
        {
            base.Init();
            claimButton.onClick.AddListener(OnClaimClick);
        }

        protected void SetClaimLoading(bool loading)
        {
            claimLoading.SetActive(loading);
            claimButton.interactable = loading == false;
        }

        private void OnClaimClick()
        {
            OnClaimClicked(Value);
        }

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp((TConfig config, TData data) value)
        {
            SetClaimLoading(false);
            var progress = value.data.GetProgress(value.config);
            title.text = App.Core.Local.GetText(value.config.titleKey, progress.target);
            description.text = App.Core.Local.GetText(value.config.descriptionKey);

            ProgressItemStatus status = value.data.GetStatus(value.config);

            foreach (StatusElement element in elements)
            {
                foreach (GameObject e in element.elements)
                {
                    e.SetActive(false);
                }
            }

            foreach (StatusElement element in elements)
            {
                foreach (GameObject e in element.elements)
                {
                    if(element.status == status) 
                    {
                        e.SetActive(true);
                    }
                  
                }
            }

            reward.text = value.data.GetReward(value.config).ToString();
            icon.sprite = value.config.icon.Value;

            progressBar.fillAmount = value.data.GetProgressNormalized(value.config);
            progressText.text = $"{progress.current}/{progress.target}";
            bool readyToClaim = status == ProgressItemStatus.Completed;
            claimButton.interactable = readyToClaim;
        }
    }

}
