﻿using SwiftFramework.Core;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SwiftFramework.ProgressTracker.UI
{
    public class OpenProgressTrackerButton : MonoBehaviour, IPointerClickHandler
    {
        private void OnEnable()
        {
            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                IProgressTracker progressTracker = App.Core.GetModule<IProgressTracker>();
                if (progressTracker == null)
                {
                    gameObject.SetActive(false);
                }
            });
          
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            IProgressTracker progressTracker = App.Core.GetModule<IProgressTracker>();
            if (progressTracker != null)
            {
                App.Core.GetModule<IWindowsManager>().Show<ProgressTrackerWindow, IProgressTracker>(progressTracker);
            }
        }
    }

}
