﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using SwiftFramework.Core.SharedData;
using SwiftFramework.Utils.UI;
using System;
using SwiftFramework.Utils;
using UnityEngine.UI;

namespace SwiftFramework.ProgressTracker.UI
{
    public class ProgressTrackerWindow : WindowWithArgs<IProgressTracker>
    {
        [SerializeField] private CanvasGroup achievementsPanel = null;
        [SerializeField] private CanvasGroup questsPanel = null;
        [SerializeField] private CanvasGroup loadingIcon = null;
        [SerializeField] private ElementSet achievementsSet = null;
        [SerializeField] private ElementSet questsSet = null;
        [SerializeField] private Button closeReconnectButton = null;
        [SerializeField] private Button questsButton = null;
        [SerializeField] private Button achievementsButton = null;
        [SerializeField] private Color selectedColor = Color.green;
        [SerializeField] private Color unselectedColor = Color.white;


        private IPromise loadingPromise;
        private IProgressTracker progressTracker;

        private float lastRefreshTime;

        private bool loading;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            loadingIcon.alpha = 1;
            questsPanel.alpha = 0;
            achievementsPanel.alpha = 0;
            questsButton.onClick.AddListener(OnQuestsClick);
            achievementsButton.onClick.AddListener(OnAchievementsClick);
            closeReconnectButton.onClick.AddListener(Hide);
            loading = true;
        }

        private void OnAchievementsClick()
        {
            achievementsButton.image.color = selectedColor;
            questsButton.image.color = unselectedColor;
            achievementsPanel.alpha = 1;
            achievementsPanel.interactable = true;
            achievementsPanel.blocksRaycasts = true;
            questsPanel.alpha = 0;
            questsPanel.interactable = false;
            questsPanel.blocksRaycasts = false;
            UpdateAchievements();

        }

        private void OnQuestsClick()
        {
            questsButton.image.color = selectedColor;
            achievementsButton.image.color = unselectedColor;
            questsPanel.alpha = 1;
            questsPanel.interactable = true;
            questsPanel.blocksRaycasts = true;
            achievementsPanel.alpha = 0;
            achievementsPanel.interactable = false;
            achievementsPanel.blocksRaycasts = false;
            UpdateQuests();
        }

        public override void OnStartShowing(IProgressTracker progressTracker)
        {
            base.OnStartShowing(progressTracker);

            if(Time.realtimeSinceStartup - lastRefreshTime > 5)
            {
                lastRefreshTime = Time.realtimeSinceStartup;
                progressTracker.RefreshCredits();
            }

            if(this.progressTracker == null)
            {
                this.progressTracker = progressTracker;
                progressTracker.OnAchievementUpdated += ProgressTracker_OnAchievementUpdated;
                progressTracker.OnQuestUpdated += ProgressTracker_OnQuestUpdated;
            }
            if (achievementsPanel.interactable)
            {
                OnAchievementsClick();
            }
            else
            {
                OnQuestsClick();
            }
         
        }

        private void ProgressTracker_OnQuestUpdated(QuestData quest)
        {
            UpdateQuests();
        }

        private void ProgressTracker_OnAchievementUpdated(AchievementData achievement)
        {
            UpdateAchievements();
        }

        private IPromise UpdateAchievements()
        {
            return progressTracker.GetAchievements().Then(achievements =>
            {
                achievementsSet.SetUp<AchievementView, (AchievementConfig, AchievementData)>(achievements);
                loading = false;
            });
        }

       
        private IPromise UpdateQuests()
        {
            return progressTracker.GetQuests().Then(quests =>
            {
                questsSet.SetUp<QuestView, (QuestConfig, QuestData)>(quests);
                loading = false;
            });
        }


        private void ShowPanel(CanvasGroup panel)
        {
            loadingIcon.alpha = 1;
            loading = true;
            UpdateAchievements();
        }

        private void Update()
        {
            if (IsShown == false)
            {
                return;
            }
            loadingIcon.alpha = Mathf.Lerp(loadingIcon.alpha, loading ? 1 : 0, Time.deltaTime * 5);
        }
    }

}
