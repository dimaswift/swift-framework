﻿using SwiftFramework.Core.SharedData;
using TMPro;
using UnityEngine;
using SwiftFramework.Core;
using System;
using SwiftFramework.Utils;
using SwiftFramework.Utils.UI;

namespace SwiftFramework.ProgressTracker.UI
{
    public class QuestView : BaseProgressView<QuestConfig, QuestData>
    {
        [SerializeField] private TMP_Text cooldownText = null;

        private IWindowsManager windowsManager;

        protected override void OnSetUp((QuestConfig config, QuestData data) value)
        {
            base.OnSetUp(value);
            windowsManager = App.Core.GetModule<IWindowsManager>();
            OnClaimClicked -= QuestView_OnClaimClicked;
            OnClaimClicked += QuestView_OnClaimClicked;
            if(value.data.GetStatus(value.config) == ProgressItemStatus.OnCooldown)
            {
                Now_OnValueChanged(App.Core.Clock.Now.Value);
                App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
                App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
            }
        }

        private void QuestView_OnClaimClicked((QuestConfig config, QuestData data) value)
        {
            SetClaimLoading(true);
            App.Core.GetModule<IProgressTracker>().ClaimQuestReward(value.config.id).Then(() =>
            {
                base.OnSetUp(Value);
                SetClaimLoading(false);
            })
            .Catch(e =>
            {
                SetClaimLoading(false);
                windowsManager.Show<PopUpWindow, string>("ad_network_error");
            }); ;
        }

        private void Now_OnValueChanged(long time)
        {
            if(Value.data == null)
            {
                return;
            }
            if (Value.data.GetStatus(Value.config) == ProgressItemStatus.OnCooldown)
            {
                var timeLeft = Value.data.unlockTimestamp - time;
                if(timeLeft >= 0)
                {
                    cooldownText.text = timeLeft.ToTimerString();
                }
                else
                {
                    cooldownText.text = "";
                }
            }
            else
            {
                App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
                base.OnSetUp(Value);
            }
        }

     

        protected override void OnClicked()
        {

        }
    }
}
