﻿using SwiftFramework.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.RateUsManager
{
    [Configurable(typeof(RateUsConfig))]
    public class RateUsManager : Module, IRateUsManager
    {
        private RateUsState state;
        private RateUsConfig config;
        public RateUsManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        protected override IPromise GetInitPromise()
        {
            config = GetModuleConfig<RateUsConfig>();

            App.Storage.OnAfterLoad += LoadState;
            App.Storage.RegisterState(() => state);
            LoadState();

            return base.GetInitPromise();
        }

        private void LoadState()
        {
            state = App.Storage.LoadOrCreateNew<RateUsState>();
        }

        public IPromise<bool> TryToRateUs()
        {
            Promise<bool> promise = Promise<bool>.Create();
            if (state.wasGameRated == false)
            {
                App.GetModule<IWindowsManager>().Show<RateUsWindow>().Done(window =>
                {
                    window.OnGameRated += () =>
                    {
                        SetGameRated(true);
                        promise.Resolve(true);
                    };
                });
            }
            else
            {
                promise.Resolve(false);
            }

            return promise;
        }

        private void SetGameRated(bool value)
        {
            state.wasGameRated = value;
        }

        [Serializable]
        private class RateUsState
        {
            public bool wasGameRated;
        }
    }
}