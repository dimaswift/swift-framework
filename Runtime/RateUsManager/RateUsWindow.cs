﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.RateUsManager
{
    [AddrSingleton]
    public class RateUsWindow : Window
    {
        [SerializeField] private Button[] nonRatingStars = { };
        [SerializeField] private Button[] ratingStars = { };
        [SerializeField] private Button notDecidedButton = null;

        public event Action OnGameRated = () => { };

        public override void WarmUp()
        {
            foreach (Button btn in nonRatingStars)
            {
                btn.onClick.AddListener(ShowRatedPopup);
            }
            foreach (Button btn in ratingStars)
            {
                btn.onClick.AddListener(RedirectToStorePage);
            }

            notDecidedButton.onClick.AddListener(Hide);
        }

        private void ShowRatedPopup()
        {
            OnGameRated();
            Hide();
        }

        private void RedirectToStorePage()
        {
            OnGameRated();
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
                    break;
                default:
                    Debug.LogError("Platform not supported");
                    break;
            }
            Hide();
        }
    }
}