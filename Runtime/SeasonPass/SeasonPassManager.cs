﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.SeasonPass;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.IdleMiner.UI;
using SwiftFramework.Utils.UI;
using System;
using System.Collections.Generic;

namespace SwiftFramework.SeasonPassManager
{
    [Configurable(typeof(SeasonPassConfig))]
    [DependsOnModules(typeof(IShopManager))]
    public class SeasonPassManager : Module, ISeasonPass
    {
        public IStatefulEvent<bool> PremiumPassPurchased => premiumPassPurchased;

        public IStatefulEvent<int> CurrentMilestone => currentMilestone;

        public IStatefulEvent<(int, int)> Points => points;

        public float CurrentPointsProgress => (float)points.Value.current / points.Value.total;

        public int MilestonesCount
        {

            get
            {
                if (TryGetCurrentSeason(out SeasonLink link))
                {
                    return link.Value.milestones.Count;
                }
                return 0;
            }
        }

        public long TimeTillStart => GetSecondsTillSeasonOpens();

        public long TimeTillEnd => GetSecondsTillSeasonCloses();

        public IStatefulEvent<bool> IsAvailable => isAvailable;

        private readonly StatefulEvent<bool> premiumPassPurchased = new StatefulEvent<bool>();
        private readonly StatefulEvent<int> currentMilestone = new StatefulEvent<int>();
        private readonly StatefulEvent<(int current, int total)> points = new StatefulEvent<(int current, int total)>();
        private readonly StatefulEvent<bool> isAvailable = new StatefulEvent<bool>();

        private SeasonPassState state;
        private SeasonPassConfig config;
        private IShopManager shop;

        public SeasonPassManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        protected override IPromise GetInitPromise()
        {
            Promise promise = Promise.Create();

            GetModuleConfigAsync<SeasonPassConfig>().Done(cfg =>
            {
                shop = App.GetModule<IShopManager>();
                shop.OnTransactionCompleted += Shop_OnTransactionCompleted;
                config = cfg;
                state = App.Storage.LoadOrCreateNew(GetDefaultData);
                App.Storage.RegisterState(() => state);

                currentMilestone.SetValue(state.milestone);

                currentMilestone.OnValueChanged += c => state.milestone = c;

                if (TryGetCurrentSeason(out SeasonLink link))
                {
                    for (int i = 0; i < link.Value.milestones.Count; i++)
                    {
                        link.Value.milestones[i].Index = i;
                    }
                    if (shop.IsPurchased(link.Value.premiumPassOffer))
                    {
                        premiumPassPurchased.SetValue(true);
                    }
                    isAvailable.SetValue(true);

                    if (state.milestone >= link.Value.milestones.Count)
                    {
                        var max = link.Value.milestones.LastFast().targetPoints;
                        points.SetValue((max, max));
                    }
                }
                else
                {
                    isAvailable.SetValue(false);
                }
                if (TryGetMilestone(state.milestone, out Milestone milestone))
                {
                    points.SetValue((state.points, milestone.targetPoints));
                }



                points.OnValueChanged += v => state.points = v.current;

                promise.Resolve();
            });

            return promise;
        }

        private void Shop_OnTransactionCompleted(ShopItemLink item, TransactionResult result)
        {
            if (TryGetCurrentSeason(out SeasonLink season))
            {
                if (season.Value.premiumPassOffer == item && result == TransactionResult.Success)
                {
                    premiumPassPurchased.SetValue(true);
                }
            }
        }

        public bool TryGetMilestone(int index, out Milestone milestone)
        {
            milestone = null;
            if (config.currentSeason == null || config.currentSeason.HasValue == false)
            {
                return false;
            }
            if (index >= config.currentSeason.Value.milestones.Count)
            {
                return false;
            }
            milestone = config.currentSeason.Value.milestones[index];
            return true;
        }

        public bool IsCurrentSeasonOpen()
        {
            if (TryGetCurrentSeason(out SeasonLink link) == false)
            {
                return false;
            }

            if (App.Clock.Now.Value < link.Value.startTime.timestampSeconds)
            {
                return false;
            }

            if (App.Clock.Now.Value > link.Value.endTime.timestampSeconds)
            {
                return false;
            }

            return true;
        }

        public void AddPoints(int amount)
        {
            int current = points.Value.current + amount;
            int total = points.Value.total;
            int milestone = currentMilestone.Value;

            while (current >= total)
            {
                milestone++;
                current -= total;
                if (TryGetMilestone(milestone, out Milestone seasonMilestone))
                {
                    total = seasonMilestone.targetPoints;
                }
                else
                {
                    if (milestone > config.currentSeason.Value.milestones.Count)
                    {
                        milestone = config.currentSeason.Value.milestones.Count;
                    }
                    current = total;
                    break;
                }
            }

            if (currentMilestone.Value != milestone)
            {
                currentMilestone.SetValue(milestone);
            }

            points.SetValue((current, total));
        }

        private SeasonPassState GetDefaultData()
        {
            return new SeasonPassState()
            {
                claimedMilestones = new List<int>(),
                claimedPremiumMilestones = new List<int>(),
                milestone = 1
            };
        }

        public bool TryGetCurrentSeason(out SeasonLink seasonLink)
        {
            if (config.currentSeason != null && config.currentSeason.HasValue)
            {
                seasonLink = config.currentSeason;
                return true;
            }
            seasonLink = null;
            return false;
        }

        public long GetSecondsTillSeasonOpens()
        {
            if (TryGetCurrentSeason(out SeasonLink link) == false)
            {
                return -1;
            }
            if (App.Clock.Now.Value >= link.Value.startTime.timestampSeconds)
            {
                return 0;
            }
            return link.Value.startTime.timestampSeconds - App.Clock.Now.Value;
        }

        public long GetSecondsTillSeasonCloses()
        {
            if (TryGetCurrentSeason(out SeasonLink link) == false)
            {
                return -1;
            }
            if (App.Clock.Now.Value > link.Value.endTime.timestampSeconds)
            {
                return 0;
            }
            return link.Value.endTime.timestampSeconds - App.Clock.Now.Value;
        }

        public bool IsMilestoneRewardClaimed(int index, bool premium)
        {
            if (premium)
            {
                return state.claimedPremiumMilestones.Contains(index);
            }

            return state.claimedMilestones.Contains(index);
        }

        public bool IsMilestoneRewardAvailable(int index, bool premium)
        {
            if (premium && premiumPassPurchased.Value == false)
            {
                return false;
            }

            if (TryGetCurrentSeason(out SeasonLink season) == false)
            {
                return false;
            }

            if (IsMilestoneRewardClaimed(index, premium))
            {
                return false;
            }

            if (state.milestone <= index)
            {
                return false;
            }

            if (TryGetMilestone(index, out Milestone milestone) == false)
            {
                return false;
            }

            if ((premium && milestone.premiumRewards.Count == 0) || (premium == false && milestone.freeRewards.Count == 0))
            {
                return false;
            }

            return true;
        }

        public IPromise<IEnumerable<RewardLink>> ClaimMilestoneRewards(int index, bool premium)
        {
            Promise<IEnumerable<RewardLink>> promise = Promise<IEnumerable<RewardLink>>.Create();

            if (IsMilestoneRewardClaimed(index, premium))
            {
                promise.Reject(new InvalidOperationException("Reward already claimed"));
                return promise;
            }

            if (TryGetMilestone(index, out Milestone milestone) == false)
            {
                promise.Reject(new InvalidOperationException($"Cannot find milestone with index {index}"));
                return promise;
            }


            List<IPromise> pendingRewardPromises = new List<IPromise>();

            List<RewardLink> rewardsToClaim = premium ? milestone.premiumRewards : milestone.freeRewards;

            foreach (RewardLink rewardLink in rewardsToClaim)
            {
                rewardLink.Load(reward =>
                {
                    pendingRewardPromises.Add(reward.AddReward());
                });
            }

            Promise.All(pendingRewardPromises).Always(() =>
            {
                promise.Resolve(rewardsToClaim);
            });

            if (premium)
            {
                state.claimedPremiumMilestones.Add(index);
            }
            else
            {
                state.claimedMilestones.Add(index);
            }

            App.Storage.Save(state);

            return promise;
        }

        public ShopItemLink GetPremiumPassOffer()
        {
            if (TryGetCurrentSeason(out SeasonLink season))
            {
                return season.Value.premiumPassOffer;
            }
            return null;
        }

        public IEnumerable<(Milestone milestone, float progress)> GetMilestones()
        {
            if (TryGetCurrentSeason(out SeasonLink season))
            {
                foreach (Milestone milestone in season.Value.milestones)
                {
                    yield return (milestone, GetMilestoneProgress(milestone));
                }
            }
        }

        private float GetMilestoneProgress(Milestone milestone)
        {
            if (state.milestone == milestone.Index)
            {
                return (float)points.Value.current / milestone.targetPoints;
            }
            if (state.milestone > milestone.Index)
            {
                return 1;
            }
            return 0;
        }

        public (Milestone milestone, float progress) GetMilestone(int index)
        {
            foreach (var item in GetMilestones())
            {
                if (item.milestone.Index == index)
                {
                    return item;
                }
            }
            return default;
        }

        [Serializable]
        private class SeasonPassState
        {
            public int points;
            public int milestone;
            public List<int> claimedPremiumMilestones = new List<int>();
            public List<int> claimedMilestones = new List<int>();
        }
    }

}
