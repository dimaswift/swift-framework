﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.ShopManager
{
    [CreateAssetMenu(menuName = "SwiftFramework/Shop/InstantFundsReward")]
    [PrewarmAsset]
    public class InstantSoftCurrencyReward : ScriptableObject, IReward
    {
        public SpriteLink itemIcon;

        public int minutes;

        public SpriteLink Icon => App.Core.GetModule<IIdleGameModule>().SoftFunds.Icon;

        public long GetDurationSeconds()
        {
            return minutes * 60;
        }

        public string GetDescription()
        {
            return App.Core.Local.GetText("instant_soft_currency", GetAmount());
        }

        public BigNumber GetAmount()
        {
            IIdleGameModule idleGameModule = App.Core.GetModule<IIdleGameModule>();

            return idleGameModule.ProductionSpeed.Value.Value.Value * GetDurationSeconds();
        }

        public IPromise AddReward()
        {
            Promise promise = Promise.Create();

            IIdleGameModule idleGameModule = App.Core.GetModule<IIdleGameModule>();

            idleGameModule.SoftFunds.Add(GetAmount());

            return promise; 
        }

        public bool IsAvailable()
        {
            return GetAmount() > 0;
        }
    }
}
