﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Core.SharedData.Shop.OfferSystem;
using System.Collections.Generic;

namespace SwiftFramework.ShopManager
{
    public class ShopConfig : ModuleConfig
    {
        public List<ShopItemLink> restorableItems;
        public List<OfferTriggerLink> offerTriggers;
    }
}
