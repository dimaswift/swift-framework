﻿using SwiftFramework.Core;
using System.Collections.Generic;
using System;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Core.SharedData.Funds;
using UnityEngine;
using SwiftFramework.Core.SharedData.Shop.OfferSystem;

namespace SwiftFramework.ShopManager
{
    [Configurable(typeof(ShopConfig))]
    [DependsOnModules(typeof(IInAppPurchaseManager))]
    public class ShopManager : Module, IShopManager
    {
        public IOfferTriggerController OfferTriggers { get; private set; }

        public event Action<ShopItemLink, TransactionResult> OnTransactionCompleted = (i, r) => { };

        private ShopConfig config;
        private ShopManagerState state;
        private IInAppPurchaseManager iap;

        public ShopManager(ModuleConfigLink configLink)
        {
            SetConfig(configLink);
        }

        protected override IPromise GetInitPromise()
        {
            config = GetModuleConfig<ShopConfig>();

            OfferTriggers = new OfferTriggerController(this, config.offerTriggers);

            iap = App.GetModule<IInAppPurchaseManager>();

            App.Storage.OnAfterLoad += LoadState;
            App.Storage.RegisterState(() => state);

            LoadState();

            App.Ready.Done(() => 
            {
                RestorePurchases();
            });

            return base.GetInitPromise();
        }

        private void LoadState()
        {
            state = App.Storage.LoadOrCreateNew<ShopManagerState>();
        }

        public IPromise<TransactionResult> Buy(ShopItemLink itemLink)
        {
            Promise<TransactionResult> promise = Promise<TransactionResult>.Create();
         
            itemLink.Load().Then(item =>
            {
                bool canAfford = item.price.CanAfford();
                item.price.Pay().Then(success => 
                {
                    if (success)
                    {
                        CompletePurchase(itemLink);
                        promise.Resolve(TransactionResult.Success);
                        OnTransactionCompleted(itemLink, TransactionResult.Success);
                    }
                    else
                    {
                        TransactionResult result = canAfford ? TransactionResult.Error : TransactionResult.InsufficientFunds;
                        promise.Resolve(result);
                        OnTransactionCompleted(itemLink, result);
                    }
                })
                .Catch(e => promise.Reject(e));
            })
            .Catch(e => promise.Reject(e));

            return promise;
        }

        private void CompletePurchase(ShopItemLink item)
        {
            PurchasedItem purchasedItem = state.items.Find(i => item == i.item);

            if (purchasedItem == null)
            {
                purchasedItem = new PurchasedItem()
                {
                    amount = 0,
                    item = item
                };

                state.items.Add(purchasedItem);
            }

            purchasedItem.amount++;

            App.Storage.Save(state);

            AddRewards(item);

            item.Value.CompletePurchase();
        }


        private void AddRewards(ShopItemLink shopItem)
        {
            foreach (IReward reward in shopItem.Value.GetAllRewards())
            {
                reward.AddReward();
            }
        }

        private void RestorePurchases()
        {
            foreach (ShopItemLink item in config.restorableItems)
            {
                NonConsumablePrice nonConsumableShopItem = item.Value.price.Value as NonConsumablePrice;
                if (nonConsumableShopItem != null)
                {
                    string id = nonConsumableShopItem.ProductId;
                    bool alreadyPurchased = iap.IsAlreadyPurchased(id);
                    if (alreadyPurchased && state.items.FindIndex(p => p.item == item) == -1)
                    {
                        CompletePurchase(item);
                    }
                }
            }
        }

        public bool IsPurchased(ShopItemLink item)
        {
            bool purchased = state.items.FindIndex(i => i.item == item) != -1;

            if (purchased)
            {
                return true;
            }

            ConsumablePrice iapPrice = item.Value.price.Value as ConsumablePrice;

            if (iapPrice != null)
            {
                return iap.IsAlreadyPurchased(iapPrice.ProductId);
            }

            foreach (ShopItemLink overlappingItem in item.Value.GetOverlappingItems())
            {
                if (overlappingItem.Value.price.Value is NonConsumablePrice)
                {
                    continue;
                }
                if (IsPurchased(overlappingItem))
                {
                    return true;
                }
            }

            return false;
        }

        public void ClaimSubscriptionBonus(ShopItemLink item)
        {
            SubscriptionShopItem subscriptionShopItem = item.Value as SubscriptionShopItem;
            if (subscriptionShopItem == null)
            {
                return;
            }
            SubscriptionClaimData data = GetSubscriptionClaimData(item);
            if (data.acumulatedBonusesAmount > 0)
            {
                data.lastClaimTimestamp = DateTime.Now.ToString();
                subscriptionShopItem.AddSubscriptionReward(data.acumulatedBonusesAmount);
                foreach (SubscriptionClaimData claimData in state.subscriptionClaims)
                {
                    if (claimData.item == item)
                    {
                        claimData.acumulatedBonusesAmount = 0;
                    }
                }
            }
        }

        public bool CanClaimSubscriptionBonus(ShopItemLink item)
        {
            if (IsSubscribed(item) == false)
            {
                return false;
            }

            SubscriptionClaimData data = GetSubscriptionClaimData(item);

            if (data.acumulatedBonusesAmount > 0)
            {
                return true;
            }

            return false;
        }

        public int GetSubscriptionBonusesAmount(ShopItemLink item)
        {
            return GetSubscriptionClaimData(item).acumulatedBonusesAmount;
        }

        private SubscriptionClaimData GetSubscriptionClaimData(ShopItemLink item)
        {
            foreach (SubscriptionClaimData data in state.subscriptionClaims)
            {
                if (data.item == item)
                {
                    if (IsSubscribed(item) == false)
                    {
                        return data;
                    }
                    data.acumulatedBonusesAmount += GetAccumulatedSubscriptionBonuses(data.lastClaimTimestamp);
                    data.lastClaimTimestamp = DateTime.Now.ToString();
                    return data;
                }
            }

            SubscriptionClaimData newData = new SubscriptionClaimData()
            {
                item = item,
                lastClaimTimestamp = null,
                acumulatedBonusesAmount = GetAccumulatedSubscriptionBonuses(null)
            };
          
            if (!IsSubscribed(item))
            {
                newData.acumulatedBonusesAmount = 0;
            }

            state.subscriptionClaims.Add(newData);

            return newData;
        }

        public bool IsSubscribed(ShopItemLink item)
        {
            SubscriptionPrice subscriptionPrice = item.Value.price.Value as SubscriptionPrice;
            if (subscriptionPrice == null)
            {
                LogWarning($"{item.GetPath()} is not a SubscriptionShopItem. Cannot be subscribed to");
                return false;
            }
            return iap.IsSubscribed(subscriptionPrice.ProductId);
        }

        public long GetSecondsTillNextSubsriptionBonus()
        {
            return (long)((DateTime.Today + TimeSpan.FromDays(1)) - DateTime.Now).TotalSeconds;
        }

        public int GetAccumulatedSubscriptionBonuses(string lastClaimTimestamp)
        {
            if (DateTime.TryParse(lastClaimTimestamp, out DateTime lastClaimDate))
            {
                return Math.Max(DateTime.Now.Day - lastClaimDate.Day, 0);
            }
            return 1;
        }

        public IEnumerable<OfferTriggerLink> GetOfferTriggers()
        {
            return config.offerTriggers;
        }

        [Serializable]
        private class ShopManagerState
        {
            public List<PurchasedItem> items = new List<PurchasedItem>();
            public List<SubscriptionClaimData> subscriptionClaims = new List<SubscriptionClaimData>();
        }

        [Serializable]
        private class PurchasedItem
        {
            public ShopItemLink item;
            public int amount;
        }

        [Serializable]
        private class SubscriptionClaimData
        {
            public ShopItemLink item;
            public string lastClaimTimestamp;
            public int acumulatedBonusesAmount;
        }
    }
}