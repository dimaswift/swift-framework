﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.ShopManager
{
    [CreateAssetMenu(menuName = "SwiftFramework/Shop/Subscription")]
    public class Subscription : SubscriptionShopItem
    {
        public string descKey;
        public Color headerColor;
        public SpriteLink mainIcon;

        public override void AddSubscriptionReward(int count)
        {
            for (int i = 0; i < count; i++)
            {
                foreach (IReward reward in GetAllRewards())
                {
                    reward.AddReward();
                }
            }
        }
    }
}
