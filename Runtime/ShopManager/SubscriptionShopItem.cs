﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using UnityEngine;

namespace SwiftFramework.ShopManager
{
    public abstract class SubscriptionShopItem : ShopItem
    {
        public SpriteLink miniIcon;

        public abstract void AddSubscriptionReward(int count);
    }
}
