﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using UnityEngine;

namespace SwiftFramework.ShopManager
{
    [CreateAssetMenu(menuName = "SwiftFramework/Shop/SupervisorReward")]
    public class SupervisorReward : ScriptableObject, IReward
    {
        public SpriteLink icon;
        public SupervisorLink supervisor;
        public string descKey;
        public SpriteLink Icon => icon;

        public IPromise AddReward()
        {
            Promise promise = Promise.Create();

            IIdleGameModule idleGame = App.Core.GetModule<IIdleGameModule>();

            idleGame.SharedSupervisors.Acquire(supervisor);

            promise.Resolve();
             
            return promise; 
        }

        public BigNumber GetAmount()
        {
            return 1;
        }

        public string GetDescription()
        {
            return App.Core.Local.GetText(descKey);
        }

        public bool IsAvailable()
        {
            return true;
        }
    }
}
