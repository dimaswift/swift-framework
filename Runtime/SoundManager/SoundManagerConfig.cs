﻿using SwiftFramework.Core;
namespace SwiftFramework.Sound
{
    public class SoundManagerConfig : ModuleConfig
    {
        public int maxActivePlayerCount = 10;
        public int startPlayersAmount = 3;
    }

}
