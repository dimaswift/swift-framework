﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using SwiftFramework.Core;

namespace SwiftFramework.Tests
{
    public class PromisesTest
    {
        [Test]
        public void Run()
        {
            bool value1 = false;
            bool value2 = false;

            int intValue1 = 0;
            int intValue2 = 0;


            Promise testPromise = Promise.Create();
            
            testPromise.Done(() => value1 = true);

            testPromise.Resolve();

            testPromise = Promise.Create();
            
            testPromise.Done(() => value2 = true);

            testPromise.Resolve();

            Assert.True(value2);

            Assert.True(value1);

            Promise<int> testPromise2 = Promise<int>.Create();
            
            testPromise2.Done((i) => intValue1 = i);
            testPromise2.Resolve(1);


            testPromise2 = Promise<int>.Create();

            testPromise2.Done((i) => intValue2 = 0);
            testPromise2.Done((i) => intValue2 = i);

            testPromise2.Then(i => intValue2 = i).Catch(e => { });

            testPromise2.Resolve(2);
            

            Assert.True(value2);
            Assert.True(value1);

            Assert.AreEqual(intValue1, 1);
            Assert.AreEqual(intValue2, 2);
        }


    }
}
