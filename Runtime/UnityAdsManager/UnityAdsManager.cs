﻿using UnityEngine;
using SwiftFramework.Core;
using System;
using UnityEngine.Advertisements;
using SwiftFramework.Utils.UI;

namespace SwiftFramework.UnityAds
{
    public class UnityAdsManager : BehaviourModule, IAdsManager, IUnityAdsListener
    {
        [SerializeField] private bool testMode = true;

        [SerializeField] private Config androidConfig = new Config();
        [SerializeField] private Config iosConfig = new Config();

        [SerializeField] private int interstitialShowFrequency = 0;

        [Serializable]
        private class Config
        {
            public float rewardedLoadingTimeout = 5;
            public bool bannerEnabled = false;
            public string gameId = "12345";
            public string bannerPlacementId = "banner";
            public string rewardedPlacementId = "rewardedVideo";
        }

        public IStatefulEvent<bool> BannerShown => bannerShown;

        public bool IsBannerEnabled => CurrentConfig.bannerEnabled;

        public event Action<string> OnRewardedShowStarted = i => { };
        public event Action OnRewardedAdLoaded = () => { };
        public event Action<string> OnRewardedSuccessfully = i => { };
        public event Action<string> OnRewardedClosed = i => { };
        public event Action<string> OnRewardedAttemptToShow = i => { };
        public event Action<RewardedAdErrorArgs> OnRewardedError = i => { };

        private float adClosedTime;

        private readonly StatefulEvent<bool> bannerShown = new StatefulEvent<bool>(false);

        private Promise<RewardedShowResult> rewardedPromise;

        private int interstitialCounter;

        private Config CurrentConfig
        {
            get
            {
                switch (Application.platform)
                {
                    case RuntimePlatform.IPhonePlayer:
                        return iosConfig;
                    default:
                        return androidConfig;
                }
            }
        }

        protected override IPromise GetInitPromise()
        {
            Advertisement.AddListener(this);
            Advertisement.Initialize(CurrentConfig.gameId, testMode);
            return base.GetInitPromise();
        }

        public void CancelRewardedShow()
        {
            
        }

        public float GetTimeSinceAdClosed()
        {
            return Time.realtimeSinceStartup - adClosedTime;
        }

        public bool IsInterstitialReady()
        {
            return Advertisement.IsReady();
        }

        public bool IsRewardedReady()
        {
            return Advertisement.IsReady(CurrentConfig.rewardedPlacementId);
        }

        public bool IsShowingRewardedAd()
        {
            return Advertisement.isShowing;
        }

        public void ResetInterstitialCounter()
        {
            interstitialCounter = 0;
        }

        public void SetBannerShown(bool shown)
        {
            if (shown)
            {
                Advertisement.Banner.Show(CurrentConfig.bannerPlacementId);
            }
            else
            {
                Advertisement.Banner.Hide();
            }
        }

        public bool ShowInterstitial(string placementId)
        {
            if (Advertisement.IsReady())
            {
                Advertisement.Show();
                return true;
            }
            return false;
        }

        public IPromise<RewardedShowResult> ShowRewarded(string placementId)
        {
            if (rewardedPromise == null)
            {
                rewardedPromise = Promise<RewardedShowResult>.Create();
            }

            Advertisement.Show(CurrentConfig.rewardedPlacementId);

            return rewardedPromise;
        }

        public IPromise<bool> ShowRewardedWithLoadingWindow(string placementId)
        {
            IWindowsManager windowsManager = App.GetModule<IWindowsManager>();

            if (windowsManager == null)
            {
                Promise<bool> promise = Promise<bool>.Create();
                Debug.Log($"Cannot show rewarded with loading window. IWindowsManager not found");
                ShowRewarded(placementId).Done(res =>
                {
                    promise.Resolve(res == RewardedShowResult.Success);
                });
                return promise;
            }

            return windowsManager.Show<AdLoadingWindow, AdLoadingWindow.Args, bool>(new AdLoadingWindow.Args()
            {
                placementId = placementId,
                onTimeout = () => { },
                timeout = CurrentConfig.rewardedLoadingTimeout,
                adsManager = this
            });
        }

        public bool TryShowInterstitial()
        {
            interstitialCounter++;
            if (interstitialCounter >= interstitialShowFrequency)
            {
                if (ShowInterstitial(null))
                {
                    interstitialCounter = 0;
                    return true;
                }
            }
            return false;
        }

        public void OnUnityAdsReady(string placementId)
        {
            
        }

        public void OnUnityAdsDidError(string message)
        {
            Debug.LogError(message); 
        }

        public void OnUnityAdsDidStart(string placementId)
        {
            
        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            adClosedTime = Time.realtimeSinceStartup;
            if (placementId == CurrentConfig.rewardedPlacementId)
            {
                if (rewardedPromise == null)
                {
                    Debug.LogError($"Unity rewarded ad shown without promise somehow?!"); 
                    return;
                }
                switch (showResult)
                {
                    case ShowResult.Failed:
                        rewardedPromise.Resolve(RewardedShowResult.Error);
                        break;
                    case ShowResult.Skipped:
                        rewardedPromise.Resolve(RewardedShowResult.Canceled);
                        break;
                    case ShowResult.Finished:
                        rewardedPromise.Resolve(RewardedShowResult.Success);
                        break;
                }
                rewardedPromise = null;
                return;
            }
        }
    }

}
