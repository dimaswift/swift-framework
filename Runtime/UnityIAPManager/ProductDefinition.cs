﻿using System;
using System.Collections.Generic;
using SwiftFramework.Core;

namespace SwiftFramework.UnityIAPManager
{
    [Serializable]
    public struct ProductDefinition
    {
        public string id;
        public IAPProductType type;

        public override bool Equals(object obj)
        {
            if (!(obj is ProductDefinition))
            {
                return false;
            }

            var definition = (ProductDefinition)obj;
            return id == definition.id &&
                   type == definition.type;
        }

        public override int GetHashCode()
        {
            var hashCode = -1056084179;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(id);
            hashCode = hashCode * -1521134295 + type.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(ProductDefinition a, ProductDefinition b)
        {
            return a.id == b.id && a.type == b.type;
        }

        public static bool operator !=(ProductDefinition a, ProductDefinition b)
        {
            return a.id != b.id && a.type != b.type;
        }
    }
}