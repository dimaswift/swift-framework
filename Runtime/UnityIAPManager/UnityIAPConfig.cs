﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.UnityIAPManager
{
    public class UnityIAPConfig : ModuleConfig
    {
        public List<ProductDefinition> products = new List<ProductDefinition>();

        [HideInInspector]
        [SerializeField] public ValidationKey googlePlayKey = new ValidationKey();
        [HideInInspector]
        [SerializeField] public ValidationKey appleKey = new ValidationKey();

        public void ObfuscateKeys(string googlePlayPublicKey, byte[] appleCert)
        {
            if (googlePlayKey.Obfuscate(Convert.FromBase64String(googlePlayPublicKey)))
            {
                Debug.Log($"Google Play Key obfuscated!");
            }

            if (appleKey.Obfuscate(appleCert))
            {
                Debug.Log($"Apple Cert obfuscated!");
            }
        }

        public bool IsObfuscated()
        {
            return googlePlayKey.GetData().Length != 0 && appleKey.GetData().Length != 0;
        }
    }
}