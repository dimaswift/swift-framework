﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core;
using System;
using UnityEngine.Purchasing.Security;
using UnityEngine.Purchasing;

namespace SwiftFramework.UnityIAPManager
{
    [Configurable(typeof(UnityIAPConfig))]
    [DependsOnModules(typeof(ITimer))]
    public class UnityIAPManager : BehaviourModule, IInAppPurchaseManager, IStoreListener
    {
        public IStatefulEvent<bool> PurchasingInitialized => initialized;

        public event Action<string> OnItemPurchased = item => { };

        public UnityIAPConfig Config => GetModuleConfig<UnityIAPConfig>();

        public bool Obfuscated => Config.IsObfuscated();

        private readonly List<Product> alreadyPurchasedProducts = new List<Product>();

        private const string defaultCurrencyCode = "USD";

        private IStoreController storeController;
        private IExtensionProvider storeExtensionProvider;
        private Promise<bool> pendingPurchasePromise;
        private Promise initPromise;
        private readonly StatefulEvent<bool> initialized = new StatefulEvent<bool>();

        private Dictionary<string, SubscriptionData> activeSubscriptions;

        public Product GetProduct(string id)
        {
            return storeController != null && storeController.products != null ? storeController.products.WithID(id) : null;
        }

        private ProductType GetType(ProductDefinition productDefinition)
        {
            switch (productDefinition.type)
            {
                case IAPProductType.Consumable:
                    return ProductType.Consumable;
                case IAPProductType.NonConsumable:
                    return ProductType.NonConsumable;
                case IAPProductType.Subscription:
                    return ProductType.Subscription;
                default:
                    return ProductType.Consumable;
            }
        }

        public SubscriptionData GetSubscriptionData(string productId)
        {
            if(activeSubscriptions == null)
            {
                FillSubscriptionsData();
            }

            if(activeSubscriptions.TryGetValue(productId, out SubscriptionData data))
            {
                return data;
            }
           
            return default;
        }

        private void FillSubscriptionsData()
        {
            activeSubscriptions = new Dictionary<string, SubscriptionData>();

            if (storeController == null)
            {
                return;
            }

            foreach (Product product in storeController.products.all)
            {
                if (activeSubscriptions.ContainsKey(product.definition.id))
                {
                    continue;
                }

                if (product.hasReceipt && product.definition.type == ProductType.Subscription)
                {
                    if(Application.isEditor == false)
                    {
                        SubscriptionManager manager = new SubscriptionManager(product, null);
                        SubscriptionInfo info = manager.getSubscriptionInfo();

                        SubscriptionData data = new SubscriptionData()
                        {
                            productId = product.definition.id,
                            expirationDate = info.getExpireDate(),
                            isFreeTrial = info.isFreeTrial() == Result.True ? true : false,
                            isSubscribed = info.isSubscribed() == Result.True ? true : false,
                            remainingTime = info.getRemainingTime(),
                            purchaseDate = info.getPurchaseDate(),
                            isCanceled = info.isCancelled() == Result.True ? true : false
                        };
                        activeSubscriptions.Add(product.definition.id, data);
                    }
                }
            }
        }

        public IPromise Init(IEnumerable<ProductDefinition> products)
        {
            initPromise = Promise.Create();

            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
            
            foreach (var product in products)
            {
                builder.AddProduct(product.id, GetType(product));
            }

            UnityPurchasing.Initialize(this, builder);

            return Promise.Race(App.Timer.WaitForUnscaled(5), initPromise);
        }

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            App.Timer.WaitForNextFrame().Done(() =>
            {
                storeController = controller;
                storeExtensionProvider = extensions;
                
                initPromise.Resolve();

                foreach (var prod in storeController.products.all)
                {
                    if (prod.hasReceipt)
                    {
                        alreadyPurchasedProducts.Add(prod);
                        OnItemPurchased(prod.definition.id);
                    }
                }

                initialized.SetValue(true);
            });
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            App.Timer.WaitForNextFrame().Done(() =>
            {
                Debug.LogError($"Unity AIP initialization failed: {error}");
                initPromise.Resolve();
                initialized.SetValue(false);
            });
        }

        public IPromise<bool> Purchase(string productId)
        {
            App.Boot.IgnoreNextPauseEvent();

            pendingPurchasePromise = Promise<bool>.Create();

            if (Initialized == false)
            {
                Debug.LogError($"Unity IAP is not initialized!");
                pendingPurchasePromise.Resolve(false);
                
                return pendingPurchasePromise;
            }

            Product product = GetProduct(productId);

            if (product == null)
            {
                Debug.LogError($"Product with id {productId} not found!");
                pendingPurchasePromise.Resolve(false);
                return pendingPurchasePromise;
            }

            storeController.InitiatePurchase(productId);

            return pendingPurchasePromise;
        }

        public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
        {
            if (pendingPurchasePromise != null)
            {
                if(failureReason == PurchaseFailureReason.DuplicateTransaction)
                {
                    Debug.Log($"{product.definition.id} restored!");
                    FillSubscriptionsData();
                    pendingPurchasePromise.Resolve(true);
                    OnItemPurchased(product.definition.id);
                }
                else
                {
                    pendingPurchasePromise.Resolve(false);
                }
            }
            Debug.LogError(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
        {
            if (pendingPurchasePromise == null)
            {
                Debug.Log($"Restoring product purchase: {args.purchasedProduct.definition.id}");
                alreadyPurchasedProducts.Add(args.purchasedProduct);
                OnItemPurchased(args.purchasedProduct.definition.id);
                return PurchaseProcessingResult.Complete;
            }

            bool isValid = true;

#if UNITY_ANDROID
            if (Obfuscated && Application.isEditor == false)
            {
                var validator = new CrossPlatformValidator(Config.googlePlayKey.GetData(), Config.appleKey.GetData(), Application.identifier);
                try
                {
                    var result = validator.Validate(args.purchasedProduct.receipt);
                    Debug.Log("Receipt is valid. Contents:");
                    foreach (IPurchaseReceipt productReceipt in result)
                    {
                        Debug.Log(productReceipt.productID);
                        Debug.Log(productReceipt.purchaseDate);
                        Debug.Log(productReceipt.transactionID);
                    }
                }
                catch (IAPSecurityException e)
                {
                    Debug.Log($"Invalid receipt: {e?.Message}");
                    isValid = false;
                }

            }
#endif

            if (isValid)
            {
                FillSubscriptionsData();
                pendingPurchasePromise.Resolve(true);
                OnItemPurchased(args.purchasedProduct.definition.id);
                IAnalyticsManager analytics = App.GetModule<IAnalyticsManager>();
                analytics?.LogPurchase(args.purchasedProduct.definition.id, args.purchasedProduct.metadata.isoCurrencyCode, args.purchasedProduct.metadata.localizedPrice, args.purchasedProduct.transactionID);
            }
            else
            {
                OnPurchaseFailed(args.purchasedProduct, PurchaseFailureReason.SignatureInvalid);
            }

            return isValid ? PurchaseProcessingResult.Complete : PurchaseProcessingResult.Pending;
        }

        public string GetPriceString(string productId)
        {
            Product product = GetProduct(productId);
            if (product == null)
            {
                Debug.LogWarning($"Cannot get price for item {productId}"); 
                return "";
            }
            return product.metadata.localizedPriceString;
        }

        public decimal GetPrice(string productId)
        {
            Product product = GetProduct(productId);
            if (product == null)
            {
                return 0;
            }
            return product.metadata.localizedPrice;
        }

        private void LogPurchasingDisabled()
        {
            Debug.LogWarning("Unity purchasing is disabled! Add define symbol ENABLE_IAP in the project settings!");
        }

        public string GetCurrencyCode(string productId)
        {
            Product product = GetProduct(productId);
            if (product == null)
            {
                return "";
            }
            return product.metadata.isoCurrencyCode;
        }


        public IPromise<bool> RestorePurchases()
        {
            Promise<bool> result = Promise<bool>.Create();
            if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
#if UNITY_IPHONE
                IAppleExtensions apple = storeExtensionProvider.GetExtension<IAppleExtensions>();
                apple.RestoreTransactions(success => 
                {
                    result.Resolve(success);
                });
#endif
            }
            else
            {
                result.Resolve(false);
            }
            return result;
        }

        protected override IPromise GetInitPromise()
        {
            return Init(Config.products);
        }

        public bool IsSubscribed(string productId)
        {
            if (storeController != null)
            {
                Product product = GetProduct(productId);
                if(product == null)
                {
                    return false;
                }
                return product.hasReceipt;
            }
            return false;
        }

        public bool IsAlreadyPurchased(string productId)
        {
            foreach (var item in alreadyPurchasedProducts)
            {
                if(item.definition.id == productId)
                {
                    return true;
                }
            }

            Product product = GetProduct(productId);
            if(product != null)
            {
                return product.hasReceipt;
            }
            return false;
        }

        public IPromise<bool> Buy(string productId)
        {
            return Purchase(productId);
        }
    }
}