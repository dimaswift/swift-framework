﻿using UnityEngine;
using System;
using UnityEngine.Purchasing.Security;
using UnityEngine.Purchasing;

namespace SwiftFramework.UnityIAPManager
{
    [Serializable]
    public class ValidationKey
    {
        public bool Obfuscate(byte[] keyData)
        {
            if (keyData.Length == 0)
            {
                data = "";
                order = new int[0];
                return false;
            }
            try
            {
                order = new int[keyData.Length / 20 + 1];
                data = Convert.ToBase64String(Obfuscator.Obfuscate(keyData, order, out key));
                return true;
            }
            catch (Exception ex)
            {
                Debug.LogError("Invalid Key: " + ex?.Message);
                return false;
            }
        }

        [SerializeField] private string data;

        [SerializeField] private int[] order;

        [SerializeField] private int key;

        public byte[] GetData()
        {
            if (string.IsNullOrEmpty(data))
            {
                return new byte[0];
            }
            return Obfuscator.DeObfuscate(Convert.FromBase64String(data), order, key);
        }
    }
}