﻿using SwiftFramework.Core;
using SwiftFramework.Core.Editor;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.EditorUtils;
using SwiftFramework.Utils.UI;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.Editor
{
    [InitializeOnLoad]
    public static class WrapperConverters
    {
        static WrapperConverters()
        {
            BaseInterfaceComponentFieldPropertyDrawer.OnTryToConvertToWrapper += OnTryToConvertToWrapper;
        }

        private static void OnTryToConvertToWrapper(GameObject target, System.Type interfaceType)
        {
            TryConvertAll(target, interfaceType);
        }


        [MenuItem("CONTEXT/ShopItem/Convert To Special Offer")]
        private static void ConvertToSpecialOffer(MenuCommand command)
        {
            var item = command.context as ShopItem;

            var dir = Util.ToRelativePath(new FileInfo(AssetDatabase.GetAssetPath(item)).Directory.FullName);

            SpecialOffer offer = Util.CreateScriptable<SpecialOffer>(item.name + "_special", dir);

            offer.icon = item.icon;
            offer.price = item.price;
            offer.rewards = item.rewards;
        }

        public static void TryConvertAll(GameObject gameObject, System.Type type)
        {
            Undo.RecordObject(gameObject, "Convert To Wrapper");

            if (type == typeof(IImage))
            {
                ConvertImage(gameObject.GetComponent<Image>());
            }
            else if (type == typeof(IButton))
            {
                ConvertButton(gameObject.GetComponent<Button>());
            }
            else if (type == typeof(IText))
            {
                ConvertTMPText(gameObject.GetComponent<TMP_Text>());
            }

            EditorUtility.SetDirty(gameObject);
        }

        private static void ConvertTMPText(TMP_Text text)
        {
            if (text == null)
            {
                return;
            }
            text.gameObject.AddComponent<TMPTextWrapper>();
        }

        private static void ConvertButton(Button button)
        {
            if (button == null || button.GetComponent<ButtonWrapper>())
            {
                return;
            }

            GameObject gameObject = button.gameObject;
            Graphic targetGraphic = button.targetGraphic;

            Object.DestroyImmediate(button);

            ButtonWrapper wrapper = gameObject.AddComponent<ButtonWrapper>();

            wrapper.targetGraphic = targetGraphic;
        }

        private static void ConvertImage(Image img)
        {
            if (img == null || img.GetComponent<ImageWrapper>())
            {
                return;
            }

            Sprite sprite = img.sprite;
            bool raycastTarget = img.raycastTarget;
            bool preserveAspect = img.preserveAspect;
            GameObject gameObject = img.gameObject;
            var type = img.type;

            Object.DestroyImmediate(img);

            var wrapper = gameObject.AddComponent<ImageWrapper>();
            wrapper.sprite = sprite;
            wrapper.raycastTarget = raycastTarget;
            wrapper.preserveAspect = preserveAspect;
            wrapper.type = type;
        }

    }

}
