﻿using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils
{
    [System.Serializable]
    public struct FloatMap
    {
        public float from;
        public float to;

        public float Evaluate(float t)
        {
            return Mathf.Lerp(from, to, t);
        }
    }

    [System.Serializable]
    public struct FloatMap2
    {
        public float from1;
        public float to1;
        public float from2;
        public float to2;

        public float Evaluate(float t)
        {
            return t.Remap(from1, to1, from2, to2);
        }
    }

}
