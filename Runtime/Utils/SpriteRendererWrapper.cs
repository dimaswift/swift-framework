﻿using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteRendererWrapper : MonoBehaviour, IImage
    {
        private SpriteRenderer spriteRenderer;
        private RectTransform rect;



        public void SetAlpha(float alpha)
        {
            spriteRenderer.color = spriteRenderer.color.SetAlpha(alpha);

        }

        public void SetSprite(SpriteLink link)
        {
            if (spriteRenderer == false)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
                rect = GetComponent<RectTransform>();
            }

            if (link.HasValue == false)
            {
                spriteRenderer.enabled = false;
                return;
            }

            link.Load(s =>
            {
                if (rect)
                {
                    transform.localScale = Vector3.one * (rect.rect.width / s.bounds.size.x);
                }
                spriteRenderer.sprite = s;
            });
        }
    }
}
