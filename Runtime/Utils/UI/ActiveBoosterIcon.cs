﻿using TMPro;
using SwiftFramework.Core;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SwiftFramework.Core.Boosters;

namespace SwiftFramework.Utils.UI
{
    public class ActiveBoosterIcon : ElementFor<(BoosterConfigLink boosterLink, IBoosterManager manager)>
    {
        [SerializeField] private TMP_Text timeLeftText = null;
        [SerializeField] private Image icon = null;

        private Coroutine timerCoroutine;

        private IBoosterManager boosterManager;

        private long SecondsLeft => boosterManager.GetTotalSecondsLeft(Value.boosterLink);

        private BoosterConfig config;

        protected override void OnClicked()
        {
            
        }

        private IEnumerator Timer()
        {
            while (boosterManager.IsExpired(Value.boosterLink) == false)
            {
                UpdateTimer();
                yield return new WaitForSeconds(1);
            }
        }

        private void UpdateTimer()
        {
            timeLeftText.text = config.durationSeconds > 0 ? SecondsLeft.ToTimerString() : "∞";
        }

        protected override void OnSetUp((BoosterConfigLink boosterLink, IBoosterManager manager) args)
        {
            boosterManager = args.manager;

            args.boosterLink.Load(cfg =>
            {
                config = cfg;
                icon.SetSpriteAsync(config.icon);

                if (config.durationSeconds > 0)
                {
                    if (SecondsLeft < 0)
                    {
                        timeLeftText.text = "";
                        return;
                    }

                    App.Core.Coroutine.Begin(Timer(), ref timerCoroutine);
                }

                UpdateTimer();
            });
        }
    }
}
