﻿using UnityEngine;
using SwiftFramework.Core;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class AmountIcon : MonoBehaviour
    {
        [SerializeField] private GenericText amountText = null;
        [SerializeField] private Image icon = null;

        private IStatefulEvent<BigNumber> source;

        public void SetUp(IStatefulEvent<BigNumber> source, SpriteLink icon)
        {
            this.source = source;
            source.OnValueChanged -= Amount_OnValueChanged;
            source.OnValueChanged += Amount_OnValueChanged;
            this.icon.SetSpriteAsync(icon);
            amountText.Value.Text = source.Value.ToString();
        }

        private void OnDisable()
        {
            if (source != null)
            {
                source.OnValueChanged -= Amount_OnValueChanged;
            }
        }

        private void OnEnable()
        {
            if (source != null)
            {
                source.OnValueChanged += Amount_OnValueChanged;
            }
        }

        private void Amount_OnValueChanged(BigNumber value)
        {
            amountText.Value.Text = value.ToString();
        }
    }
}
