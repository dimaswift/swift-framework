﻿using SwiftFramework.Core.SharedData.Shop;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    [RequireComponent(typeof(ShopItemView))]
    public class BonusBoosterView : MonoBehaviour
    {
        [SerializeField] private BoosterShopItemView boosterView = null;

        private void Awake()
        {
            GetComponent<ShopItemView>().OnItemSetUp += OnItemSetUp;
        }

        private void OnItemSetUp(ShopItemLink link)
        {
            link.Load(item =>
            {
                ShopItemWithBonusBooster itemWithBonus = item as ShopItemWithBonusBooster;
                if (itemWithBonus == null)
                {
                    gameObject.SetActive(false);
                }
                gameObject.SetActive(true);
                boosterView.SetUp(link);
            });
        }
    }
}
