﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Core.SharedData.Boosters;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Core.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class BoosterForAdView : ElementFor<ShopItemLink>
    {
        [SerializeField] private TMP_Text amountText = null;
        [SerializeField] private TMP_Text descriptionText = null;
        [SerializeField] private TMP_Text timerText = null;
        [SerializeField] private Image iconImage = null;
        [SerializeField] private Button watchButton = null;
        [SerializeField] private Image amountBarFiller = null;
        [SerializeField] private Color limitReachedAmountCollor = Color.green;
        [SerializeField] private Color limitNotReachedAmountColor = Color.white;
        [SerializeField] private PriceView hardPrice = null;

        private IBoosterManager boosterManager;
        private PriceLink price;
        private BoosterConfigLink boosterLink;
        private BoosterConfig boosterConfig;

        public override void Init()
        {
            base.Init();
            watchButton.onClick.AddListener(OnWatchClick);
            boosterManager = App.Core.GetModule<IIdleGameModule>().GetCurrentBoosterManager();
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
            boosterManager.OnBoosterExpired += BoosterManager_OnBoosterExpired;
            boosterManager.OnInventoryChanged += BoosterManager_OnInventoryChanged;
        }

        private void BoosterManager_OnInventoryChanged()
        {
            OnSetUp(Value);
        }

        private void OnHardClicked()
        {
            App.Core.GetModule<IWindowsManager>().Show<ConfirmShopItemPurchaseWindow, ShopItemLink, bool>(Value).Done(confirmed =>
            {
                if (confirmed)
                {
                    App.Core.GetModule<IShopManager>().Buy(Value).Done(result =>
                    {
                        if (result == TransactionResult.Success)
                        {
                            OnSetUp(Value);
                        }
                        else if (result == TransactionResult.InsufficientFunds)
                        {
                            App.Core.GetModule<IWindowsManager>().Show<HardCurrencyPurchaseSuggestionWindow, BigNumber>(price.amount);
                        }
                    });
                }
            });
        }

        private void BoosterManager_OnBoosterExpired(Booster booster)
        {
            OnSetUp(Value);
        }

        private void Now_OnValueChanged(long time)
        {
            if (gameObject.activeInHierarchy && boosterManager.IsActive(boosterLink))
            {
                timerText.text = boosterManager.GetTotalSecondsLeft(boosterLink).ToTimerString();
            }
            UpdateAmount();
        }

        private void OnWatchClick()
        {
            App.Core.GetModule<IAdsManager>().ShowRewardedWithLoadingWindow(boosterLink.Value.descriptionKey).Done(success =>
            {
                if (success)
                {
                    if (boosterLink.HasValue)
                    {
                        boosterManager.AddBoosterToInventory(boosterLink, 1);
                        App.Core.Storage.Save(boosterManager.State);
                        OnSetUp(Value);
                    }
                }
            });
        }


        private IPromise<BoosterConfigLink> GetFirstBooster(ShopItemLink shopItem)
        {
            Promise<BoosterConfigLink> promise = Promise<BoosterConfigLink>.Create();

            shopItem.Load(item =>
            {
                foreach (var reward in item.rewards)
                {
                    reward.Load(rew =>
                    {
                        BoosterReward bus = rew as BoosterReward;

                        if (bus != null)
                        {
                            if (promise.CurrentState == PromiseState.Pending)
                            {
                                promise.Resolve(bus.booster);
                            }
                        }
                    });
                }
            });


            return promise;
        }

        protected override void OnClicked()
        {

        }

        private void UpdateAmount()
        {
            if (boosterConfig)
            {
                long activeBoosters = boosterManager.GetActiveBoostersAmount(boosterLink);
                long maxActiveAmount = boosterConfig.maxActiveBoostersAmount;
                amountText.text = $"{activeBoosters}/{maxActiveAmount}";
                if (amountBarFiller != null)
                {
                    amountBarFiller.fillAmount = (float)activeBoosters / maxActiveAmount;
                }
                watchButton.interactable = activeBoosters < maxActiveAmount;
                hardPrice.SetInteractable(activeBoosters < maxActiveAmount);
                amountText.color = activeBoosters < maxActiveAmount ? limitNotReachedAmountColor : limitReachedAmountCollor;
                timerText.gameObject.SetActive(activeBoosters > 0 && boosterConfig.durationSeconds > 0);
            }
           
        }

        protected override void OnSetUp(ShopItemLink config)
        {
            GetFirstBooster(config).Done(loadedCfgLink =>
            {
                boosterLink = loadedCfgLink;
                config.Load(item =>
                {
                    price = item.price;
                    hardPrice.Init(price, OnHardClicked);
                });

                loadedCfgLink.Load(loadedCfg =>
                {
                    boosterConfig = loadedCfg;
                    iconImage.SetSpriteAsync(loadedCfg.icon);
                    descriptionText.text = App.Core.Local.GetText(loadedCfg.descriptionKey);
                    UpdateAmount();
                });
            });

        }
    }
}