﻿using UnityEngine;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class BoosterIcon : ElementFor<(BoosterConfigLink link, int amount)>
    {
        [SerializeField] private GenericText durationText = null;
        [SerializeField] private GenericText amountText = null;
        [SerializeField] private GenericImage icon = null;

        protected override void OnClicked()
        {

        }

        protected override void OnSetUp((BoosterConfigLink link, int amount) container)
        {
            container.link.Load(config => 
            {
                durationText.Value.Text = config.durationSeconds > 0 ? config.durationSeconds.ToDurationString(App.Core.Local) : "∞";
                icon.Value.SetSprite(config.icon);

                if (amountText.HasValue)
                {
                    amountText.Value.Text = container.amount.ToMultiplierString();
                }
            }); 
        }
    }
}