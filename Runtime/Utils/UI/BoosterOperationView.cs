﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class BoosterOperationView : ElementFor<BoosterSign>
    {
        [SerializeField] private TMP_Text titleText = null;
        [SerializeField] private TMP_Text durationText = null;
        [SerializeField] private TMP_Text signText = null;

        private BoosterSign operation;

        public override void Init()
        {
            base.Init();
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
        }

        private void Now_OnValueChanged(long now)
        {
            if(operation.expirationTime > 0)
            {
                durationText.text = App.Core.Clock.GetSecondsLeft(operation.expirationTime).ToTimerString();
            }
        }

        protected override void OnClicked()
        {
            
        }

        public override void Clear()
        {
            base.Clear();
            operation = new BoosterSign();
        }

        protected override void OnSetUp(BoosterSign operation)
        {
            this.operation = operation;
            titleText.text = operation.title;
            signText.text = operation.sign;
            if (operation.expirationTime > 0)
            {
                durationText.text = App.Core.Clock.GetSecondsLeft(operation.expirationTime).ToTimerString();
            }
            else if (operation.expirationTime == -1)
            {
                durationText.text = App.Core.Local.GetText("#forever");
            }
            else durationText.text = "";
        }
    }

    public struct BoosterSign
    {
        public string sign;
        public string title;
        public long expirationTime;
    }
}
