﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace SwiftFramework.Utils.UI
{
    [RequireComponent(typeof(BounceAnimation))]
    public class ButtonClickBounce : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IPointerExitHandler
    {
        private BounceAnimation bounce;

        private void Awake()
        {
            bounce = gameObject.GetComponent<BounceAnimation>();
            if(bounce == null)
            {
                bounce = gameObject.AddComponent<BounceAnimation>();
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            bounce?.Click();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            bounce?.Release();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            bounce?.Release();
        }
    }
}
