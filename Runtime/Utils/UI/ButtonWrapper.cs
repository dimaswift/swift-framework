﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class ButtonWrapper : Button, IButton, IAffordableHandler
    {
        public bool Interactable
        {
            get
            {
                return interactable;
            }
            set
            {
                interactable = value;
            }
        }

        public void AddListener(UnityAction action)
        {
            onClick.AddListener(action);
        }

        public void SetAfforfable(bool affordable)
        {
            var c = colors;
            c.colorMultiplier = affordable ? 1 : 0.7f;
            colors = c;
        }
    }
}
