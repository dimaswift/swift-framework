﻿using UnityEngine;
using System;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Core;
using SwiftFramework.Core.Views;

namespace SwiftFramework.Utils.UI
{
    public class BuyShopItemButton : ElementFor<ShopItemLink>
    {
        [SerializeField] private bool showConfirmationWindow = true;
        [SerializeField] private PriceView priceView = null;

        public event Action<TransactionResult> OnPurchased = i => { };

        protected override void OnClicked()
        {
            if (showConfirmationWindow && Value.Value.price.CanAfford())
            {
                App.Core.GetModule<IWindowsManager>().Show<ConfirmShopItemPurchaseWindow, ShopItemLink, bool>(Value).Done(confirmed =>
                {
                    if (confirmed)
                    {
                        Purchase();
                    }
                });
            }
            else
            {
                Purchase();
            }
        }

        private void Purchase()
        {
            App.Core.GetModule<IShopManager>().Buy(Value).Then(result =>
            {
                OnPurchased(result);
                GetComponent<IPurchaseTransactionHandler>()?.HandlePurchaseTransaction(Value, result);
            })
           .Catch(e => Debug.LogException(e));
        }

        protected override void OnSetUp(ShopItemLink shopItemLink)
        {
            priceView.Init(shopItemLink.Value.price, () => { }); 
        }
    }
}
