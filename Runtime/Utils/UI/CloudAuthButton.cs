﻿using SwiftFramework.Core;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SwiftFramework.Utils.UI
{
    public class CloudAuthButton : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            App.Core.GetModule<ICloudAuthentication>().OpenWebInterface();
        }
    }
}