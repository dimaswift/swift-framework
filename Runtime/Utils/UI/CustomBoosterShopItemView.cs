﻿using SwiftFramework.Core.SharedData.Boosters;
using SwiftFramework.Core.SharedData.Shop;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class CustomBoosterShopItemView : ShopItemView
    {
        [SerializeField] private BoosterIcon boosterIcon = null;
        [SerializeField] private ShopItemLink shopItem = null;

        private void OnEnable()
        {
            OnSetUp(shopItem);
        }

        protected override void OnSetUp(ShopItemLink shopItem)
        {
            base.OnSetUp(shopItem);
            shopItem.Load(item =>
            {
                BoosterReward boosterReward = item.GetAllRewards().FirstOrDefaultFast() as BoosterReward;
                boosterIcon.SetUp((boosterReward.booster, (int)boosterReward.amount.Value));
            });
        }

    }
}
