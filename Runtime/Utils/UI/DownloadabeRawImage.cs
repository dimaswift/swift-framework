﻿using UnityEngine;
using SwiftFramework.Core;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{

    [RequireComponent(typeof(RawImage))]
    public class DownloadabeRawImage : MonoBehaviour
    {
        [SerializeField] private Transform loadingIcon = null;
        [SerializeField] private Texture2D fallbackTexture = null;

        private RawImage image;

        private RawImage Image
        {
            get
            {
                if(image == null)
                {
                    image = GetComponent<RawImage>();
                }
                return image;
            }
        }

        private void Update()
        {
            if(loadingIcon != null && loadingIcon.gameObject.activeInHierarchy)
            {
                loadingIcon.Rotate(Vector3.forward * Time.unscaledDeltaTime * 360);
            }
        }

        public void SetUp(IPromise<Texture2D> promise)
        {
            if(promise == null)
            {
                Image.enabled = true;
                Image.texture = fallbackTexture;
                loadingIcon.gameObject.SetActive(false);
                return;
            }

            if(loadingIcon != null)
            {
                loadingIcon.gameObject.SetActive(true);
            }

            Image.enabled = false;
            promise.Done(t => 
            {
                Image.enabled = true;
                Image.texture = t;
                if(loadingIcon != null)
                {
                    loadingIcon.gameObject.SetActive(false);
                }
            });
        }
    }

}
