﻿using UnityEngine;
using UnityEngine.EventSystems;
using SwiftFramework.Core;
using TMPro;

namespace SwiftFramework.Utils.UI
{
    public class FacebookLoginButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private TMP_Text statusText = null;
        [SerializeField] private GameObject statusCheckmark = null;

        [SerializeField] private TMP_Text nameText = null;

        private IFacebookProfile facebook;

        private void OnEnable()
        {
            if(facebook != null)
            {
                return;
            }
            App.InitPromise.Done(() =>
            {
                facebook = App.Core.GetModule<IFacebookProfile>();

                if (facebook == null)
                {
                    return;
                }

                facebook.OnLogin += Facebook_OnLogin;

                if(facebook.Profile != null)
                {
                    UpdateProfile(facebook.Profile);
                }
                else
                {
                    SetLoggedOut();
                }
            });
        }

        private void Facebook_OnLogin(FacebookProfile profile)
        {
            UpdateProfile(profile);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if(facebook != null)
            {
                if(facebook.Profile == null)
                {
                    facebook.Login().Done(result =>
                    {
                        if (result.Success)
                        {
                            UpdateProfile(result.Profile);
                        }
                        else
                        {
                            SetLoggedOut();
                        }
                    });
                }
                else
                {
                    facebook.Logout();
                    SetLoggedOut();
                }
            }
        }

        private void SetLoggedOut()
        {
            if(nameText != null)
            {
                nameText.text = "";
            }
            statusText.text = App.Core.Local.GetText("#login");
            statusCheckmark.gameObject.SetActive(false);
        }

        private void UpdateProfile(FacebookProfile profile)
        {
            statusCheckmark.gameObject.SetActive(true);
            statusText.text = App.Core.Local.GetText("#logout");
            if(nameText != null)
            {
                nameText.text = "...";
                profile.Name.Done(n => nameText.text = n);
            }
        }
    }

}
