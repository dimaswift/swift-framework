﻿using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class FundsAmountIcon : MonoBehaviour
    {
        [SerializeField] private GenericText amountText = null;
        [SerializeField] private GenericImage icon = null;

        public void SetUp(IPromise<(BigNumber value, FundsSourceLink source)> args)
        {
            icon.SetActive(false);
            amountText.SetActive(false);
            args.Done(a =>
            {
                icon.SetActive(true);
                amountText.SetActive(true);
                SetUp(a);
            });

        }

        public void SetUp((BigNumber value, FundsSourceLink source) args)
        {
            icon.Value.SetSprite(args.source.Value.Icon);
            amountText.Value.Text = args.value.ToString();
        }
    }
}
