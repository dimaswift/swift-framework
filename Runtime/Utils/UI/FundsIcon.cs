﻿using UnityEngine;
using SwiftFramework.Core;
using TMPro;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class FundsIcon : MonoBehaviour
    {
        [SerializeField] private FundsSourceLink fundsLink = Link.CreateNull<FundsSourceLink>();
        [SerializeField] private GenericText amountText = null;
        [SerializeField] private Image icon = null;

        private IFundsSource funds;

        private void OnEnable()
        {
            if (funds != null || fundsLink.HasValue == false)
            {
                return;
            }

            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                fundsLink.Load(f => 
                {
                    funds = f;
                    SetUp(funds);
                });
            });
        }

        public void SetUp(IFundsSource funds)
        {
            if (this.funds != null && funds != this.funds)
            {
                this.funds.Amount.OnValueChanged -= Amount_OnValueChanged;
            }

            this.funds = funds;
            icon.SetSpriteAsync(funds.Icon);
            amountText.Value.Text = funds.Amount.Value.ToString();
            funds.Amount.OnValueChanged += Amount_OnValueChanged;
        }

        public void SetUp(FundsSourceLink fundsLink)
        {
            amountText.Value.Text = "...";
            fundsLink.Load(funds => SetUp(funds));
        }


        private void OnDestroy()
        {
            if (funds != null)
            {
                funds.Amount.OnValueChanged -= Amount_OnValueChanged;
            }
        }

        private void Amount_OnValueChanged(BigNumber value)
        {
            amountText.Value.Text = value.ToString();
        }
    }
}
