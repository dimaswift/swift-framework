﻿using UnityEngine;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Funds;

namespace SwiftFramework.Utils.UI
{
    public class HardCurrencyPuchaseSuggester : MonoBehaviour, IPurchaseTransactionHandler
    {
        public void HandlePurchaseTransaction(ShopItemLink shopItem, TransactionResult result)
        {
            if (result != TransactionResult.InsufficientFunds)
            {
                return;
            }

            FundsPrice price = shopItem.Value.price.Value as FundsPrice;
            IFundsSource hardCurrency = App.Core.GetModule<IIdleGameModule>().HardFunds;
            if (price != null)
            {
                if (price.source.Value == hardCurrency)
                {
                    App.Core.GetModule<IWindowsManager>().Show<HardCurrencyPurchaseSuggestionWindow, BigNumber>(shopItem.Value.price.amount);
                }
            }
        }
    }
}
