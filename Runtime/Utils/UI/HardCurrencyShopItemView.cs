﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class HardCurrencyShopItemView : ShopItemView
    {
        [SerializeField] private TMP_Text amount = null; 

        protected override void OnSetUp(ShopItemLink shopItem)
        {
            base.OnSetUp(shopItem);
            shopItem.Load(item =>
            {
                amount.text = item.GetAllRewards().FirstOrDefaultFast().GetAmount().stringValue;
            });
        }
    }
}
