﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;
using System;

namespace SwiftFramework.Utils.UI
{
    [RequireComponent(typeof(TMP_Text))]
    public class HyperLink : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private bool openLinkOnClick = true;
        public event Action<string> OnLinkClicked = link => { };
        private TMP_Text text;

        public TMP_Text Text
        {
            get
            {
                if (text == null)
                {
                    text = GetComponent<TMP_Text>();
                }
                return text;
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            int linkIndex = TMP_TextUtilities.FindIntersectingLink(Text, Input.mousePosition, eventData.enterEventCamera);

            if (linkIndex != -1)
            { 
                TMP_LinkInfo linkInfo = Text.textInfo.linkInfo[linkIndex];
                OnLinkClicked(linkInfo.GetLinkID());
                if(openLinkOnClick )
                {
                    Application.OpenURL(linkInfo.GetLinkID());
                }
            }
        }
    }

}
