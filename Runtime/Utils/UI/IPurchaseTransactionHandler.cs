﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;

namespace SwiftFramework.Utils.UI
{
    public interface IPurchaseTransactionHandler
    {
        void HandlePurchaseTransaction(ShopItemLink shopItem, TransactionResult result);
    }
}
