﻿using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public interface ITab
    {
        string Name { get; }
        Sprite Icon { get; }
    }
}
