﻿using SwiftFramework.Core;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class ImageWrapper : Image, IImage
    {
        public void SetAlpha(float alpha)
        {
            Color color = this.color;
            color.a = alpha;
            this.color = color;
        }

        public void SetSprite(SpriteLink sprite)
        {
            this.SetSpriteAsync(sprite);
        }
    }
}
