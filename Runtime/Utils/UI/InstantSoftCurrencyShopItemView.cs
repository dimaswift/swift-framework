﻿using SwiftFramework.Core.SharedData.Shop;
using UnityEngine;
using SwiftFramework.Core;
using SwiftFramework.ShopManager;

namespace SwiftFramework.Utils.UI
{
    public class InstantSoftCurrencyShopItemView : ShopItemView
    {
        [SerializeField] private GenericText amountText = null;
        [SerializeField] private GenericText durationText = null;
        [SerializeField] private GenericImage currencyIcon = null;

        protected override void OnSetUp(ShopItemLink shopItem)
        {
            base.OnSetUp(shopItem);
            shopItem.Load(item =>
            {
                InstantSoftCurrencyReward reward = item.GetAllRewards().FirstOrDefaultFast() as InstantSoftCurrencyReward;
                currencyIcon.Value.SetSprite(reward.Icon);
                amountText.Value.Text = "+" + reward.GetAmount().ToString();
                durationText.Value.Text = App.Core.Local.GetText("instant_cash_duration", reward.GetDurationSeconds().ToDurationString(App.Core.Local));
            });
        }
    }
}
