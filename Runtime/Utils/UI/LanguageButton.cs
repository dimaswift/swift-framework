﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class LanguageButton : ElementFor<LanguageView>
    {
        [SerializeField] private Image icon = null;
        [SerializeField] private TMP_Text languageName = null;
        [SerializeField] private GameObject selectionFrame = null;

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp(LanguageView lang)
        {
            if(icon != null)
            {
                icon.sprite = lang.Icon;
            }
            languageName.text = lang.Language.ToString();
            selectionFrame.SetActive(lang.IsCurrentLanguage);
        }
    }
}
