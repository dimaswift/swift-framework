﻿using UnityEngine;
using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop.OfferSystem;
using SwiftFramework.Core.SharedData.Shop;

namespace SwiftFramework.Utils.UI
{

    public class LimitedOfferButton : ElementFor<OfferTriggerLink>
    {
        [SerializeField] private OfferTriggerLink trigger = null;
        [SerializeField] private GenericText durationText = null;
        [SerializeField] private GenericImage icon = null;

        private IShopManager shop;
        private long expireTimestamp;

        protected override void OnClicked()
        {
            SpecialOffer offer = trigger.Value.offer.Value as SpecialOffer;
            App.Core.GetModule<IWindowsManager>().Show<OfferWindow, ShopItemLink>(Value.Value.offer, offer.window);
        }

        private void Awake()
        {
            if (trigger.HasValue)
            {
                Init();
            }
        }

        public override void Init()
        {
            base.Init();
            shop = App.Core.GetModule<IShopManager>();
            shop.OfferTriggers.OnOfferTriggered += OfferTriggers_OnOfferTriggered;
            if (trigger.HasValue)
            {
                SetUp(trigger);
            }
        }

        protected override void OnSetUp(OfferTriggerLink value)
        {
            if (shop.OfferTriggers.TryGetOfferState(Value, out expireTimestamp) == false)
            {

                gameObject.SetActive(false);
                return;
            }

            shop.OnTransactionCompleted -= Shop_OnTransactionCompleted;
            shop.OnTransactionCompleted += Shop_OnTransactionCompleted;

            icon.Value.SetSprite(value.Value.offer.Value.icon);

            if (value.Value.limited == false)
            {
                durationText.Value.Text = "";
            }
            else
            {
                shop.OfferTriggers.OnOfferExpired += OfferTriggers_OnOfferExpired;

                App.OnClockTick -= App_OnClockTick;
                App.OnClockTick += App_OnClockTick;
                UpdateTimer();
            }
            gameObject.SetActive(true);
        }

        private void OfferTriggers_OnOfferExpired(OfferTriggerLink triggerLink)
        {
            if (triggerLink == Value)
            {
                gameObject.SetActive(false);
            }
        }

        private void OfferTriggers_OnOfferTriggered((OfferTriggerLink trigger, long expireTimestamp) value)
        {
            if (trigger.HasValue == false)
            {
                return;
            }
            if (value.trigger == trigger)
            {
                SetUp(trigger);
            }
        }

        private void Shop_OnTransactionCompleted(ShopItemLink item, TransactionResult result)
        {
            if (result == TransactionResult.Success && item == Value.Value.offer)
            {
                gameObject.SetActive(false);
            }
        }

        private void App_OnClockTick(long value)
        {
            UpdateTimer();
        }

        private void UpdateTimer()
        {
            long timeLeft = expireTimestamp - App.Now;
            if (timeLeft < 0)
            {
                App.OnClockTick -= App_OnClockTick;
                gameObject.SetActive(false);
                return;
            }
            durationText.Value.Text = timeLeft.ToTimerString();
        }
    }

}