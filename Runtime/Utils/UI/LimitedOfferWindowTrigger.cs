﻿using UnityEngine;
using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop.OfferSystem;
using SwiftFramework.Core.SharedData.Shop;

namespace SwiftFramework.Utils.UI
{
    public class LimitedOfferWindowTrigger : MonoBehaviour
    {
        private void Awake()
        {
            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                App.Core.GetModule<IShopManager>().OfferTriggers.OnOfferTriggered += OfferTriggers_OnOfferTriggered;
            });
        }

        private void OfferTriggers_OnOfferTriggered((OfferTriggerLink trigger, long expireTimestamp) value)
        {
            if (value.trigger.Value.showOfferWindowOnTrigger)
            {
                SpecialOffer offer = value.trigger.Value.offer.Value as SpecialOffer;
                App.Core.GetModule<IWindowsManager>().ShowChained<OfferWindow, ShopItemLink>(value.trigger.Value.offer, offer.window);
            }
        }
    }

}