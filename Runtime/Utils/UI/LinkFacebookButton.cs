﻿using UnityEngine;
using UnityEngine.EventSystems;
using SwiftFramework.Core;
using TMPro;

namespace SwiftFramework.Utils.UI
{
    public class LinkFacebookButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private TMP_Text statusText = null;
        [SerializeField] private GameObject loadingIcon = null;
        [SerializeField] private GameObject statusCheckmark = null;
        [SerializeField] private GameObject contentRoot = null;
        [SerializeField] private TMP_Text nameText = null;


        private IFacebookCloudAuthentication facebook;

        private void OnEnable()
        {
            if (facebook != null)
            {
                return;
            }
            App.InitPromise.Done(() =>
            {
                facebook = App.Core.GetModule<IFacebookCloudAuthentication>();

                if (facebook == null)
                {
                    gameObject.SetActive(false);
                    return;
                }

                facebook.OnLogin += CloudManager_OnLogin;

                CloudManager_OnLogin();
            });
        }

        private void CloudManager_OnLogin()
        {
            SetLoading(false);
            if (facebook.IsLinked())
            {
                SetLinked(facebook.Profile);
            }
            else
            {
                SetUnlinked();
            }
        }

        private void SetLoading(bool loading)
        {
            loadingIcon.SetActive(loading);
            contentRoot.SetActive(loading == false);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (loadingIcon.gameObject.activeSelf)
            {
                SetLoading(false);
                return;
            }
            if (facebook != null)
            {
                SetLoading(true);
                if (facebook.IsLinked())
                {
                    facebook.UnlinkAccount().Then(() =>
                    {
                        SetUnlinked();
                    })
                    .Catch(e => 
                    {
                        SetLoading(false);
                        ShowError(e.Message);
                        Debug.LogException(e);
                    });
                }
                else
                {
                    facebook.LinkAccount().Then(facebookProfile =>
                    {
                        SetLinked(facebookProfile);
                    })
                    .Catch(e =>
                    {
                        SetLoading(false);
                        ShowError(e.Message);
                        Debug.LogException(e);
                    });
                }
            }
        }

        private void ShowError(string errorKey)
        {
            App.Core.GetModule<IWindowsManager>()?.Show<PopUpWindow, string>(errorKey);
        }

        private void SetUnlinked()
        {
            SetLoading(false);
            if (nameText != null)
            {
                nameText.text = "";
            }
            statusText.text = App.Core.Local.GetText("#link_account");
            statusCheckmark.gameObject.SetActive(false);
        }

        private void SetLinked(FacebookProfile profile)
        {
            SetLoading(false);
            statusCheckmark.gameObject.SetActive(true);
            statusText.text = App.Core.Local.GetText("#unlink_account");
            if (nameText != null)
            {
                nameText.text = "...";
                profile.Name.Done(n => nameText.text = n);
            }
        }
    }

}
