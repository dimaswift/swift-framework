﻿using UnityEngine;
using SwiftFramework.Core;
using UnityEngine.UI;
using TMPro;

namespace SwiftFramework.Utils.UI
{
    public class LoadingScreen : MonoBehaviour, ILoadingScreen
    {
        [SerializeField] private Slider loadingBar = null;
        [SerializeField] private TMP_Text versionText = null;

        private float currentLoadingProgress;

        public IPromise Hide()
        {
            gameObject.SetActive(false);
            return Promise.Resolved();
        }

        public void SetLoadProgress(float progress)
        {
            currentLoadingProgress = progress;
        }

        private void Update()
        {
            if(loadingBar == null)
            {
                return;
            }
            loadingBar.value = Mathf.Lerp(loadingBar.value, currentLoadingProgress, Time.deltaTime * 5);
        }

        public void SetVersion(string version)
        {
            if(versionText == null)
            {
                return;
            }
            versionText.text = "v" + version;
        }

        public IPromise Show()
        {
            if(loadingBar != null)
            {
                loadingBar.value = 0;
            }
            gameObject.SetActive(true);
            return Promise.Resolved();
        }
    }

}
