﻿using TMPro;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class LocalizedText : MonoBehaviour
    {
        [SerializeField] private string key = null;
        [Tooltip("Optional")]
        [SerializeField] private string parameter = null;

        private TMP_Text text;

        private void Start()
        {
            App.WaitForState(AppState.CoreModulesInitialized, () =>
            {
                ILocalizationManager localization = App.Core.GetModule<ILocalizationManager>();
                localization.OnLanguageChanged += Localization_OnLanguageChanged;
            });
        }

        private void Localization_OnLanguageChanged()
        {
            Translate();
        }

        private void Translate()
        {
            if (text == null)
            {
                text = GetComponent<TMP_Text>();
            }
            ILocalizationManager localization = App.Core.GetModule<ILocalizationManager>();
            text.text = localization.GetText(key, parameter);
        }

        private void OnEnable()
        {
            App.WaitForState(AppState.CoreModulesInitialized, Translate);
        }
    }
}
