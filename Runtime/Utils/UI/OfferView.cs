﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Core.Views;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class OfferView : MonoBehaviour
    {
        [SerializeField] [LinkFilter(typeof(ISpecialOfferTag))] private ShopItemLink item = null;
        [SerializeField] private PriceView priceText = null;

        private IShopManager shop;

        private void Awake()
        {
            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                if (item.HasValue == false)
                {
                    gameObject.SetActive(false);
                    return;
                }
                shop = App.Core.GetModule<IShopManager>();

                if (shop.IsPurchased(item))
                {
                    gameObject.SetActive(false);
                    return;
                }

                shop.OnTransactionCompleted += Shop_OnTransactionCompleted;

                priceText.Init(item.Value.price, OpenOffer);
            });
        }

        private void Shop_OnTransactionCompleted(ShopItemLink item, TransactionResult result)
        {
            if (result == TransactionResult.Success && item == this.item)
            {
                gameObject.SetActive(false);
            }
        }

        private void OpenOffer()
        {
            SpecialOffer offer = item.Value as SpecialOffer;
            if (offer == null)
            {
                Debug.LogError($"Only special offers can be used in OfferView");
                return;
            }
            App.Core.GetModule<IWindowsManager>().Show<OfferWindow, ShopItemLink>(item, offer.window);
        }
    }
}
