﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class RandomRewardShopItemView : ShopItemView
    {
        [SerializeField] private GenericText descText = null;

        public override void Init()
        {
            base.Init();
            buyButton.OnPurchased += BuyButton_OnPurchased;
            buyButton.OnClick += BuyButton_OnClick;
        }

        private void BuyButton_OnClick(ElementFor<ShopItemLink> element)
        {
            RandomRewardShopItem item = Value.Value as RandomRewardShopItem;
            item.RollReward();
        }

        protected override void OnSetUp(ShopItemLink link)
        {
            base.OnSetUp(link);

            if (descText.HasValue)
            {
                descText.Value.Text = App.Core.Local.GetText(link.GetName() + "_desc");
            }
        }

        private void BuyButton_OnPurchased(TransactionResult result)
        {
            if (result == TransactionResult.Success)
            {
                RandomRewardShopItem item = Value.Value as RandomRewardShopItem;
                App.Core.GetModule<IWindowsManager>().Show<RandomPurchaseResultWindow, ShopItemLink>(Value);
            }
        }
    }
}
