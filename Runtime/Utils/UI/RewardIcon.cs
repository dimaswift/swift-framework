﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class RewardIcon : ElementFor<RewardLink>
    {
        [SerializeField] private TMP_Text descText = null;
        [SerializeField] private TMP_Text amountText = null;
        [SerializeField] private Image rewardIcon = null;

         
        protected override void OnClicked()
        {
           
        }

        protected override void OnSetUp(RewardLink link)
        {
            link.Load(reward =>
            {
                if (descText != null)
                {
                    descText.text = App.Core.Local.GetText(reward.GetDescription());
                }

                amountText.text = reward.GetAmount().ToString();
                rewardIcon.SetSpriteAsync(reward.Icon);
            });
        }
    }
}
