﻿using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class RewardText : ElementFor<RewardLink>
    {
        [SerializeField] private GenericText rewardText = null;
        [SerializeField] private GenericImage rewardIcon = null;

        protected override void OnClicked()
        {

        }

        protected override void OnSetUp(RewardLink link)
        {
            link.Load(reward =>
            {
                rewardText.Value.Text = App.Core.Local.GetText(reward.GetDescription());
                rewardIcon.Value.SetSprite(reward.Icon);
            });
        }
    }
}
