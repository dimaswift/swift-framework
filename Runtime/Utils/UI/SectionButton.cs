﻿using UnityEngine;
using System;
using UnityEngine.EventSystems;
using SwiftFramework.Core.SharedData.Shop;

namespace SwiftFramework.Utils.UI
{
    public class SectionButton : MonoBehaviour, IPointerClickHandler
    {
        public ShopSectionLink SectionLink => section;
        public RectTransform SectionRect => sectionRect;

        [SerializeField] private ShopSectionLink section = null;
        [SerializeField] private RectTransform sectionRect = null;

        public event Action<SectionButton> OnClick = s => { };

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick(this);
        }
    }

}
