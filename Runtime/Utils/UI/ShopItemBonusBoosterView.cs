﻿using UnityEngine;
using UnityEngine.UI;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class ShopItemBonusBoosterView : ElementFor<BoosterConfigLink>
    {
        [SerializeField] private Image boosterIcon = null;

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp(BoosterConfigLink booster)
        {
            booster.Load(cfg =>
            {
                boosterIcon.SetSpriteAsync(cfg.icon);
            });
        }
    }
}
