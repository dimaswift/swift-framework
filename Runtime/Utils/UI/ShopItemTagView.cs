﻿using SwiftFramework.Core.SharedData.Shop;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class ShopItemTagView : MonoBehaviour
    {
        [SerializeField] private ShopItemTagLink itemTag = null;

        public void SetUp(ShopItem shopItem)
        {
            gameObject.SetActive(itemTag == shopItem.tag);
        }
    }
}
