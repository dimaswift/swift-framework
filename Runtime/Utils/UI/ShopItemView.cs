﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace SwiftFramework.Utils.UI
{
    public abstract class ShopItemView : ElementFor<ShopItemLink>
    {
        public event Action<ShopItemLink> OnItemSetUp = i => { };
        public event Action<TransactionResult, ShopItemLink> OnPurchased = (r, i) => { };

        [SerializeField] protected GenericText titleText = null;
        [SerializeField] protected GenericImage iconImage = null;
        [SerializeField] protected BuyShopItemButton buyButton = null;


        private List<ShopItemTagView> tags = new List<ShopItemTagView>();

        public override void Init()
        {
            GetComponentsInChildren(true, tags);
            buyButton.OnPurchased += BuyButton_OnPurchased;
            base.Init();
        }

        protected sealed override void OnClicked()
        {

        }

        protected override void OnSetUp(ShopItemLink link)
        {
            gameObject.SetActive(true);
            if (buyButton != null)
            {
                buyButton.SetUp(link);

            }
            link.Load(shopItem =>
            {
                if (titleText.HasValue)
                {
                    titleText.Value.Text = App.Core.Local.GetText(shopItem.name + "_title");
                }

                if (iconImage.HasValue)
                {
                    iconImage.Value.SetSprite(shopItem.icon);
                }

                foreach (var tag in tags)
                {
                    tag.SetUp(shopItem);
                }

                OnItemSetUp(link);
            });
        }

        private void BuyButton_OnPurchased(TransactionResult result)
        {
            OnPurchased(result, Value);
        }
    }
}
