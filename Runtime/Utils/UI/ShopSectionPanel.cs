﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Core.SharedData.Shop;
using System;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class ShopSectionPanel : MonoBehaviour
    {
        public event Action<ShopItemLink> OnBuyClick = i => { };

        [SerializeField] private ShopSectionLink section = null;
        [SerializeField] private GenericText titleText = null;
        [SerializeField] private ElementSet shopItemsSet = null;

        public ShopSectionLink Section => section;

        private void Awake()
        {
            App.WaitForState(AppState.ModulesInitialized, Init);
        }

        public void Init()
        {
            if (titleText.HasValue)
            {
                titleText.Value.Text = App.Core.Local.GetText(section.GetName() + "_title");
            }

            section.Load(s =>
            {
                shopItemsSet.SetUp<ShopItemView, ShopItemLink>(s.items, (i) => OnBuyClick(i.Value));
            });
        }
    }
}
