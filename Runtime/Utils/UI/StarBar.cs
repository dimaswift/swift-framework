﻿using SwiftFramework.Core;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    [RequireComponent(typeof(ElementSet))]
    public class StarBar : ElementFor<(int current, int total)>, IProgressBar
    {
        [SerializeField] private int maxAmount = 3;
        [SerializeField] private ElementSet starsSet = null;

        public void SetUp(float progressNormalized)
        {
            OnSetUp((Mathf.FloorToInt(maxAmount * progressNormalized), maxAmount));
        }

        public void SetUp(int current, int total)
        {
            OnSetUp((current, total));
        }

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp((int current, int total) value)
        {
            IEnumerable<bool> result (int current, int total)
            {
                for (int i = 0; i < total; i++)
                {
                    yield return i < current;
                }
            }
            starsSet.SetUp<StarElement, bool>(result(value.current, value.total));
        }

    }
}
