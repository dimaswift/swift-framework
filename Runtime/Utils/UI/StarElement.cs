﻿using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class StarElement : ElementFor<bool>
    {
        [SerializeField] private GameObject activeStar = null;

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp(bool value)
        {
            activeStar.SetActive(value);
        }
    }
}
