﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using System;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class SupervisorAssignConfirmationPopUp : MonoBehaviour
    {
        [SerializeField] private GenericButton assignButton = null;
        [SerializeField] private GenericButton cancelButton = null;
        [SerializeField] private GenericImage supervisorIcon = null;
        [SerializeField] private GenericImage zoneIcon = null;
        [SerializeField] private LinkIconHandler zoneIconHandler = null;


        private Action onConfirm;

        private void Awake()
        {
            assignButton.Value.AddListener(OnAssignClick);
            cancelButton.Value.AddListener(OnCancelClick);
        }

        private void OnCancelClick()
        {
            onConfirm = null;
            gameObject.SetActive(false);
        }

        private void OnAssignClick()
        {
            onConfirm?.Invoke();
            gameObject.SetActive(false);
        }

        public void Show(SharedSupervisorState state, Action onConfirm)
        {
            gameObject.SetActive(true);

            this.onConfirm = onConfirm;

            zoneIcon.Value.SetSprite(zoneIconHandler.Value.GetIcon(state.assginedZone));

            supervisorIcon.Value.SetSprite(state.supervisor.Value.skin.Value.icon);
        }
    }
}
