﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class TMPTextWrapper : MonoBehaviour, IText
    {
        private TMP_Text text;


        public Color Color
        {
            get
            {
                return text.color;
            }
            set
            {
                text.color = value;
            }
        }


        public string Text
        {
            get
            {
                Awake();
                return text.text;
            }
            set
            {
                Awake();
                text.text = value;
            }
        }

        public void SetAsync<T>(IPromise<T> promise)
        {
            text.text = "...";
            promise.Done(v => text.text = v.ToString());
        }

        public void SetAsync<T>(IPromise<T> promise, string format)
        {
            text.text = "...";
            promise.Done(v => text.text = string.Format(format, v.ToString()));
        }

        private void Awake()
        {
            if (!text)
            {
                text = GetComponent<TMP_Text>();
            }
        }
    }
}
