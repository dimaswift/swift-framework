﻿using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class TiledSpriteProgressBar : MonoBehaviour, IProgressBar
    {
        [SerializeField] private SpriteRenderer sprite = null;
        [SerializeField] private GenericText progressText = null;
        [SerializeField] private bool vertical = false;

        private Vector2? fullSize;


        public void SetUp(float progressNormalized)
        {
            if (fullSize.HasValue == false)
            {
                fullSize = sprite.size;
            }
            if (vertical)
            {
                sprite.size = new Vector2(fullSize.Value.x, fullSize.Value.y * progressNormalized);
            }
            else
            {
                sprite.size = new Vector2(fullSize.Value.x * progressNormalized, fullSize.Value.y);
            }
        }

        public void SetUp(int current, int total)
        {
            SetUp((float)current / total);
            if (progressText.HasValue)
            {
                progressText.Value.Text = $"{current}/{total}";
            }
        }
    }

}
