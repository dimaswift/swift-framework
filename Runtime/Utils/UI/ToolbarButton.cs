﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class ToolbarButton : ElementFor<ITab>
    {
        public bool Selected { get; private set; }

        [SerializeField] private TMP_Text nameText = null;
        [SerializeField] private Image icon = null;
        [SerializeField] private GameObject selectedState = null;
        [SerializeField] private GameObject unSelectedState = null;

        protected override void OnClicked()
        {

        }

        public void SetSelected(bool selected)
        {
            Selected = selected;
            selectedState.SetActive(selected);
            unSelectedState.SetActive(!selected);
        }

        protected override void OnSetUp(ITab value)
        {
            nameText.text = value.Name;
            if(icon != null)
            {
                icon.sprite = value.Icon;
            }
        }
    }
}
