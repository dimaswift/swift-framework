﻿using UnityEngine;
using SwiftFramework.Core;
using UnityEngine.UI;
using SwiftFramework.Core.SharedData;
using System;

namespace SwiftFramework.Utils.UI
{
    public class TopDownWindowAnimation : MonoBehaviour, IAppearAnimationHandler
    {
        [SerializeField] private float speed = 1f;
        [SerializeField] private Image coverImage = null;
        [SerializeField] private RectTransform panel = null;
        [SerializeField] private CurveLink showCurve = Link.CreateNull<CurveLink>();
        [SerializeField] private CurveLink hideCurve = Link.CreateNull<CurveLink>();

        private float coverAlpha;

        private void Awake()
        {
            if (coverImage)
            {
                coverAlpha = coverImage.color.a;
            }
        }

        public void ProcessHiding(float timeNormalized)
        {
            if (coverImage)
            {
                coverImage.color = coverImage.color.SetAlpha(Mathf.Lerp(coverAlpha, 0, timeNormalized * speed));
            }
            panel.localPosition = new Vector3(panel.localPosition.x, Mathf.Lerp(0, panel.rect.height, hideCurve.Evaluate(timeNormalized * speed)));
        }

        public void ProcessShowing(float timeNormalized)
        {
            if (coverImage)
            {
                coverImage.color = coverImage.color.SetAlpha(Mathf.Lerp(0, coverAlpha, timeNormalized * speed));
            }
            panel.localPosition = new Vector3(panel.localPosition.x, Mathf.Lerp(panel.rect.height, 0, showCurve.Evaluate(timeNormalized * speed)));
        }
    }
}