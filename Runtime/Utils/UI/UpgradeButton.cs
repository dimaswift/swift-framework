﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    [RequireComponent(typeof(Physics2DButton))]
    public class UpgradeButton : MonoBehaviour
    {
        [SerializeField] private GameObject upgradeAvailableState = null;
        [SerializeField] private TextMeshPro levelText = null;

        private Physics2DButton button;
        private IUpgradable upgradable;
        private IWindowsManager windowsManager;

        public void Init(IUpgradable upgradable)
        {
            if (button == null)
            {
                button = GetComponent<Physics2DButton>();
                button.AddListener(() => OnClick());
            }

            this.upgradable = upgradable;
            this.upgradable = upgradable;
            windowsManager = App.Core.GetModule<IWindowsManager>();
            upgradeAvailableState.SetActive(upgradable.UpgradeController.CanAffordLevelUp(1));
            upgradable.UpgradeController.UpgradeAvailable.OnValueChanged += UpgradeAvailable_OnValueChanged;
            upgradable.UpgradeController.OnLeveledUp += UpdateLevelUpText;
            UpdateLevelUpText();
        }

        private void UpdateLevelUpText()
        {
            levelText.text = App.Core.Local.GetText("#lvl", upgradable.UpgradeState.Level + 1);
        }

        private void UpgradeAvailable_OnValueChanged(bool available)
        {
            upgradeAvailableState.SetActive(available);
        }

        private void OnClick()
        {
            windowsManager.Show<UpgradeWindow, UpgradeWindow.Args>(new UpgradeWindow.Args()
            {
                upgradable = upgradable,
            });
        }
    }

}
