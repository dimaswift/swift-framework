﻿using UnityEngine;
using SwiftFramework.Core;
using TMPro;

namespace SwiftFramework.Utils.UI
{
    public class WalletView : MonoBehaviour
    {
        [SerializeField] private TMP_Text amountText = null;

        private void Awake()
        {
            App.InitPromise.Done(() =>
            {
                IWalletManager walletManager = App.Core.GetModule<IWalletManager>();
                if (walletManager != null)
                {
                    amountText.text = walletManager.Credits.Value.ToString();
                    walletManager.Credits.OnValueChanged += Credits_OnValueChanged;
                }
            });
        }

        private void Credits_OnValueChanged(int value)
        {
            amountText.text = value.ToString();
        }
    }

}
