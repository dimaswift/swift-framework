using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using System;
using System.Collections;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class AdLoadingWindow : WindowWithArgsAndResult<AdLoadingWindow.Args, bool>
    {
        public class Args
        {
            public string placementId;
            public float timeout;
            public Action onTimeout;
            public IAdsManager adsManager;
        }

        public override void OnShown()
        {
            base.OnShown();
            SetCloseButtonActive(true);
            StartCoroutine(ShowingRewardedCoroutine(Arguments.placementId));
        }

        private IEnumerator ShowingRewardedCoroutine(string placementId)
        {
            float timeout = Arguments.timeout;

            while (Arguments.adsManager.IsRewardedReady() == false)
            {
                timeout -= Time.unscaledDeltaTime;
                if (timeout < 0)
                {
                    Arguments.onTimeout?.Invoke();
                    Resolve(false);
                    Hide();
                    yield break;
                }
                yield return null;
            }

            Arguments.adsManager.ShowRewarded(placementId).Done(result =>
            {
                if (Resolved == false)
                {
                    if (result == RewardedShowResult.Success)
                    {
                        Resolve(true);
                    }
                    else if (result == RewardedShowResult.NotReady)
                    {
                        Resolve(false);
                    }
                }
                Hide();
            });
        }
    }
}