﻿using System;
using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class AppRedirectWindow : Window
    {
        [SerializeField] private Button openStoreButton = null;

        private string newBundleId;

        public override void WarmUp()
        {
            ICloudAuthentication cloudManager = App.Core.GetModule<ICloudAuthentication>();
            if (cloudManager != null)
            {
                openStoreButton.onClick.AddListener(OnOpenStoreClick);
                cloudManager.OnAppRedirectionReceived += CloudManager_OnAppRedirectionReceived;
            }
        }

        private void OnOpenStoreClick()
        {
            Application.OpenURL($"https://play.google.com/store/apps/details?id={newBundleId}");
        }

        private void CloudManager_OnAppRedirectionReceived(string newBundleId)
        {
            this.newBundleId = newBundleId;
            Show();
        }
    }
}