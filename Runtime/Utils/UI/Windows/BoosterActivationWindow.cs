﻿using SwiftFramework.Core.Windows;
using SwiftFramework.Core.Boosters;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class BoosterActivationWindow : WindowWithArgs<(BoosterConfigLink booster, IBoosterManager manager)>
    {
        [SerializeField] private BoosterIcon boosterIcon = null;
        [SerializeField] private GenericText descriptionText = null;
        [SerializeField] private GenericButton confirmButton = null;
        [SerializeField] private GenericButton increaseAmountButton = null;
        [SerializeField] private GenericButton decreaseAmountButton = null;
        [SerializeField] private GenericText selectedAmountText = null;
        [SerializeField] private ProgressBar selectionBar = null;

        private int selectedAmount;
        private int maxAmount;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            confirmButton.Value.AddListener(OnConfrimClick);
            increaseAmountButton.Value.AddListener(OnIncreaseAmountClick);
            decreaseAmountButton.Value.AddListener(OnDecreaseAmountClick);
        }

        private void OnDecreaseAmountClick()
        {
            if (selectedAmount <= 1)
            {
                return;
            }
            selectedAmount--;
            UpdateContent();
        }

        private void OnIncreaseAmountClick()
        {
            if (selectedAmount >= maxAmount)
            {
                return;
            }
            selectedAmount++;
            UpdateContent();
        }

        private void OnConfrimClick()
        {
            Arguments.manager.ActivateBooster(Arguments.booster, selectedAmount);
            UpdateContent();
            Hide();
            App.Core.GetModule<IWindowsManager>().Show<BoosterAnnounceWindow, BoosterConfigLink>(Arguments.booster);
        }

        public override void OnStartShowing((BoosterConfigLink booster, IBoosterManager manager) arguments)
        {
            base.OnStartShowing(arguments);
            boosterIcon.SetUp((arguments.booster, 1));

            maxAmount = Arguments.manager.GetBoosterAmountInInventory(Arguments.booster); 
            selectedAmount = 1;
            UpdateContent();
        }

        private void UpdateContent()
        {
            selectedAmountText.Value.Text = $"{selectedAmount}";
            descriptionText.Value.Text = App.Core.Local.GetText("booster_income_desc", Arguments.booster.Value.multiplier, (Arguments.booster.Value.durationSeconds * selectedAmount).ToDurationString(App.Core.Local));
            selectionBar.Value.SetUp(selectedAmount, maxAmount);
        }
    }
}