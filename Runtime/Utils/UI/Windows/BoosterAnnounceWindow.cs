﻿using SwiftFramework.Core.Windows;
using SwiftFramework.Core.Boosters;
using UnityEngine;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class BoosterAnnounceWindow : WindowWithArgs<BoosterConfigLink>
    {
        [SerializeField] private GenericText boosterDescription = null;
        [SerializeField] private GenericImage boosterIcon = null;
        [SerializeField] private float showDuration = 3;


        public override void OnStartShowing(BoosterConfigLink arguments)
        {
            base.OnStartShowing(arguments);
            boosterDescription.Value.Text = App.Core.Local.GetText(arguments.Value.descriptionKey);
            boosterIcon.Value.SetSprite(arguments.Value.icon);
            App.Core.Timer.WaitFor(showDuration).Done(() => 
            {
                if (IsShown)
                {
                    Hide();
                }
            });
        }
    }

}