﻿using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;
using SwiftFramework.Core.SharedData.Shop;
using SwiftFramework.Core.Windows;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{

    [AddrSingleton]
    public class BoostersForAdsWindow : WindowWithArgs<BoosterManagerConfig>
    {
        [SerializeField] private ElementSet boostersViewSet = null;

        public override void OnStartShowing(BoosterManagerConfig config)
        {
            base.OnStartShowing(config);
            boostersViewSet.SetUp<BoosterForAdView, ShopItemLink>(config.boostersForAds);
        }
    }
}