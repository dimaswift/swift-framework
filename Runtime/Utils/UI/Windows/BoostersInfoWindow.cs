﻿using System.Collections.Generic;
using UnityEngine;
using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using SwiftFramework.Core.Boosters;

namespace SwiftFramework.Utils.UI
{

    [AddrSingleton]
    public class BoostersInfoWindow : WindowWithArgs<IBoosterManager>
    {
        [SerializeField] private BoosterTargetLink boosterTarget = null;
        [SerializeField] private ElementSet itemsSet = null;

        private void BoosterController_OnBoosterExpired(Booster booster)
        {
            OnStartShowing();
        }

        public override void OnStartShowing(IBoosterManager boosterManager)
        {
            base.OnStartShowing(boosterManager);
            boosterManager.OnBoosterExpired -= BoosterController_OnBoosterExpired;
            boosterManager.OnBoosterExpired += BoosterController_OnBoosterExpired;
            if (Arguments.GetTotalMultiplier(boosterTarget) > 0)
            {
                itemsSet.SetUp<BoosterOperationView, BoosterSign>(GetOperations());
            }
            else
            {
                itemsSet.Clear();
            }
        }

        private IEnumerable<BoosterSign> GetOperations()
        {
            float multiplier = 1;
            IBoosterManager manager = Arguments;
            IEnumerable<Booster> activeBoosters = manager.GetActiveBoosters(BoosterType.Temporary, BoosterOperation.Addition, boosterTarget);
            int count = activeBoosters.CountFast();
            int i = 0;
            foreach (Booster booster in activeBoosters)
            {
                BoosterConfig config = manager.TryGetConfig(booster.link);
                yield return new BoosterSign()
                {
                    expirationTime = manager.GetExpirationTimeStamp(booster.link),
                    title = App.Core.Local.GetText(config.OverviewDescKey)
                };

                if (i < count - 1)
                {
                    yield return new BoosterSign() { sign = "+" };
                }

                if (multiplier == 1)
                {
                    multiplier = 0;
                }

                multiplier += config.multiplier;
                i++;
            }

            IEnumerable<Booster> permanentRevenueBoosters = manager.GetActiveBoosters(BoosterType.Permanent, BoosterOperation.Multiplication, boosterTarget);
            count = permanentRevenueBoosters.CountFast();
            i = 0;
            if (permanentRevenueBoosters.CountFast() > 0)
            {
                if (activeBoosters.CountFast() > 0)
                {
                    yield return new BoosterSign()
                    {
                        sign = "=",
                        title = App.Core.Local.GetText("#chipboost", multiplier)
                    };
                }

                yield return new BoosterSign() { sign = "x" };

                foreach (Booster permanentBooster in permanentRevenueBoosters)
                {
                    BoosterConfig config = manager.TryGetConfig(permanentBooster.link);
                    yield return new BoosterSign()
                    {
                        title = App.Core.Local.GetText(config.OverviewDescKey),
                        expirationTime = -1
                    };
                    multiplier *= config.multiplier;
                    if (i < count - 1)
                    {
                        yield return new BoosterSign() { sign = "+" };
                    }
                    i++;
                }
               
            }

            IEnumerable<Booster> ultimateMultipliers = manager.GetActiveBoosters(BoosterType.Temporary, BoosterOperation.Multiplication, boosterTarget);
            if (ultimateMultipliers.CountFast() > 0)
            {
                yield return new BoosterSign()
                {
                    sign = "=",
                    title = App.Core.Local.GetText("#chipboost", multiplier)
                };
                foreach (Booster booster in ultimateMultipliers)
                {
                    BoosterConfig config = manager.TryGetConfig(booster.link);
                    yield return new BoosterSign() { sign = "x" };
                    yield return new BoosterSign()
                    {
                        title = App.Core.Local.GetText(config.OverviewDescKey),
                        expirationTime = manager.GetExpirationTimeStamp(booster.link)
                    };
                    multiplier *= config.multiplier;
                }
            } 
          

            yield return new BoosterSign();

            yield return new BoosterSign()
            {
                sign = "=",
                title = App.Core.Local.GetText("#income", multiplier)
            };

        }
    }
}