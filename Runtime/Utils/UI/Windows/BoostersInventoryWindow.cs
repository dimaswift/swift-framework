using UnityEngine;
using SwiftFramework.Core.Windows;
using UnityEngine.UI;
using SwiftFramework.Core;
using TMPro;
using SwiftFramework.Core.Boosters;
using System.Collections.Generic;
using System;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class BoostersInventoryWindow : WindowWithArgs<BoostersInventoryWindow.Args>
    {
        public class Args
        {
            public readonly IBoosterManager manager;
            public readonly Action openShopHandler;
            public readonly Action showInfoHandler;

            public Args(IBoosterManager manager, Action openShopHandler, Action showInfoHandler)
            {
                this.manager = manager;
                this.openShopHandler = openShopHandler;
                this.showInfoHandler = showInfoHandler;
            }
        }

        [SerializeField] private BoosterTargetLink boosterTarget = null;
        [SerializeField] private BoosterConfigLink adBooster = null;
        [SerializeField] private ProgressBar adBoosterBar = null;
        [SerializeField] private Button watchAdForBooster = null;

        [SerializeField] private ScrollRect activeBoostersScroll = null;
        [SerializeField] private ScrollRect inventoryScroll = null;

        [SerializeField] private ElementSet activeBoosters = null;
        [SerializeField] private ElementSet inventory = null;
        [SerializeField] private Button shopButton = null;
        [SerializeField] private Button infoButton = null;
        [SerializeField] private TMP_Text totalMultiplierText = null;
        [SerializeField] private TMP_Text totalTimeLeftText = null;
        [SerializeField] private TMP_Text adBoosterTimeLeft = null;
        [SerializeField] private TMP_Text adBoosterReminder = null;


        private IBoosterManager manager;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            shopButton.onClick.AddListener(OnShopClick);
            infoButton.onClick.AddListener(OnInfoClick);
            watchAdForBooster.onClick.AddListener(OnWatchAdForBoosterClick);
        }

        private void OnWatchAdForBoosterClick()
        {
            App.Core.GetModule<IAdsManager>().ShowRewardedWithLoadingWindow("ad_booster_from_booster_menu").Done(success =>
            {
                manager.AddBoosterToInventory(adBooster, 1);
            });
        }

        private void OnInfoClick()
        {
            Arguments.showInfoHandler?.Invoke();
        }

        public override void OnStartShowing(Args args)
        {
            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;

            manager = args.manager;

            manager.OnBoosterActivated -= Controller_OnBoosterActivated;
            manager.OnBoosterExpired -= Controller_OnBoosterExpired;

            manager.OnBoosterActivated += Controller_OnBoosterActivated;
            manager.OnBoosterExpired += Controller_OnBoosterExpired;

            manager.OnInventoryChanged -= OnInventoryChanged;
            manager.OnInventoryChanged += OnInventoryChanged;

            SetContent();
            App.Core.Timer.WaitForNextFrame().Done(() =>
            {
                activeBoostersScroll.horizontalNormalizedPosition = 0;
                inventoryScroll.horizontalNormalizedPosition = 0;
            });
        }

        private void OnInventoryChanged()
        {
            SetContent();
        }

        private void Controller_OnBoosterExpired(Booster booster)
        {
            SetContent();
        }

        private void Controller_OnBoosterActivated(Booster booster)
        {
            SetContent();
        }

        private void OnShopClick()
        {
            Arguments.openShopHandler?.Invoke();
        }

        private IEnumerable<(BoosterConfigLink booster, IBoosterManager manager)> GetBoosters()
        {
            foreach (Booster booster in manager.GetActiveBoosters(BoosterType.Temporary, boosterTarget))
            {
                yield return (booster.link, manager);
            }
        }

        private void SetContent()
        {
            activeBoosters.SetUp<ActiveBoosterIcon, (BoosterConfigLink booster, IBoosterManager manager)>(GetBoosters());

            inventory.SetUp<BoosterIcon, (BoosterConfigLink link, int amount)>(manager.GetBoostersInInventory(), OnBoosterClick);

            App.Core.Timer.WaitForNextFrame().Done(() => activeBoostersScroll.horizontalNormalizedPosition = 0);

            totalMultiplierText.text = $"{manager.GetTotalMultiplier(boosterTarget)}x";

            UpdateTimers();
        }

        public override void OnHidden()
        {
            base.OnHidden();
            App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
        }

        private void Now_OnValueChanged(long value)
        {
            UpdateTimers();
        }

        private void UpdateTimers()
        {
            long totalTimeLeft = manager.GetTotalSecondsLeft(boosterTarget);
            long adTimeLeft = manager.GetTotalSecondsLeft(adBooster);
            long activeAdBoosters = manager.GetActiveBoostersAmount(adBooster);

            adBoosterTimeLeft.text = adTimeLeft > 0 ? adTimeLeft.ToTimerString() : App.Core.Local.GetText("#inactive");

            adBoosterReminder.text = adTimeLeft == 0 ? App.Core.Local.GetText("#watch_ad_to_double_boost") 
                : (activeAdBoosters < adBooster.Value.maxActiveBoostersAmount ? App.Core.Local.GetText("#watch_ad_to_extend") : App.Core.Local.GetText("#completely_boosted"));

            totalTimeLeftText.text = totalTimeLeft > 0 ? totalTimeLeft.ToTimerString() : App.Core.Local.GetText("#no_active_boosts");
            watchAdForBooster.interactable = activeAdBoosters < adBooster.Value.maxActiveBoostersAmount;
            float maxAdSecondsLeft = adBooster.Value.durationSeconds * adBooster.Value.maxActiveBoostersAmount;
            adBoosterBar.Value.SetUp(adTimeLeft / maxAdSecondsLeft);
        }

        private void OnBoosterClick(ElementFor<(BoosterConfigLink link, int amount)> icon) 
        {
            App.Core.GetModule<IWindowsManager>().Show<BoosterActivationWindow, (BoosterConfigLink booster, IBoosterManager manager)>((icon.Value.link, manager));
        }
    }
}