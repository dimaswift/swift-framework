﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class CloudManagerWindow : WindowWithArgs<CloudManagerWindow.Args>
    {
        public class Args
        {
            public ICloudAuthentication Manager { get; set; }
            public IFacebookCloudAuthentication Facebook { get; set; }
            public Type CloudSaveType { get; set; } 
            public Action<ICloudSave> OnSyncCloudSave { get; set; }
        }

        [Header("Panels")]
        [SerializeField] private GameObject loadingPanel = null;
        [SerializeField] private GameObject loginPanel = null;
        [SerializeField] private GameObject profilePanel = null;
        [SerializeField] private GameObject[] buttonsContainers = null;

        [Header("Buttons")]
        [SerializeField] private Button playNowButton = null;
        [SerializeField] private Button logoutButton = null;
        [SerializeField] private Button loginToFacebookButton = null;
        [SerializeField] private Button downloadCloudSaveButton = null;
        [SerializeField] private Button connectButton = null;
        [SerializeField] private Button changeNameButton = null;
        [SerializeField] private LinkFacebookButton linkFacebookButton = null;


        [Header("Texts")]
        [SerializeField] private TMP_Text connectionStatusText = null;
        [SerializeField] private TMP_Text profileNameText = null;

        [SerializeField] private Color onlineStatusColor = Color.green;
        [SerializeField] private Color offlineStatusColor = Color.red;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            playNowButton.onClick.AddListener(OnPlayNowClick);
            loginToFacebookButton.onClick.AddListener(OnLoginToFacebookButtonClick);
            downloadCloudSaveButton.onClick.AddListener(OnDownloadCloudSaveClick);
            logoutButton.onClick.AddListener(OnLogoutClick);
            connectButton.onClick.AddListener(OnConnectClick);
            changeNameButton.onClick.AddListener(OnChangeNameClick);
            SetLoading(false);
        }

        private void OnChangeNameClick()
        {
           
            if (Arguments.Manager.Connected.Value)
            {
                EnterTextArgs args = new EnterTextArgs()
                {
                    characherLimit = 12,
                    minCharsAmount = 3,
                    originalText = Arguments.Manager.Profile.DisplayName,
                    titleKey = "#enter_name"
                };
                App.Core.GetModule<IWindowsManager>().Show<EnterTextWindow, EnterTextArgs, string>(args).Done(newName => 
                {
                    SetLoading(true);
                    Arguments.Manager.SetDisplayName(newName).Then(n => OnStartShowing(Arguments));
                });
            }
        }

        private void OnLogoutClick()
        {
            Arguments.Manager.Logout();
            OnStartShowing(Arguments);
        }

        private void OnConnectClick()
        {
            connectButton.gameObject.SetActive(false);
            SetLoading(true);
            Arguments.Manager.Login().Then(result => 
            {
                SetLoading(false);
                connectButton.gameObject.SetActive(true);
                if (result != CloudActionResult.Success)
                {
                    ShowError(result);
                }
                FillProfileInfo();
            })
            .Catch(e =>
            {
                connectButton.gameObject.SetActive(true);
                ShowError(CloudActionResult.UnknownError);
                SetLoading(false);
                FillProfileInfo();
            });
        }

        private void OnDownloadCloudSaveClick()
        {
            SetLoading(true);
            Arguments.Manager.DownloadSaveData(Arguments.CloudSaveType).Then(save =>
            {
                ShowCloudSaveInfo(save);
                SetLoading(false);
            })
            .Catch(e =>
            {
                SetLoading(false);
                ShowError(CloudActionResult.UnknownError);
            });
        }

        private void ShowCloudSaveInfo(ICloudSave save)
        {
            App.Core.GetModule<IWindowsManager>().Show<SyncCloudSaveWindowPopUp, ICloudSave, bool>(save).Done(syncPressed => 
            {
                if (syncPressed)
                {
                    Arguments.OnSyncCloudSave(save);
                }
            });
        }

        public override void OnStartShowing(Args arguments)
        {
            base.OnStartShowing(arguments);
            SetLoading(false);
            profilePanel.SetActive(arguments.Manager.LoggedIn);
            loginPanel.SetActive(arguments.Manager.LoggedIn == false);
            
            if (arguments.Manager.LoggedIn)
            {
                FillProfileInfo();
            }
        }

        private void FillProfileInfo()
        {
            bool connected = Arguments.Manager.LoggedIn;
            profileNameText.text = Arguments.Manager.Profile.DisplayName;
            connectionStatusText.text = connected ? App.Core.Local.GetText("#online") : App.Core.Local.GetText("#offline");
            connectionStatusText.color = connected ? onlineStatusColor : offlineStatusColor;
            connectButton.gameObject.SetActive(connected == false);
            linkFacebookButton.gameObject.SetActive(connected);
            downloadCloudSaveButton.gameObject.SetActive(connected);
        }

        private void OnLoginToFacebookButtonClick()
        {
            SetLoading(true);
            Arguments.Facebook.Login().Then(result =>
            {
                if (result == CloudActionResult.Success)
                {
                    Hide();
                }
                else
                {
                    ShowError(result);
                }
            });
        }

        private void ShowError(CloudActionResult resultError)
        {
            SetLoading(false);
            App.Core.GetModule<IWindowsManager>().Show<PopUpWindow, string>(App.Core.Local.GetText($"#{resultError}"));
        }

        private void SetLoading(bool loading)
        {
            loadingPanel.SetActive(loading);
            foreach (GameObject gameObject in buttonsContainers)
            {
                gameObject.SetActive(loading == false);
            }
        }

        private void OnPlayNowClick()
        {
            SetLoading(true);
            Arguments.Manager.LoginWithDeviceId().Then(result =>
            {
                Hide();
            });
        }
    }
}