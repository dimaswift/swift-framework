﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class CloudSaveValueDescription : ElementFor<(string nameKey, string value)>
    {
        [SerializeField] private TMP_Text nameText = null;
        [SerializeField] private TMP_Text valueText = null;

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp((string nameKey, string value) v)
        {
            nameText.text = App.Core.Local.GetText(v.nameKey);
            valueText.text = v.value;
        }
    }
}