﻿using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using UnityEngine;
using SwiftFramework.Core.Views;

namespace SwiftFramework.Utils.UI
{
    public class ConfirmPurchaseWindow : WindowWithArgsAndResult<PriceLink, bool>
    {
        [SerializeField] private GenericText message = null;
        [SerializeField] private PriceView price = null;

        protected virtual void OnConfirmClick()
        {
            Resolve(true);
            Hide();
        }

        public override void HandleCloseButtonClick()
        {
            Resolve(false);
            base.HandleCloseButtonClick();
        }

        public override void OnStartShowing(PriceLink priceLink)
        {
            base.OnStartShowing(priceLink);
            price.Init(priceLink, OnConfirmClick);
            if (message.HasValue)
            {
                message.Value.Text = App.Core.Local.GetText("purchase_confirmation_title", priceLink.GetPriceString());
            }
        }
    }
}