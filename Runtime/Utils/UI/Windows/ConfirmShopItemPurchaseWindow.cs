﻿using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using UnityEngine;
using SwiftFramework.Core.Views;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class ConfirmShopItemPurchaseWindow : WindowWithArgsAndResult<ShopItemLink, bool>
    {
        [SerializeField] private PriceView price = null;

        private void OnConfirmClick()
        {
            Resolve(true);
            Hide();
        }

        public override void HandleCloseButtonClick()
        {
            Resolve(false);
            base.HandleCloseButtonClick();
        }

        public override void OnStartShowing(ShopItemLink item)
        {
            base.OnStartShowing(item);
            price.Init(item.Value.price, OnConfirmClick);
        }
    }
}