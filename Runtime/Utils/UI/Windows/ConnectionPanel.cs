﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class ConnectionPanel : MonoBehaviour
    {
        [SerializeField] private GameObject[] connectedState = { };
        [SerializeField] private GameObject[] disconnectedState = { };
        [SerializeField] private Button reconnectButton = null;
        [SerializeField] private RectTransform connectingIcon = null;
        [SerializeField] private TMP_Text statusText = null;

        private ICloudAuthentication cloud;

        private void Awake()
        {
            App.WaitForState(AppState.ModulesInitialized, () => 
            {
                cloud = App.Core.GetModule<ICloudAuthentication>();
                if (cloud == null)
                {
                    gameObject.SetActive(false);
                    return;
                }

                cloud.Connected.OnValueChanged += Connected_OnValueChanged;
                reconnectButton.onClick.AddListener(OnReconnectClick);
                SetConnected(cloud.Connected.Value);
                cloud.OnLoginAttempt += Cloud_OnLoginAttempt;
                statusText.text = App.Core.Local.GetText("connection_lost");
                SetConnecting(false);
            });
        }

        private void Cloud_OnLoginAttempt()
        {
            if (gameObject.activeInHierarchy)
            {
                SetConnecting(true);
            }
        }

        private void OnReconnectClick()
        {
            Promise.Race(App.Core.Timer.WaitFor(5), cloud.Login().Then()).Done(() => 
            {
                SetConnecting(false);
            });
        }

        private void SetConnected(bool connected)
        {
            foreach (GameObject item in disconnectedState)
            {
                item.SetActive(connected == false);
            }
            foreach (GameObject item in connectedState)
            {
                item.SetActive(connected);
            }
        }

        private void SetConnecting(bool isConnecting)
        {
            statusText.text = isConnecting ? App.Core.Local.GetText("connecting") : App.Core.Local.GetText("connection_lost");
            connectingIcon.gameObject.SetActive(isConnecting);
            reconnectButton.gameObject.SetActive(isConnecting == false);
        }

        private void Connected_OnValueChanged(bool connected)
        {
            SetConnected(connected);
        }
    }
}