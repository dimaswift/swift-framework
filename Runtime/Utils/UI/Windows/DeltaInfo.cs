﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class DeltaInfo : Element
    {
        [SerializeField] private TMP_Text baseValueText = null;
        [SerializeField] private TMP_Text deltaValueText = null;
        [SerializeField] private TMP_Text nameText = null;
        [SerializeField] private Color positiveDeltaColor = Color.green;
        [SerializeField] private Color negativeDeltaColor = Color.red;
        [SerializeField] private Color constantDeltaColor = Color.white;
        [SerializeField] private GenericImage icon = null;

        public void SetUp(ValueDelta value)
        {
            baseValueText.text = value.IsBigNumber ? value.baseValue.ToString() + value.postFix : value.baseFloatValue.ToString("0.00") + value.postFix;

            icon.Value.SetSprite(value.icon);

            if (!value.reachedMaxLevel)
            {
                string valueText = value.IsBigNumber ? value.Delta.ToString() + value.postFix : value.FloatDelta.ToString("0.00") + value.postFix;
                deltaValueText.text = $"{value.Sign}{valueText}";
            }
            else
            {
                deltaValueText.text = App.Core.Local.GetText("#max");
            }

            nameText.text = App.Core.Local.GetText(value.name);

            switch (value.Type)
            {
                case ValueDelta.DeltaType.Positive:
                    deltaValueText.color = positiveDeltaColor;
                    break;
                case ValueDelta.DeltaType.Negative:
                    deltaValueText.color = negativeDeltaColor;
                    break;
                case ValueDelta.DeltaType.Constant:
                    deltaValueText.color = constantDeltaColor;
                    break;
                default:
                    break;
            }
        }
    }
}
