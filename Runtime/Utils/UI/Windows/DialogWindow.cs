﻿using UnityEngine;
using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using TMPro;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{

    [AddrSingleton]
    public class DialogWindow : Window
    {
        [SerializeField] private TMP_Text messageText = null;
        [SerializeField] private TMP_Text cancelText = null;
        [SerializeField] private TMP_Text yesText = null;
        [SerializeField] private Button yesButton = null;
        [SerializeField] private Button cancelButton = null;

        private Promise<bool> promise;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            cancelButton.onClick.AddListener(OnCancelClick);
            yesButton.onClick.AddListener(OnYesClick);
        }

        private void OnCancelClick()
        {
            if(promise != null)
            {
                promise.Resolve(false);
                promise = null;
            }

            Hide();
        }

        private void OnYesClick()
        {
            if (promise != null)
            {
                promise.Resolve(true);
                promise = null;
            }

            Hide();
        }

        public IPromise<bool> ShowDialog(string messageKey, string yesKey = "#yes", string cancelKey = "#cancel")
        {
            promise = Promise<bool>.Create();
            messageText.text = App.Core.Local.GetText(messageKey);
            cancelText.text = App.Core.Local.GetText(cancelKey);
            yesText.text = App.Core.Local.GetText(yesKey);
            return promise;
        }
    }
}