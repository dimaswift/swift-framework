﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class EnterTextArgs
    {
        public string titleKey;
        public int characherLimit = 12;
        public int minCharsAmount = 3;
        public string originalText;
    }

    public class EnterTextWindow : WindowWithArgsAndResult<EnterTextArgs, string>
    {
        [SerializeField] private TMP_InputField textField = null;
        [SerializeField] private TMP_Text title = null;
        [SerializeField] private Button confirmButton = null;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            confirmButton.onClick.AddListener(OnConfirmClick);
        }

        private void OnConfirmClick()
        {
            if (textField.text.Length < Arguments.minCharsAmount)
            {
                App.Core.GetModule<IWindowsManager>().Show<PopUpMessage, string>(App.Core.Local.GetText("#too_short"));
                return;
            }
            Resolve(textField.text);
            Hide();
        }

        public override void OnStartShowing(EnterTextArgs args)
        {
            base.OnStartShowing(args);
            title.text = App.Core.Local.GetText(args.titleKey);
            textField.text = args.originalText;
            textField.characterLimit = args.characherLimit;
        }
    }
}