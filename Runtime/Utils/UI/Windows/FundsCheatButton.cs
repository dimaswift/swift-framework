﻿using SwiftFramework.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    [RequireComponent(typeof(Image))]
    public class FundsCheatButton : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private FundsSourceLink cheatFunds = null;
        [SerializeField] private BigNumber amount = 1000000000;
        [SerializeField] private int activationThreshold = 10;

        private int timesClicked = 0;
        private Image image;

        private void OnEnable()
        {
            image = GetComponent<Image>();
            image.color = Color.clear;
            timesClicked = 0;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (activationThreshold <= timesClicked)
            {
                image.color = Color.white;
                cheatFunds.Load(f =>
                {
                    f.Add(amount);
                });
            }
        }
    }
}