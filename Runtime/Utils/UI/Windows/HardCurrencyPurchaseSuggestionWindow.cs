﻿using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using UnityEngine;
using SwiftFramework.Core.SharedData.Shop;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class HardCurrencyPurchaseSuggestionWindow : WindowWithArgs<BigNumber>
    {
        [SerializeField] private SuggestedItem[] itemsToSuggest = { };
        [SerializeField] private HardCurrencyShopItemView itemView = null;
        [SerializeField] private GenericText titleText = null;

        [System.Serializable]
        private class SuggestedItem
        {
            public BigNumber amount = 0;
            public ShopItemLink item = new ShopItemLink();
        }

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            itemView.Init(); 
            itemView.OnPurchased += OnPurchase;
        }

        private void OnPurchase(TransactionResult result, ShopItemLink item)
        {
            if (result == TransactionResult.Success && IsShown)
            {
                Hide();
            }
        }

        public override void OnStartShowing(BigNumber amountNeeded)
        {
            base.OnStartShowing(amountNeeded);

            IIdleGameModule idleGame = App.Core.GetModule<IIdleGameModule>();
            titleText.Value.Text = App.Core.Local.GetText("get_needed_amount_here", amountNeeded - idleGame.HardFunds.Amount.Value);
            foreach (SuggestedItem item in itemsToSuggest)
            {
                if (amountNeeded <= item.amount)
                {
                    itemView.SetUp(item.item);
                    break;
                }
            }
        }
    }
}