﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class LeaderboardButton : ElementFor<ILeaderboard>
    {
        [SerializeField] private TMP_Text nameText = null;

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp(ILeaderboard value)
        {
            nameText.text = App.Core.Local.GetText(value.GetTitle());
        }
    }
}