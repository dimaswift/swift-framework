﻿using SwiftFramework.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class LeaderboardEntryView : ElementFor<LeaderboardPlayerEntry>
    {
        [SerializeField] private TMP_Text displayName = null;
        [SerializeField] private TMP_Text position = null;
        [SerializeField] private TMP_Text score = null;
        [SerializeField] private DownloadabeRawImage avatar = null;
        [SerializeField] private Image valueIcon = null;

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp(LeaderboardPlayerEntry entry)
        {
            displayName.text = entry.DisplayName;
            position.text = entry.Position.ToString();
            score.text = entry.Score.ToString();

            if(avatar != null)
            {
                avatar.SetUp(entry.Avatar);
            }

            if (valueIcon != null && entry.ValueIcon != null)
            {
                valueIcon.SetSpriteAsync(entry.ValueIcon);
            }
        }
    }
}