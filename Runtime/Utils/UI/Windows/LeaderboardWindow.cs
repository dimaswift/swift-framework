﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class LeaderboardWindow : WindowWithArgs<IEnumerable<ILeaderboard>>
    {
        [SerializeField] private InfiniteElementSet elements = null;
        [SerializeField] private LeaderboardEntryView playerEntry = null;

        [SerializeField] private Toolbar toolbar = null;
        [SerializeField] private GameObject loadingPanel = null;

        [SerializeField] private Button globalLeaderboardsButton = null;
        [SerializeField] private Button friendsOnlyLeaderboardButton = null;

        private List<LeaderboardTab> leaderboards = new List<LeaderboardTab>();

        private bool isCurrentlyFriendsOnly = false;
        public class LeaderboardTab : ITab
        {
            public ILeaderboard Leaderboard;

            public LeaderboardTab(ILeaderboard leaderboard)
            {
                Leaderboard = leaderboard;
            }

            public string Name => App.Core.Local.GetText(Leaderboard.GetTitle());

            public Sprite Icon => null;
        }

        public override void OnStartShowing(IEnumerable<ILeaderboard> arguments)
        {
            base.OnStartShowing(arguments);
            leaderboards.Clear();

            foreach (ILeaderboard leaderboard in arguments)
            {
                leaderboards.Add(new LeaderboardTab(leaderboard));
                leaderboard.OnPlayerEntryUpdated += Leaderboard_OnPlayerEntryUpdated;
            }

            toolbar.SetUp(leaderboards, 0);

            toolbar.SelectedTab.OnValueChanged -= SelectedTab_OnValueChanged;
            toolbar.SelectedTab.OnValueChanged += SelectedTab_OnValueChanged;
            toolbar.SetSelected(0);

            globalLeaderboardsButton.onClick.RemoveAllListeners();
            friendsOnlyLeaderboardButton.onClick.RemoveAllListeners();

            globalLeaderboardsButton.onClick.AddListener(() => SetLeaderboardTypeAsFriendsOnly(false));
            friendsOnlyLeaderboardButton.onClick.AddListener(() => SetLeaderboardTypeAsFriendsOnly(true));
        }

        private void SetLeaderboardTypeAsFriendsOnly(bool isFriendsOnly)
        {
            if (isFriendsOnly == true)
            {
                isCurrentlyFriendsOnly = isFriendsOnly;
                var fbAuth = App.Core.GetModule<IFacebookCloudAuthentication>();

                if (fbAuth.IsLinked() == false)
                {
                    fbAuth.LinkAccount().Done(r =>
                    {
                        UpdateLeaderboards();
                    });
                }
                else
                {
                    UpdateLeaderboards();
                }
                 
            }
            else
            {
                isCurrentlyFriendsOnly = isFriendsOnly;
                UpdateLeaderboards();
            }
          
        }

        private void UpdateLeaderboards()
        {
            globalLeaderboardsButton.gameObject.SetActive(isCurrentlyFriendsOnly);
            friendsOnlyLeaderboardButton.gameObject.SetActive(!isCurrentlyFriendsOnly);

            foreach (LeaderboardTab leaderboard in leaderboards)
            {
                FetchEntries(leaderboard.Leaderboard, isCurrentlyFriendsOnly);
            }
        }

        public override void OnStartHiding()
        {
            base.OnStartHiding();
            foreach (LeaderboardTab leaderboard in leaderboards)
            {
                leaderboard.Leaderboard.OnPlayerEntryUpdated -= Leaderboard_OnPlayerEntryUpdated;
            }

        }

        private void SelectedTab_OnValueChanged(int selectedTab)
        {
            DisplayLeaderboard(GetSelectedLeaderboard());
        }

        private ILeaderboard GetSelectedLeaderboard()
        {
            return leaderboards[toolbar.SelectedTab.Value].Leaderboard;
        }

        private void Update()
        {
            if (elements == null)
            {
                return;
            }

            elements.UpdateElements<LeaderboardEntryView, LeaderboardPlayerEntry>();
        }

        private void DisplayLeaderboard(ILeaderboard leaderboard)
        {
            elements.Clear();

            if (leaderboard.GetPlayerEntry() != null)
            {
                playerEntry.gameObject.SetActive(true);
                playerEntry.SetUp(leaderboard.GetPlayerEntry());
            }
            else
            {
                playerEntry.gameObject.SetActive(false);
                leaderboard.FetchPlayerEntry().Done(e =>
                {
                    playerEntry.gameObject.SetActive(true);
                    playerEntry.SetUp(e);
                });
            }

            if (leaderboard.GetPlayers().Count() > 0 && leaderboard.EntriesInSync)
            {
                loadingPanel.gameObject.SetActive(false);
                elements.SetUp<LeaderboardEntryView, LeaderboardPlayerEntry>(leaderboard.GetPlayers());
                elements.UpdateElements<LeaderboardEntryView, LeaderboardPlayerEntry>();
            }
            else
            {
                FetchEntries(leaderboard, isCurrentlyFriendsOnly);
            }
        }

        private void Leaderboard_OnPlayerEntryUpdated()
        {
            ILeaderboard leaderboard = GetSelectedLeaderboard();
            playerEntry.SetUp(leaderboard.GetPlayerEntry());
        }

        private void FetchEntries(ILeaderboard leaderboard, bool isFriendsOnly)
        {
            elements.Clear();
            loadingPanel.gameObject.SetActive(true);
            leaderboard.FetchEntries(isFriendsOnly).Done(entries =>
            {
                loadingPanel.gameObject.SetActive(false);
                elements.SetUp<LeaderboardEntryView, LeaderboardPlayerEntry>(entries);
                elements.UpdateElements<LeaderboardEntryView, LeaderboardPlayerEntry>();
            });
        }

    }
}