﻿using SwiftFramework.Core.Windows;
using UnityEngine;
using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;

namespace SwiftFramework.Utils.UI
{
    [AddrGroup("offers")]
    public class OfferWindow : WindowWithArgs<ShopItemLink>, ISpecialOfferTag
    {
        [SerializeField] private BuyShopItemButton buyButton = null;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            buyButton.OnPurchased += OnPurchased;
        }

        public override void OnStartShowing(ShopItemLink item)
        {
            base.OnStartShowing(item);
            buyButton.SetUp(item);
        }

        private void OnPurchased(TransactionResult result)
        {
            if (result == TransactionResult.Success)
            {
                Hide();
            }
        }
    }
}