﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.SeasonPass;
using System;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class OpenSeasonPassButton : MonoBehaviour
    {
        public event Action OnGoToEventClick = () => { };
        public event Action OnOpenSeasonViewClick = () => { };

        [SerializeField] private GenericButton openButton = null;
        [SerializeField] private GenericImage icon = null;
        [SerializeField] private GenericText statusText = null;
        [SerializeField] private GenericText nameText = null;

        private ISeasonPass seasonPass;

        private void Awake()
        {
            App.WaitForState(AppState.ModulesInitialized, () =>
            {
                seasonPass = App.Core.GetModule<ISeasonPass>();
                if (seasonPass != null)
                {
                    if (seasonPass.TryGetCurrentSeason(out SeasonLink season))
                    {
                        openButton.Value.AddListener(OnOpenClick);
                        App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
                        Now_OnValueChanged(0);
                        nameText.Value.Text = App.Core.Local.GetText(season.Value.nameKey);
                        icon.Value.SetSprite(season.Value.buttonIcon);
                    }
                    else
                    {
                        gameObject.SetActive(false);
                    }
                }
            });
        }

        private void OnOpenClick()
        {
            if (seasonPass.TimeTillStart > 0 || seasonPass.TimeTillEnd == 0)
            {
                return;
            }

            OnOpenSeasonViewClick();
        }

        private void HandleGoToEventClick()
        {
            OnGoToEventClick();
        }

        private void Now_OnValueChanged(long value)
        {
            statusText.Value.Text = seasonPass.GetDescription();
        }
    }
}