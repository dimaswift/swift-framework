﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class PopUpMessage : WindowWithArgs<string>
    {
        [SerializeField] private float showDuration = 1f;
        [SerializeField] private TMP_Text messageText = null;

        public override void OnStartShowing(string args)
        {
            base.OnStartShowing(args);
            messageText.text = args;
            App.Core.Timer.WaitForUnscaled(showDuration).Done(Hide);
        }

        public static void ShowNoGold()
        {
            ShowLocalized("#nogold");
        }

        public static void ShowLocalized(string key)
        {
            IWindowsManager windows = App.Core.GetModule<IWindowsManager>();
            if (windows == null)
            {
                Debug.LogError("IWindowsManager not found! Cannot show No Gold Message");
                return;
            }
            windows.Show<PopUpMessage, string>(App.Core.Local.GetText(key));
        }
    }
}