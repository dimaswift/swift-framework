﻿using UnityEngine;
using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using TMPro;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class PopUpWindow : WindowWithArgs<string>
    {
        [SerializeField] private TMP_Text messageText = null;
        [SerializeField] private Button okayButton = null;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            okayButton.onClick.AddListener(OnOkayClicked);
        }

        private void OnOkayClicked()
        {
            Hide();
        }

        public override void OnStartShowing(string messageKey)
        {
            base.OnStartShowing(messageKey);
            messageText.text = App.Core.Local.GetText(messageKey);
        }
    }
}