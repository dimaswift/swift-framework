﻿using UnityEngine;
using SwiftFramework.Core.Windows;
using SwiftFramework.Core;

namespace SwiftFramework.Utils.UI
{
    public class ProcessWindow : WindowWithArgs<IPromise>
    {
        [SerializeField] private GameObject errorPanel = null;
        [SerializeField] private RectTransform progressIcon = null;
        [SerializeField] private GameObject processPanel = null;

        public override void OnStartShowing(IPromise promise)
        {
            base.OnStartShowing(promise);
            errorPanel.SetActive(false);
            SetCloseButtonActive(false);
            progressIcon.gameObject.SetActive(true);
            processPanel.SetActive(true);
            promise.Done(Hide);
            promise.Catch(e =>
             {
                 progressIcon.gameObject.SetActive(false);
                 errorPanel.SetActive(true);
                 SetCloseButtonActive(true);
                 processPanel.SetActive(false);
             });
        }

        private void Update()
        {
            if(progressIcon != null)
            {
                progressIcon.Rotate(Time.unscaledDeltaTime * Vector3.forward * 360);
            }
        }
    }
}