﻿using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.Shop;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;


namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class RandomPurchaseResultWindow : WindowWithArgs<ShopItemLink>
    {
        [SerializeField] private BuyShopItemButton buyAgainButton = null;
        [SerializeField] private GenericImage itemIcon = null;
        [SerializeField] private RectTransform rectToAnimate = null;
        [SerializeField] private AnimationCurve rollingCurve = null;
        [SerializeField] private AnimationCurve revealCurve = null;
        [SerializeField] private float rollDuration = 2f;
        [SerializeField] private float revealDuration = .5f;
        [SerializeField] private float rollFrequency = 0.05f;

        private RandomRewardShopItem item;

        private readonly List<SpriteLink> rewardsIconsCache = new List<SpriteLink>();

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            buyAgainButton.OnPurchased += BuyAgainButton_OnPurchased;
            buyAgainButton.OnClick += BuyAgainButton_OnClick;
        }

        private void BuyAgainButton_OnClick(ElementFor<ShopItemLink> obj)
        {
            item.RollReward();
        }

        private void BuyAgainButton_OnPurchased(TransactionResult result)
        {
            if (result == TransactionResult.Success)
            {
                StartCoroutine(RollingRoutine());
            }
        }

        public override void OnStartShowing(ShopItemLink itemLink)
        {
            base.OnStartShowing(itemLink);
            item = itemLink.Value as RandomRewardShopItem;
            buyAgainButton.SetUp(itemLink);
            StartCoroutine(RollingRoutine());
        }

        private IEnumerator RollingRoutine()
        {
            float t = 0;
            float flashTimer = 0;
            rewardsIconsCache.Clear();
            SetCloseButtonActive(false);
            buyAgainButton.gameObject.SetActive(false);

            foreach (RewardLink reward in item.GetPossibleRewards())
            {
                rewardsIconsCache.Add(reward.Value.Icon);
            }

            rewardsIconsCache.Shuffle();

            int i = 0;

            while (t < 1f)
            {

                flashTimer += Time.unscaledDeltaTime;

                if (flashTimer >= rollFrequency)
                {
                    if (i >= rewardsIconsCache.Count)
                    {
                        i = 0;
                    }
                    itemIcon.Value.SetSprite(rewardsIconsCache[i++]);
                    flashTimer = 0;
                }

                rectToAnimate.localScale = Vector3.one * rollingCurve.Evaluate(t);

                t += Time.unscaledDeltaTime / rollDuration;
                yield return null;
            }


            itemIcon.Value.SetSprite(item.GetLastRolledReward().Value.Icon);

            t = 0;


            while (t < 1f)
            {
                rectToAnimate.localScale = Vector3.one * revealCurve.Evaluate(t);
                t += Time.unscaledDeltaTime / revealDuration;
                yield return null;
            }

            buyAgainButton.gameObject.SetActive(true);
            SetCloseButtonActive(true);
        }
    }
}