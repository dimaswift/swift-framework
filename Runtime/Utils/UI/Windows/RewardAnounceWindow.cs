﻿using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using SwiftFramework.Core.SharedData;
using UnityEngine.EventSystems;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class RewardAnounceWindow : WindowWithArgs<IEnumerable<RewardLink>>, IPointerDownHandler
    {
        [SerializeField] private RewardIcon icon = null;
        [SerializeField] private CurveLink showCurve = null;
        [SerializeField] private float anounceTime = 1f;
        [SerializeField] private RectTransform animatedContainer = null;

        private bool tapped;

        public void OnPointerDown(PointerEventData eventData)
        {
            tapped = true;
        }

        public override void OnStartShowing(IEnumerable<RewardLink> arguments)
        {
            base.OnStartShowing(arguments);
            tapped = false;
            StartCoroutine(ShowingRoutine());
        }

        private IEnumerator ShowingRoutine()
        {
            foreach (RewardLink reward in Arguments)
            {
                icon.SetUp(reward);
                float t = 0f;
                while(t < 1)
                {
                    if (tapped)
                    {
                        tapped = false;
                        animatedContainer.localScale = Vector3.one;
                        break;
                    }
                    animatedContainer.localScale = Vector3.one * showCurve.Evaluate(t);
                    t += Time.unscaledDeltaTime / anounceTime;
                    yield return null;
                }
                while (true)
                {
                    if (tapped)
                    {
                        tapped = false;
                        break;
                    }
                    yield return null;
                }
            }

            while (true)
            {
                if (tapped)
                {
                    tapped = false;
                    break;
                }
                yield return null;
            }

            Hide();
        }
    }
}