﻿using SwiftFramework.Core;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class SeasonPassMilestoneRewardView : ElementFor<(RewardLink reward, bool claimed, int targetPoints, int milestoneIndex)>
    {
        [SerializeField] private GenericText descText = null;
        [SerializeField] private GenericImage icon = null;
        [SerializeField] private GameObject claimedState = null;

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp((RewardLink reward, bool claimed, int targetPoints, int milestoneIndex) value)
        {
            descText.Value.Text = value.reward.Value.GetDescription();
            value.reward.Load(reward => 
            {
                icon.Value.SetSprite(reward.Icon);
            });
            claimedState.SetActive(value.claimed);
        }
    }
}