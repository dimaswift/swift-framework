﻿using SwiftFramework.Core;
using SwiftFramework.Core.SharedData.SeasonPass;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class SeasonPassMilestoneView : MonoBehaviour
    {
        public event Action<(int index, bool premium)> OnClaim = i => { };
        public event Action<(SeasonPassMilestoneRewardView view, RewardLink reward, bool claimed, int targetPoints)> OnRewardClick = i => { };


        [SerializeField] private GenericButton claimPremiumButton = null;
        [SerializeField] private GenericButton claimFreeButton = null;
        [SerializeField] private GenericText indexText = null;
        [SerializeField] private ProgressBar progress = null;

        [SerializeField] private ElementSet freeFewards = null;
        [SerializeField] private ElementSet premiumFewards = null;
        [SerializeField] private GameObject[] premiumPurchasedState = { };
        [SerializeField] private GameObject[] premiumLockedState = { };

        private (ISeasonPass seasonPass, int index) value;

        private void Awake()
        {
            claimFreeButton.Value.AddListener(OnFreeClaimClick);
            claimPremiumButton.Value.AddListener(OnPremiumClaimClick);
        }

        private void OnFreeClaimClick()
        {
            OnClaim((value.index, false));
        }

        private void OnPremiumClaimClick()
        {
            OnClaim((value.index, true));
        }

        public void SetUp((ISeasonPass seasonPass, int index) value)
        {
            this.value = value;
            var milestone = value.seasonPass.GetMilestone(value.index);
            bool hasPremium = value.seasonPass.PremiumPassPurchased.Value;


            foreach (GameObject item in premiumLockedState)
            {
                item.SetActive(hasPremium == false);
            }

            foreach (GameObject item in premiumPurchasedState)
            {
                item.SetActive(hasPremium);
            }

            indexText.Value.Text = (value.index + 1).ToString();
            progress.Value.SetUp(milestone.progress);

            claimFreeButton.SetActive(value.seasonPass.IsMilestoneRewardAvailable(value.index, false));
            claimPremiumButton.SetActive(value.seasonPass.IsMilestoneRewardAvailable(value.index, true));

            freeFewards.gameObject.SetActive(milestone.milestone.freeRewards.Count > 0);
            premiumFewards.gameObject.SetActive(milestone.milestone.premiumRewards.Count > 0);

            freeFewards.SetUp<SeasonPassMilestoneRewardView, (RewardLink reward, bool claimed, int targetPoints, int milestoneIndex)>(GetRewards(false, milestone.milestone), HandleRewardClick);
            premiumFewards.SetUp<SeasonPassMilestoneRewardView, (RewardLink reward, bool claimed, int targetPoints, int milestoneIndex)>(GetRewards(true, milestone.milestone), HandleRewardClick);
        }

        private void HandleRewardClick(ElementFor<(RewardLink reward, bool claimed, int targetPoints, int milestoneIndex)> element)
        {
            OnRewardClick((element as SeasonPassMilestoneRewardView, element.Value.reward, element.Value.claimed, element.Value.targetPoints));
        }

        private IEnumerable<(RewardLink reward, bool claimed, int targetPoint, int milestoneIndex)> GetRewards(bool premium, Milestone milestone)
        {
            var rewards = premium ? milestone.premiumRewards : milestone.freeRewards;
            foreach (RewardLink rewardLink in rewards)
            {
                yield return (rewardLink, value.seasonPass.IsMilestoneRewardClaimed(value.index, premium), milestone.targetPoints, milestone.Index);
            }
        }
    }
}