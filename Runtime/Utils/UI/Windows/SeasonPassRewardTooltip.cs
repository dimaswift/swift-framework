﻿using SwiftFramework.Core;
using System.Collections;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    public class SeasonPassRewardTooltip : MonoBehaviour
    {
        [SerializeField] private GameObject lockedState = null;
        [SerializeField] private GenericText milestoneText = null;
        [SerializeField] private GenericText descText = null;
        [SerializeField] private AnimationCurve showCurve = null;
        [SerializeField] private float showTime = .5f;

        private Coroutine showCoroutine;

        public void Show(Vector3 position, RewardLink reward, int milestoneIndex, bool locked)
        {
            transform.position = position;
            gameObject.SetActive(true);
            if (showCoroutine != null)
            {
                StopCoroutine(showCoroutine);
            }
            showCoroutine = StartCoroutine(ShowRoutine());
            lockedState.SetActive(locked);
            milestoneText.Value.Text = (milestoneIndex + 1).ToString();
            descText.Value.Text = reward.Value.GetDescription();
        }

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        private IEnumerator ShowRoutine()
        {
            float t = 0f;
            while (t < 1)
            {
                transform.localScale = Vector3.one * showCurve.Evaluate(t);
                t += Time.unscaledDeltaTime / showTime;
                yield return null;
            }
            transform.localScale = Vector3.one;

            while (true)
            {
                if (Input.anyKey)
                {
                    break;
                }
                yield return null;
            }
            t = 0;
            while (t < 1)
            {
                transform.localScale = Vector3.one * showCurve.Evaluate(1f - t);
                t += Time.unscaledDeltaTime / showTime;
                yield return null;
            }
            showCoroutine = null;
            gameObject.SetActive(false);
        }
    }
}