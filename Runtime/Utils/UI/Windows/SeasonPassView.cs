﻿using SwiftFramework.Core.Windows;
using SwiftFramework.Core;
using UnityEngine;
using System;
using SwiftFramework.Core.SharedData.Shop;
using System.Collections.Generic;
using SwiftFramework.Core.Views;
using SwiftFramework.Core.SharedData.SeasonPass;
using UnityEngine.UIElements;

namespace SwiftFramework.Utils.UI
{
    public class SeasonPassView : View, ISeasonPassView
    {
        [SerializeField] private GenericText titleText = null;
        [SerializeField] private GenericText timeTillEnd = null;
        [SerializeField] private GenericButton premiumPassOffer = null;
        [SerializeField] private GameObject[] freePassOnlyState = null;
        [SerializeField] private GameObject[] premiumPassState = null;
        [SerializeField] private ProgressBar pointsProgressBar = null;
        [SerializeField] private GenericText currentMilestoneText = null;
        [SerializeField] private GenericText nextMilestoneText = null;
        [SerializeField] private SeasonPassRewardTooltip rewardTooltip = null;

        private ISeasonPass seasonPass;
        private readonly List<SeasonPassMilestoneView> milestones = new List<SeasonPassMilestoneView>();


        protected override void OnInit()
        {
            base.OnInit();
            App.Core.GetModule<IShopManager>().OnTransactionCompleted += SeasonPassView_OnTransactionCompleted;
            premiumPassOffer.Value.AddListener(OnPremiumPassOfferClick);
            GetComponentsInChildren(true, milestones);
        }

        private void SeasonPassView_OnTransactionCompleted(ShopItemLink item, TransactionResult result)
        {
            if (result == TransactionResult.Success && item == seasonPass.GetPremiumPassOffer())
            {
                SetUp();
            }
        }

        public void SetUp()
        {
            seasonPass = App.Core.GetModule<ISeasonPass>();
            SetPremiumPassState(seasonPass.PremiumPassPurchased.Value);
            seasonPass.PremiumPassPurchased.OnValueChanged += PremiumPassPurchased_OnValueChanged;
            pointsProgressBar.Value.SetUp(seasonPass.Points.Value.current, seasonPass.Points.Value.total);
            currentMilestoneText.Value.Text = (seasonPass.CurrentMilestone.Value).ToString();
            nextMilestoneText.Value.Text = (seasonPass.CurrentMilestone.Value + 1).ToString();
            seasonPass.TryGetCurrentSeason(out SeasonLink season);

            titleText.Value.Text = App.Core.Local.GetText(season.Value.nameKey);
            int i = 0;

            foreach (var milestone in GetMilestones())
            {
                if (i >= milestones.Count)
                {
                    break;
                }

                SeasonPassMilestoneView milestoneView = milestones[i++];
                milestoneView.OnClaim -= OnMilestoneClaimClick;
                milestoneView.OnClaim += OnMilestoneClaimClick;

                milestoneView.OnRewardClick -= OnRewardClick;
                milestoneView.OnRewardClick += OnRewardClick;
                milestoneView.SetUp(milestone);
            }

            UpdateTimers();
            App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
        }

        private void OnPremiumPassOfferClick()
        {
            ShopItemLink premiumPass = seasonPass.GetPremiumPassOffer();
            SpecialOffer offer = premiumPass.Value as SpecialOffer;
            App.Core.GetModule<IWindowsManager>().Show<OfferWindow, ShopItemLink>(premiumPass, offer.window);
        }

        private void SetPremiumPassState(bool isPremium)
        {
            foreach (GameObject item in premiumPassState)
            {
                item.SetActive(isPremium);
            }
            foreach (GameObject item in freePassOnlyState)
            {
                item.SetActive(isPremium == false);
            }
        }

        private void OnRewardClick((SeasonPassMilestoneRewardView view, RewardLink reward, bool claimed, int targetPoints) value)
        {
            rewardTooltip.Show(value.view.transform.position, value.reward, value.view.Value.milestoneIndex, seasonPass.CurrentMilestone.Value <= value.view.Value.milestoneIndex);
        }

        private void OnMilestoneClaimClick((int index, bool premium) value)
        {
            seasonPass.ClaimMilestoneRewards(value.index, value.premium).Done(rewards =>
            {
                App.Core.GetModule<IWindowsManager>().Show<RewardAnounceWindow, IEnumerable<RewardLink>>(rewards);
                SetUp();
            });
        }

        private void Now_OnValueChanged(long value)
        {
            UpdateTimers();
        }

        private IEnumerable<(ISeasonPass seasonPass, int index)> GetMilestones()
        {
            for (int i = 0; i < seasonPass.MilestonesCount; i++)
            {
                yield return (seasonPass, i);
            }
        }



        public override void ReturnToPool()
        {
            base.ReturnToPool();
            if (seasonPass != null)
            {
                seasonPass.PremiumPassPurchased.OnValueChanged -= PremiumPassPurchased_OnValueChanged;
                App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
            }

        }

        private void PremiumPassPurchased_OnValueChanged(bool isPremium)
        {
            SetPremiumPassState(isPremium);
        }

        private void UpdateTimers()
        {
            timeTillEnd.Value.Text = App.Core.Local.GetText("#season_ends_in", seasonPass.TimeTillEnd.ToDurationString(App.Core.Local));
        }

        public IPromise CloseStage(IStageManager stageManager)
        {
            Active = false;
            return Promise.Resolved();
        }

        public IPromise OpenStage(IStageManager stageManager)
        {
            App.Core.Windows.Show<SeasonPassWindow>();
            Active = true;
            SetUp();
            return Promise.Resolved();
        }

        public IPromise UnloadStage(IStageManager stageManager)
        {
            ReturnToPool();
            return Promise.Resolved();
        }
    }
}