﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class SeasonPassWindow : Window
    {
        public override void HandleCloseButtonClick()
        {
            App.Core.GetModule<IIdleGameModule>().StageManager.LoadPrevious();
        }
    }


}
