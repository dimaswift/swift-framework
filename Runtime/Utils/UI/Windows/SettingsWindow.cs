﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class SettingsWindow : Window
    {
        [SerializeField] private SimpleToggle soundToggle = null;
        [SerializeField] private SimpleToggle musicToggle = null;
        [SerializeField] private SimpleToggle notificationsToggle = null;
        [SerializeField] private Button languagesButton = null;
        [SerializeField] private Button rateUsButton = null;
        [SerializeField] private Button syncToCloudButton = null;
        [SerializeField] private Button restorePurchasesButton = null;
        [SerializeField] private GenericText languageText = null;

        [SerializeField] private Button addMoneyButton = null;
        [SerializeField] private Button resetProgress = null;
        [SerializeField] private Button checkUpdates = null;


        private ISoundManager soundManager;
        public override void OnStartShowing()
        {
            base.OnStartShowing();
        }

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);

            soundManager = App.Core.GetModule<ISoundManager>();

            languagesButton.onClick.AddListener(OnLaguageButtonClick);
            rateUsButton.onClick.AddListener(RateUsButtonClicked);
            soundToggle.OnValueChanged.AddListener(OnSoundToggleValueChanged);
            musicToggle.OnValueChanged.AddListener(OnMusicToggleValueChanged);
            notificationsToggle.OnValueChanged.AddListener(OnNotificationToggleValueChanged);

            soundToggle.SetValue(!soundManager.IsMuted(SoundType.SFX));
            musicToggle.SetValue(!soundManager.IsMuted(SoundType.Music));
            notificationsToggle.SetValue(false);
     
            App.Core.Local.OnLanguageChanged += Local_OnLanguageChanged;

            Local_OnLanguageChanged();

            if (syncToCloudButton != null)
            {
                syncToCloudButton.onClick.AddListener(OnSyncToCloudButtonClick);
            }

            if(restorePurchasesButton != null)
            {
                restorePurchasesButton.transform.parent.gameObject.SetActive(Application.platform == RuntimePlatform.IPhonePlayer);
                //restorePurchasesButton.onClick.AddListener(null);
            }

            addMoneyButton.onClick.AddListener(OnAddMoneyClick);
            resetProgress.onClick.AddListener(OnResetProgressClick);
            checkUpdates.onClick.AddListener(OnCheckForUpdatesClick);
        }

        private void OnCheckForUpdatesClick()
        {
            checkUpdates.interactable = false;
            App.Core.GetModule<IContentUpdater>().CheckForUpdatesManually().Done(() =>
            {
                checkUpdates.interactable = true;
            });
        }

        private void OnResetProgressClick()
        {
            App.Core.Storage.DeleteAll();
            App.Core.Timer.WaitForNextFrame().Done(() => 
            {
                Application.Quit();
            });
        }

        private void OnAddMoneyClick()
        {
            App.Core.GetModule<IIdleGameModule>().SoftFunds.Add(new BigNumber("10000000000000000000000000000000000000000000000000000000000000000000"));
        }

        private void Local_OnLanguageChanged()
        {
            languageText.Value.Text = App.Core.Local.CurrentLanguage.ToString();
        }

        private void RateUsButtonClicked()
        {
            Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
        }

        private void OnSyncToCloudButtonClick()
        {

        }

        private void OnNotificationToggleValueChanged(bool isOn)
        {
           
        }

        private void OnSoundToggleValueChanged(bool isOn)
        {
            soundManager.SetMuted(!isOn, SoundType.SFX);
        }

        private void OnMusicToggleValueChanged(bool isOn)
        {
            soundManager.SetMuted(!isOn, SoundType.Music);
        }

        private void OnLaguageButtonClick()
        {
            windowsManager.Show<LanguageSelectorWindow>();
        }
    }
}