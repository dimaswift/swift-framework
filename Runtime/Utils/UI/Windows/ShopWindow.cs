﻿using SwiftFramework.Core.Windows;
using UnityEngine.UI;
using SwiftFramework.Core;
using UnityEngine;
using System.Collections;
using SwiftFramework.Core.SharedData.Shop;
using System.Collections.Generic;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class ShopWindow : WindowWithArgs<ShopSectionLink>
    {
        [SerializeField] private ScrollRect scroll = null;
        [SerializeField] private GameObject onlineContent = null;
        [SerializeField] private GameObject offlineMessage = null;
        [SerializeField] private AnimationCurve scrollCurve = null;
        [SerializeField] private RectTransform anchor = null;

        private IShopManager shop;
        private IInAppPurchaseManager iap;
        private Coroutine scrollRoutine;

        private List<SectionButton> sections = new List<SectionButton>();

        public override void WarmUp()
        {
            shop = App.Core.GetModule<IShopManager>();
            iap = App.Core.GetModule<IInAppPurchaseManager>();
            shop.OnTransactionCompleted += Shop_OnTransactionCompleted;
            App.Core.Local.OnLanguageChanged += Local_OnLanguageChanged;
            iap.Init().Done(UpdateContent);
            iap.PurchasingInitialized.OnValueChanged += PurchasingInitialized_OnValueChanged;
            GetComponentsInChildren(true, sections);
            foreach (SectionButton button in sections)
            {
                button.OnClick += OnSectionClick;
            }
        }

        private void Shop_OnTransactionCompleted(ShopItemLink item, TransactionResult result)
        {
            UpdateContent();
        }

        private void OnSectionClick(SectionButton section)
        {
            if (scrollRoutine != null)
            {
                StopCoroutine(scrollRoutine);
            }
            scrollRoutine = StartCoroutine(ScrollToSection(section));
        }

        private void PurchasingInitialized_OnValueChanged(bool value)
        {
            onlineContent.SetActive(value);
            offlineMessage.SetActive(!value);
            UpdateContent();
        }

        private void Local_OnLanguageChanged()
        {
            UpdateContent();
        }

        public override void OnStartShowing(ShopSectionLink section)
        {
            base.OnStartShowing(section);

            UpdateContent();
        }

        public override void OnShown()
        {
            base.OnShown();

            foreach (SectionButton button in sections)
            {
                if (button.SectionLink == Arguments)
                {
                    OnSectionClick(button);
                    break;
                }
            }

        }

        private void ShopController_OnPurchaseCompleted(ShopItemLink item)
        {
            UpdateContent();
        }

        private IEnumerator ScrollToSection(SectionButton sectionButton)
        {
            float distance = anchor.position.y - sectionButton.SectionRect.position.y;

            float targetPos = scroll.content.position.y + distance;

            float startPos = scroll.content.position.y;

            float t = 0f;

            scroll.enabled = false;

            while (t < 1)
            {
                t += Time.unscaledDeltaTime * 3;
                var pos = scroll.content.position;
                pos.y = Mathf.Lerp(startPos, targetPos, scrollCurve.Evaluate(t));
                scroll.content.position = pos;
                yield return null;
            }

            scroll.enabled = true;
            scrollRoutine = null;
        }

        public void UpdateContent()
        {
            UpdateLayout();
        }

        private void UpdateLayout()
        {
            foreach (var layout in GetComponentsInChildren<LayoutGroup>(true))
            {
                layout.SetLayoutVertical();
                layout.SetLayoutHorizontal();
            }
        }
    }
}