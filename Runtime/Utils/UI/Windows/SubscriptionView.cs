﻿using System;
using SwiftFramework.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using SwiftFramework.ShopManager;
using SwiftFramework.Core.SharedData.Shop;

namespace SwiftFramework.Utils.UI
{
    public class SubscriptionView : ShopItemView
    {
        [SerializeField] private TMP_Text weeklyDeliveryAmountText = null;
        [SerializeField] private TMP_Text rewardDesc = null;
        [SerializeField] private TMP_Text amountToClaimText = null;
        [SerializeField] private TMP_Text timeTillNextClaimText = null;
        [SerializeField] private GameObject timerRoot = null;
        [SerializeField] private GameObject accumulatedRewardRoot = null;
        [SerializeField] private Button claimButton = null;

        [SerializeField] private Image mainIcon = null;
        [SerializeField] private Image headerImage = null;

        private float updateTime;
        private int day;
        private IShopManager shop;
        private Subscription subscription;

        public override void Init()
        {
            base.Init();
            shop = App.Core.GetModule<IShopManager>();
            claimButton.onClick.AddListener(OnClaimClick);
        }

        private void OnClaimClick()
        {
            shop.ClaimSubscriptionBonus(Value);
            OnSetUp(Value);
        }

        protected override void OnSetUp(ShopItemLink shopItem)
        {
            base.OnSetUp(shopItem);

            shopItem.Load(cfg =>
            {
                subscription = cfg as Subscription;

                if (mainIcon != null)
                {
                    mainIcon.SetSpriteAsync(subscription.mainIcon);
                }
                if (headerImage != null)
                {
                    headerImage.color = subscription.headerColor;
                }

                IReward reward = cfg.GetAllRewards().FirstOrDefaultFast();

                rewardDesc.text = reward.GetDescription();

                weeklyDeliveryAmountText.text = App.Core.Local.GetText("#totalweekly", reward.GetAmount() * 7);

                bool subscribed = shop.IsSubscribed(Value);

                day = DateTime.Now.Day;

                SetTimerActive(false);

                SetClaimActive(false);

                SetAccumulatedRewardActive(false);

                if (subscribed)
                {
                    bool canClaim = shop.CanClaimSubscriptionBonus(Value);
                    amountToClaimText.text = (shop.GetSubscriptionBonusesAmount(Value) * reward.GetAmount().Value).ToString();
                    SetClaimActive(canClaim);
                    SetTimerActive(canClaim == false);
                    SetAccumulatedRewardActive(true);
                    if (canClaim == false)
                    {
                        timeTillNextClaimText.text = shop.GetSecondsTillNextSubsriptionBonus().ToTimerString();
                    }
                }
                else
                {
                    if (shop.GetSubscriptionBonusesAmount(Value) > 0)
                    {
                        SetClaimActive(true);
                        amountToClaimText.text = (shop.GetSubscriptionBonusesAmount(Value) * reward.GetAmount().Value).ToString();
                        SetAccumulatedRewardActive(true);
                    }
                }
            });
        }

        private void SetClaimActive(bool active)
        {
            if (claimButton != null)
            {
                claimButton.gameObject.SetActive(active);
            }
        }

        private void SetAccumulatedRewardActive(bool active)
        {
            if (accumulatedRewardRoot != null)
            {
                accumulatedRewardRoot.SetActive(active);
            }
        }

        private void SetTimerActive(bool active)
        {
            if (timerRoot != null)
            {
                timerRoot.SetActive(active);
            }
        }

        private void Update()
        {
            if (timeTillNextClaimText == null)
            {
                return;
            }
            if (subscription != null && DateTime.Now.Day > day)
            {
                OnSetUp(Value);
                day = DateTime.Now.Day;
                return;
            }
            if (subscription != null)
            {
                if(updateTime >= 1 && timeTillNextClaimText.gameObject.activeSelf)
                {
                    long secondsLeft = shop.GetSecondsTillNextSubsriptionBonus();
                    timeTillNextClaimText.text = secondsLeft.ToTimerString();
                    updateTime = 0;
                }
                updateTime += Time.unscaledDeltaTime;
            }
        }
    }
}
