﻿using System;
using System.Collections;
using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class SupervisorButtonPanel : SupervisorInfoPanel
    {
        public event Action<SupervisorLink> OnAssign = s => { };
        public event Action<SupervisorLink> OnUnassign = s => { };
        public event Action<SupervisorLink> OnSell = s => { };

        [SerializeField] private Button assignButton = null;
        [SerializeField] private Button unassignButton = null;
        [SerializeField] private Button sellButton = null;
        [SerializeField] private AnimationCurve popCurve = null;


        public override void Init()
        {
            base.Init();
            assignButton.onClick.AddListener(OnAssignClick);
            unassignButton.onClick.AddListener(OnUnassignClick);
            sellButton.onClick.AddListener(OnSellClick);
        }

        private void OnSellClick()
        {
            OnSell(Value.link);
        }

        private void OnUnassignClick()
        {
            OnUnassign(Value.link);
        }

        private void OnAssignClick()
        {
            OnAssign(Value.link);
        }

        protected override void OnClicked()
        {

        }

        public void Pop()
        {
            StartCoroutine(PopCoroutine());
        }

        private IEnumerator PopCoroutine()
        {
            float t = 0f;
            while (t < 1)
            {
                t += Time.deltaTime * 5;
                transform.localScale = Vector3.one * popCurve.Evaluate(t);
                yield return null;
            }
            transform.localScale = Vector3.one;
        }

        protected override void OnSetUp((ISupervisorsManager manager, SupervisorLink link) args)
        {
            base.OnSetUp(args);
            sellButton.gameObject.SetActive(args.link.Value.isShared == false);
            if (args.link.Value.isShared)
            {
                SetIconDimmed(args.manager.Shared.IsAssigned(args.link) && args.manager.Current != args.link);
            }
            assignButton.gameObject.SetActive(args.manager.Current != args.link);
            unassignButton.gameObject.SetActive(args.manager.Current == args.link);
            transform.localScale = Vector3.one;
        }
    }
}

