﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class SupervisorInfoPanel : ElementFor<(ISupervisorsManager manager, SupervisorLink link)>
    {
        [SerializeField] private Image icon = null;
        [SerializeField] private AbilityTimer timer = null;
        [SerializeField] private TMP_Text abilityDesc = null;
        [SerializeField] private TMP_Text abilityDuration = null;
        [SerializeField] private TMP_Text nameText = null;
        [SerializeField] private TMP_Text levelText = null;
        [SerializeField] private GameObject noManagerState = null;
        [SerializeField] private GameObject validManagerState = null;
        private CanvasGroup canvas;

        public override void Init()
        {
            base.Init();
            canvas = gameObject.AddComponent<CanvasGroup>();
        }

        protected override void OnClicked()
        {

        }

        private void SetManagerState(bool hasManager)
        {
            if (noManagerState)
            {
                noManagerState.SetActive(hasManager == false);
            }
            if (validManagerState)
            {
                validManagerState.SetActive(hasManager);
            }
        }

        protected void SetIconDimmed(bool dimmed)
        {
            icon.color = dimmed ? Color.gray : Color.white;
        }

        protected override void OnSetUp((ISupervisorsManager manager, SupervisorLink link) args)
        {
            if (Value != args)
            {
                canvas.alpha = 0;
                App.Core.Timer.WaitFor(.1f).Done(() => canvas.alpha = 1);
            }
        
            if (args.link == null)
            {
                nameText.text = App.Core.Local.GetText("no_manager");
                SetManagerState(false);
                return;
            }

            SetManagerState(true);

            Supervisor supervisor = args.link.Value;
      
            if (args.manager.TryGetAbility(args.link, out AbilityState state))
            {
                timer.Init(state, OnAbilityClick);
            }
            else
            {
                timer.Clear();
            }

            levelText.text = App.Core.Local.GetText("#level", supervisor.level);

            supervisor.skin.Load(skin =>
            {
                icon.SetSpriteAsync(skin.icon);
            });

            nameText.text = App.Core.Local.GetText(supervisor.nameKey);
            supervisor.ability.Load(ability =>
            {
                abilityDesc.text = App.Core.Local.GetText(ability.descriptionKey);
                abilityDuration.text = ability.duration.GetValue(supervisor.level).ToTimerString();
            });
        
        }

        private void OnAbilityClick()
        {
            if (Value.link != Value.manager.Current)
            {
                return;
            }
            Value.manager.ActivateCurrentAbility().Done(s =>
            {
                OnSetUp(Value);
            });
        }
    }
}

