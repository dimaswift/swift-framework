﻿using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.Core.Views;
using SwiftFramework.Core.Windows;
using System;
using UnityEngine;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class SupervisorSellWindow : WindowWithArgs<SupervisorSellWindow.Args>
    {
        public class Args
        {
            public ISupervisorsManager manager;
            public SupervisorLink link;
            public Action onSell;
        }

        [SerializeField] private SupervisorButtonPanel supervisorView = null;
        [SerializeField] private PriceView originalPrice = null;
        [SerializeField] private PriceView partialSellPrice = null;

        public override void OnStartShowing(Args args)
        {
            base.OnStartShowing(args);

            supervisorView.SetUp((args.manager, args.link));
            args.link.Load(supervisor =>
            {
                originalPrice.Init(supervisor.price, OnSellForOriginalPriceClick, 0);
                partialSellPrice.Init(supervisor.price, OnSellWithPenaltyClick, 0.5f);
            });
        }

        private void OnSellWithPenaltyClick()
        {
            Arguments.manager.Sell(Arguments.link, .5f).Done(OnSell);
        }

        private void OnSell()
        {
            Arguments.onSell();
            Hide();
        }

        private void OnSellForOriginalPriceClick()
        {
            IAdsManager ads = App.Core.GetModule<IAdsManager>();
            ads.ShowRewardedWithLoadingWindow("supervisor_sell_full_price").Done(success => 
            {
                Arguments.manager.Sell(Arguments.link, 0).Done(OnSell);
            });
        }
    }
}

