﻿using SwiftFramework.Core;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SwiftFramework.Utils.UI
{
    public class SupervisorSortButton : ElementFor<AbilityLink>
    {
        [SerializeField] private GenericImage abilityIcon = null;
        [SerializeField] private GameObject selectionFrame = null;

        private bool selected;

        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
                selectionFrame.SetActive(value);
            }
        }

        protected override void OnClicked()
        {
            
        }

        protected override void OnSetUp(AbilityLink value)
        {
            value.Load(a =>
            {
                abilityIcon.Value.SetSprite(a.icon);
            });
        }
    }
}

