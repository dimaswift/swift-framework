﻿using System.Collections.Generic;
using SwiftFramework.Core;
using SwiftFramework.Core.Supervisors;
using SwiftFramework.Core.Views;
using SwiftFramework.Core.Windows;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    public class SupervisorsSelectionWindow : WindowWithArgs<ISupervisorsManager>
    {
        [SerializeField] private SupervisorButtonPanel assignedSupervisorView = null;
        [SerializeField] private InfiniteElementSet unassignedSupervisors = null;
        [SerializeField] private TMP_Text title = null;
        [SerializeField] private PriceView price = null;
        [SerializeField] private ElementSet sortToggleSet = null;
        [SerializeField] private Button sharedSupervisorSortButton = null;
        [SerializeField] private GameObject sharedSupervisorSortSelection = null;
        [SerializeField] private SupervisorAssignConfirmationPopUp confirmAssignPopUp = null;
        [SerializeField] private float sortingEnabledListHeight = 0;
        [SerializeField] private float sortingDisabledListHeight = 0;
        [SerializeField] private RectTransform unasignedSupervisorsViewport = null;

        private ISupervisorsManager manager;
        private AbilityLink selectedSortAbility;
        private SupervisorLink newlyPurchasedSupervisor;
        private readonly List<(ISupervisorsManager manager, SupervisorLink link)> buffer = new List<(ISupervisorsManager manager, SupervisorLink link)>();
        private static readonly SupervisorSorter sorter = new SupervisorSorter();

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            assignedSupervisorView.Init();
            sharedSupervisorSortButton.onClick.AddListener(OnSuperManagerSortClick);
            SubscribeToPanelEvents(assignedSupervisorView);
        }

        private void OnSuperManagerSortClick()
        {
            sharedSupervisorSortSelection.SetActive(true);
            foreach (SupervisorSortButton other in sortToggleSet.GetActiveElements<SupervisorSortButton>())
            {
                other.Selected = false;
            }
            selectedSortAbility = null;
            UpdateContent();
        }

        private void AssignedSupervisorView_OnSell(SupervisorLink supervisor)
        {
            SupervisorSellWindow.Args args = new SupervisorSellWindow.Args()
            {
                manager = manager,
                onSell = UpdateContent,
                link = supervisor
            };
            App.Core.GetModule<IWindowsManager>().Show<SupervisorSellWindow, SupervisorSellWindow.Args>(args);
        }

        private void Update()
        {
            unassignedSupervisors.UpdateElements<SupervisorButtonPanel, (ISupervisorsManager, SupervisorLink)>();
        }

        private void AssignedSupervisorView_OnUnassign(SupervisorLink supervisor)
        {
            manager.Unassign().Done(r => UpdateContent());
        }

        private void AssignedSupervisorView_OnAssign(SupervisorLink supervisor)
        {
            void DoAssign()
            {
                manager.Assign(supervisor).Done(r => UpdateContent());
            }
            if (supervisor.Value.isShared)
            {
                SharedSupervisorState state = manager.Shared.GetSupervisorState(supervisor);
                if (state.assginedZone != null && state.assginedZone != manager.Zone)
                {
                    confirmAssignPopUp.Show(state, DoAssign);
                }
                else
                {
                    DoAssign();
                }
            }
            else
            {
                DoAssign();
            }
        }

        private void OnBuyClick()
        {
            if (price.CanAfford() == false)
            {
                PopUpMessage.ShowNoGold();
                return;
            }
            Arguments.BuyNew().Done(link =>
            {
                link.Load(supervisor =>
                {
                    newlyPurchasedSupervisor = link;

                    if (supervisor.isShared)
                    {
                        sharedSupervisorSortSelection.SetActive(true);
                    }
                    else
                    {
                        foreach (var element in sortToggleSet.GetActiveElements<SupervisorSortButton>())
                        {
                            if (element.Value == supervisor.ability)
                            {
                                OnSortButtonClick(element);
                                break;
                            }
                        }
                    }
                });
            });
        }

        public override void OnStartShowing(ISupervisorsManager manager)
        {
            this.manager = manager;
            base.OnStartShowing(manager);

            sortToggleSet.SetUp<SupervisorSortButton, AbilityLink>(manager.GetAllAbilities(), OnSortButtonClick);

            title.text = manager.Title;

            SupervisorSortButton sortButtonToEnable = GetSortButtonToEnableByDefault();

            if (sortButtonToEnable != null)
            {
                OnSortButtonClick(sortButtonToEnable);
            }
            else
            {
                OnSuperManagerSortClick();
            }

            UpdateContent();
        }

        private SupervisorSortButton GetSortButtonToEnableByDefault()
        {
            foreach (SupervisorSortButton button in sortToggleSet.GetActiveElements<SupervisorSortButton>())
            {
                foreach (SupervisorLink supervisorLink in manager.GetUnassigned())
                {
                    if (supervisorLink.Value.ability == button.Value)
                    {
                        return button;
                    }
                }
            }

            return null;
        }

        private void OnSortButtonClick(ElementFor<AbilityLink> button)
        {
            sharedSupervisorSortSelection.SetActive(false);
            foreach (SupervisorSortButton other in sortToggleSet.GetActiveElements<SupervisorSortButton>())
            {
                other.Selected = other == button;
                if (other.Selected)
                {
                    selectedSortAbility = other.Value;
                }
            }
            UpdateContent();
        }

        private void UpdateContent()
        {
            sortToggleSet.gameObject.SetActive(manager.GetUnassigned().CountFast() >= 10);

            Vector2 viewportSize = unasignedSupervisorsViewport.sizeDelta;

            viewportSize.y = sortToggleSet.gameObject.activeSelf ? sortingEnabledListHeight : sortingDisabledListHeight;

            unasignedSupervisorsViewport.sizeDelta = viewportSize;

            price.Init(manager.GetTemplateSupervisorPrice(), OnBuyClick);

            if (manager.IsAssigned.Value)
            {
                assignedSupervisorView.gameObject.SetActive(true);
                assignedSupervisorView.SetUp((manager, manager.Current));
                if (newlyPurchasedSupervisor != null && manager.Current == newlyPurchasedSupervisor)
                {
                    assignedSupervisorView.Pop();
                    newlyPurchasedSupervisor = null;
                }
            }
            else
            {
                assignedSupervisorView.gameObject.SetActive(false);
            }


            unassignedSupervisors.SetUp<SupervisorButtonPanel, (ISupervisorsManager, SupervisorLink)>(GetFilteredUnassigned(), InitView);
        }

        private void SubscribeToPanelEvents(SupervisorButtonPanel view)
        {
            view.OnAssign -= AssignedSupervisorView_OnAssign;
            view.OnUnassign -= AssignedSupervisorView_OnUnassign;
            view.OnSell -= AssignedSupervisorView_OnSell;

            view.OnAssign += AssignedSupervisorView_OnAssign;
            view.OnUnassign += AssignedSupervisorView_OnUnassign;
            view.OnSell += AssignedSupervisorView_OnSell;
        }

        private void InitView((ISupervisorsManager manager, SupervisorLink supervisor) data, SupervisorButtonPanel view)
        {
            view.SetUp(data);
            if (data.supervisor == newlyPurchasedSupervisor)
            {
                view.Pop();
                newlyPurchasedSupervisor = null;
            }
            SubscribeToPanelEvents(view);
        }

        private IEnumerable<(ISupervisorsManager, SupervisorLink)> GetFilteredUnassigned()
        {
            buffer.Clear();

            var all = manager.GetUnassigned();

            var count = all.CountFast();

            foreach (SupervisorLink link in manager.GetUnassigned())
            {
                if (count < 10)
                {
                    buffer.Add((manager, link));
                    continue;
                }
                if (selectedSortAbility != null)
                {
                    if (link.Value.isShared == false && link.Value.ability == selectedSortAbility)
                    {
                        buffer.Add((manager, link));
                    }
                }
                else
                {
                    if (link.Value.isShared)
                    {
                        buffer.Add((manager, link));
                    }
                }
            }

            buffer.Sort(sorter);

            return buffer;
        }


        private sealed class SupervisorSorter : IComparer<(ISupervisorsManager manager, SupervisorLink link)>
        {
            public int Compare((ISupervisorsManager manager, SupervisorLink link) v1, (ISupervisorsManager manager, SupervisorLink link) v2)
            {
                Supervisor s1 = v1.link.Value;
                Supervisor s2 = v2.link.Value;
                if (s1.level == s2.level)
                {
                    v1.manager.TryGetAbility(v1.link, out AbilityState a1);
                    v2.manager.TryGetAbility(v1.link, out AbilityState a2);
                    if (a1 == null)
                    {
                        return 1;
                    }
                    if (a2 == null)
                    {
                        return -1;
                    }
                    return a1.refreshTime.CompareTo(a2.refreshTime);
                }
                return -s1.level.CompareTo(s2.level);
            }
        }
    }
}

