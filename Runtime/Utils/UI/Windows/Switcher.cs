﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SwiftFramework.Utils.UI
{
    public class Switcher : Element, IPointerClickHandler
    {
        [SerializeField] private GameObject activeIcon = null;

        public long amount;
        public bool isMax;

        public event Action<Switcher> OnActivated = s => { };

        public void SetEnabled(bool isOn)
        {
            activeIcon.SetActive(isOn);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnActivated(this);
            SetEnabled(true);
        }
    }
}
