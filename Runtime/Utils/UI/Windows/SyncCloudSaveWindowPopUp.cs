﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    public class SyncCloudSaveWindowPopUp : WindowWithArgsAndResult<ICloudSave, bool>
    {
        [SerializeField] private Button syncCloudSaveButton = null;
        [SerializeField] private ElementSet cloudSaveValues = null;
        [SerializeField] private TMP_Text saveTimestampText = null;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            syncCloudSaveButton.onClick.AddListener(OnSyncCloudSaveClick);
        }

        public override void OnStartShowing(ICloudSave save)
        {
            base.OnStartShowing(save);
            cloudSaveValues.SetUp<CloudSaveValueDescription, (string, string)>(save.GetValuesDescription());
            TimeSpan savedTimeAgo = TimeSpan.FromSeconds(App.Core.Clock.Now.Value - save.SaveTimestamp);
            saveTimestampText.text = $"{App.Core.Local.GetText("#saved")} {savedTimeAgo.ToTimeAgoString(App.Core.Local)}";
        }

        private void OnSyncCloudSaveClick()
        {
            Resolve(true);
            Hide();
        }

    }
}