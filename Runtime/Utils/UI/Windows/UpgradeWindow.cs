﻿using SwiftFramework.Core;
using SwiftFramework.Core.Windows;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SwiftFramework.Utils.UI
{
    [AddrSingleton]
    [WarmUpInstance]
    public class UpgradeWindow : WindowWithArgs<UpgradeWindow.Args>
    {
        [SerializeField] private ElementSet upgradeStatsSet = null;
        [SerializeField] private TMP_Text upgradableNameText = null;
        [SerializeField] private TMP_Text levelText = null;
        [SerializeField] private TMP_Text nextBoostLevelText = null;
        [SerializeField] private TMP_Text upgradePriceText = null;
        [SerializeField] private TMP_Text upgradeLevelsAmountText = null;
        [SerializeField] private Image iconImage = null;
        [SerializeField] private Switcher[] upgradeCountSwitchers = { };
        [SerializeField] private Button upgradeButton = null;
        [SerializeField] private Button upgradeForAdsButton = null;
        [SerializeField] private TMP_Text upgradeLevelsForAdsAmountText = null;
        [SerializeField] private Image boostProgressBar = null;
        [SerializeField] private Image nextBoostProgressBar = null;
        [SerializeField] private RewardIcon rewardIcon = null;

        public struct Args
        {
            public IUpgradable upgradable;
        }

        private long selectedUpgradesCount = 1;
        private bool upgradeForMaxLevels;
        private long currentUpgradesCount = 1;
        private IAdsManager adsManager;
        private IUpgradable upgradable;

        public override void Init(WindowsManager windowsManager)
        {
            base.Init(windowsManager);
            adsManager = App.Core.GetModule<IAdsManager>();
            selectedUpgradesCount = 1;
            upgradeForMaxLevels = false;
            foreach (var s in upgradeCountSwitchers)
            {
                s.SetEnabled(false);
            }
            upgradeCountSwitchers[0].SetEnabled(true);
            upgradeButton.onClick.AddListener(OnUpgradeClick);
            upgradeForAdsButton.onClick.AddListener(OnUpgradeForAdsClick);

            foreach (var switcher in upgradeCountSwitchers)
            {
                switcher.OnActivated += OnUpgradeCountChanged;
            }
        }

        private void OnUpgradeForAdsClick()
        {
            adsManager.ShowRewardedWithLoadingWindow(upgradable.UpgradeController.UpgradableName).Done(success =>
            {
                if (success)
                {
                    upgradable.UpgradeController.LevelUpForAd();
                    SetUpTexts();
                }
            });
        }

        private void OnUpgradeClick()
        {
            if (upgradable.UpgradeController.CanAffordLevelUp(currentUpgradesCount) == false)
            {
                PopUpMessage.ShowNoGold();
                return;
            }

            upgradable.UpgradeController.LevelUp(currentUpgradesCount);

            SetUpTexts();
        }

        private void OnUpgradeCountChanged(Switcher switcher)
        {
            foreach (var s in upgradeCountSwitchers)
            {
                if(switcher != s)
                    s.SetEnabled(false);
            }
            selectedUpgradesCount = switcher.amount;
            upgradeForMaxLevels = switcher.isMax;
            SetUpTexts();
        }

        public override void OnStartShowing(Args args)
        {
            base.OnStartShowing(args);

            upgradable = args.upgradable;

            upgradable.UpgradeController.Funds.Amount.OnValueChanged += OnFundsChanged;
            
            upgradableNameText.text = App.Core.Local.GetText(upgradable.UpgradeController.UpgradableName);

            iconImage.SetSpriteAsync(upgradable.UpgradeController.UpgradeSettings.icon);

            SetUpTexts();
        }

        private void OnFundsChanged(BigNumber value)
        {
            SetUpTexts();
        }

        public override void OnHidden()
        {
            base.OnHidden();
            upgradable.UpgradeController.Funds.Amount.OnValueChanged -= OnFundsChanged;
        }

        private void UpdateUpgradeButtons()
        {
            if (upgradeButton.interactable)
            {
                if (upgradable.UpgradeController.CanUpgradeForAd() && adsManager != null)
                {
                    if (upgradable.UpgradeController.CanAffordLevelUp(1) == false)
                    {
                        upgradeButton.gameObject.SetActive(false);
                        upgradeForAdsButton.gameObject.SetActive(true);
                        upgradePriceText.color = Color.red;
                        upgradeLevelsForAdsAmountText.text = App.Core.Local.GetText("#levelup", upgradable.UpgradeController.GetUpgradesAmountForAd());
                    }
                    else
                    {
                        upgradeButton.gameObject.SetActive(true);
                        upgradeForAdsButton.gameObject.SetActive(false);
                        upgradePriceText.color = Color.white;
                        UpdateUpgradeButtonColor();
                    }
                      
                }
                else
                {
                    upgradeForAdsButton.gameObject.SetActive(false);
                    UpdateUpgradeButtonColor();
                }
            }
            else
            {
                upgradeButton.gameObject.SetActive(true);
                upgradeForAdsButton.gameObject.SetActive(false);
                upgradePriceText.color = Color.white;
            }
        }

        private void UpdateUpgradeButtonColor()
        {
            if (upgradable.UpgradeController.CanAffordLevelUp(currentUpgradesCount) == false)
            {
                upgradeButton.GetComponent<Image>().color = Color.gray;
            }
            else
            {
                upgradeButton.GetComponent<Image>().color = Color.white;
            }
        }

        private void SetUpTexts()
        {
            levelText.text = App.Core.Local.GetText("#level", (upgradable.UpgradeState.Level + 1).ToString());
            currentUpgradesCount = upgradeForMaxLevels ? upgradable.UpgradeController.GetMaxPossibleUpgradeLevels() : upgradable.UpgradeController.AdjustCountToLimit(selectedUpgradesCount);

            upgradePriceText.text = upgradable.UpgradeController.GetUpgradePrice(currentUpgradesCount).ToString();

            if (!upgradable.UpgradeController.IsMaxLevel())
            {
                nextBoostLevelText.text = App.Core.Local.GetText("#nextboostat", (upgradable.UpgradeController.GetNextBoostLevel()).ToString());
                upgradeLevelsAmountText.text = App.Core.Local.GetText("#levelup", currentUpgradesCount.ToString());
                boostProgressBar.fillAmount = upgradable.UpgradeController.GetBoostProgress();
                nextBoostProgressBar.fillAmount = upgradable.UpgradeController.GetBoostProgress(currentUpgradesCount + 1);
            }
            else
            {
                nextBoostProgressBar.fillAmount = 1;
                nextBoostLevelText.text = App.Core.Local.GetText("#max");
                upgradeLevelsAmountText.text = App.Core.Local.GetText("#max");
                boostProgressBar.fillAmount = 1;
            }

            upgradeStatsSet.SetUp<DeltaInfo, ValueDelta>(upgradable.UpgradeController.GetUpgradedValues(currentUpgradesCount), (value, element) =>
            {
                element.SetUp(value);
            });

            upgradeButton.interactable = upgradable.UpgradeController.IsMaxLevel() == false;

            rewardIcon.SetUp(upgradable.UpgradeController.GetBoostReward());

            UpdateUpgradeButtons();
            
        }
    }
}
