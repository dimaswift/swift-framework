﻿using UnityEngine;

namespace SwiftFramework.Utils
{
    public static class Vibration
    {
        private const string VibrateMethod = "vibrate";

#if UNITY_ANDROID && !UNITY_EDITOR
        private static readonly AndroidJavaObject Vibrator = new AndroidJavaClass("com.unity3d.player.UnityPlayer")
        .GetStatic<AndroidJavaObject>("currentActivity")
        .Call<AndroidJavaObject>("getSystemService", "vibrator");
#endif

        static Vibration()
        {
#if UNITY_ANDROID || UNITY_IOS
            if (Application.isEditor) Handheld.Vibrate();
#endif
        }

        public static void Vibrate(long milliseconds)
        {
            try
            {
#if UNITY_ANDROID && !UNITY_EDITOR
                Vibrator.Call(VibrateMethod, milliseconds);
#endif
            }
            catch
            {
                
            }

        }

        public static void Vibrate(long[] pattern, int repeat)
        {
            try
            {
#if UNITY_ANDROID && !UNITY_EDITOR
                Vibrator.Call(VibrateMethod, pattern, repeat);
#endif
            }
            catch
            {

            }
        }
    }

}
