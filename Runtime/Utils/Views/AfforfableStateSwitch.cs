﻿using UnityEngine;

namespace SwiftFramework.Core.Views
{
    public class AfforfableStateSwitch : MonoBehaviour, IAffordableHandler
    {
        [SerializeField] private GameObject canAfford = null;
        [SerializeField] private GameObject cannotAfford = null;

        public void SetAfforfable(bool affordable)
        {
            canAfford.SetActive(affordable);
            cannotAfford.SetActive(affordable == false);
        }
    }
}
