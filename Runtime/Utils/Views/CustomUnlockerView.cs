﻿using UnityEngine;

namespace SwiftFramework.Core.Views
{
    public class CustomUnlockerView : MonoBehaviour
    {
        [SerializeField] private GameObject locked = null;
        [SerializeField] private GameObject isUnlocking = null;
        [SerializeField] private GameObject unlocked = null;

        public GameObject Locked => locked;
        public GameObject IsUnlocking => isUnlocking;
        public GameObject Unlocked => unlocked;
    }
}