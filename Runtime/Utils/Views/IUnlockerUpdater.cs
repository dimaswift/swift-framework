﻿using System;

namespace SwiftFramework.Core.Views
{
    public interface IUnlockerUpdater
    {
        event Action OnUnlockedShouldUpdate;
    }

    [System.Serializable]
    public class UnlockerUpdater : InterfaceComponentField<IUnlockerUpdater>
    {

    }

}
