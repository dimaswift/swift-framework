﻿using UnityEngine;

namespace SwiftFramework.Core.Views
{
    public class MovingProgressBar : MonoBehaviour, IProgressBar
    {
        [SerializeField] private Transform bar = null;
        [SerializeField] private Transform start = null;
        [SerializeField] private Transform finish = null;

        public void SetUp(float progressNormalized)
        {
            bar.position = Vector3.Lerp(start.position, finish.position, progressNormalized);
        }

        public void SetUp(int current, int total)
        {
            SetUp((float)current / total);
        }
    }
}
