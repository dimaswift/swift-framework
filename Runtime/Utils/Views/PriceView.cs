﻿using System;
using TMPro;
using UnityEngine;

namespace SwiftFramework.Core.Views
{
    public class PriceView : MonoBehaviour
    {
        [SerializeField] private GenericText priceText = null;
        [SerializeField] private AffordableHandler cannotAffordState = null;
        [SerializeField] private GenericButton buyButton = null;
        [SerializeField] private GenericImage icon = null;

        private Action onClick;
        private PriceLink price;
        private float discount;

        private void OnBuyClick()
        {
            onClick?.Invoke();
        }

        private void Awake()
        {
            buyButton.Value.AddListener(OnBuyClick);
        }

        public bool CanAfford()
        {
            return price.CanAfford(discount);
        }

        public void Init(PriceLink price, Action onClick, float discount = 0)
        {
            price.Value.OnAvailabilityChanged -= Value_OnAvailabilityChanged;
            price.Value.OnAvailabilityChanged += Value_OnAvailabilityChanged;
            this.discount = discount;
            this.onClick = onClick;
            this.price = price;
            UpdateContent();
        }

        private void UpdateContent()
        {
            if (cannotAffordState.HasValue)
            {
                cannotAffordState.Value.SetAfforfable(price.CanAfford(discount));
            }

            if (priceText.HasValue)
            {
                priceText.Value.Text = price.GetPriceString(discount);
            }
            else
            {
                Debug.LogWarning($"Price text reference is missing on {name}");
            }

            if (icon.HasValue)
            {
                icon.Value.SetSprite(price.Icon);
            }

            buyButton.SetActive(onClick != null);
        }

        private void Value_OnAvailabilityChanged()
        {
            UpdateContent();
        }

        public void SetInteractable(bool value)
        {
            buyButton.Value.Interactable = value;
        }
    }
}
