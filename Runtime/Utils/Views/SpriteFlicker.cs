﻿using UnityEngine;

namespace SwiftFramework.Core.Views
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class SpriteFlicker : MonoBehaviour
    {
        [SerializeField] private float frequency = .1f;
        [SerializeField] private float force = .5f;

        private SpriteRenderer spriteRenderer;

        private float offset;

        private void Awake()
        {
            offset = Random.Range(0, 10f);
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Update()
        {
            Color c = spriteRenderer.color;
            c.a = Mathf.PerlinNoise(offset + (Time.time * frequency), 0) * force;
            spriteRenderer.color = c;
        }
    }
}
