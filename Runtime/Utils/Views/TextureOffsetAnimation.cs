﻿using UnityEngine;

namespace SwiftFramework.Core.Views
{
    public class TextureOffsetAnimation : MonoBehaviour
    {
        [SerializeField] private int sortingOrder = 0;
        [SerializeField] private string layer = "Default";
        [SerializeField] private Vector2 direction = Vector2.left;
        [SerializeField] private float speed = 1;

        private Material mat;
        private int offsetId;
        private Vector2 offset;

        private void OnEnable()
        {
            MeshRenderer rend = GetComponent<MeshRenderer>();
            rend.sortingOrder = sortingOrder;
            rend.sortingLayerName = layer;
            mat = rend.material;
            offset = mat.mainTextureOffset;
            offsetId = Shader.PropertyToID("_MainTex");
        }

        private void Update()
        {
            if (mat)
            {
                offset += direction * speed * Time.deltaTime;
                mat.SetTextureOffset(offsetId, offset);
            }
        }
    }
}
