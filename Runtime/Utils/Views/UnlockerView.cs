﻿using SwiftFramework.Utils;
using SwiftFramework.Utils.UI;
using UnityEngine;

namespace SwiftFramework.Core.Views
{
    public class UnlockerView : MonoBehaviour
    {
        public GameObject Locked => locked;
        public GameObject IsUnlocking => isUnlocking;
        public GameObject Unlocked => unlocked;

        [Header("States")]
        [SerializeField] private GameObject locked = null;
        [SerializeField] private GameObject isUnlocking = null;
        [SerializeField] private GameObject unlocked = null;
        [SerializeField] private GameObject justUnlocked = null;
        [SerializeField] private GameObject wasUnlocked = null;
        [SerializeField] private Physics2DButton completeUnlockButton = null;

        [SerializeField] private GenericText unlockTimer = null;
        [SerializeField] private GenericText reduceTimeTxt = null;
        [SerializeField] private PriceView unlockButton = null;
        [SerializeField] private PriceView reduceTimeButton = null;
        [SerializeField] private PriceView skipTimeButton = null;

        [SerializeField] private ProgressBar timeBar = null;
        [SerializeField] private UnlockerUpdater updater = null;
        [SerializeField] private WindowLink unlockConfirmationWindow = null;

        private IUnlocker unlocker;
        private IUnlockable unlockable;

        private void Now_OnValueChanged(long now)
        {
            if (unlocker == null || unlocker.UnlockStatus.Value != UnlockStatus.IsUnlocking)
            {
                return;
            }
            unlockTimer.Value.Text = unlocker.GetTimeTillUnlock().ToTimerString();
            UpdateTimeBar();
        }

        private void OnSkipTimeClick()
        {
            if (unlocker == null || unlocker.UnlockStatus.Value != UnlockStatus.IsUnlocking)
            {
                return;
            }

            App.Core.GetModule<IWindowsManager>().Show<ConfirmPurchaseWindow, PriceLink, bool>(unlocker.SkipUnlockPrice, unlockConfirmationWindow).Done(confirmed =>
            {
                if (confirmed)
                {
                    unlocker.SkipUnlockWaitTime().Done(success =>
                    {
                        if(success == true)
                        {
                            UpdateVisuals();
                        }
                        else
                        {
                            App.Core.GetModule<IWindowsManager>().Show<HardCurrencyPurchaseSuggestionWindow, BigNumber>(unlocker.UnlockPrice.amount);
                        }
                     
                    });
                }

            });
         
        }

        private void UpdateVisuals()
        {
            UnlockStatus status = unlocker.UnlockStatus.Value;
            locked.SetActive(status == UnlockStatus.Locked);
            isUnlocking.SetActive(status == UnlockStatus.IsUnlocking);
            unlocked.SetActive(status == UnlockStatus.Unlocked || status == UnlockStatus.JustUnlocked);
            justUnlocked.SetActive(status == UnlockStatus.JustUnlocked);
            wasUnlocked.SetActive(status != UnlockStatus.JustUnlocked);
        }

        private void OnReduceTimeClick()
        {
            if (unlocker == null || unlocker.UnlockStatus.Value != UnlockStatus.IsUnlocking)
            {
                return;
            }
            unlocker.ReduceUnlockWaitTime().Done(success =>
            {
                UpdateVisuals();
            });
        }

        private void OnUnlockClick()
        {
            if (unlocker == null || unlocker.UnlockStatus.Value != UnlockStatus.Locked)
            {
                return;
            }

            if (unlockConfirmationWindow.HasValue)
            {
                App.Core.GetModule<IWindowsManager>().Show<ConfirmPurchaseWindow, PriceLink, bool>(unlocker.UnlockPrice, unlockConfirmationWindow).Done(confirmed =>
                {
                    if (confirmed)
                    {
                    
                        if (unlocker.UnlockPrice.CanAfford() == true)
                        {
                            DoUnlock();
                        }
                        else
                        {
                            PopUpMessage.ShowNoGold();
                        }
                    }

                });
            }
            else
            {
                DoUnlock();
            }
        }

        private void DoUnlock()
        {
            unlocker.Unlock().Done(success =>
            {
                UpdateTimeBar();
                UpdateVisuals();
            });
        }

        private void UpdateTimeBar()
        {
            if (timeBar != null)
            {
                timeBar.Value.SetUp(1 - unlocker.GetTimeTillUnlock() / unlocker.UnlockTimeTotal);
            }
        }

        public void Init(IUnlockable unlockable)
        {
            this.unlockable = unlockable;
            unlocker = unlockable.Unlocker;

            unlocker.UnlockStatus.OnValueChanged += UnlockStatus_OnValueChanged;
            reduceTimeButton.Init(unlocker.ReduceUnlockTimePrice, () => unlocker.ReduceUnlockWaitTime());
            reduceTimeTxt.Value.Text = "-" + unlocker.UnlockTimeToReduce.ToTimerString();

            skipTimeButton.Init(unlocker.SkipUnlockPrice, OnSkipTimeClick);

            unlockButton.Init(unlocker.UnlockPrice, OnUnlockClick, unlocker.GetUnlockDiscount());
            completeUnlockButton.AddListener(OnCompleteUnlockClick);

            if (unlocker.UnlockStatus.Value != UnlockStatus.Unlocked)
            {
                App.Core.Clock.Now.OnValueChanged -= Now_OnValueChanged;
                App.Core.Clock.Now.OnValueChanged += Now_OnValueChanged;
            }

            if (updater.HasValue)
            {
                updater.Value.OnUnlockedShouldUpdate -= Value_OnUnlockedShouldUpdate;
                updater.Value.OnUnlockedShouldUpdate += Value_OnUnlockedShouldUpdate;
            }

            UpdateVisuals();
        }


        private void OnCompleteUnlockClick()
        {
            unlocker.CompleteUnlock();
        }

        private void Value_OnUnlockedShouldUpdate()
        {
            if (unlockable != null)
            {
                Init(unlockable);
            }
        }

        private void UnlockStatus_OnValueChanged(UnlockStatus value)
        {
            UpdateVisuals();
        }
    }
}
