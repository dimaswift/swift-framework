﻿using SwiftFramework.Core.SharedData;
using SwiftFramework.Core;

namespace SwiftFramework.WalletManager
{
    [DependsOnModules(typeof(ICloudScriptManager))]
    public class WalletManager : Module, IWalletManager
    {
        public IStatefulEvent<int> Credits => credits;

        private readonly StatefulEvent<int> credits = new StatefulEvent<int>();

        private Wallet wallet;
        private ICloudScriptManager cloudScriptManager;

        protected override IPromise GetInitPromise()
        {
            App.Boot.OnResumed += Boot_OnResumed;
            cloudScriptManager = App.GetModule<ICloudScriptManager>();
            Promise initPromise = Promise.Create();
            if (App.Storage.Exists<Wallet>())
            {
                wallet = App.Storage.Load<Wallet>();
            }
            Refresh().Always(() => initPromise.Resolve());
            return initPromise;
        }

        private void Boot_OnResumed()
        {
            Refresh();
        }

        public IPromise Refresh()
        {
            IPromise promise = cloudScriptManager.ExecuteCloudScript<Wallet>("GetWallet", new object()).Then(wallet =>
            {
                this.wallet = wallet.result;
                App.Storage.Save(this.wallet);
                credits.SetValue(wallet.result.rubyCredits);
            });

            promise.Catch(e =>
            {
                credits.SetValue(wallet.rubyCredits);
            });

            return promise;

        }

        public void ForceRefresh(int amount)
        {
            credits.SetValue(amount);
            wallet.rubyCredits = amount;
            App.Storage.Save(wallet);
        }
    }
}
